import requests
import time
import json
api_embibe = 'https://api.embibe.com/content_ms/v1/embibe/en/countries-goals-exams'
test_tree_items = 'https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-main/fetch-test-tree-items?siblings=true'
jee_advanced = 'https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-advanced/test-types?language=en'
jee_main = 'https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-main/test-types?language=en'
test_types = 'https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-main/fetch-test-list?test_type=full_test&language=en&offset=0'

# querystring = {"siblings":"true"}

# headers = {
#     'embibe-token': "eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTI1MTg3NjkxNywiZW1haWwiOiJndWVzdF8xNTc1NTY3NTI1MjAzNUBlbWJpYmUuY29tIiwiaXNfZ3Vlc3QiOnRydWUsInJvbGUiOiJzdHVkZW50IiwidGltZV9zdGFtcCI6IjIwMTktMTItMDVUMTc6Mzg6NDYuNDg1WiJ9.vT3EJD8Lxa9RyfX2TR7E7xgNkVgYCp1KsoNn8SU_-DpKcClZL5ucQkiHlK7TCIfFkfGrjstY8J2GRcqQghhx-Q",
#     'Connection': "keep-alive",
#     'Host': "api.embibe.com",
#     'Origin': "https://www.embibe.com",
#     'Referer': "https://www.embibe.com/mock-test/engineering/jee-main-mock-test-series",
#     'Sec-Fetch-Mode': "cors",
#     'Sec-Fetch-site': "same-site",
#     'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
#     'Accept': "*/*",
#     'Cache-Control': "no-cache",
#     'Postman-Token': "47c7c99f-cad9-4d98-aa97-7399df15d174,08dc8130-1b2a-4dd8-ba6f-59c1644964cf",
#     'Accept-Encoding': "gzip, deflate",
#     'cache-control': "no-cache"
#     }

# response = requests.request("GET", test_tree_items, headers=headers, params=querystring)


# print response.status_code
# api_data = response.text
# json_data = json.loads(api_data)
# #print api_data
# siblings = json_data['results']['siblings']
# for s in siblings:
# 	print s['name']

url = "https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-main/test-types"

querystring = {"language":"en"}

headers = {
    'embibe-token': "eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTI1MTg3NjkxNywiZW1haWwiOiJndWVzdF8xNTc1NTY3NTI1MjAzNUBlbWJpYmUuY29tIiwiaXNfZ3Vlc3QiOnRydWUsInJvbGUiOiJzdHVkZW50IiwidGltZV9zdGFtcCI6IjIwMTktMTItMDVUMTc6Mzg6NDYuNDg1WiJ9.vT3EJD8Lxa9RyfX2TR7E7xgNkVgYCp1KsoNn8SU_-DpKcClZL5ucQkiHlK7TCIfFkfGrjstY8J2GRcqQghhx-Q",
    'Connection': "keep-alive",
    'Host': "api.embibe.com",
    'Origin': "https://www.embibe.com",
    'Referer': "https://www.embibe.com/mock-test/engineering/jee-main-mock-test-series",
    'Sec-Fetch-Mode': "cors",
    'Sec-Fetch-site': "same-site",
    'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "e0da601e-e123-4087-8eda-b9bd109adba4,32d785d1-3d02-40e0-b50a-93c6de929715",
    'Accept-Encoding': "gzip, deflate",
    'cache-control': "no-cache"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)


# print api_data