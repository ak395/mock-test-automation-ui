from flask import Flask,render_template,request,redirect
import requests
from selenium import webdriver
import time
from bs4 import BeautifulSoup as BS
import requests
import json
import os
import csv

app = Flask(__name__)

api_embibe = 'https://api.embibe.com/content_ms/v1/embibe/en/countries-goals-exams'
test_tree_items = 'https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-main/fetch-test-tree-items?siblings=true'
jee_advanced = 'https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-advanced/test-types?language=en'
jee_main = 'https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-main/test-types?language=en'
test_types = 'https://api.embibe.com/content_ms/v2/learning-maps//engineering/jee-main/fetch-test-list?test_type=full_test&language=en&offset=0'
siblings=""
alltests=""
querystring = {"siblings":"true"}
headers = {
    'embibe-token': "eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTI1MTg3NjkxNywiZW1haWwiOiJndWVzdF8xNTc1NTY3NTI1MjAzNUBlbWJpYmUuY29tIiwiaXNfZ3Vlc3QiOnRydWUsInJvbGUiOiJzdHVkZW50IiwidGltZV9zdGFtcCI6IjIwMTktMTItMDVUMTc6Mzg6NDYuNDg1WiJ9.vT3EJD8Lxa9RyfX2TR7E7xgNkVgYCp1KsoNn8SU_-DpKcClZL5ucQkiHlK7TCIfFkfGrjstY8J2GRcqQghhx-Q",
    'Connection': "keep-alive",
    'Host': "api.embibe.com",
    'Origin': "https://www.embibe.com",
    'Referer': "https://www.embibe.com/mock-test/engineering/jee-main-mock-test-series",
    'Sec-Fetch-Mode': "cors",
    'Sec-Fetch-site': "same-site",
    'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "47c7c99f-cad9-4d98-aa97-7399df15d174,08dc8130-1b2a-4dd8-ba6f-59c1644964cf",
    'Accept-Encoding': "gzip, deflate",
    'cache-control': "no-cache"
    }

@app.route("/", methods=['GET','POST'])
def hello():
	response = requests.request("GET", test_tree_items, headers=headers, params=querystring)
	api_data = response.text
	json_data = json.loads(api_data)
	#print api_data
	siblings = json_data['results']['siblings']
	return render_template('index.html',siblings = siblings)

@app.route("/PostUserData",methods=['GET','POST'])
def getUrl():
	if(request.method == 'POST'):
		form = request.form
        data = []
        # row = {
        #     'exam': form['ddsib'],
        #     'test-types': form['sel-types'],
        #     'test-list' : form['sel-tests'],
        #     'name' : form['name'],
        #     'pswd' : form['passw'],
        #     'pa' : form['PA'],
        #     'wa' : form['WA'],            
        #     'tfc': form['TFC'],
        #     'ia' : form['IA'],
        #     'otc' : form['OTC'],
        #     'oti' : form['OTI']                                            
        # }
        d = {}
        d['exam'] = form['ddsib'].split(' - ')[0]
        d['test-type'] = form['sel-types']    
        d['test-list'] = form['sel-tests']
        d['name'] = form['name']
        d['pswd'] = form['passw']
        d['pa'] = form['PA']
        d['wa'] = form['WA']
        d['tfc'] = form['TFC']
        d['ia'] = form['IA']
        d['otc'] = form['OTC']
        d['oti'] = form['OTI']
        d['test-count'] = form['sel-tests'].split('. ')[0]
        data.append(d)        
        # col = ['exam','test-types','test-list','name','pswd','pa','wa','tfc','ia','otc','oti']
        with open("testdata.csv",'wb') as f:
            writer = csv.DictWriter(f, ['exam','test-type','test-list','test-count','name','pswd','pa','wa','tfc','ia','otc','oti'])                
            writer.writeheader()
            for da in data:
                writer.writerow(da)
            # f.write(str(row))
        # os.system("mv /Users/abhinavkumar/Documents/python/autotesting/testdata.rtf /Users/abhinavkumar/Documents/java/git/test-using-ui/")
        os.system("cd /Users/abhinavkumar/Documents/java/git/test-using-ui/ && gradle clean SanityTests -DdriverEnvironment=local -Dgroups=TRP_Test_Mock_Test_Using_UI -Denv=production -Ddriver=chrome")
        os.system("cp -R /Users/abhinavkumar/Documents/java/git/test-using-ui/build/reports/tests /Users/abhinavkumar/Documents/python/autotesting/templates")        
        # return data
	return render_template('/tests/SanityTests/index.html')
if __name__ == "__main__":
	app.run(debug = True, threaded = True)
	