import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;

import static constants.Badge.*;
import static mockTestFactory.MyChoice.*;

public class MockTestSectionTests extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForMockTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.setTestDataService(new TestDataService(App.WEB));
        mockTestPlanner.setTestId(mockTestPlanner.getTestDataService().getMockTestTab().getExam().getTestId());
        mockTestPlanner.addMockTestData(new MockTestData(1, 10, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(11, 20, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(21, 25, CHOICE_ANSWERED_REVIEW_LATER, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(26, 30, CHOICE_UNATTEMPTED, BADGE_UNATTEMPTED));
//        mockTestPlanner.addMockTestData(new MockTestData(31, 60, CHOICE_REVIEW_LATER, BADGE_UNATTEMPTED));
//        mockTestPlanner.addMockTestData(new MockTestData(61, 90, CHOICE_NOT_VISITED, BADGE_UNATTEMPTED));
    }

    @Test(groups = Categories.MOCKTEST, description = "Student takes an end to end Mock Test" +
            "take_a_test_21\n" +
            "take_a_test_60\n" +
            "take_a_test_61\n" +
            "take_a_test_64\n" +
            "take_a_test_66\n" +
            "take_a_test_67\n" +
            "take_a_test_97\n" +
            "take_a_test_98\n" +
            "take_a_test_106\n")
    public void endToEndMockTest() {
        navigateTo("https://embibe-staging.embibe.com/mock-test/mb34");
//        mockTestHomePage.clickOnStartTestButton();
        instructionPage.dismissInstructions();
        instructionPage.continueAsGuest();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.verifyBadgeSummary(mockTestPlanner);
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
//        feedbackHomePage.verifyAnswerCountFor(mockTestPlanner, BADGE_TOO_FAST_CORRECT);
//        feedbackHomePage.verifyAnswerCountFor(mockTestPlanner, BADGE_WASTED_ATTEMPT);
//        feedbackHomePage.verifyAnwerCountFor(mockTestPlanner, BADGE_UNATTEMPTED);
    }
}
