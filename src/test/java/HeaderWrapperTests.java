import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.landingPage.leftContainer.AboutSection;
import page.sections.headerWrapper.HeaderWrapperSection;
import testBase.TestBase;
import verification.TestFailures;
import verification.TestListener;

@Listeners({TestListener.class})
public class HeaderWrapperTests extends TestBase {

    @Test(description = "User is logging in", groups = {"smoke"})
    public void shouldAbleToLogin() {

        driver.get("https://www.embibe.com/engineering/practice/solve");
        HeaderWrapperSection headerWrapperSection = practiceHomePage.getHeaderWrapperSection();
        headerWrapperSection.login("test01@embibe.com","embibe1234");

        TestFailures.reportIfAny();

    }

    @Test(description = "User is landing on Rank Up Page", groups = {"smoke"})
    public  void shouldAbleToLandOnRankUp() {
        driver.get("https://www.embibe.com/");
        HeaderWrapperSection headerWrapperSection = studyHomePage.getHeaderWrapperSection();
        AboutSection aboutSection = landingHomePage.getAboutSection();
        aboutSection.clickStartNowButton();
        studyHomePage.assertThatStudyHomePageIsLoaded();
        headerWrapperSection.assertRankUpTabIsPresentInHeader();
        headerWrapperSection.rankUp();
        headerWrapperSection.assertUrlHasSetTo("https://www.embibe.com/rankup");

        TestFailures.reportIfAny();

    }


}

