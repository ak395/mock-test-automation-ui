package unit;

import org.testng.annotations.Test;
import testBase.TestBase;
import verification.TestFailures;
import verification.Verify;

import java.util.Optional;

public class DropDownTests  extends TestBase {

    @Test
    public void shouldSelectExamFromDropDown() {

        driver.get("https://www.embibe.com/engineering/test");

        Verify.assertEquals("a", "b", "Should have been same char", Optional.of(true));
        Verify.assertEquals(1, 2, "Should have been same numb", Optional.of(true));

        TestFailures.reportIfAny();

    }

}
