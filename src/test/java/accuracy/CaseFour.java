package accuracy;

import org.openqa.selenium.By;
import testBase.TestBase;
import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static utils.AttemptType.*;

public class CaseFour extends TestBase {
    private PracticePlanner planner;
    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 25, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(1, 25, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(1, 25, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(1, 25, PERFECT_ATTEMPT));
    }
    @Test
    public void verifyFlukeBehaviourMeterIsDisplayed() throws Exception {
        navigateTo("https://preprod.embibe.com/questions/jee-main/physics/electricity-and-magnetism/electrostatics-questions/session");
        driver.findElement(By.cssSelector(".btn-container.round.login-modal-loginBtn ")).click();
        driver.findElement(By.cssSelector(".form-control.emailField")).sendKeys("automation_221019190229047@mail.com");
        driver.findElement(By.cssSelector(".form-control.passwordField")).sendKeys("password");
        driver.findElement(By.cssSelector(".login-logout-button")).click();
        practiceQuestionHomePage.selectAnswers(planner);
        planner.getQuestionDetails();
    }
}
