package accuracy;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import static utils.AttemptType.TOO_FAST_CORRECT;

public class AdhocTestCaseOne extends TestBase {

    private PracticePlanner planner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 2, TOO_FAST_CORRECT));
    }

    @Test
    public void verifyFlukeBehaviourMeterIsDisplayed() throws Exception {
        navigateTo("https://preprod.embibe.com/questions/jee-main/physics/electricity-and-magnetism/electrostatics-questions/session");
        driver.findElement(By.cssSelector(".btn-container.round.login-modal-loginBtn ")).click();
        driver.findElement(By.cssSelector(".form-control.emailField")).sendKeys("automation_221019185700602@mail.com");
        driver.findElement(By.cssSelector(".form-control.passwordField")).sendKeys("password");
        driver.findElement(By.cssSelector(".login-logout-button")).click();
        practiceQuestionHomePage.selectAnswers(planner);
        planner.getQuestionDetails();
    }
}