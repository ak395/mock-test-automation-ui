package accuracy;

import entities.MockTestData;
import entities.MockTestPlanner;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import testBase.TestBase;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class TwentyPracHundredTest extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @Test
    public void verifyHundredAccInTestWithNewUser() throws Exception{
        mockTestPlanner = new MockTestPlanner();
        navigateTo("https://www.embibe.com/");
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("automation_221019190101828@mail.com", "password");
        Thread.sleep(3000);
        navigateTo("https://preprod.embibe.com/mock-test/jee-main/physics/electrostatics-chapter-test/electrostatics-4/session");
        driver.findElement(By.cssSelector(".checkmark  ")).click();
        driver.findElement(By.cssSelector(".btn-container.round.instruction-next-button.common-button ")).click();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
    }
}
