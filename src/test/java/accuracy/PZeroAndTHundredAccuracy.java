package accuracy;

import entities.MockTestData;
import entities.MockTestPlanner;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import testBase.TestBase;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class PZeroAndTHundredAccuracy extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @Test
    public void verifyZeroInPAndHundredAccInTest() throws Exception{
        mockTestPlanner = new MockTestPlanner();
        navigateTo("https://preprod.embibe.com/");
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("automation_221019190324621@mail.com", "password");
        Thread.sleep(3000);
        navigateTo(" https://preprod.embibe.com/mock-test/jee-main/physics/current-electricity-chapter-test/current-electricity-4/session");
        driver.findElement(By.cssSelector(".checkmark  ")).click();
        driver.findElement(By.cssSelector(".btn-container.round.instruction-next-button.common-button ")).click();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
    }
}
