package accuracy;

import constants.Goals;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import org.openqa.selenium.By;
import static constants.Badge.BADGE_WASTED_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;


public class HundredAccuracyInTestWithNewUser  extends TestBase{

    private MockTestPlanner mockTestPlanner;

    @Test
    public void verifyHundredAccInTestWithNewUser(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo("https://preprod.embibe.com/");
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        String email = landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        System.out.println("----------" + email + "-----------------------");
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        navigateTo("https://preprod.embibe.com/mock-test/jee-main/physics/electrostatics-chapter-test/electrostatics-4/session");
        driver.findElement(By.cssSelector(".checkmark  ")).click();
        driver.findElement(By.cssSelector(".btn-container.round.instruction-next-button.common-button ")).click();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
    }

}