package accuracy;

import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static constants.Badge.BADGE_WASTED_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;



public class CaseSeventyFiveAccInTest  extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 12, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(13, 30, CHOICE_INCORRECT,BADGE_WASTED_ATTEMPT));
    }
    @Test
    public void verifySeventyFiveToFortyAccuracyInTest() throws Exception {
        navigateTo("https://preprod.embibe.com/");
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("automation_221019190158714@mail.com", "password");
        Thread.sleep(3000);
        navigateTo("https://preprod.embibe.com/mock-test/jee-main/physics/electrostatics-chapter-test/electrostatics-04-1/session");
//        driver.findElement(By.cssSelector(".checkmark  ")).click();
//        driver.findElement(By.cssSelector(".btn-container.round.instruction-next-button.common-button ")).click();
        questionsHomePage.selectAnswers(mockTestPlanner);
    }
}
