package accuracy;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;

import static utils.AttemptType.TOO_FAST_CORRECT;
import static utils.AttemptType.WASTED_ATTEMPT;

public class CaseThree extends TestBase {
    private PracticePlanner planner;
    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 4, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(5, 5, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(6, 9, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(10, 10, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(11, 14, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(15, 15, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(16, 19, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(20, 20, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(21, 23, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(24, 25, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(26, 28, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(29, 30, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(31, 33, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(34, 35, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(36, 38, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(39, 40, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(41, 43, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(44, 45, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(46, 47, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(48, 50, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(51, 52, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(53, 55, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(56, 57, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(58, 60, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(61, 62, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(63, 65, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(66, 67, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(68, 70, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(71, 72, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(73, 75, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(76, 76, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(77, 80, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(81, 92, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(93, 95, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(96, 109, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(110, 117, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(118, 129, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(130, 130, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(131, 134, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(135, 135, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(136, 139, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(139,139,TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(140, 140, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(141, 144, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(145, 145, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(146, 148, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(149, 150, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(151, 153, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(154, 155, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(156, 158, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(159, 160, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(161, 163, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(164, 165, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(166, 168, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(169, 170, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(171, 172, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(173, 175, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(1, 3, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(1, 2, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(1, 2, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(1, 3, WASTED_ATTEMPT));
    }
    @Test
    public void verifyFlukeBehaviourMeterIsDisplayed() throws Exception {
        navigateTo("https://www.embibe.com/questions/jee-main/physics/electricity-and-magnetism/electrostatics-questions");
//        driver.findElement(By.cssSelector(".btn-container.round.login-modal-loginBtn ")).click();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("academic_testing_eng6@embibe.com","embibe1234");
        Thread.sleep(5000);
//        searchHomePage.clickOnStartPracticeButton(planner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswers(planner);
//        driver.findElement(By.cssSelector(".form-control.emailField")).sendKeys("academic_testing_eng6@embibe.com");
//        driver.findElement(By.cssSelector(".form-control.passwordField")).sendKeys("embibe1234");
//        driver.findElement(By.cssSelector(".login-logout-button")).click();
//        practiceQuestionHomePage.selectAnswers(planner);
        planner.getQuestionDetails();
    }
}