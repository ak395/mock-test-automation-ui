import globalSearchApi.helper.WidgetCountChecker;
import globalSearchApi.mapper.MapperClass;
import globalSearchApi.pojo.GlobalSearchResponsePojo;
import globalSearchApi.properties.PropertyRetriever;
import org.testng.annotations.Test;
import page.landingPage.leftContainer.AboutSection;
import page.studyPage.searchWrapper.SearchWrapperSection;
import studyKeywords.SearchDataDrivenTests;
import testBase.TestBase;
import utilityClass.ReadCSV;
import utils.Properties;
import verification.TestFailures;

import java.util.ArrayList;

public class SearchWrapperTests extends TestBase {


    @Test (dataProvider = "Ambiguous Keywords", dataProviderClass = SearchDataDrivenTests.class)
    public void shouldNavigateToPracticePage(String keyword) {
        navigateTo(Properties.baseUrl);

        AboutSection aboutSection = landingHomePage.getAboutSection();
        aboutSection.clickStartNowButton();
        SearchWrapperSection searchWrapperSection = studyHomePage.getSearchWrapperSection();
        searchWrapperSection.keywordToSearch(keyword);


        TestFailures.reportIfAny();


    }

    @Test
    public void widgetCheckerUsingPropertiesFile() {
        String ambiguousKeywords =  PropertyRetriever.getInstance().getValueForPropertyName("disambiguousKeywords");

        String [] keywords = ambiguousKeywords.split(",");
        for (String keyword : keywords) {
            MapperClass mapper = new MapperClass();
            GlobalSearchResponsePojo globalSearchResponsePojo = mapper.readJson(keyword);
            WidgetCountChecker widgetCountChecker = new WidgetCountChecker();
            widgetCountChecker.checkNoOfWidgets(globalSearchResponsePojo, keyword);
            widgetCountChecker.verifyWidgetCountFor(globalSearchResponsePojo, keyword);
            // studyResultantHomePage.getLeftWidgetSection().isDescriptionWidgetDisplayed();
        }

    }

    @Test (dataProvider = "Ambiguous Keywords", dataProviderClass = SearchDataDrivenTests.class)
    public void widgetCheckerUsingDataProvider(String keyword) {

        MapperClass mapper = new MapperClass();
        GlobalSearchResponsePojo globalSearchResponsePojo = mapper.readJson(keyword);
        WidgetCountChecker widgetCountChecker = new WidgetCountChecker();
        widgetCountChecker.checkNoOfWidgets(globalSearchResponsePojo, keyword);
        widgetCountChecker.verifyWidgetCountFor(globalSearchResponsePojo, keyword);

    }

    @Test
    public void widgetCheckerUsingCSVFile() {
        ReadCSV readCSV = null;
        try {
            readCSV = new ReadCSV();
            ArrayList<String> keywords =  readCSV.getKeywords();
            for (String keyword : keywords) {
                MapperClass mapper = new MapperClass();
                GlobalSearchResponsePojo globalSearchResponsePojo = mapper.readJson(keyword);
                WidgetCountChecker widgetCountChecker = new WidgetCountChecker();
                widgetCountChecker.checkNoOfWidgets(globalSearchResponsePojo, keyword);
                widgetCountChecker.verifyWidgetCountFor(globalSearchResponsePojo, keyword);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
