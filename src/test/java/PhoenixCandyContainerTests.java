import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import testBase.TestBase;
import verification.TestFailures;
import verification.TestListener;

@Listeners({TestListener.class})
public class PhoenixCandyContainerTests extends TestBase {

    @Test (groups = "smoke")
    public void someTest() {
        driver.get("https://www.embibe.com/engineering/test");
        mockTestHomePage.getPhoenixCandyContainerSection().assertHeaderToBe("Take a mock test, review performance of past tests");

        TestFailures.reportIfAny();
    }

}
