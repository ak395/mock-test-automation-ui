import org.testng.annotations.Test;
import page.FooterPage.Footer;
import testBase.TestBase;

public class FooterTest extends TestBase
{
    @Test(description = "User clicks on all the links in the footer section")
public void shouldClickOnAllLinksInFooterSection()
{
    driver.get("https://www.embibe.com");
    driver.manage().window().maximize();
    driver.navigate().refresh();
    Footer footer=new Footer(driver);
    footer.fetchAllLinks();
}

}
