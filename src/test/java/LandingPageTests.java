import org.testng.annotations.Test;
import page.Landing.LandingPageKnowledgeTreeSection;
import page.Landing.LandingPageSearchSection;
import page.Landing.LandingPageStartSection;
import page.Landing.LandingPageVideoSection;
import page.Search.SearchPageTitleSection;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;
import verification.TestFailures;

public class LandingPageTests extends TestBase
{
    @Test(groups = {Categories.TRP_Test_Mock_Test_Using_UI,"test1"}, description = "User clicks on the Start Now Button and verifies user navigates to Study Page ")
    public void shouldClickOnStartNowButton()
    {
        driver.get(Properties.baseUrl);
        LandingPageStartSection landingPageStartSection = landingHomePage.getLandingPageStartSection();
        landingHomePage.clickOnStartNowButton();
        landingHomePage.assertUrlHasBeenResetTo(Properties.baseUrl);
        SearchPageTitleSection searchPageTitleSection=new SearchPageTitleSection(driver);
        searchPageTitleSection.assertHeaderToBe(searchPageTitleSection.getSearchTitleElement().getText());
        TestFailures.reportIfAny();
    }

    @Test(description = "User clicks on the Mute Button and UnMute Button", alwaysRun = true)
    public void shouldClickOnMuteUnMuteButton()
    {
        driver.get(Properties.baseUrl);
        LandingPageVideoSection landingPageVideoSection = landingHomePage.getLandingPageVideoSection();
        landingHomePage.clickOnMuteButton();
        landingHomePage.clickOnUnMuteButton();
        TestFailures.reportIfAny();
    }

    @Test(description = "User clicks on the Search Now Button also verifies that user navigates to the Study Page")
    public void shouldClickOnSearchNowButton()
    {
        driver.get(Properties.baseUrl);
        driver.manage().window().maximize();
        LandingPageSearchSection landingPageSearchSection = landingHomePage.getLandingPageSearchSection();
        landingPageSearchSection.scrollToSearchButton();
        landingPageSearchSection.clickOnSearchNowButton();
        landingPageSearchSection.assertUrlHasBeenResetTo(Properties.baseUrl);
        SearchPageTitleSection searchPageTitleSection=new SearchPageTitleSection(driver);
        searchPageTitleSection.assertHeaderToBe(searchPageTitleSection.getSearchTitleElement().getText());
        TestFailures.reportIfAny();
    }

    @Test(description = "User mouse hovers on the Knowledge Tree")
    public void shouldHoverOnKnowledgeTree()
    {
        driver.get(Properties.baseUrl);
        driver.manage().window().maximize();
        LandingPageKnowledgeTreeSection landingPageKnowledgeTreeSection=landingHomePage.getLandingPageKnowledgeTreeSection();
        landingPageKnowledgeTreeSection.scrollToKnowledgeTree();
        landingPageKnowledgeTreeSection.hoverOnKnowledgeTree();
        landingPageKnowledgeTreeSection.assertHeaderToBe();
        TestFailures.reportIfAny();
    }
}

