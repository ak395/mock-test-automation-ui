package studyKeywords;

import globalSearchApi.properties.PropertyRetriever;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import java.util.Arrays;
import java.util.ArrayList;

public class SearchDataDrivenTests extends TestBase {

@Test
    @DataProvider (name = "Ambiguous Keywords")
    public static Object[][] ambiguousKeywords() {
        return new Object[][] {

       {"Integration by parts"},{"indefinite integration"},{"Biological Community"},{"Biology in Human Welfare"},{"Biological Pollutant"},{"Biological Oxygen Demand"},{"BitSat Test"},{"AIIMS Test"}

        };

    }

    @DataProvider (name = "DisAmbiguous Keywords")
    public static Object[][] disAmbiguousKeywords() {

        return new Object[][] {

                {"heat"},{"water"}

        };
    }

}
