package utils;

public class Categories {
    public static final String PRP_PRACTICE_SMOKE = "PRP_PRACTICE_SMOKE";
    public static final String PRP_PRACTICE_REGRESSION = "PRP_PRACTICE_REGRESSION";
    public static final String PRP_SKIP_PRACTICE_QUESTION = "PRP_SKIP_PRACTICE_QUESTION";
    public static final String PRP_PRACTICE_ATTEMPTS = "PRP_PRACTICE_ATTEMPTS";
    public static final String PRP_PRACTICE_QUESTIONS = "PRP_PRACTICE_QUESTIONS";
    public static final String PRP_RESUME_PRACTICE = "PRP_RESUME_PRACTICE";
    public static final String PRP_BOOKMARKS = "PRP_BOOKMARKS";
    public static final String PRP_PRACTICE_HINT = "PRP_PRACTICE_HINT";
    public static final String PRP_PRACTICE_SEARCH = "PRP_PRACTICE_SEARCH";
    public static final String PRP_QUIT_PRACTICE_SESSION = "PRP_QUIT_PRACTICE_SESSION";
    public static final String PRP_DIFFERENT_ATTEMPTS_PRACTICE = "PRP_DIFFERENT_ATTEMPTS_PRACTICE";
    public static final String PRP_PRACTICE_SKILLS = "PRP_PRACTICE_SKILLS";
    public static final String PRP_PRACTICE_END_TO_END = "PRP_PRACTICE_END_TO_END";
    public static final String MOCKTEST = "MOCKTEST";
    public static final String PRP_BEHAVIOUR_METER = "PRP_BEHAVIOUR_METER";
    public static final String PRP_ERROR_SCENARIOS="PRP_ERROR_SCENARIOS";
    public static final String PRP_EMBIUM_POINTS = "PRP_EMBIUM_POINTS";
    public static final String TRP_TIMER = "TRP_TIMER";
    public static final String TRP_TEST_REGRESSION = "TRP_TEST_REGRESSION";
    public static final String TRP_NAVIGATION_TESTS = "TRP_NAVIGATION_TESTS";
    public static final String TRP_TAKE_A_TEST ="TRP_TAKE_A_TEST";
    public static final String TRP_TAKE_A_TEST_END_TO_END ="TRP_TAKE_A_TEST_END_TO_END";
    public static final String TRP_TAKE_A_TEST_CHATBOT ="TRP_TAKE_A_TEST_CHATBOT";
    public static final String TRP_AUTO_SUBMIT = "TRP_AUTO_SUBMIT";
    public static final String TRP_TAKE_A_TEST_INSTRUCTIONPAGE = "TRP_TAKE_A_TEST_INSTRUCTIONPAGE";
    public static final String TRP_TAKE_A_TEST_TEST_LISTING = "TRP_" + "TAKE_A_TEST_TEST_LISTING";
    public static final String TRP_TAKE_A_TEST_TEST_LISTING_MEDICAL = "TRP_TAKE_A_TEST_TEST_LISTING_MEDICAL";
    public static final String TRP_TAKE_A_TEST_TEST_LISTING_Engineering = "TRP_TAKE_A_TEST_TEST_LISTING_Engineering";
    public static final String TRP_TEST_FEEDBACK = "TRP_TEST_FEEDBACK";
    public static final String TRP_QUESTION_WISE_ANALYSIS = "TRP_QUESTION_WISE_ANALYSIS";
    public static final String TRP_TAKE_A_TEST_CHATBOT1 = "TRP_TAKE_A_TEST_CHATBOT1";
    public static final String TRP_TEST_ON_TEST_ANALYSIS = "TRP_TEST_ON_TEST_ANALYSIS";
    public static final String TRP_ANALYSE_TESTING = "TRP_ANALYSE_TESTING";
    public static final String TRP_Test_Mock_Test_Using_UI = "TRP_Test_Mock_Test_Using_UI";


    //In order to run failed test cases
    //public static final String TRP_Failed_Test = "TRP_FAILED_TEST";


}
