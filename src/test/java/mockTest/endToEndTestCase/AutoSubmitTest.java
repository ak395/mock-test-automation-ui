package mockTest.endToEndTestCase;


import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;
import utils.Properties;

import static constants.Badge.*;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class AutoSubmitTest extends TestBase {
    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForMockTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.setTestDataService(new TestDataService(App.WEB));
        mockTestPlanner.setTestId(mockTestPlanner.getTestDataService().getMockTestTab().getExam().getTestId());
        mockTestPlanner.addMockTestData(new MockTestData(1, 6, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));

    }

    @Test(groups = {Categories.TRP_AUTO_SUBMIT, Categories.TRP_TAKE_A_TEST_END_TO_END},
            description = "Verify once test duration is over, test is getting auto submitted"
                    + "End to End Scenario_10")
    public void verifyTestIsAutoSubmitted() throws Exception {
        navigateTo(Properties.twoMinutesTestUrl);
        instructionPage.dismissInstructions();
        instructionPage.continueAsGuest();
        questionsHomePage.selectAnswers(mockTestPlanner);
        Thread.sleep(120000);
        mockTestHomePage.verifyTestIsGettingAutoSubmitted();

    }

    @Test(groups = {Categories.TRP_AUTO_SUBMIT, Categories.TRP_TAKE_A_TEST_END_TO_END},
            description = "Verify in offline mode User is not able to Submit Test"
                    + "End to End Scenario_11" +
                      "End to End Scenario_13" +
                      "End to End Scenario_16")
    public void verifyInOfflineModeTestIsNotSubmitted() throws Exception {
        navigateTo(Properties.twoMinutesTestUrl);
        instructionPage.dismissInstructions();
        instructionPage.continueAsGuest();
        Thread.sleep(2000);
        disconnectInternet();
        mockTestHomePage.verifyUserIsNotConnectedToInternet();
        questionsHomePage.selectAnswers(mockTestPlanner);
        Thread.sleep(2000);
//        connectToInternet();
//        mockTestHomePage.verifyUserIsConnectedToInternet();
        Thread.sleep(120000);
        examSummaryPage.verifyUserIsNotAbleToSubmitTestOffline();
    }


}

