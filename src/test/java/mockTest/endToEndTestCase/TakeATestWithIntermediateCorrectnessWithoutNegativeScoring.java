package mockTest.endToEndTestCase;

import builders.MockTestBuilder;
import constants.Badge;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class TakeATestWithIntermediateCorrectnessWithoutNegativeScoring extends TestBase {
    private MockTestPlanner mockTestPlanner;


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_END_TO_END},
            description = "Verify user answer with intermediate correctness without negative Scoring" +
                    "Test must not contain negative scoring" +
                    "End to End Scenario_7")
    public void takeATestWithIntermediateCorrectnessWithoutNegativeScoring() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_MHT_CET).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(1);
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 4, CHOICE_CORRECT, Badge.BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 4, CHOICE_INCORRECT, Badge.BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        examSummaryPage.verifyIntermediateCorrectness(mockTestPlanner);

    }
}
