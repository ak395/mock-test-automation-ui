package mockTest.endToEndTestCase.testListingEndToEndTestCaseForMedical;

import constants.Goals;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import testBase.TestBase;
import utils.Properties;

import static constants.Badge.*;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class MedicalPartTest extends TestBase {
    private MockTestPlanner mockTestPlanner;


    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
    }
//
//    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING, Categories.TRP_TAKE_A_TEST_TEST_LISTING_MEDICAL},
//            description = "Verify  Medical User take a part test  by answering all question correct  " +
//                    "Test_Listing_End to End Scenario_3")
    public void takeANeetPartTestTest() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_PART_TEST);
        mockTestHomePage.startTest();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT,getmockTestResponse ()));
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();


    }

}
