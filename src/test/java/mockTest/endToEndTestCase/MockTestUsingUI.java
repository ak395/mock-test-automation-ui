package mockTest.endToEndTestCase;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;
import utils.Properties;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import static constants.Badge.*;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class MockTestUsingUI extends TestBase {
    private MockTestPlanner mockTestPlanner;
    private static String exam = null;
    private static String ty = null;
    private static String type = null;
    private static String test = null;
    private static String name = null;
    private static String pswd = null;
    private static int test_index = 0;
    private static int pa;
    private static int wa;
    private static int tfc;
    private static int ia;
    private static int otc;
    private static int oti;

    public static void testTypes(String t){
        if(t.contains("mini_test"))  type = TestTypes.TEST_TYPE_MINI_TEST;
        else if(t.contains("previous_year_test"))  type = TestTypes.TEST_TYPE_PREVIOUS__YEAR_TEST;
        else if(t.contains("full_test"))  type = TestTypes.TEST_TYPE_FULL_TEST;
        else if(t.contains("revision_test"))  type = TestTypes.TEST_TYPE_REVISION_TEST;
    }
    public static void readFile(){

        String fileName = "/Users/abhinavkumar/Documents/python/autotesting/testdata.csv";
        File file = new File(fileName);
        try{
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] data = null;
            while((line = br.readLine()) != null){
                data = line.split(",");
            }
            exam = data[0];
            ty = data[1];
            test = data[2];
            test_index = Integer.parseInt(data[3])-1;
            name = data[4];
            pswd = data[5];
            pa = Integer.parseInt(data[6]);
            wa = Integer.parseInt(data[7]);
            tfc = Integer.parseInt(data[8]);
            ia = Integer.parseInt(data[9]);
            otc = Integer.parseInt(data[10]);
            oti = Integer.parseInt(data[11]);
            testTypes(ty);

        }catch (Exception e){
            System.out.println(e);
        }

    }

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest()
    {
        readFile();
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.setTestDataService(new TestDataService(App.WEB));
        mockTestPlanner.setTestId(mockTestPlanner.getTestDataService().getMockTestTab().getExam().getTestId());
//        mockTestPlanner.addMockTestData(new MockTestData(1, pa, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT));
//        mockTestPlanner.addMockTestData(new MockTestData(pa+1, pa+wa, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
//        mockTestPlanner.addMockTestData(new MockTestData(wa+1, pa+wa+tfc, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
//        mockTestPlanner.addMockTestData(new MockTestData(tfc+1, pa+wa+tfc+ia, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT));
//        mockTestPlanner.addMockTestData(new MockTestData(ia+1, pa+wa+tfc+ia+otc, CHOICE_CORRECT, BADGE_OVERTIME_CORRECT));
//        mockTestPlanner.addMockTestData(new MockTestData(otc+1, pa+wa+tfc+ia+otc+oti, CHOICE_INCORRECT, BADGE_OVERTIME_INCORRECT));
    }

    @Test(groups = {Categories.TRP_Test_Mock_Test_Using_UI},
            description = "Verifies whether the given mock test is running properly")
    public void testMockTestUsingUI() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest mockTest = new MockTestBuilder().withExamName(exam).build();
        leftFilter.selectExamName(mockTest);
        mockTestHomePage.selectTestType(type);
        mockTestHomePage.startTest(test_index);
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, pa, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(pa+1, pa+wa, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(pa+wa+1, pa+wa+tfc, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(pa+wa+tfc+1, pa+wa+tfc+ia, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(pa+wa+tfc+ia+1, pa+wa+tfc+ia+otc, CHOICE_CORRECT, BADGE_OVERTIME_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(pa+wa+tfc+ia+otc+1, pa+wa+tfc+ia+otc+oti, CHOICE_INCORRECT, BADGE_OVERTIME_INCORRECT));
        questionsHomePage.selectAnswers(mockTestPlanner);
//        try{
//            Thread.sleep(60000);
//        }catch (Exception e){ }
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
//        try{
//            Thread.sleep(100000);
//        }catch (Exception e){}
    }
}
