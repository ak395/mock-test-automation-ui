package mockTest.mocktestInMultipleTabTest;

import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import driver.DriverInitializer;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import page.LandingHomePage;
import page.mockTestQuestionsPage.QuestionsHomePage;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;
import utils.Properties;

import java.util.ArrayList;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class VerifyMockTestInMultipleTabOrBrowser extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.setTestDataService(new TestDataService(App.WEB));
        mockTestPlanner.setTestId(mockTestPlanner.getTestDataService().getMockTestTab().getExam().getTestId());
        mockTestPlanner.addMockTestData(new MockTestData(1, 5, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify if user trying to take test in more than one tab, user is able to \n" +
            "take the test in the respective tab only after refreshing the page." +
            "TRP_290_1")
    public void verifyUserAbleToResumeTestInOneTabWhenUserOpenMultipleTab() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        String currentTest = getCurrentURl();
        questionsHomePage.openTestInDifferentTab(currentTest);
        navigateToRefresh();
        questionsHomePage.verifyTestInDifferentBrowserOrTab();
    }

//    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_Failed_Test}, description = "Verify if user trying to take test in more than one browser, user is able to \n" +
//            "take the test in the respective browser only." +
//            "TRP_290_2" +
//            "EQA-3809")
//    public void userShouldBeAbleToResumeTestInRespectiveBrowserWhenUserOpenMultipleBrowser() throws Exception {
//        navigateTo(Properties.baseUrl);
//        landingHomePage.clickOnStartNowButton();
//        landingHomePage.clickOnLoginButton();
//        landingHomePage.clickOnRegisterHereLink();
//        String email = landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
//        editCookies();
//        searchHomePage.clickOnTakeATestButton();
//        mockTestHomePage.verifyUserLandedOnTestListingPage();
//        mockTestHomePage.clearSearchField();
//        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
//        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
//        mockTestHomePage.startTest();
//        instructionPage.dismissInstructions();
//        Thread.sleep(2000);
//
//        driver = new DriverInitializer("firefox").init();
//        driver.manage().window().maximize();
//        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
//        System.out.println(tabs.size());
//        landingHomePage = PageFactory.initElements(driver, LandingHomePage.class);
//        questionsHomePage = PageFactory.initElements(driver, QuestionsHomePage.class);
//        navigateTo(Properties.baseUrl);
//        driver.navigate().refresh();
//        landingHomePage.clickOnLoginButton();
//        landingHomePage.loginAs(email, "password");
//        landingHomePage.verifyUserIsLoggedIn();
//        editCookies();
//        searchHomePage.clickTakeTestWithNewDriverSession(driver);
//        driver.findElement(By.cssSelector(".btn-box")).click();
//        questionsHomePage.verifySwitchToActiveTab();
//        questionsHomePage.verifyTestInDifferentBrowserOrTab();
//    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify user is able to resume the Test with in the Test duration." +
            "TRP_290_3")
    public void verifyUserAbleToResumeTestAfterClosingTheBrowserAndAgainReopenIt() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        String email = landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        String testNameBeforeResume = questionsHomePage.getTestName();
        questionsHomePage.selectAnswers(mockTestPlanner);
        Thread.sleep(2000);
        driver.close();
        Thread.sleep(2000);

        driver = new DriverInitializer("chrome").init();
        landingHomePage = PageFactory.initElements(driver, LandingHomePage.class);
        questionsHomePage = PageFactory.initElements(driver, QuestionsHomePage.class);
        navigateTo(Properties.baseUrl);
        driver.navigate().refresh();
        landingHomePage.logIn(email, "password");
        landingHomePage.verifyUserIsLoggedIn();
        editCookies();
        searchHomePage.clickTakeTestWithNewDriverSession(driver);
        driver.findElement(By.cssSelector(".btn-box")).click();
        questionsHomePage.verifySwitchToActiveTab();
        String testNameAfterResume = questionsHomePage.getTestName();
        questionsHomePage.verifyTestNameAfterResumingTest(testNameBeforeResume, testNameAfterResume);
    }
}