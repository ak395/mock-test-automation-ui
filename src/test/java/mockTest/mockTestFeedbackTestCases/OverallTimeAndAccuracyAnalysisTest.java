package mockTest.mockTestFeedbackTestCases;

import constants.Goals;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import io.restassured.response.ResponseBody;
import mockTestService.MockTestClient;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import readExcelpoiji.ExamDifficultyModel;
import readExcelpoiji.ReadExamDifficulty;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class OverallTimeAndAccuracyAnalysisTest extends TestBase {

    private MockTestPlanner mockTestPlanner;
    ReadExamDifficulty ReadExamDifficulty = new ReadExamDifficulty();

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 5, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
//        mockTestPlanner.addMockTestData(new MockTestData(3,4,CHOICE_CORRECT,BADGE_TOO_FAST_CORRECT));
//        mockTestPlanner.addMockTestData(new MockTestData(9,11,CHOICE_CORRECT,BADGE_TOO_FAST_CORRECT));
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK},
            description = "Verify the Widgets Under Overall time and Accuracy Section"
                    + "Overall_Performance_27")
    public void verifyOverallTimeAndAccuracyWidgets() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        overallPerformanceHomePage.verifyOverallTimeAndAccuracyAnalysisFields();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK},
            description = "Verify the Functionality of Attempted Time Spent Widget under Overall time and Accuracy Analysis Section"
                    + "Overall_Performance_28" +
                      "Overall_Performance_29" +
                      "Overall_Performance_30" +
                      "Overall_Performance_31" +
                      "Overall_Performance_32"
    )
    public void verifyAttemptedTimeSpentFunctionality() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        overallPerformanceHomePage.verifyTheAxisLabelForAttemptedTimeSpent();
        overallPerformanceHomePage.verifyTheDefinitionsOfSubHeadingInAttemptedTimeSpent(driver);
        overallPerformanceHomePage.verifyInfoIconOfAttemptedTimeSpent(driver);

    }

    @Test
    public void verifyAccuracyWithDifficultyFunctionality() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        overallPerformanceHomePage.verifyTheAxisLabelForAccuracyWithDifficulty();
        overallPerformanceHomePage.verifyInfoIconOfAccuracyWithDifficulty(driver);

    }

    @Test
    public void verifyTheDataForEasyQuestionsInAttemptedTimeSpent() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_REVISION_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        ResponseBody responseBody = new MockTestClient().getMockTestFullPaperInfo(getCookies(), getMockTestBundlePath());
        List<String> difficultyBands = mockTestClient.getDifficultyOfQuestions(responseBody);
        System.out.println("Size of Easy Questions: "+ Collections.frequency(difficultyBands,"easy"));
        int totalNumberOfEasyQuestions = Collections.frequency(difficultyBands,"easy");
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        float attemptedTimeSpent = new MockTestClient().getAttemptedTimeSpentFromSummaryApi(getCookies(),getMockTestBundlePath(),"easy");
        System.out.println("Attempted Time Spent: "+ attemptedTimeSpent);
        ExamDifficultyModel examDifficultyModel = ReadExamDifficulty.getExamDifficultyModel("jee-main");
        List<Integer> easyDifficultyList = Stream.of(examDifficultyModel.getEasyDifficultyLevel().split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> mediumDifficultyList = Stream.of(examDifficultyModel.getMediumDifficultyLevel().split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> hardDifficultyList = Stream.of(examDifficultyModel.getHardDifficultyLevel().split(",")).map(Integer::parseInt).collect(Collectors.toList());
//         String[] difficultyLevel = examDifficultyModel.getEasyDifficultyLevel().split(",");
//
//        Set<String> hashset = new HashSet<String>(Arrays.asList(difficultyLevel));
        System.out.println(easyDifficultyList);

        int numberOfEasyQuestions = mockTestPlanner.getNumberOfQuestionsAccordingToDifficulty(easyDifficultyList);
        int numberOfMediumQuestions = mockTestPlanner.getNumberOfQuestionsAccordingToDifficulty(mediumDifficultyList);
        int numberOfHardQuestions = mockTestPlanner.getNumberOfQuestionsAccordingToDifficulty(hardDifficultyList);
        System.out.println( "no. of Easy Questions through mock test plannar: "+ numberOfEasyQuestions);
        int idealTime = mockTestPlanner.getIdealTimeAccordingToDifficulty(easyDifficultyList);
//        overallPerformanceHomePage.verifyTheAxisLabelForAccuracyWithDifficulty();
//        overallPerformanceHomePage.verifyInfoIconOfAccuracyWithDifficulty(driver);
        overallPerformanceHomePage.verifyTheNoOfEasyQuestions(driver,numberOfEasyQuestions,idealTime,attemptedTimeSpent,totalNumberOfEasyQuestions);



    }

    @Test
    public void verifyTheDataForMediumQuestions() {

        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        ResponseBody responseBody = new MockTestClient().getMockTestFullPaperInfo(getCookies(), getMockTestBundlePath());
        mockTestClient.getDifficultyOfQuestions(responseBody);
        List<String> difficultyBands = mockTestClient.getDifficultyOfQuestions(responseBody);

        System.out.println("Size of Easy Questions: "+ Collections.frequency(difficultyBands,"medium"));

        int totalNumberOfMediumQuestions = Collections.frequency(difficultyBands,"medium");
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        float attemptedTimeSpent = new MockTestClient().getAttemptedTimeSpentFromSummaryApi(getCookies(),getMockTestBundlePath(),"medium");
        System.out.println("Attemoted Time Spent: "+ attemptedTimeSpent);
        ExamDifficultyModel examDifficultyModel = ReadExamDifficulty.getExamDifficultyModel("aiims");
        List<Integer> easyDifficultylist = Stream.of(examDifficultyModel.getEasyDifficultyLevel().split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> mediumDifficultyList = Stream.of(examDifficultyModel.getMediumDifficultyLevel().split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> hardDifficultyList = Stream.of(examDifficultyModel.getHardDifficultyLevel().split(",")).map(Integer::parseInt).collect(Collectors.toList());
        System.out.println(easyDifficultylist);

        int numberOfMediumQuestions = mockTestPlanner.getNumberOfQuestionsAccordingToDifficulty(mediumDifficultyList);
        System.out.println("Ideal Time For Medium Questions:"+ mockTestPlanner.getIdealTimeAccordingToDifficulty(mediumDifficultyList));
        int idealTime = mockTestPlanner.getIdealTimeAccordingToDifficulty(mediumDifficultyList);
        overallPerformanceHomePage.verifyTheNoOfMediumQuestions(driver,numberOfMediumQuestions,idealTime,attemptedTimeSpent,totalNumberOfMediumQuestions);


    }

    @Test
    public void verifyTheDataForHardQuestions() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab(3);
        instructionPage.dismissInstructions();
        ResponseBody responseBody = new MockTestClient().getMockTestFullPaperInfo(getCookies(), getMockTestBundlePath());
        mockTestClient.getDifficultyOfQuestions(responseBody);
        List<String> difficultyBands = mockTestClient.getDifficultyOfQuestions(responseBody);
        int totalNumberOfHardQuestions = Collections.frequency(difficultyBands,"hard");
        System.out.println("Size of Easy Questions: "+ Collections.frequency(difficultyBands,"hard"));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        float attemptedTimeSpent = new MockTestClient().getAttemptedTimeSpentFromSummaryApi(getCookies(),getMockTestBundlePath(),"hard");
        int totalQuestionFromApi= new MockTestClient().getTotalQuestionsFromSummaryApi(getCookies(),getMockTestBundlePath(),"hard");
        ExamDifficultyModel examDifficultyModel = ReadExamDifficulty.getExamDifficultyModel("jee-main");
        List<Integer> hardDifficultyList = Stream.of(examDifficultyModel.getHardDifficultyLevel().split(",")).map(Integer::parseInt).collect(Collectors.toList());
        int numberAttemptedOfHardQuestions = mockTestPlanner.getNumberOfQuestionsAccordingToDifficulty(hardDifficultyList);
        int idealTime = mockTestPlanner.getIdealTimeAccordingToDifficulty(hardDifficultyList);
        overallPerformanceHomePage.verifyTheNoOfHardQuestions(driver, numberAttemptedOfHardQuestions,idealTime,attemptedTimeSpent,totalNumberOfHardQuestions,totalQuestionFromApi);
    }





}

