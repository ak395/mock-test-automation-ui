package mockTest.mockTestFeedbackTestCases;

import constants.Goals;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import io.restassured.response.ResponseBody;
import mockTestFactory.MyChoice;
import mockTestService.MockTestClient;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import readExcelpoiji.ExamDifficultyModel;
import readExcelpoiji.ReadExamDifficulty;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;
import static mockTestFactory.MyChoice.CHOICE_UNATTEMPTED;

public class SubjectWiseAttemptsTest extends TestBase {


    private MockTestPlanner mockTestPlanner;
    ReadExamDifficulty ReadExamDifficulty = new ReadExamDifficulty();


    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(3,5, CHOICE_UNATTEMPTED,BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(6,9,CHOICE_INCORRECT,BADGE_TOO_FAST_CORRECT));
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK},
            description = "Verify the Graphs for Subject Wise Attempts"
                    + "Overall_Performance_22" +
                      "Overall_Performance_23" +
                      "Overall_Performance_24"
    )
    public void verifySubjectWiseAttemptsData() {

        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        List<String> subjectNames = questionsHomePage.getSubjectLabelSection().getSubjectName();
        int noOfSubjects = questionsHomePage.getSubjectLabelSection().getNumberOfSubjects();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        System.out.println("Total Ideal Time for the Subject " + mockTestPlanner.getIdealTimeForSubject("Physics"));
//        overallPerformanceHomePage.verifyAxisLabelForSubjectWiseAttempts();
//        overallPerformanceHomePage.verifyTheHeadingAndSubjectNamesInSubjectWiseAttempts(noOfSubjects,subjectNames);
//        overallPerformanceHomePage.verifyTheNumberOfCorrectQuestionsInSubjectWiseAttempts(driver);

    }

    @Test
    public void verifyQuestionAttempts() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        List<String> subjectNames = questionsHomePage.getSubjectLabelSection().getSubjectName();
        int noOfSubjects = questionsHomePage.getSubjectLabelSection().getNumberOfSubjects();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        int correctQuestionss =  mockTestPlanner.getNumberOfQuestionsFor(MyChoice.CHOICE_CORRECT,"Physics");
        System.out.println("Number of Correct Questions:" + correctQuestionss);
        List<Integer> correctQuestionsList = new ArrayList();
        List<Integer> inCorrectQuestionsList = new ArrayList();
        List<Integer> unAttemptedQuestionsList = new ArrayList<>();
        for (String subjectName:
             subjectNames) {
            System.out.println("First Letter Capital "+ subjectName.substring(0,1).toUpperCase() + subjectName.substring(1).toLowerCase());
            int correctQuestions = mockTestPlanner.getNumberOfQuestionsFor(MyChoice.CHOICE_CORRECT,subjectName.substring(0,1).toUpperCase() + subjectName.substring(1).toLowerCase());
            correctQuestionsList.add(correctQuestions);
            int inCorrectQuestions = mockTestPlanner.getNumberOfQuestionsFor(CHOICE_INCORRECT, subjectName.substring(0, 1).toUpperCase() + subjectName.substring(1).toLowerCase());
            inCorrectQuestionsList.add(inCorrectQuestions);
            int unAttemptedQuestions = mockTestPlanner.getNumberOfQuestionsFor(CHOICE_UNATTEMPTED, subjectName.substring(0, 1).toUpperCase() + subjectName.substring(1).toLowerCase());
            unAttemptedQuestionsList.add(unAttemptedQuestions);

        }

        overallPerformanceHomePage.verifyTheNumberOfCorrectQuestionsInSubjectWiseAttempts(driver,correctQuestionsList,inCorrectQuestionsList);
        overallPerformanceHomePage.verifyTheDataForIncorrectQuestionsInSubjectWiseAttempts(driver,inCorrectQuestionsList);
        overallPerformanceHomePage.verifyTheDataForUnAttemptedQuestions(driver,unAttemptedQuestionsList);

    }


    @Test
    public void testingSummaryApi() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        List<String> subjectNames = questionsHomePage.getSubjectLabelSection().getSubjectName();
        int noOfSubjects = questionsHomePage.getSubjectLabelSection().getNumberOfSubjects();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
//        float attemptedTimeSpent = new MockTestClient().getAttemptedTimeSpentFromSummaryApi(getCookies(),getMockTestBundlePath(),"medium");
//        System.out.println("Attemoted Time Spent: "+ attemptedTimeSpent);
        new MockTestClient().getSectionWiseTotalQuestionsFromSummaryApi(getCookies(),getMockTestBundlePath());
        }


    @Test
    public void testingDifficultyTestCase() {


        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        List<String> subjectNames = questionsHomePage.getSubjectLabelSection().getSubjectName();
        int noOfSubjects = questionsHomePage.getSubjectLabelSection().getNumberOfSubjects();
        ResponseBody responseBody = new MockTestClient().getMockTestFullPaperInfo(getCookies(), getMockTestBundlePath());
        mockTestClient.getDifficultyOfQuestions(responseBody);
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        ExamDifficultyModel examDifficultyModel = (ReadExamDifficulty.getExamDifficultyModel("jee-main"));
        List<Integer> list = Stream.of(examDifficultyModel.getMediumDifficultyLevel().split(",")).map(Integer::parseInt).collect(Collectors.toList());
//         String[] difficultyLevel = examDifficultyModel.getEasyDifficultyLevel().split(",");
//
//        Set<String> hashset = new HashSet<String>(Arrays.asList(difficultyLevel));
        System.out.println(list);

        int numberOfQuestions = mockTestPlanner.getNumberOfQuestionsAccordingToDifficulty(list);
        System.out.println("Medium" + numberOfQuestions);


    }

}
