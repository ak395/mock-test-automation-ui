package mockTest.mockTestFeedbackTestCases;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.*;
import static mockTestFactory.MyChoice.*;

public class VerifyQuestionStatistics extends TestBase {
    private MockTestPlanner mockTestPlanner;


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK, Categories.TRP_TAKE_A_TEST_CHATBOT1},
            description = "Verify the Question Statistics for the  test taken"
                    + "Test_Feedback_Test_3")
    public void verifyQuestionStatisticsField() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(1);
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, 5, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(6, 10, CHOICE_INCORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(11, 15, CHOICE_UNATTEMPTED, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(16, 20, CHOICE_ANSWERED_REVIEW_LATER, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(21, 25, CHOICE_REVIEW_LATER, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        examSummaryPage.verifyQuestionStatisticeFieldIsDisplay();

    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST,Categories.TRP_TEST_FEEDBACK,Categories.TRP_TAKE_A_TEST_CHATBOT1},
            description = "Verify the count exactly matches in the Question Statistics"
                    + "Test_Feedback_Test_4")
    public void verifyQuestionStatisticsQuestionCount() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(1);
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, 5, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(6, 10, CHOICE_INCORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(11, 15, CHOICE_UNATTEMPTED, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(16, 20, CHOICE_ANSWERED_REVIEW_LATER, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(21, 25, CHOICE_REVIEW_LATER, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        examSummaryPage.verifyQuestionStatistics(mockTestPlanner);

    }

    @Test(groups = { Categories.TRP_TAKE_A_TEST,Categories.TRP_TEST_FEEDBACK, Categories.TRP_TAKE_A_TEST_CHATBOT1},
            description = "Verify the Attempt Analysis" + "The Attempt Analysis chould contain.\n" +
                    " A. Correct Attempt .\n" +
                    " B. Incorrect Attempt." +
                    "Test_Feedback_Test_5")
    public void verifyQuestionStatisticsForCorrectAndIncorrectTypeQuestionAttempt() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(1);
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, 5, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(6, 10, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        examSummaryPage.verifyQuestionStatisticsForCorrectAndIncorrectAttemptType(mockTestPlanner);

    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK, Categories.TRP_TAKE_A_TEST_CHATBOT1},
            description = "Verify the count matches with the Attempt Analysis " +
                    "a. Over time Correct.\n" +
                    " B Perfect Attempt.\n" +
                    " C. Too Fast correct.\n" +
                    " 3. Verify for incorrect attempt .\n" +
                    " A. Wasted Attempt.\n" +
                    " B. Incorrect Attempt.\n" +
                    " C. Over time incorrect." +
                    "Test_Feedback_Test_6")
    public void verifyQuestionStatisticsForZAllAttemptType() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(1);
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(3, 3, CHOICE_CORRECT, BADGE_OVERTIME_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(4, 5, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(6, 7, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(8, 9, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(10, 10, CHOICE_INCORRECT, BADGE_OVERTIME_INCORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        examSummaryPage.verifyQuestionStatisticsForAllAttemptType(mockTestPlanner);

    }

}
