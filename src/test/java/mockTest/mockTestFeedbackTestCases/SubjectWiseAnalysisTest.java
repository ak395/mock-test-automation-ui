package mockTest.mockTestFeedbackTestCases;

import constants.Goals;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import java.util.List;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class SubjectWiseAnalysisTest extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(3,5,CHOICE_INCORRECT,BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(6,9,CHOICE_CORRECT,BADGE_TOO_FAST_CORRECT));
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK},
            description = "Verify that under Subject Wise Analysis, Subject-Wise Time Spent and Subject-Wise Accuracy should be displayed"
                    + "Overall_Performance_8")
    public void verifySubjectWiseAnalysisFunctionality() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        overallPerformanceHomePage.verifyTimeSpentAndAccuracyGraphIsDisplayed();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK},
            description = "Verify the Headings and axis labels in subject-wise time spent graph"
                    + "Overall_Performance_9"  +
                      "Overall_Performance_10" +
                      "Overall_Performance_11" +
                      "Overall_Performance_14")
    public void verifySubjectWiseTimeSpentFunctionality() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_Railway);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        List<String> subjectNames = questionsHomePage.getSubjectLabelSection().getSubjectName();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        overallPerformanceHomePage.verifyAxisLabelsInSubjectWiseTimeSpentGraph(subjectNames);
        subjectWiseAnalysisSection.verifyTheSubHeadingOfTheSubjectWiseTimeSpentWidget();
        subjectWiseAnalysisSection.verifyTheHeaderInSubjectWiseTimeSpent();
        overallPerformanceHomePage.verifyTheDataForSubjectWiseTimeSpentInfoIcon(driver);
        overallPerformanceHomePage.verifyTheHeaderDefinitionsInSubjectWiseTimeSpent(driver);

    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK},
            description = "Verify on Mouse hover tooltip should appear for the bar graph of every subject in subject wise time spent graph"
                    + "Overall_Performance_12" +
                      "Overall_Performance_13" )
    public void verifyTheTooltipOnMouseHoverForSubjectWiseTimeSpent() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        overallPerformanceHomePage.verifyTheDataForSubjectWiseTimeSpentGraph(driver);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST},
            description = "Verify the ToolTip content for subject wise accuracy graph"
                    + "Overall_Performance_18")
    public void verifyTheTooltipOnMouseHoverForSubjectWiseAccuracy() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        feedbackHomePage.verifyTheTooltipForSubjectWiseAccuracy(driver);
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_FEEDBACK},
            description = "Verify the Headings and axis labels in subject-wise Accuracy graph"
                    + "Overall_Performance_19"  +
                    "Overall_Performance_20" +
                    "Overall_Performance_17" )
    public void verifySubjectWiseAccuracyFunctionality() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        List<String> subjectNames = questionsHomePage.getSubjectLabelSection().getSubjectName();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        overallPerformanceHomePage.verifyAxisLabelsInSubjectWiseAccuracyGraph(subjectNames);
        subjectWiseAnalysisSection.verifyTheSubHeadingOfTheSubjectWiseAccuracyWidget();
        overallPerformanceHomePage.verifyTheDataForSubjectWiseAccuracyInfoIcon(driver);

    }


}