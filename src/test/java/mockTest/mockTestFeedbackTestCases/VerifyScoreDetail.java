package mockTest.mockTestFeedbackTestCases;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class VerifyScoreDetail extends TestBase {
    private MockTestPlanner mockTestPlanner;

    public void setup() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST,Categories.TRP_TEST_FEEDBACK},
            description = "Verify the score detail with  cutoff marks and percentage and if user attempt all question correct  matches with the exam taken"
                    + "Test_Feedback_Test_2")
    public void verifyScoreDetail_1() {
        setup();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        examSummaryPage.verifyPerformanceForAllQuestionAttemptedCorrect(mockTestPlanner);
        examSummaryPage.verifiedCutOffMarks(mockTestPlanner);
        examSummaryPage.verifiedPercentageMarks();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST,Categories.TRP_TEST_FEEDBACK},
            description = "Verify the score detail cutoff marks and percentage and marks of user with intermediate Correctness"
                    + "Test_Feedback_Test_2")
    public void verifyScoreDetailWitInterMediateCorrectness() {
        setup();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_INCORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        examSummaryPage.verifyIntermediateCorrectness(mockTestPlanner);
        examSummaryPage.verifiedCutOffMarks(mockTestPlanner);
        examSummaryPage.verifiedPercentageMarks();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST,Categories.TRP_TEST_FEEDBACK},
            description = "Verify the score detail matches with the exam taken with cut off marks and percentage if user attempt all question incorrect"
                    + "Test_Feedback_Test_2")
    public void verifyScoreDetailWitAllQuestionIncorrectness() {
        setup();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_INCORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        examSummaryPage.verifyIntermediateCorrectness(mockTestPlanner);
        examSummaryPage.verifiedCutOffMarks(mockTestPlanner);
        examSummaryPage.verifiedPercentageMarks();
    }


}
