package mockTest.integerTypeQuesTestCases;

import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class IntegerTypeQuesTest extends TestBase {

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify if user has typed a number and moved for other " +
            "question without clicking on next and previous button then " +
            "the number entered should not save" +
            "TRP_462_3")
    public void verifyDataShouldNotSaveWhenUserMoveOtherQuesWithoutClickPrevAndNextButton() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.jumpToQuestionUsingQuestionPad(16);
        questionsHomePage.clickOnNumericalKeyPad();
        questionsHomePage.jumpToQuestionUsingQuestionPad(18);
        questionsHomePage.jumpToQuestionUsingQuestionPad(16);
        questionsHomePage.verifyQuestionNumberColourChangesToRed();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify when user save the option and clears the selected option and mark for review" +
            "TRP_437_4")
    public void verifyWhenUserSaveAndComeBackAndMarkReviewOption() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.jumpToQuestionUsingQuestionPad(5);
        questionsHomePage.clickOnAnyOption();
        questionsHomePage.clickOnNextButton();
        questionsHomePage.clickOnPreviousButton();
        questionsHomePage.clickMarkForReviewQuestion();
        questionsHomePage.verifyQuestionNumberColourChangesToBlue();
    }
}
