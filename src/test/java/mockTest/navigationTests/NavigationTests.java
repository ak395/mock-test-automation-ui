package mockTest.navigationTests;

import constants.Goals;
import constants.TestTypes;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class NavigationTests extends TestBase {

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_NAVIGATION_TESTS, Categories.TRP_TEST_REGRESSION},
            description = "Verifies that student is able to navigate to different sections and different questions by different ways" +
                    "TRP-433_1\n" +
                    "TRP-433_2\n" +
                    "TRP-434_6\n" +
                    "TRP_434_7\n" +
                    "TRP_435_2"
    )
    public void studentShouldAbleToNavigateToSectionsAndQuestions() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(2);
        instructionPage.dismissInstructions();
        questionsHomePage.verifyPreviousButtonFunctionality();
        questionsHomePage.verifyNextButtonFunctionality();
        questionsHomePage.userIsAbleToNavigateBetweenDifferentSections();
        questionsHomePage.finishTheTest();
        questionsHomePage.continueTheTest();
        questionsHomePage.userIsAbleToNavigateBetweenDifferentSections();
        questionsHomePage.jumpToQuestionUsingQuestionPad(5);


    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_REGRESSION},
            description = "Verifies Numbering in Each Section should start from 1" +
                    "TRP_453_2")
    public void verifyNumberingInEachSection() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(2);
        instructionPage.dismissInstructions();
        questionsHomePage.verifyNumberingInEachSection();
    }
}
