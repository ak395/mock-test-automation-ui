package mockTest.testOnTestAnalysis;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestPerformanceFilter;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static constants.Badge.BADGE_WASTED_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class TestOnTestAnalysisTests extends TestBase {

    private MockTestPlanner mockTestPlanner;
    private MockTestPlanner mockTestPlanner1;

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify for the validation message " +
            "when user has attended only 1 test." +
            "Test_On_Test_Analysis_1")
    public void userShouldGetValidationMessageWhenAttemptedOnlyOneTest(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.verifyValidationMessageWhenAttemptedOnlyOneTest();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify the Test-On -Test Analysis is divided into 2 section." +
            "Test_On_Test_Analysis_2" +
            "Test_On_Test_Analysis_3" +
            "Test_On_Test_Analysis_TLP")
    public void userShouldGetTestAnalysisDividedIntoTwoSections(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        String parentWindow = driver.getWindowHandle();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_REVISION_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse())-85, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();

        mockTestPlanner1 = new MockTestPlanner();
        driver.switchTo().window(parentWindow);
        navigateToRefresh();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_REVISION_TEST);
        mockTestHomePage.startTestWithDifferentTab(1);
        instructionPage.dismissInstructions();
        mockTestPlanner1.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse())-85, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner1);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.verifyTestPerformanceSectionDisplaying();
        testOnTestAnalysisPage.verifyChapterWisePerformanceDisplaying();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.TOO_FAST_CORRECT_FILTER);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.WASTED_ATTEMPT_FILTER);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.OVERTIME_ATTEMPT_FILTER);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.ACCURACY_FILTER);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.PERFECT_ATTEMPT_FILTER);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.FIRST_LOOK_ACCURACY);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.INCORRECT_ATTEMPT);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.TIME_SPENT_FILTER);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.EFFORT_FILTER);

        navigateToRefresh();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickFilterTypeDropDown();
        testOnTestAnalysisPage.selectAndVerifyByFilterType(TestPerformanceFilter.MARKS);

        testOnTestAnalysisPage.verifyTestListingButtonDisplayingAndNavigatingToTlp();
       // testOnTestAnalysisPage.verifyOverallWidgetDisplaying();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify Rankup card is displayed under feedback page, and " +
            "clicking on it page must navigate to feedback page." +
            "Test_On_Test_Analysis_16" +
            "Test_On_Test_Analysis_9")
    public void userShouldBeAbleToNavigateRankUpPage(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        String parentWindow = driver.getWindowHandle();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        String firstTestName = questionsHomePage.getTestName();
        System.out.println(firstTestName);
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse())-88, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();

        mockTestPlanner1 = new MockTestPlanner();
        driver.switchTo().window(parentWindow);
        navigateToRefresh();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab(1);
        instructionPage.dismissInstructions();
        String secondTestName = questionsHomePage.getTestName();
        System.out.println(secondTestName);
        mockTestPlanner1.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) - 88, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner1);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.verifyExamType(TestTypes.TEST_TYPE_FULL_TEST);
        testOnTestAnalysisPage.verifyTestNamesMatchingWrtTest(firstTestName, secondTestName);
        testOnTestAnalysisPage.verifyDateWithExam();
        testOnTestAnalysisPage.verifyPracticeWidgetDisplaying();
        testOnTestAnalysisPage.verifyRankUpSectionDisplaying();
        testOnTestAnalysisPage.clickOnRankUpAndVerifyNavigation();
        testOnTestAnalysisPage.verifyRankUpSignUpDisplaying();
        testOnTestAnalysisPage.verifySignupNavigation();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_ON_TEST_ANALYSIS}, description = "Verify rank up navigation" +
                    "Verify login functionality for rank up" +
                    "Verify user logged in successfully and navigate to rank up page" +
                    "Verify drop down is working and clicking on profile link" +
                    "Verify navigation for pack history page" +
                    "Verify pack cards are displaying or not")
    public void userPackBoughtShouldShownUnderPackHistory(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        String parentWindow = driver.getWindowHandle();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        String firstTestName = questionsHomePage.getTestName();
        System.out.println(firstTestName);
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse())-88, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();

        mockTestPlanner1 = new MockTestPlanner();
        driver.switchTo().window(parentWindow);
        navigateToRefresh();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab(1);
        instructionPage.dismissInstructions();
        String secondTestName = questionsHomePage.getTestName();
        System.out.println(secondTestName);
        mockTestPlanner1.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) - 88, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner1);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.clickOnRankUpAndVerifyNavigation();
        testOnTestAnalysisPage.rankUpLogIn();
        //testOnTestAnalysisPage.verifyUserLoggedInSuccessfullyForrankUp();
        testOnTestAnalysisPage.clickOnRankUpDropDownForProfileLink();
        testOnTestAnalysisPage.verifyProfileForPack();
        testOnTestAnalysisPage.verifyNavigationPackHistoryPage();
        testOnTestAnalysisPage.verifyUserShouldAbleToSeePackCards();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_ON_TEST_ANALYSIS}, description = "Verify user should not be able to login with wrong credentials")
    public void userShouldNotBeAbleToLoginWithWrongCredentials(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        String parentWindow = driver.getWindowHandle();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        String firstTestName = questionsHomePage.getTestName();
        System.out.println(firstTestName);
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) -88, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();

        mockTestPlanner1 = new MockTestPlanner();
        driver.switchTo().window(parentWindow);
        navigateToRefresh();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab(1);
        instructionPage.dismissInstructions();
        String secondTestName = questionsHomePage.getTestName();
        System.out.println(secondTestName);
        mockTestPlanner1.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) -88, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner1);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        testOnTestAnalysisPage.clickTestOnTestAnalysis();
        testOnTestAnalysisPage.verifyRecommendedPackDisplayed();
        testOnTestAnalysisPage.clickAndVerifyUnlockPopUpkDisplayed();
        testOnTestAnalysisPage.verifyUnlockNowButtonDisplayingAndClickable();
        testOnTestAnalysisPage.verifyEmbiumBankWidgetDisplaying();
        testOnTestAnalysisPage.clickOnRankUpAndVerifyNavigation();
        testOnTestAnalysisPage.rankUpLoginWithWrongCredential();
    }
}
