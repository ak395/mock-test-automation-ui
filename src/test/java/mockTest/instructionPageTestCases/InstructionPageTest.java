package mockTest.instructionPageTestCases;

import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import driver.DriverInitializer;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import page.LandingHomePage;
import page.mockTestQuestionsPage.QuestionsHomePage;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;


public class InstructionPageTest extends TestBase {

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_INSTRUCTIONPAGE},
            description = "Verify user has to acknowledge that" +
                    "instruction read carefully and proceed to begin test paper" +
                    "TRP_429_1")
    public void verifyUserAcknowledgeInstructionAndProceedToBeginWithTest() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clickShowAllOrHideButton();
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyUserLandedOnTestPage();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_INSTRUCTIONPAGE},
            description = "Verify if user has not checked the check box then he/she are not allowed to proceed for taking a test" +
                    "TRP_429_2")
    public void verifyWhenUserDoesNotAcknowledgeInstructionAndNotAllowedToProceedWithTest() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.verifyTermsAndConditionUncheckedThenUserNotAllowedToTakeATest();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_INSTRUCTIONPAGE},
            description = "Verify if user has not checked the check box then he/she are not allowed to proceed for taking a test" +
                    "TRP_429-4")
    public void verifyWhenUserDoesNotAcknowledgeInstructionAndNotAllowedToProceedWithTes() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.verifyInstructionPageTitleDisplaying();
        instructionPage.verifyTermsAndConditionUncheckedThenUserNotAllowedToTakeATest();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_INSTRUCTIONPAGE},
            description = "Verify when user selecting the check box and reopen the same test" +
                    "then again user needs to get from instruction page" +
                    "TRP_429_3")
    public void verifyUserAcknowledgeInstructionAndReopenTestAndThenUserNeedToAgainAcknowledgeInstruction() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        String currentTest = getCurrentURl();
        instructionPage.clickTermsAndConditionCheckbox();

        navigateTo(currentTest);
        instructionPage.verifyInstructionPageTitleDisplaying();
        instructionPage.dismissInstructions();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_INSTRUCTIONPAGE}, description = "Verify instruction window is closed when user clicks on refresh page when Instruction Page is open" +
            "TRP_439_11")
    public void verifyInstructionCloseWhenUserRefreshPageInTestSection() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyInstructionPopUpDisappearOnRefreshingPage();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_INSTRUCTIONPAGE}, description = "Verify if user has clicked on the check box and started the test and then he/she closed the browser and again reopened it then instruction page should not appear" +
            "TRP_429_5")
    public void verifyUserAcknowledgeInstructionAndStartTestAndCloseBrowserThenInstructionShouldNotAppear() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        String email = landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        landingHomePage.verifyUserIsLoggedIn();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        Thread.sleep(2000);
        driver.close();
        Thread.sleep(2000);

        driver = new DriverInitializer("chrome").init();
        landingHomePage = PageFactory.initElements(driver, LandingHomePage.class);
        questionsHomePage = PageFactory.initElements(driver, QuestionsHomePage.class);
        navigateTo(Properties.baseUrl);
        driver.navigate().refresh();
        landingHomePage.logIn(email, "password");
        landingHomePage.verifyUserIsLoggedIn();
        editCookies();
        searchHomePage.clickTakeTestWithNewDriverSession(driver);
        driver.findElement(By.cssSelector(".btn-box")).click();
        questionsHomePage.verifySwitchToActiveTab();
        questionsHomePage.verifyUserLandedOnTestPage();
    }
}