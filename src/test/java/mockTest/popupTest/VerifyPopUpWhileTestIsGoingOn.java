package mockTest.popupTest;


import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class VerifyPopUpWhileTestIsGoingOn extends TestBase {


    @Test(groups = {Categories.TRP_TAKE_A_TEST},
            description = "Verify Popup has to appear as per marvel when user" +
                    "is closing the tab when test is going on" +
                    "TRP_274_1")
    public void verifyPopUpWhenUserSubmittingTheTestWhileTestIsGoingOn() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        testFeedBackHeader.testFeedBackHeadingIsDisplayed();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST},
            description = "Verify Popup has to appear as per marvel when user" +
                    "is closing the tab when test is going on" +
                    "TRP_274_2" +
                    "TRP_274_3")
    public void verifyPopUpWhenUserRefreshingTheTabWhileTestIsGoingOn() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyBrowserPopUpWhileRefreshingTheTab();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify user should be able to back to test when user click on question paper")
    public void verifyUserShouldAbleToBackToTestWhenUserClickOnQuestionPaper() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyBackToTest();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify user should be able to view options when user clicks on question with option")
    public void verifyUserAbleToSeeOptionWhenUserClicksOnQuestionWithOptions() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyQuestionWithOption();
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify user should not be able to view options when user clicks on question without option")
    public void verifyUserShouldNotBeAbleToSeeOptionWhenUserClicksOnQuestionWithOutOptions() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyQuestionWithOutOption();
    }
}
