package mockTest.questionStatus;

import constants.Goals;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;
import utils.Properties;
import static constants.Badge.*;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_UNATTEMPTED;

public class verifyMarkForReviewStatus extends TestBase {

    private MockTestPlanner mockTestPlanner;
    public static final Integer QUESTIONNUMBER = 1;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForMockTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.setTestDataService(new TestDataService(App.WEB));
        mockTestPlanner.setTestId(mockTestPlanner.getTestDataService().getMockTestTab().getExam().getTestId());
        mockTestPlanner.addMockTestData(new MockTestData(1, 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(3, 4, CHOICE_UNATTEMPTED, BADGE_UNATTEMPTED));

    }

    @Test(groups= {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_REGRESSION},
            description = "Verify the question status changes for different attempt choices and can reselect any option in a particular question" +
                    "TRP_437_5" +
                    "TRP_437_7" )
    public void verifyQuestionStatusForAttemptType(){
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(2);
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.jumpToQuestionUsingQuestionPad(QUESTIONNUMBER);
        questionsHomePage.clearSelectedOption();
        questionsHomePage.verifyQuestionNumberColourChangesToRed();
        questionsHomePage.markForReview();
        questionsHomePage.verifyQuestionIsMarkedForReview();
        questionsHomePage.jumpToQuestionUsingQuestionPad(3);
        questionsHomePage.clickOnAnyOption();
        questionsHomePage.markForReview();
        questionsHomePage.goToNextQuestion();
        questionStatusSection.verifyQuestionIsAnsweredAndMarkForReview(3);


    }
}
