package mockTest.questionStatus;
import constants.Goals;
import constants.TestTypes;
import driver.DriverInitializer;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import page.LandingHomePage;
import page.mockTestQuestionsPage.LegendsAndStatsSection;
import page.mockTestQuestionsPage.QuestionStatsData;
import page.mockTestQuestionsPage.QuestionsHomePage;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static constants.Badge.BADGE_UNATTEMPTED;
import static mockTestFactory.MyChoice.*;

public class VerifyTestFunctionalityInDifferentBrowser extends TestBase {

    private MockTestPlanner mockTestPlanner;
    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForMockTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.setTestDataService(new TestDataService(App.WEB));
        mockTestPlanner.setTestId(mockTestPlanner.getTestDataService().getMockTestTab().getExam().getTestId());
        mockTestPlanner.addMockTestData(new MockTestData(1, 3, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(4, 5, CHOICE_UNATTEMPTED, BADGE_UNATTEMPTED));
        mockTestPlanner.addMockTestData(new MockTestData(6, 8, CHOICE_ANSWERED_REVIEW_LATER, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(9, 11, CHOICE_REVIEW_LATER, BADGE_UNATTEMPTED));
    }

    @Test(groups= {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_REGRESSION},
            description = "Verify the question stats should remain same when user resumes the test in different browser" +
                    "TRP_435_4"
                    )
    public void verifyTestFunctionalityShouldRemainSameInDifferentBrowser() throws Exception{
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        String email =landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(2);
        instructionPage.dismissInstructions();
        String testUrl= getCurrentURl();
        questionsHomePage.selectAnswers(mockTestPlanner);
        Thread.sleep(5000);
        QuestionStatsData questionStatsData = legendsAndStatsSection.addDataToQuestionStats();
        Thread.sleep(5000);
        driver.close();
        Thread.sleep(2000);
        driver = new DriverInitializer("firefox").init();
        landingHomePage = PageFactory.initElements(driver, LandingHomePage.class);
        questionsHomePage = PageFactory.initElements(driver, QuestionsHomePage.class);
        legendsAndStatsSection = PageFactory.initElements(driver, LegendsAndStatsSection.class);
        navigateTo(Properties.baseUrl);
        driver.navigate().refresh();
        landingHomePage.clickOnStartNowButton();
        landingHomePage.logIn(email, "password");
        landingHomePage.verifyUserIsLoggedIn();
        editCookies();
        searchHomePage.clickTakeTestWithNewDriverSession(driver);
        navigateTo(testUrl);
        QuestionStatsData questionStatsDataNew = legendsAndStatsSection.addDataToQuestionStats();
        legendsAndStatsSection.compareTheData(questionStatsData,questionStatsDataNew);
    }
}
