package mockTest.chatboat;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class EmbibeGuideSignUpUsingMobileTest extends TestBase {

    private MockTestPlanner mockTestPlanner;


    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT}, description = "verify the chat bot should reopen on the same point where the user had left the chat." +
            "ChatBotTestCase_195_2")
    public void verifyUserShouldBeAbleToSkipTheChatBot() {
        navigateTo(Properties.mockTestWithNegativeMarking);
        instructionPage.dismissInstructions();
        instructionPage.continueAsGuest();
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        examSummaryPage.clickOnOverallPerformanceSection();
        chatBotPage.clickEmbibeGuideTab();
        chatBotPage.verifyOptionSureAndNopeIsDisplayForGuestUser();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT}, description = "Verify the embibe guide for the guest user,  user journey >> guest user >> sign up using mobile number >>Analysis." +
            "ChatBotTestCase_159_2" +
            "ChatBotTestCase_160_3" +
            "ChatBotTestCase_161_4" +
            "ChatBotTestCase_162_5")
    public void verifyEmbibeGuidePageWhenSignUpUsingMobileNumber() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        instructionPage.continueAsGuest();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT,getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.verifyUserNameIsNotDisplayForGuestUser();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickFeedBackImage();
        chatBotPage.clickOnShowMeMoreButton();
    }
}
