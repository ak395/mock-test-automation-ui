package mockTest.chatboat.embibeGuide;

import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import testBase.TestBase;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static constants.Badge.BADGE_WASTED_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class OverAllMarksWidgetForLessThenCutOff extends TestBase {
    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    public void preparedTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 1, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(2, 3, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
    }

    //@Test(groups = {Categories.TRP_TEST_FEEDBACK}, description = "Verify in Overall marks widget, if user scores below average marks and also not cleared Cut off. But if score is positive then user is getting the widget insight as "Almost there! You lost Y marks in wrong atttempts. "
    //   + "TRP_383_6")
    public void verifyOverallMarksWidgetMessageForLessThenCutOff() {

        navigateTo(Properties.mockTestWithOutNegativeMarking);
        instructionPage.dismissInstructions();
        instructionPage.continueAsGuest();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();

        overallMarksWidgetPage.navigateToOverallMarksWidgetForGuestUser();


    }
}
