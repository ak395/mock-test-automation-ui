package mockTest.chatboat.embibeGuide;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class OverallMarksWidgetTest extends TestBase {
    private MockTestPlanner mockTestPlanner;


    @Test(groups = {Categories.TRP_TEST_FEEDBACK}, description = "Verify in Overall marks widget, user is getting -score gained, Max score for the test,Cutoff marks for the Test, average " +
            "marks scored by others, Top score gained for the Test"
            + "TRP_380_1\n"
            + "TRP_380_2\n" +
            "TRP_383_1\n" +
            "TRP_383_3")
    public void verifyOverallMarksWidget() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        overallMarksWidgetPage.navigateToOverallMarksWidgetForGuestUser();
        overallMarksWidgetPage.verifyAverageMarksIsDisplay();
        overallMarksWidgetPage.verifyMinScoreIsDisplay();
        overallMarksWidgetPage.verifyTopScoreIsDisplay();
        overallMarksWidgetPage.verifyTotalMarksIsDisplay();
        overallMarksWidgetPage.verifyUserScoreIsDisplay();
        overallMarksWidgetPage.verifyUserScoreMessageIsDisplayForAllQuestionCorrect();

    }

    @Test(groups = {Categories.TRP_TEST_FEEDBACK}, description = "Verify in \"Overall marks\" widget, proper data is appearing with respect to the attempts user did in the Test"
            + "TRP_380_3\n" +
            "TRP_380_4")
    public void verifyOverallMarksWidgetWRTOverAllPerformance() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        overallMarksWidgetPage.navigateToOverallMarksWidgetForGuestUser();
        overallMarksWidgetPage.verifyOverallMarksWRTOverallPerformance();


    }

    @Test(groups = {Categories.TRP_TEST_FEEDBACK}, description = "Verify when user clicks on Rankers option, User marks are comparing with other Ranker's score who have taken the same Test."
            + "TRP_380_5 ")
    public void verifyUserMarksComparedWithOtherUserInOverallMarksWidget() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        overallMarksWidgetPage.navigateToOverallMarksWidgetForGuestUser();
        overallMarksWidgetPage.verifyCompareSliderIsDisplay();
        overallMarksWidgetPage.clickOnCompareSlider();
        overallMarksWidgetPage.verifyUserMarksComparedWithRanker();


    }

    @Test(groups = {Categories.TRP_TEST_FEEDBACK}, description = "Verify when user clicks on Ranker option, User score is comparing with maximum of 5 Ranker's Score(if available)."
            + "TRP_380_6 ")
    public void verifyUserMarksComparedWithMaximum5Ranker() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        overallMarksWidgetPage.navigateToOverallMarksWidgetForGuestUser();
        overallMarksWidgetPage.clickOnCompareSlider();
        overallMarksWidgetPage.verifyUserMarksCompareWithMaximum5Ranker();


    }
}
