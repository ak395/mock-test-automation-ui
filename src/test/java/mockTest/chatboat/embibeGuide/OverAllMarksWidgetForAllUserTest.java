package mockTest.chatboat.embibeGuide;

import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class OverAllMarksWidgetForAllUserTest extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    public void preparedTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 9, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
    }

    @Test(groups = {Categories.TRP_TEST_FEEDBACK}, description = "Verify both Registered and Guest user are able to get the \"Overall marks\" widget displayed in the Embibe guide"
            + "TRP_383_2")
    public void verifyOverallMarksWidgetDisplayForLoginUser() {
        navigateTo("https://embibe-staging.embibe.com");
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser();
        landingHomePage.verifyUserIsLoggedIn();
        navigateTo(Properties.miniMockTest);
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();

        overallMarksWidgetPage.navigateToOverallMarksWidgetForLoginUser();
        overallMarksWidgetPage.verifyUserScoreIsDisplay();

    }

    @Test(groups = {Categories.TRP_TEST_FEEDBACK}, description = "Verify both Registered and Guest user are able to get the \"Overall marks\" widget displayed in the Embibe guide"
            + "TRP_383_2")
    public void verifyOverallMarksWidgetDisplayForGuestUser() {
        navigateTo(Properties.miniMockTest);
        instructionPage.dismissInstructions();
        instructionPage.continueAsGuest();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();

        overallMarksWidgetPage.navigateToOverallMarksWidgetForGuestUser();
        overallMarksWidgetPage.verifyUserScoreIsDisplay();

    }

}
