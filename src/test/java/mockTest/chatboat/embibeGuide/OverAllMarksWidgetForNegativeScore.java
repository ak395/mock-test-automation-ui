package mockTest.chatboat.embibeGuide;

import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_WASTED_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class OverAllMarksWidgetForNegativeScore extends TestBase {
    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    public void preparedTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 2, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
    }

    @Test(groups = {Categories.TRP_TEST_FEEDBACK}, description = "Verify in Overall marks widget, if user scores negative and is below average marks for the respective test then user is getting the Widget insight as \"Oh no! You lost X marks in wrong attempts. Make sure you revise your answers next time."
            + "TRP_383_7")
    public void verifyOverallMarksWidgetMessageForNegativeScore() {

        navigateTo(Properties.mockTestWithNegativeMarking);
        instructionPage.dismissInstructions();
        instructionPage.continueAsGuest();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();

        overallMarksWidgetPage.navigateToOverallMarksWidgetForGuestUser();
        overallMarksWidgetPage.verifyUserScoreMessageIsDisplayForNegativeScore();


    }
}
