package mockTest.chatboat;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_PERFECT_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class ChatBotForAttemptQ_AndTimeSpendMoreThen10Percentage_2 extends TestBase {
    public MockTestPlanner mockTestPlanner;

    public void FinishTest() {
        System.out.println("class leval @before method");
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) - 4, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.showMeFeedbackButtonIsDisplayed();
        examSummaryPage.clickOnShowMeFeedBackButton();

    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for more than 18 minutes) and attempt more than 10% of questions." +
            "48. Submit test button should take to Show me feedback button\n" +
            "49. Show me Feedback button should take me to Embibe Guide on start chatting Button\n" +
            "50. Start chatting button should take to Hey Button\n" +
            "51. Hey Button should take me to Awesome Looking forward to it." +
            "52. Awesome Looking forward to it should take me to 3 Face icons." +
            "53. All the three faces on getting clicked should take me to Overall marks where he can click on compare\n" +
            "EG_48\n" +
            "EG_49\n" +
            "EG_50\n" +
            "EG_51\n" +
            "EG_52\n" +
            "EG_53")

    public void verifyEmbibeGuideForAttemptQuestionMoreThen10Percentage() {
        FinishTest();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.verifyHeyButtonIsDisplay();
        chatBotPage.clickOnHey();
        chatBotPage.verifyAwesomeButtonIsDisplay();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyTestFedBackEmojiIsDisplay();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.verifyOverAllMarksWidgetCellIsDisplay();
        overallMarksWidgetPage.verifyCompareSliderIsDisplay();
        overallMarksWidgetPage.clickOnCompareSlider();
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for less than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for more than 18 minutes) and attempt more than 10% of questions." +
            "54. The compare button should show the 5 unique toppers marks. Show me more button should be active\n" +
            "55.On Clicking show me more, a message should come \"Guess what.....and a button Sure Let's see " +
            "56. On clicking Sure Let's see True score Should come and He can compare with other students. A button \"let's move on\" should appear\n" +
            "57. On Clicking \"Let's move on\" Text - Do you know in which subject.... and a button Physics, Chemistry and Mathematics should come (For JEE Main)" +
            "58. On Clicking any subject, Subject wise Marks should come and a button Thanks Let's move ahead should come" +
            "59. On Clicking Thanks Let's move ahead. Message - Did you know..... and a button Yes and No.\n" +
            "60. On Clicking Yes - Section wise time should come with two buttons - Yes and Nope let's move ahead." +
            "61. On Clicking Yes, PROVIDE DETAILS OF College Predictor should come. Where Any state and any college with any branch and category should be inserted\n" +
            "62. Click Save on College Predictor. Suggested College comes and a button comes " +
            "63. OK Let's move ahead will come." +
            "64. On Clicking Let's move ahead, Accuracy Vs Difficulty. and Nice Button should come.\n" +
            "65. On Clicking Nice Button Marks per Interval should come and let's Proceed button should come\n" +
            "66. On Clicking let's Proceed we get text - Did you find... and two buttons Yes It was really cool, and No not really.\n" +
            "67. On Clicking No not really Overall performance will get opened.\n" +
            "EG_54\n" +
            "EG_55\n" +
            "EG_56\n" +
            "EG_57\n" +
            "EG_58\n" +
            "EG_59\n" +
            "EG_60\n" +
            "EG_61\n" +
            "EG_62\n" +
            "EG_63\n" +
            "EG_64\n" +
            "EG_65\n" +
            "EG_66\n" +
            "EG_67\n")

    public void verifyEmbibeGuideForAttemptQuestionMoreThen10Percentage2() {
        FinishTest();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.clickOnCompareSlider();
        overallMarksWidgetPage.verifyUserMarksCompareWithMaximum5Ranker();
        overallMarksWidgetPage.verifyRankerNameAndMarksUnique();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifyTrueScoreMessage();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        trueScorePage.clickOnTrueScoreCompareSlider();
        trueScorePage.verifyTrueScoreMarksCompared();
        chatBotPage.verifyLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnLetsMoveOn();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        sectionsWiseMarks.verifySectionWiseMarksIsDisplay();
        chatBotPage.verifyThanksLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickThanksLetsMoveAhead();
        chatBotPage.verifyTimeManagementMessage();
        chatBotPage.verifyYesAndNoButtonIsDisplay();
        chatBotPage.clickOnYesButton();
        sectionWiseTime.verifySectionWiseTimeIsDisplay();
        chatBotPage.verifyYesAndNopeLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickOnYesButtonAfterSectionWiseTime();
        collagePredictor.verifyCollagePredictorIsDisplay();
        collagePredictor.selectDreamCollage();
        collagePredictor.selectBranch();
        collagePredictor.selectState();
        collagePredictor.selectCategory();
        collagePredictor.clickOnSaveButton();
        collagePredictor.verifyPredictedCollageWidgetDisplay();
        collagePredictor.verifyPredictedCollageListDisplay();
        collagePredictor.verifyShowMoreButtonIsDisplay();
        chatBotPage.verifyAwesomeLetsMoveAheadIsDisplay();
        chatBotPage.clickAwesomeLetsMoveAhead();
        accuracyVsDifficultyWidget.verifyAccuracyWidgetIsDisplay();
        chatBotPage.verifyNiceButtonIsDisplay();
        chatBotPage.clickOnNiceButton();
        marksPerTimeInterval.verifyMarksPerTimeWidgetIsDisplay();
        chatBotPage.verifyLetsProcessedButtonIsDisplay();
        chatBotPage.clickOnLetsProcessedButton();
        chatBotPage.verifyLetsProcessedMessage();
        chatBotPage.verifyNoReallyAndYesItWasCoolButtonIsDisplay();
        chatBotPage.clickOnNoReallyButton();


    }


}
