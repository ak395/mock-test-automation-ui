package mockTest.chatboat;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_PERFECT_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class ChatBotForAttemptQ_AndTimeSpendMoreThen10Percentage extends TestBase {
    public MockTestPlanner mockTestPlanner;

    public void finishTest() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) - 4, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.showMeFeedbackButtonIsDisplayed();
        examSummaryPage.clickOnShowMeFeedBackButton();

    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for more than 18 minutes) and attempt more than 10% of questions." +
            "21. Submit test button should take to Show me feedback button\n" +
            "22. Show me Feedback button should take me to Embibe Guide on start chatting Button\n" +
            "23. Start chatting button should take to Hey Button\n" +
            "24. Hey Button should take me to Awesome Looking forward to it." +
            "25. Awesome Looking forward to it should take me to 3 Face icons." +
            "26. All the three faces on getting clicked should take me to Overall marks where he can click on compare\n" +
            "EG_21\n" +
            "EG_22\n" +
            "EG_23\n" +
            "EG_24\n" +
            "EG_25\n" +
            "EG_26")

    public void verifyEmbibeGuideForAttemptQuestionMoreThen10Percentage() {
        finishTest();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.verifyHeyButtonIsDisplay();
        chatBotPage.clickOnHey();
        chatBotPage.verifyAwesomeButtonIsDisplay();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyTestFedBackEmojiIsDisplay();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.verifyOverAllMarksWidgetCellIsDisplay();
        overallMarksWidgetPage.verifyCompareSliderIsDisplay();
        overallMarksWidgetPage.clickOnCompareSlider();
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for less than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for less than 18 minutes) and attempt more than 10% of questions." +
            "28.On Clicking show me more, a message should come \"Guess what.....and a button Sure Let's see " +
            "27. The compare button should show the 5 unique toppers marks. Show me more button should be active\n" +
            "29. On clicking Sure Let's see True score Should come and He can compare with other students. A button \"let's move on\" should appear\n" +
            "30. On Clicking \"Let's move on\" Text - Do you know in which subject.... and a button Physics, Chemistry and Mathematics should come (For JEE Main)" +
            "31. On Clicking any subject, Subject wise Marks should come and a button Thanks Let's move ahead should come" +
            "32. On Clicking Thanks Let's move ahead. Message - Did you know..... and a button Yes and No.\n" +
            "33. On Clicking No - Overall Performance should get opened" +
            "EG_27\n" +
            "EG_28\n" +
            "EG_29\n" +
            "EG_30\n" +
            "EG_31\n" +
            "EG_32\n" +
            "EG_33\n")

    public void verifyEmbibeGuideForAttemptQuestionMoreThen10Percentage2() {
        finishTest();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.clickOnCompareSlider();
        overallMarksWidgetPage.verifyUserMarksCompareWithMaximum5Ranker();
        overallMarksWidgetPage.verifyRankerNameAndMarksUnique();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifyTrueScoreMessage();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        trueScorePage.clickOnTrueScoreCompareSlider();
        trueScorePage.verifyTrueScoreMarksCompared();
        chatBotPage.verifyLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnLetsMoveOn();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        sectionsWiseMarks.verifySectionWiseMarksIsDisplay();
        chatBotPage.verifyThanksLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickThanksLetsMoveAhead();
        chatBotPage.verifyTimeManagementMessage();
        chatBotPage.verifyYesAndNoButtonIsDisplay();
        chatBotPage.clickOnNoButton();
        //chatBotPage.verifyOverAllPerformancePageIsDisplay();


    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for more than 18 minutes) and attempt more than 10% of questions." +
            "34. Submit test button should take to Show me feedback button\n" +
            "35. Show me Feedback button should take me to Embibe Guide on start chatting Button\n" +
            "36. Start chatting button should take to Hey Button\n" +
            "37. Hey Button should take me to Awesome Looking forward to it." +
            "38. Awesome Looking forward to it should take me to 3 Face icons." +
            "39. All the three faces on getting clicked should take me to Overall marks where he can click on compare\n" +
            "EG_34\n" +
            "EG_35\n" +
            "EG_36\n" +
            "EG_37\n" +
            "EG_38\n" +
            "EG_39")

    public void verifyEmbibeGuideForAttemptQuestionMoreThen10Percentage3() {
        finishTest();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.verifyHeyButtonIsDisplay();
        chatBotPage.clickOnHey();
        chatBotPage.verifyAwesomeButtonIsDisplay();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyTestFedBackEmojiIsDisplay();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.verifyOverAllMarksWidgetCellIsDisplay();
        overallMarksWidgetPage.verifyCompareSliderIsDisplay();
        overallMarksWidgetPage.clickOnCompareSlider();
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for less than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for less than 18 minutes) and attempt more than 10% of questions." +
            "40.On Clicking show me more, a message should come \"Guess what.....and a button Sure Let's see " +
            "41. The compare button should show the 5 unique toppers marks. Show me more button should be active\n" +
            "42. On clicking Sure Let's see True score Should come and He can compare with other students. A button \"let's move on\" should appear\n" +
            "43. On Clicking \"Let's move on\" Text - Do you know in which subject.... and a button Physics, Chemistry and Mathematics should come (For JEE Main)" +
            "44. On Clicking any subject, Subject wise Marks should come and a button Thanks Let's move ahead should come" +
            "45. On Clicking Thanks Let's move ahead. Message - Did you know..... and a button Yes and No.\n" +
            "46. On Clicking Yes - Section wise time should come with two buttons - Yes and Nope let's move ahead." +
            "EG_40\n" +
            "EG_41\n" +
            "EG_42\n" +
            "EG_43\n" +
            "EG_44\n" +
            "EG_45\n" +
            "EG_46\n")

    public void verifyEmbibeGuideForAttemptQuestionMoreThen10Percentage4() {
        finishTest();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.clickOnCompareSlider();
        overallMarksWidgetPage.verifyUserMarksCompareWithMaximum5Ranker();
        overallMarksWidgetPage.verifyRankerNameAndMarksUnique();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifyTrueScoreMessage();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        trueScorePage.clickOnTrueScoreCompareSlider();
        trueScorePage.verifyTrueScoreMarksCompared();
        chatBotPage.verifyLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnLetsMoveOn();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        sectionsWiseMarks.verifySectionWiseMarksIsDisplay();
        chatBotPage.verifyThanksLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickThanksLetsMoveAhead();
        chatBotPage.verifyTimeManagementMessage();
        chatBotPage.verifyYesAndNoButtonIsDisplay();
        chatBotPage.clickOnYesButton();
        sectionWiseTime.verifySectionWiseTimeIsDisplay();
        chatBotPage.verifyYesAndNopeLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickOnNopeLetsMoveAhead();
        //overall performance page is not display after click on NopLetMove Ahead button


    }
}
