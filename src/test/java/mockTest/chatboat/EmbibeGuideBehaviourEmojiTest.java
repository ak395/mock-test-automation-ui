package mockTest.chatboat;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_PERFECT_ATTEMPT;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class EmbibeGuideBehaviourEmojiTest extends TestBase {
    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Verify once user is sign in, the embibe guide must show the emoji according to user performed\n" +
            "case 1 : If user was happy."
            + "ChatBot_10_1")
    public void verifyEmbibeGuideInControlBehaviourEmojiAccordingToUserPerformance() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyInControlBehaviourEmoji();


    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Verify once user is sign in, the embibe guide must show the emoji according to user performed.\n" +
            "case 2 : If user was stressed."
            + "ChatBot_10_2")
    public void verifyEmbibeGuideSlowBehaviourEmojiAccordingToUserPerformance() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifySlowBehaviourEmoji();


    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Verify once user is sign in, the embibe guide must show the emoji according to user performed.\n" +
            "case 2 : If user was stressed."
            + "ChatBot_10_3\n" +
            "ChatBot_11")
    public void verifyEmbibeGuideStrugglingBehaviourEmojiAccordingToUserPerformance() {
        MockTestPlanner mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(2, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyStrugglingBehaviourEmoji();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifyTrueScoreMessage();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();


    }
}
