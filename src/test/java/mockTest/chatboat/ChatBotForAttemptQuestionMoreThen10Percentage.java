package mockTest.chatboat;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class ChatBotForAttemptQuestionMoreThen10Percentage extends TestBase {
    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for less than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for less than 18 minutes) and attempt more than 10% of questions." +
            "1. Submit test button should take to Show me feedback button\n" +
            "2. Show me Feedback button should take me to Embibe Guide on start chatting Button\n" +
            "3. Start chatting button should take to Hey Button\n" +
            "4. Hey Button should take me to Awesome Looking forward to it." +
            "5. Awesome Looking forward to it should take me to 3 Face icons." +
            "6. All the three faces on getting clicked should take me to Overall marks where he can click on compare\n" +
            "EG_1\n" +
            "EG_2\n" +
            "EG_3\n" +
            "EG_4\n" +
            "EG_5\n" +
            "EG_6")

    public void verifyEmbibeGuideForAttemptQuestionMoreThen10Percentage() {
        MockTestPlanner mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 8, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.showMeFeedbackButtonIsDisplayed();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.verifyHeyButtonIsDisplay();
        chatBotPage.clickOnHey();
        chatBotPage.verifyAwesomeButtonIsDisplay();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyTestFedBackEmojiIsDisplay();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.verifyOverAllMarksWidgetCellIsDisplay();
        overallMarksWidgetPage.verifyCompareSliderIsDisplay();
        overallMarksWidgetPage.clickOnCompareSlider();
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT, Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for less than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for less than 18 minutes) and attempt more than 10% of questions." +
            "8.On Clicking show me more, a message should come \"Oh No Looks like you ......\" and a button Ok Got it\n" +
            "7. The compare button should show the 5 unique toppers marks. Show me more button should be active\n" +
            "9. On Clicking Ok Got it message should come \"From next time ......\". and View Overall performance button\n" +
            "10 On Clicking View Overall Performance button Overall performance Tab should get opened." +
            "5. Awesome Looking forward to it should take me to 3 Face icons." +
            "6. All the three faces on getting clicked should take me to Overall marks where he can click on compare\n" +
            "EG_7\n" +
            "EG_8\n" +
            "EG_9\n" +
            "EG_10")

    public void verifyEmbibeGuideForAttemptQuestionMoreThen10Percentage2() {
        MockTestPlanner mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 8, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.showMeFeedbackButtonIsDisplayed();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.clickOnCompareSlider();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        overallMarksWidgetPage.verifyUserMarksCompareWithMaximum5Ranker();
        overallMarksWidgetPage.verifyRankerNameAndMarksUnique();
        chatBotPage.verifyShowMeMoreMessageIsDisplay();
        chatBotPage.verifyOkGotItIsDisplay();
        chatBotPage.clickOnOkGotItButton();
        chatBotPage.verifyOkGotMessageIsDisplay();
        chatBotPage.verifyOverAllPerformanceButtonIsDisplay();
        chatBotPage.clickOverAllPerformanceButton();
        chatBotPage.verifyOverAllPerformancePageIsDisplay();


    }


}
