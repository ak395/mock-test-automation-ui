package mockTest.chatboat;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import java.util.List;

import static constants.Badge.BADGE_PERFECT_ATTEMPT;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class ChatBotTrueScoreTestCase extends TestBase {
    private MockTestPlanner mockTestPlanner;

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "All the three faces on getting clicked should take me to Overall marks where.\n" +
            " this value should match with the scoring." +
            "ChatBot_12")
    public void verifyEmbibeGuideOverAllScoreMarks() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyInControlBehaviourEmoji();
        overallMarksWidgetPage.verifyOverAllMarks();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "Verify if user moves forward by \"lets move on\" for mini /full test Do you know in which subject you scored the highest ? subject should display" +
            "ChatBot_13")
    public void verifySubjectIsDisplayInChatBotMinTest() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(0, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        chatBotPage.verifyLeftSideSubjectMessageAfterClickOnSubject();


    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "Verify user select the subject /section choose value correct." +
            "ChatBot_14")
    public void verifyEmbibeGuideSubjectWiseReportForMinTest() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1,5,CHOICE_CORRECT,BADGE_PERFECT_ATTEMPT,getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(1,4,CHOICE_CORRECT,BADGE_TOO_FAST_CORRECT,getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        List<String> subject = questionsHomePage.getSubjectName();
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        sectionsWiseMarks.verifySectionWiseMarksIsDisplay();
        sectionsWiseMarks.verifySubjectDisplayInSectionsWiseMarks(subject);
        sectionsWiseMarks.verifySubjectMarksInSectionsWiseMarksForAllQuestionCorrect();
        subjectWiseAttempts.verifySubjectWiseAttemptsWrapper();
        subjectWiseAttempts.verifySubjectDisplayInSubjectWiseAttempts(subject);


    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "If user clicks on \"thanks ! lets move ahead\" System message should appear related to time spent across the subject" +
            "ChatBot_15")
    public void verifyTimeSpendInTestRelatedMessageInChatBot() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(0, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        chatBotPage.clickThanksLetsMoveAhead();
        chatBotPage.verifyTimeManagementMessage();


    }

}
