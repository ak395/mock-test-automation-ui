package mockTest.chatboat;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_PERFECT_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;

public class ChatBotForAttemptQ_AndTimeSpendMoreThen10Percentage_3 extends TestBase {
    public MockTestPlanner mockTestPlanner;

    public void FinishTest() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) - 4, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.showMeFeedbackButtonIsDisplayed();
        examSummaryPage.clickOnShowMeFeedBackButton();

    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for more than 18 minutes) and attempt more than 10% of questions." +
            "EG_68\n" +
            "EG_69\n" +
            "EG_70\n" +
            "EG_71\n" +
            "EG_72\n" +
            "EG_73\n" +
            "EG_74\n" +
            "EG_75\n" +
            "EG_76\n" +
            "EG_77\n" +
            "EG_78\n" +
            "EG_79\n" +
            "EG_80\n" +
            "EG_81\n" +
            "EG_82\n" +
            "EG_83\n" +
            "EG_84\n" +
            "EG_85\n" +
            "EG_86\n" +
            "EG_87\n" +
            "EG_88\n" +
            "EG_89")
    public void verifyChatBotFunctionalityAfterClickOnNoButtonFollowedByCollagePredictor_YesButton() {
        FinishTest();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.clickOnCompareSlider();
        overallMarksWidgetPage.verifyUserMarksCompareWithMaximum5Ranker();
        overallMarksWidgetPage.verifyRankerNameAndMarksUnique();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifyTrueScoreMessage();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        trueScorePage.clickOnTrueScoreCompareSlider();
        trueScorePage.verifyTrueScoreMarksCompared();
        chatBotPage.verifyLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnLetsMoveOn();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        sectionsWiseMarks.verifySectionWiseMarksIsDisplay();
        chatBotPage.verifyThanksLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickThanksLetsMoveAhead();
        chatBotPage.verifyTimeManagementMessage();
        chatBotPage.verifyYesAndNoButtonIsDisplay();
        chatBotPage.clickOnYesButton();
        sectionWiseTime.verifySectionWiseTimeIsDisplay();
        chatBotPage.verifyYesAndNopeLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickOnYesButtonAfterSectionWiseTime();
        collagePredictor.verifyCollagePredictorIsDisplay();
        collagePredictor.selectDreamCollage();
        collagePredictor.selectBranch();
        collagePredictor.selectState();
        collagePredictor.selectCategory();
        collagePredictor.clickOnSaveButton();
        collagePredictor.verifyPredictedCollageWidgetDisplay();
        collagePredictor.verifyPredictedCollageListDisplay();
        collagePredictor.verifyShowMoreButtonIsDisplay();
        chatBotPage.verifyAwesomeLetsMoveAheadIsDisplay();
        chatBotPage.clickAwesomeLetsMoveAhead();
        accuracyVsDifficultyWidget.verifyAccuracyWidgetIsDisplay();
        chatBotPage.verifyNiceButtonIsDisplay();
        chatBotPage.clickOnNiceButton();
        marksPerTimeInterval.verifyMarksPerTimeWidgetIsDisplay();
        chatBotPage.verifyLetsProcessedButtonIsDisplay();
        chatBotPage.clickOnLetsProcessedButton();
        chatBotPage.verifyLetsProcessedMessage();
        chatBotPage.verifyNoReallyAndYesItWasCoolButtonIsDisplay();
        chatBotPage.clickOnYesItWasCoolProcessedButton();
        chatBotPage.verifyOkLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnOkLetsMoveOnButton();
        subjectSwapsPage.verifySubjectSwapsWidgetIsDisplay();
        chatBotPage.verifyYesLetsSeeMoreAndNotAnyMoreButtonIsDisplay();
        chatBotPage.clickOnNotAnyMoreButton();
        chatBotPage.verifyOverAllPerformanceButtonIsDisplay();
        chatBotPage.clickOverAllPerformanceButton();
        chatBotPage.verifyOverAllPerformancePageIsDisplay();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for more than 18 minutes) and attempt more than 10% of questions." +
            "EG_90\n" +
            "EG_91\n" +
            "EG_92\n" +
            "EG_93\n" +
            "EG_94\n" +
            "EG_95\n" +
            "EG_96\n" +
            "EG_97\n" +
            "EG_98\n" +
            "EG_99\n" +
            "EG_100\n" +
            "EG_101\n" +
            "EG_102\n" +
            "EG_103\n" +
            "EG_104\n" +
            "EG_105\n" +
            "EG_106\n" +
            "EG_107\n" +
            "EG_108\n" +
            "EG_109\n" +
            "EG_110\n" +
            "EG_111\n" +
            "EG_112\n" +
            "EG_113\n" +
            "EG_114")
    public void verifyChatBotFunctionalityAfterClickOnYesButtonFollowedByCollagePredictor_YesButton() {
        FinishTest();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.clickOnCompareSlider();
        overallMarksWidgetPage.verifyUserMarksCompareWithMaximum5Ranker();
        overallMarksWidgetPage.verifyRankerNameAndMarksUnique();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifyTrueScoreMessage();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        trueScorePage.clickOnTrueScoreCompareSlider();
        trueScorePage.verifyTrueScoreMarksCompared();
        chatBotPage.verifyLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnLetsMoveOn();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        sectionsWiseMarks.verifySectionWiseMarksIsDisplay();
        chatBotPage.verifyThanksLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickThanksLetsMoveAhead();
        chatBotPage.verifyTimeManagementMessage();
        chatBotPage.verifyYesAndNoButtonIsDisplay();
        chatBotPage.clickOnYesButton();
        sectionWiseTime.verifySectionWiseTimeIsDisplay();
        chatBotPage.verifyYesAndNopeLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickOnYesButtonAfterSectionWiseTime();
        collagePredictor.verifyCollagePredictorIsDisplay();
        collagePredictor.selectDreamCollage();
        collagePredictor.selectBranch();
        collagePredictor.selectState();
        collagePredictor.selectCategory();
        collagePredictor.clickOnSaveButton();
        collagePredictor.verifyPredictedCollageWidgetDisplay();
        collagePredictor.verifyPredictedCollageListDisplay();
        collagePredictor.verifyShowMoreButtonIsDisplay();
        chatBotPage.verifyAwesomeLetsMoveAheadIsDisplay();
        chatBotPage.clickAwesomeLetsMoveAhead();
        accuracyVsDifficultyWidget.verifyAccuracyWidgetIsDisplay();
        chatBotPage.verifyNiceButtonIsDisplay();
        chatBotPage.clickOnNiceButton();
        marksPerTimeInterval.verifyMarksPerTimeWidgetIsDisplay();
        chatBotPage.verifyLetsProcessedButtonIsDisplay();
        chatBotPage.clickOnLetsProcessedButton();
        chatBotPage.verifyLetsProcessedMessage();
        chatBotPage.verifyNoReallyAndYesItWasCoolButtonIsDisplay();
        chatBotPage.clickOnYesItWasCoolProcessedButton();
        chatBotPage.verifyOkLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnOkLetsMoveOnButton();
        subjectSwapsPage.verifySubjectSwapsWidgetIsDisplay();
        chatBotPage.verifyYesLetsSeeMoreAndNotAnyMoreButtonIsDisplay();
        chatBotPage.clickOnYesLetsSeeMoreButton();
        timeUtilizationWidget.verifyTimeUtilizationWidgetIsDisplay();
        chatBotPage.verifyShowMeMoreFollowedBYTimeUtilizationWidgetButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreFollowedBYTimeUtilizationWidget();
        firstLookVsReattempted.verifyTimeUtilizationWidgetIsDisplay();
        chatBotPage.verifyLetsGoFollowedByFirstLookWidgetButtonIsDisplay();
        chatBotPage.clickOnLetsGoFollowedByFirstLookWidget();
        chatBotPage.verifyYesShowMeAndNotReallyButtonIsDisplay();
        chatBotPage.clickOnNotReallyFollowedByFirstLookWidgetAnyMoreButton();
        chatBotPage.verifyOverAllPerformanceButtonIsDisplay();
        chatBotPage.clickOverAllPerformanceButton();
        chatBotPage.verifyOverAllPerformancePageIsDisplay();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "then he should take the test for more than 18 minutes) and attempt more than 10% of questions." +
            "EG_115\n" +
            "EG_116\n" +
            "EG_117\n" +
            "EG_118\n" +
            "EG_119\n" +
            "EG_120\n" +
            "EG_121\n" +
            "EG_122\n" +
            "EG_123\n" +
            "EG_124\n" +
            "EG_125\n" +
            "EG_126\n" +
            "EG_127\n" +
            "EG_128\n" +
            "EG_129\n" +
            "EG_130\n" +
            "EG_131\n" +
            "EG_132\n" +
            "EG_133\n" +
            "EG_134\n" +
            "EG_135\n" +
            "EG_136\n" +
            "EG_137\n" +
            "EG_138\n" +
            "EG_139\n" +
            "EG_140\n" +
            "EG_141")
    public void verifyChatBotFunctionalityAfterClickOnYesButtonFollowedByFirstLookWidget() {
        FinishTest();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.clickOnCompareSlider();
        overallMarksWidgetPage.verifyUserMarksCompareWithMaximum5Ranker();
        overallMarksWidgetPage.verifyRankerNameAndMarksUnique();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifyTrueScoreMessage();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        trueScorePage.clickOnTrueScoreCompareSlider();
        trueScorePage.verifyTrueScoreMarksCompared();
        chatBotPage.verifyLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnLetsMoveOn();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        sectionsWiseMarks.verifySectionWiseMarksIsDisplay();
        chatBotPage.verifyThanksLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickThanksLetsMoveAhead();
        chatBotPage.verifyTimeManagementMessage();
        chatBotPage.verifyYesAndNoButtonIsDisplay();
        chatBotPage.clickOnYesButton();
        sectionWiseTime.verifySectionWiseTimeIsDisplay();
        chatBotPage.verifyYesAndNopeLetsMoveAheadButtonIsDisplay();
        chatBotPage.clickOnYesButtonAfterSectionWiseTime();
        collagePredictor.verifyCollagePredictorIsDisplay();
        collagePredictor.selectDreamCollage();
        collagePredictor.selectBranch();
        collagePredictor.selectState();
        collagePredictor.selectCategory();
        collagePredictor.clickOnSaveButton();
        collagePredictor.verifyPredictedCollageWidgetDisplay();
        collagePredictor.verifyPredictedCollageListDisplay();
        collagePredictor.verifyShowMoreButtonIsDisplay();
        chatBotPage.verifyAwesomeLetsMoveAheadIsDisplay();
        chatBotPage.clickAwesomeLetsMoveAhead();
        accuracyVsDifficultyWidget.verifyAccuracyWidgetIsDisplay();
        chatBotPage.verifyNiceButtonIsDisplay();
        chatBotPage.clickOnNiceButton();
        marksPerTimeInterval.verifyMarksPerTimeWidgetIsDisplay();
        chatBotPage.verifyLetsProcessedButtonIsDisplay();
        chatBotPage.clickOnLetsProcessedButton();
        chatBotPage.verifyLetsProcessedMessage();
        chatBotPage.verifyNoReallyAndYesItWasCoolButtonIsDisplay();
        chatBotPage.clickOnYesItWasCoolProcessedButton();
        chatBotPage.verifyOkLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnOkLetsMoveOnButton();
        subjectSwapsPage.verifySubjectSwapsWidgetIsDisplay();
        chatBotPage.verifyYesLetsSeeMoreAndNotAnyMoreButtonIsDisplay();
        chatBotPage.clickOnYesLetsSeeMoreButton();
        timeUtilizationWidget.verifyTimeUtilizationWidgetIsDisplay();
        chatBotPage.verifyShowMeMoreFollowedBYTimeUtilizationWidgetButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreFollowedBYTimeUtilizationWidget();
        firstLookVsReattempted.verifyTimeUtilizationWidgetIsDisplay();
        chatBotPage.verifyLetsGoFollowedByFirstLookWidgetButtonIsDisplay();
        chatBotPage.clickOnLetsGoFollowedByFirstLookWidget();
        chatBotPage.verifyYesShowMeAndNotReallyButtonIsDisplay();
        chatBotPage.clickOnYesShowMeFollowedByFirstLookWidgetButton();
        chatBotPage.verifyKeepMovingButtonIsDisplay();
        chatBotPage.clickOnKeepMovingButton();
        attemptTypesWidget.verifyAttemptTypeChartsIsDisplay();
        chatBotPage.verifyOverAllPerformanceButtonIsDisplay();
        chatBotPage.clickOverAllPerformanceButton();
        chatBotPage.verifyOverAllPerformancePageIsDisplay();


    }
}
