package mockTest.chatboat;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.*;
import static mockTestFactory.MyChoice.*;

public class verifySubjectWiseAttemptForMiniTest extends TestBase{
    public MockTestPlanner mockTestPlanner;

    public void finishTest(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1,5,CHOICE_CORRECT,BADGE_PERFECT_ATTEMPT,getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(1,3,CHOICE_INCORRECT,BADGE_WASTED_ATTEMPT,getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(1,1,CHOICE_UNATTEMPTED,BADGE_TOO_FAST_CORRECT,getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.showMeFeedbackButtonIsDisplayed();
        examSummaryPage.clickOnShowMeFeedBackButton();

    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "2. True Score should be more than the Total marks if the student has done any Wasted Attempts or incorrect attempts")

    public void verifyTruesScoreMarks(){
        finishTest();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.verifyAwesomeButtonIsDisplay();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyTestFedBackEmojiIsDisplay();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.verifyOverAllMarksWidgetCellIsDisplay();
        overallMarksWidgetPage.verifyCompareSliderIsDisplay();
        overallMarksWidgetPage.clickOnCompareSlider();
        int overallMarsObtainByUser = overallMarksWidgetPage.marksObtainByUser();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        trueScorePage.clickOnTrueScoreCompareSlider();
        trueScorePage.verifyTrueScoreMarksCompared();
        int trueScoreMarksObtainByUser = trueScorePage.marksObtainByUser();
        Assert.assertTrue(trueScoreMarksObtainByUser > overallMarsObtainByUser,"true score marks obtain by user is not greater then overall marks obtain by user");
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT,Categories.TRP_TAKE_A_TEST}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "3. True Score should be more than the Total marks if the student has done any Wasted Attempts or incorrect attempts")

    public void verifySubjectWiseAttempt(){
        finishTest();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.verifyAwesomeButtonIsDisplay();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyTestFedBackEmojiIsDisplay();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.verifyOverAllMarksWidgetCellIsDisplay();
        overallMarksWidgetPage.verifyCompareSliderIsDisplay();
        overallMarksWidgetPage.clickOnCompareSlider();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        chatBotPage.verifyLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnLetsMoveOn();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        chatBotPage.verifyLeftSideSubjectMessageAfterClickOnSubject();
        subjectWiseAttempts.verifySubjectWiseAttemptsWrapper();
        subjectWiseAttempts.verifySubjectWiseQuestionAttempted(mockTestPlanner);

    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST_CHATBOT}, description = "Test to be taken for more than 10% of the time (For Example if the test is of 180 minutes, " +
            "4.Subject-Wise attempts should be correctly calculated. (Correct, Incorrect, Unattempted).")

    public void verifyTimeWidgetFunctionality() {
        finishTest();
        chatBotPage.verifyStartChatButtonIsDisplay();
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.verifyAwesomeButtonIsDisplay();
        chatBotPage.clickOnAwesome();
        chatBotPage.verifyTestFedBackEmojiIsDisplay();
        chatBotPage.clickOnTestFedBackButton();
        overallMarksWidgetPage.verifyOverAllMarksWidgetCellIsDisplay();
        overallMarksWidgetPage.verifyCompareSliderIsDisplay();
        overallMarksWidgetPage.clickOnCompareSlider();
        chatBotPage.verifyShowMeMoreButtonIsDisplay();
        chatBotPage.clickOnShowMeMoreButton();
        chatBotPage.verifySureLetsSeeButtonIsDisplay();
        chatBotPage.clickOnSureLetSeeButton();
        trueScorePage.verifyTrueScoreWidgetDisplay();
        chatBotPage.verifyLetsMoveOnButtonIsDisplay();
        chatBotPage.clickOnLetsMoveOn();
        chatBotPage.verifySubjectIsDisplay();
        chatBotPage.clickOnSubject();
        chatBotPage.verifyLeftSideSubjectMessageAfterClickOnSubject();
        subjectWiseAttempts.verifySubjectWiseAttemptsWrapper();
        subjectWiseAttempts.verifySubjectWiseQuestionAttempted(mockTestPlanner);
        chatBotPage.verifyThanksLetsMoveAheadButtonIsDisplay();


    }

}
