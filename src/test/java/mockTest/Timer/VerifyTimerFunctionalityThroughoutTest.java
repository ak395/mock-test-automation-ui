package mockTest.Timer;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import driver.DriverInitializer;
import entities.MockTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import page.LandingHomePage;
import page.SearchHomePage;
import page.mockTestPage.MockTestHomePage;
import page.mockTestQuestionsPage.QuestionsHomePage;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import java.text.ParseException;

public class VerifyTimerFunctionalityThroughoutTest extends TestBase {
    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify while user taking the Test, timer is appearing and working properly"
            + "TRP_448_1")
    public void verifyTimerAppearingAndWorkingProperlyDuringTest() throws ParseException {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyTimerIsWorkingProperlyDuringTest(5);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify user is able to view the Timer in submit test pop up."
            + "TRP_448_5")
    public void verifyTimerAppearingOnSubmitTestPopUp() throws ParseException {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyTimerIsDisplaySubmitTestPopUp();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify for a test, timer is appearing properly in all screen sizes"
            + "TRP_448_6")
    public void verifyTimerAppearingForAllScreenSize() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyTimerIsDisplayInDifferentScreenSize();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify if user resumes the test after some time, test's timer should also get updated and work properly"
            + "TRP_448_7")
    public void verifyTimerIsWorkingAfterResumingTest() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        String email = landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        String examStartCurrentTime = questionsHomePage.getExamCurrentTime();
        Thread.sleep(2000);
        driver.close();
        Thread.sleep(2000);
        driver = new DriverInitializer("chrome").init();
        landingHomePage = PageFactory.initElements(driver, LandingHomePage.class);
        questionsHomePage = PageFactory.initElements(driver, QuestionsHomePage.class);
        searchHomePage = PageFactory.initElements(driver, SearchHomePage.class);
        mockTestHomePage = PageFactory.initElements(driver, MockTestHomePage.class);
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs(email, "password");
        landingHomePage.verifyUserIsLoggedIn();
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clickResume();
        String timeAfterResumingTest = questionsHomePage.getExamCurrentTime();
        questionsHomePage.verifyTimerIsWorkingProperlyAfterResumingTest(examStartCurrentTime, timeAfterResumingTest);

    }


}
