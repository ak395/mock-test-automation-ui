package mockTest.Timer;

import constants.Goals;
import constants.TestTypes;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class TimerTests extends TestBase {

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TIMER,Categories.TRP_TEST_REGRESSION},
            description = "Verifies that student is able to view to the timer throughout the test" +
            "TRP-439_1\n" +
            "TRP-439_3\n" +
            "TRP-439_6\n" +
            "TRP-439_7\n" +
            "TRP-439_8\n" +
            "TRP-448_2\n" +
            "TRP-448_3\n"
    )
    public void studentUsingDifferentFeaturesInTest() {

        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithIndex(2);
        instructionPage.dismissInstructions();
        questionsHomePage.clickingOnInstructionPopupAppears();
        questionsHomePage.verifyTimerIsRunningWhileReadingInstructions();
        questionsHomePage.verifyTimerIsVisibleWhileReadingQuestionPaper();
    }
}

