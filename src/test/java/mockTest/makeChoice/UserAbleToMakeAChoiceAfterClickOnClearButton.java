package mockTest.makeChoice;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class UserAbleToMakeAChoiceAfterClickOnClearButton extends TestBase {

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify after selecting any answer if the user clicks on clear button followed by save button, the user is navigating to the next question"
            + "TRP_438_4")
    public void userAbleToNavigateNextQuestionWithoutSelectionAnyOption() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyUserAbleToNavigateNextQuestionAfterClickOnClearButtonFollowedByNextButton();

    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify after selecting any answer if user clicks on clear button followed by Review later option, user is navigating to next question"
            + "TRP_438_5")
    public void userAbleToNavigateNextQuestionWithoutSelectionAnyOptionFollowedByReviewLaterButton() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.verifyUserAbleToNavigateNextQuestionAfterClickOnClearButtonFollowedByReviewLaterAndNextButton();
    }


}
