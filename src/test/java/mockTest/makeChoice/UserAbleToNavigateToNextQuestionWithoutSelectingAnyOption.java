package mockTest.makeChoice;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_PERFECT_ATTEMPT;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_ANSWERED_REVIEW_LATER;
import static mockTestFactory.MyChoice.CHOICE_REVIEW_LATER;

public class UserAbleToNavigateToNextQuestionWithoutSelectingAnyOption extends TestBase {
    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.setTestDataService(new TestDataService(App.WEB));
        mockTestPlanner.setTestId(mockTestPlanner.getTestDataService().getMockTestTab().getExam().getTestId());
        mockTestPlanner.addMockTestData(new MockTestData(1, 2, CHOICE_REVIEW_LATER, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(3, 4, CHOICE_ANSWERED_REVIEW_LATER, BADGE_TOO_FAST_CORRECT));


    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify without selecting any option, if user selects Review Later option, user is navigating to next question"
            + "TRP_438_3")
    public void userAbleToNavigateNextQuestionWithoutSelectionAnyOption() {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_REVIEW_LATER, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_ANSWERED_REVIEW_LATER, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        testFeedBackHeader.verifyBadgeSummaryForReviewLater(mockTestPlanner);
    }

}
