package mockTest.makeChoice;

import builders.MockTestBuilder;
import constants.Exams;
import constants.Goals;
import constants.TestTypes;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_PERFECT_ATTEMPT;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_UNATTEMPTED;

public class UserAbleToNavigateToNextQuestionWithoutSelectionAnyOption extends TestBase {
    private MockTestPlanner mockTestPlanner;
    @Test(groups={Categories.TRP_TAKE_A_TEST},description = "Verify without selecting any option, if user selects Review Later option, user is navigating to next question"
            +"TRP_438_2")
    public void userAbleToNavigateNextQuestionWithoutSelectionAnyOption()
    {
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_UNATTEMPTED, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        testFeedBackHeader.verifyBadgeSummaryForNotAnsweredQuestions(mockTestPlanner);
    }

}
