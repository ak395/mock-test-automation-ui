package mockTest.testListing;

import constants.Goals;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static constants.Badge.BADGE_WASTED_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;


public class VerifyTestListingTestCases extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 15, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(16, 30, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
//        mockTestPlanner.addMockTestData(new MockTestData(3,4,CHOICE_CORRECT,BADGE_TOO_FAST_CORRECT));
//        mockTestPlanner.addMockTestData(new MockTestData(9,11,CHOICE_CORRECT,BADGE_TOO_FAST_CORRECT));
    }
    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TEST_REGRESSION},
            description = "Verify In Progress Test is shown in Test Listing Page" +
                    "Test_Listing_24" +
                    "Test_Listing_25" +
                    "Test_Listing_28" +
                    "Test_Listing_29" +
                    "Test_Listing_34"
    )
    public void verifyInProgressTestStatus() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        String testName = questionsHomePage.getTestName();
        mockTestHomePage.verifyTheDetailsOfInProgressSection(testName);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST,Categories.TRP_TEST_REGRESSION},
            description = "Verifies the functionality when user click on more-info link on Test Listing Page" +
                    "Test_Listing_35" +
                    "Test_Listing_36" +
                    "Test_Listing_37" +
                    "Test_Listing_38"
    )
    public void verifyFeaturesOfMoreInfoLink() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        testListContainerSection.testCardDetailsIsDisplayed();
        mockTestHomePage.verifyMoreInfoFunctionality();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST,Categories.TRP_TEST_REGRESSION},
            description = "Verifies the attempt count should be same on feedback page and test listing page" +
                    "Test_Listing_46" +
                    "Test_Listing_47" +
                    "Test_Listing_48"
    )
    public void verifyTestDataIsSameOnTestListingAndFeedBackPage() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        examSummaryPage.clickOnOverallPerformanceSection();
        mockTestHomePage.verifyTestDataIsSameOnFeedbackAndTestListingPage();

    }



}

