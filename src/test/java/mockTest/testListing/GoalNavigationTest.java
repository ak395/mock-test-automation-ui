package mockTest.testListing;

import constants.Goals;
import constants.TestTypes;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class GoalNavigationTest extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
                    "fo SSC")
    public void verifyGoalNavigationForSSC() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_SSC);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_SSC);
        mockTestHomePage.verifyTestCardsNumber();
        mockTestHomePage.verifyShowMoreLink();
    }

//    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario"+
//                   "for Bank")
//    public void verifyGoalNavigationForBank() throws ParseException {
//        navigateTo(Properties.baseUrl);
//        landingHomePage.clickOnStartNowButton();
//        landingHomePage.clickOnLoginButton();
//        landingHomePage.clickOnRegisterHereLink();
//        landingHomePage.registerANewUser(Goals.GOAL_BANK);
//        editCookies();
//        searchHomePage.clickOnTakeATestButton();
//        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_BANK);
//        //mockTestHomePage.verifyTestCardsNumber();
//        //mockTestHomePage.verifyShowMoreLink();
//        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
//        mockTestHomePage.startTestWithDifferentTab();
//        instructionPage.dismissInstructions();
//        String testName = questionsHomePage.getTestName().substring(0, 25);
//        mockTestHomePage.verifyUserNavigateToTestListing();
//        navigateToRefresh();
//        mockTestHomePage.verifyInprogressTest(testName);
//        mockTestHomePage.verifyTimerWorkingForInProgressTest(5, 0);
//    }

//    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify on changing goal, " +
//            "user is navigating to respective test listing page." +
//            "TestListing_7" +
//            "TestListing_9")
//    public void verifyGoalNavigationForEighthFoundation() {
//        navigateTo(Properties.baseUrl);
//        landingHomePage.clickOnStartNowButton();
//        landingHomePage.clickOnLoginButton();
//        landingHomePage.clickOnRegisterHereLink();
//        landingHomePage.registerANewUser(Goals.GOAL_8th_FOUNDATION);
//        editCookies();
//        searchHomePage.clickOnTakeATestButton();
//        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_8th_FOUNDATION);
//        mockTestHomePage.verifyTestCardsNumber();
//        mockTestHomePage.verifyShowMoreLink();
//    }

//    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify on changing goal, " +
//            "user is navigating to respective test listing page." +
//            "TestListing_7" +
//            "TestListing_9")
//    public void verifyGoalNavigationForNinethFoundation() {
//        navigateTo(Properties.baseUrl);
//        landingHomePage.clickOnStartNowButton();
//        landingHomePage.clickOnLoginButton();
//        landingHomePage.clickOnRegisterHereLink();
//        landingHomePage.registerANewUser(Goals.GOAL_9th_FOUNDATION);
//        editCookies();
//        searchHomePage.clickOnTakeATestButton();
//        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_9th_FOUNDATION);
//        mockTestHomePage.verifyTestCardsNumber();
//        mockTestHomePage.verifyShowMoreLink();
//    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
                   "for Railway")
    public void verifyGoalNavigationForRailway() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_Railway);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_Railway);
        mockTestHomePage.verifyTestCardsNumber();
        mockTestHomePage.verifyShowMoreLink();
    }

//    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario"+
//                   "for Lectureship")
//    public void verifyGoalNavigationForLectureship() throws Exception{
//        navigateTo(Properties.baseUrl);
//        landingHomePage.clickOnStartNowButton();
//        landingHomePage.clickOnLoginButton();
//        landingHomePage.clickOnRegisterHereLink();
//        landingHomePage.registerANewUser(Goals.GOAL_LECTURESHIP);
//        editCookies();
//        searchHomePage.clickOnTakeATestButton();
//        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_LECTURESHIP);
//        mockTestHomePage.verifyTestCardsNumber();
//        mockTestHomePage.verifyShowMoreLink();
//        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
//        mockTestHomePage.startTestWithDifferentTab();
//        instructionPage.dismissInstructions();
//        String testName = questionsHomePage.getTestName().substring(0, 25);
//        mockTestHomePage.verifyUserNavigateToTestListing();
//        navigateToRefresh();
//        mockTestHomePage.verifyInprogressTest(testName);
//        mockTestHomePage.verifyTimerWorkingForInProgressTest(5, 0);
//    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario"+
                   "for Management")
    public void verifyGoalNavigationForManagement() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MANAGEMENT);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_MANAGEMENT);
        mockTestHomePage.verifyTestCardsNumber();
        mockTestHomePage.verifyShowMoreLink();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
                   "for Insurance")
    public void verifyGoalNavigationForInsurance() throws Exception{
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_INSURANCE);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_INSURANCE);
        mockTestHomePage.verifyTestCardsNumber();
        mockTestHomePage.verifyShowMoreLink();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        String testName = questionsHomePage.getTestName();
        mockTestHomePage.verifyUserNavigateToTestListing();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        navigateToRefresh();
        mockTestHomePage.verifyInprogressTest(testName);
        mockTestHomePage.verifyTimerWorkingForInProgressTest(5, 0);
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
                   "Defence")
    public void verifyGoalNavigationForDefence() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_DEFENCE);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_DEFENCE);
        mockTestHomePage.verifyTestCardsNumber();
        mockTestHomePage.verifyShowMoreLink();
    }

//    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING, Categories.TRP_Failed_Test}, description = "Goal navigation and End to end scenario" +
//                   "for CBSE 12")
//    public void verifyGoalNavigationForCBSETwelth() {
//        navigateTo(Properties.baseUrl);
//        landingHomePage.clickOnStartNowButton();
//        landingHomePage.clickOnLoginButton();
//        landingHomePage.clickOnRegisterHereLink();
//        landingHomePage.registerANewUser(Goals.GOAL_CBSE_12);
//        editCookies();
//        searchHomePage.clickOnTakeATestButton();
//        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_CBSE_12);
//        mockTestHomePage.verifyTestCardsNumber();
//        mockTestHomePage.verifyShowMoreLink();
//    }
//
//    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING, Categories.TRP_Failed_Test}, description = "Goal navigation and End to end scenario"+
//                   "for CBSE")
//    public void verifyGoalNavigationForCBSE() {
//        navigateTo(Properties.baseUrl);
//        landingHomePage.clickOnStartNowButton();
//        landingHomePage.clickOnLoginButton();
//        landingHomePage.clickOnRegisterHereLink();
//        landingHomePage.registerANewUser(Goals.GOAL_CBSE);
//        editCookies();
//        searchHomePage.clickOnTakeATestButton();
//        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_CBSE);
//        mockTestHomePage.verifyTestCardsNumber();
//        mockTestHomePage.verifyShowMoreLink();
//    }


    @Test(description = "Goal navigation and End to end scenario"+
                   "for 10th foundation")
    public void verifyGoalNavigationForTenthFoundation()throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_10th_FOUNDATION);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_10th_FOUNDATION);
        mockTestHomePage.verifyTestCardsNumber();
        mockTestHomePage.verifyShowMoreLink();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        String testName = questionsHomePage.getTestName();
        mockTestHomePage.verifyUserNavigateToTestListing();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        navigateToRefresh();
        mockTestHomePage.verifyInprogressTest(testName);
        mockTestHomePage.verifyTimerWorkingForInProgressTest(5, 0);
    }
}
