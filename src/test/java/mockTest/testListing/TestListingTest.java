package mockTest.testListing;

import builders.MockTestBuilder;
import constants.*;
import entities.MockTest;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import java.text.ParseException;
import java.util.ArrayList;

public class TestListingTest extends TestBase {

    private MockTestPlanner mockTestPlanner;


    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in study page, on clicking Take a test button user navigates to Test listing page." +
            "TestListing_1" +
            "TestListing_12" +
            "TestListing_15" +
            "TestListing_16")
    public void verifyUserNavigateToTestListingPageWhenClickOnTakeATest() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.isDescriptionShowing();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.verifyClearSearchIsWorking();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.isShowMoreORShowLessDisplayingAndWorking();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify user navigation with respect to subject search")
    public void verifyUserNavigateToSubjectPageWithRespectiveToSubjectSearch() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.isDescriptionShowing();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Subjects.SUBJECT_MATHEMATICS);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Subjects.SUBJECT_MATHEMATICS);
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "When user click on any exam then user navigate to particular exam page")
    public void verifyUserNavigateToSubjectPageWithRespectiveToExamSearch() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.verifySearchNavigationWithTestName(Exams.EXAM_JEE_ADVANCED);
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify open close button in left filter section")
    public void verifyOpenCloseButtonInLeftFilter() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        leftFilter.verifyLeftFilterDisplaying();
        leftFilter.openCloseLeftFilterSection();
        leftFilter.verifyLeftFilterNotDisplaying();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify left filter " +
            "TestListing_5\n" +
            " TestListing_21\n" +
            "TestListing_23" +
            "TestListing_22" +
            "TestListing_24")
    public void verifyLeftFilterAndUserNavigationForJEEMain() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest subject1 = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(subject1);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Subjects.SUBJECT_PHYSICS);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.verifyBreadCrumb(Goals.GOAL_ENGINEERING, Exams.EXAM_JEE_MAIN, Subjects.SUBJECT_PHYSICS, SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS, Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS);

        mockTestHomePage.clickExamLink(Exams.EXAM_JEE_MAIN);
        MockTest subject2 = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_CHEMISTRY).build();
        leftFilter.selectSubjectName(subject2);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Subjects.SUBJECT_CHEMISTRY);
        MockTest subSubject2 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_CHEMISTRY_INORGANIC).build();
        leftFilter.selectSubSubjectName(subSubject2);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(SubSubjects.SUB_SUBJECT_CHEMISTRY_INORGANIC);
        MockTest concept2 = new MockTestBuilder().withConcept(Concepts.CONCEPT_P_BLOCK_ELEMENTS).build();
        leftFilter.selectConceptName(concept2);
        mockTestHomePage.verifyBreadCrumb(Goals.GOAL_ENGINEERING, Exams.EXAM_JEE_MAIN, Subjects.SUBJECT_CHEMISTRY, SubSubjects.SUB_SUBJECT_CHEMISTRY_INORGANIC, Concepts.CONCEPT_P_BLOCK_ELEMENTS);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Concepts.CONCEPT_P_BLOCK_ELEMENTS);

        mockTestHomePage.clickExamLink(Exams.EXAM_JEE_MAIN);
        MockTest subject3 = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_MATHEMATICS).build();
        leftFilter.selectSubjectName(subject3);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Subjects.SUBJECT_MATHEMATICS);
        MockTest subSubject3 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_MATHEMATICS_TRIOGONOMETRY).build();
        leftFilter.selectSubSubjectName(subSubject3);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(SubSubjects.SUB_SUBJECT_MATHEMATICS_TRIOGONOMETRY);
        MockTest concept3 = new MockTestBuilder().withConcept(Concepts.CONCEPT_HEIGHTS_AND_DISTANCES).build();
        leftFilter.selectConceptName(concept3);
        mockTestHomePage.verifyBreadCrumb(Goals.GOAL_ENGINEERING, Exams.EXAM_JEE_MAIN, Subjects.SUBJECT_MATHEMATICS, SubSubjects.SUB_SUBJECT_MATHEMATICS_TRIOGONOMETRY, Concepts.CONCEPT_HEIGHTS_AND_DISTANCES);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Concepts.CONCEPT_HEIGHTS_AND_DISTANCES);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in any of the Test card(Full test/previous test...)- " +
            "when a user clicks on \"hide\" link -  the list of the tests available is navigating to hidden. " +
            "TestListing_33")
    public void verifyHideLinkForEngineering() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyHideLink();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in any of the Test card(Full test/previous test...)- " +
            "when a user clicks on \"hide\" link -  the list of the tests available is navigating to hidden. " +
            "TestListing_33")
    public void verifyHideLinkForMedical() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyHideLink();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in any of the Test card(Full test/previous test..)-" +
            " on clicking show all link user is navigating to a maximum of 10 tests displayed in the list." +
            "TestListing_31")
    public void userShouldNotGetTestCardsMoreThanTenForEngineering() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyTestCardsNumber();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in any of the Test card(Full test/previous test..)-" +
            " on clicking show all link user is navigating to a maximum of 10 tests displayed in the list." +
            "TestListing_31")
    public void userShouldNotGetTestCardsMoreThanTenForMedical() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyTestCardsNumber();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in any of the Test list(Full test/previous test..)-if tests available are more than 10, " +
            "then Show more link is displayed in the Test's list." +
            "TestListing_32")
    public void userShouldGetShowMoreWhenMoreThanTenTestAvailableForEngineering() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyShowMoreLink();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in any of the Test list(Full test/previous test..)-if tests available are more than 10, " +
            "then Show more link is displayed in the Test's list." +
            "TestListing_32")
    public void userShouldGetShowMoreWhenMoreThanTenTestAvailableForMedical() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyShowMoreLink();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify on changing goal, " +
            "user is navigating to respective test listing page." +
            "TestListing_7" +
            "TestListing_9")
    public void verifyGoalNavigationAndDescriptionForEngineering() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_ENGINEERING);
        leftFilter.verifyDescriptionForAllExams();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify on changing goal," +
            " user is navigating to respective test listing page." +
            "TestListing_7" +
            "TestListing_9")
    public void verifyGoalNavigationAndDescriptionForMedical() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_MEDICAL);
        leftFilter.verifyDescriptionForAllExams();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in test listing page, user having a Practice Button- to practice the Searched Concepts." +
            "Test_Listing_49")
    public void verifyPracticeCardForAllExamInEngineering() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_ENGINEERING);
        leftFilter.verifyPracticeOnTestListingForAllExam();
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in test listing page, user having a Practice Button- to practice the Searched Concepts." +
            "Test_Listing_49")
    public void verifyPracticeCardForAllExamInMedical() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_MEDICAL);
        leftFilter.verifyPracticeOnTestListingForAllExam();
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify if user search for any of the Exam, in the Test " +
            "listing page user should get Subjects available for the respective Exam searched for." +
            "TestListing_47")
    public void verifySubjectsAvailableForExam() throws InterruptedException {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        MockTest subPhysics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(subPhysics);
        mockTestHomePage.verifySubjectAvailableForExam(Subjects.SUBJECT_PHYSICS);
        Thread.sleep(5000);
        mockTestHomePage.clickExamLink(Exams.EXAM_JEE_MAIN);
        MockTest subChemistry = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_CHEMISTRY).build();
        leftFilter.selectSubjectName(subChemistry);
        mockTestHomePage.verifySubjectAvailableForExam(Subjects.SUBJECT_CHEMISTRY);
        mockTestHomePage.clickExamLink(Exams.EXAM_JEE_MAIN);
        MockTest subMathematics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_MATHEMATICS).build();
        leftFilter.selectSubjectName(subMathematics);
        mockTestHomePage.verifySubjectAvailableForExam(Subjects.SUBJECT_MATHEMATICS);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify In a Test card(Full test, previous test...)- on clicking \"Show All\" link - available" +
            " test's list is displayed." +
            "TestListing_55")
    public void userShouldNavigateBackToTestListingPage() throws ParseException {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_REVISION_TEST);
        String test = mockTestHomePage.getFirstTestName();
        mockTestHomePage.startTestAndBackToTestListing(0);
        instructionPage.verifyUserLandedOnInstructionPageWrtTest(test);
        instructionPage.dismissInstructions();
        String testName = questionsHomePage.getTestName();
        mockTestHomePage.verifyUserNavigateToTestListing();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        navigateToRefresh();
        mockTestHomePage.verifyInprogressTest(testName);
        mockTestHomePage.verifyTimerWorkingForInProgressTest(5, 0);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify if user has multiple tests in progress, all the Test cards are " +
            "appearing in the Testlisting age with timer displayed in the Test cards." +
            "TestListing_56" +
            "TestListing_40" +
            "TestListing_41")
    public void userShouldHaveMultipleTestInProgress() throws ParseException {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_REVISION_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        String firstTestName = questionsHomePage.getTestName();
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
        mockTestHomePage.startSecondTest();
        instructionPage.dismissInstructions();
        String secondTestName = questionsHomePage.getTestName();
        driver.switchTo().window(tabs.get(0));
        navigateToRefresh();
        mockTestHomePage.verifyInProgressTestForMultipleTest(firstTestName, secondTestName);
        mockTestHomePage.verifyTimerWorkingForInProgressTest(5, 0);
        mockTestHomePage.verifyTimerWorkingForInProgressTest(5, 1);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify in Test listing page, in the Test list section user is navigating to " +
            "two drop downs - Test type and Language." +
            "TestListing_24")
    public void userShouldGetTestTypeAndLanguageDropdownInTestListing() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyTestTypeAndLanguageDropdown();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify In a Test card(Full test, previous test...)- \" +\n" +
                  "\"on clicking \\\"Show All\\\" link - available test's list is displayed."  +
                  "TestListing_30")
    public void userShouldGetAllTestCardsWrtTestTypesForEngineering(){
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyTestCardsCountForAllTestTypes();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "verify all available goals" +
            "TestListing_6")
    public void userShouldHaveAllGoalsAvailable() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_10th_FOUNDATION);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_BANK);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_ENGINEERING);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_MEDICAL);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_8th_FOUNDATION);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_9th_FOUNDATION);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_CBSE);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_CBSE_12);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_DEFENCE);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_INSURANCE);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_LECTURESHIP);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_MANAGEMENT);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_Railway);
        landingHomePage.clickOnGoals();
        landingHomePage.verifyAllGoalsAvailable(Goals.GOAL_SSC);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify description for subjects" +
         "TestListing_10")
    public void userShouldGetSubjectLevelDescription(){
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest physicsJEE = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physicsJEE);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_PHYSICS, Exams.EXAM_JEE_MAIN);

        mockTestHomePage.clickExamLink(Exams.EXAM_JEE_MAIN);
        MockTest chemistryJEE = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_CHEMISTRY).build();
        leftFilter.selectSubjectName(chemistryJEE);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_CHEMISTRY, Exams.EXAM_JEE_MAIN);

        mockTestHomePage.clickExamLink(Exams.EXAM_JEE_MAIN);
        MockTest mathJEE = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_MATHEMATICS).build();
        leftFilter.selectSubjectName(mathJEE);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_MATHEMATICS, Exams.EXAM_JEE_MAIN);

        mockTestHomePage.clearSearchField();
        mockTestHomePage.verifyClearSearchIsWorking();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_ADVANCED);
        MockTest physicsAdvanced = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physicsAdvanced);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_PHYSICS, Exams.EXAM_JEE_ADVANCED);

        mockTestHomePage.clickExamLink(Exams.EXAM_JEE_ADVANCED);
        MockTest chemistryAdvanced = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_CHEMISTRY).build();
        leftFilter.selectSubjectName(chemistryAdvanced);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_CHEMISTRY, Exams.EXAM_JEE_ADVANCED);

        mockTestHomePage.clickExamLink(Exams.EXAM_JEE_ADVANCED);
        MockTest mathAdvanced = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_MATHEMATICS).build();
        leftFilter.selectSubjectName(mathAdvanced);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_MATHEMATICS, Exams.EXAM_JEE_ADVANCED);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify description for subjects" +
            "TestListing_10")
    public void userShouldGetSubjectLevelDescriptionForMedical() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest physicsJEE = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physicsJEE);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_PHYSICS, Exams.EXAM_NEET);

        mockTestHomePage.clickExamLink(Exams.EXAM_NEET);
        MockTest chemistryAdvanced = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_CHEMISTRY).build();
        leftFilter.selectSubjectName(chemistryAdvanced);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_CHEMISTRY, Exams.EXAM_NEET);

        mockTestHomePage.clickExamLink(Exams.EXAM_NEET);
        MockTest biology = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_BIOLOGY).build();
        leftFilter.selectSubjectName(biology);
        mockTestHomePage.verifySubjectLevelDescription(Subjects.SUBJECT_BIOLOGY, Exams.EXAM_NEET);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify the test listing page UI, when a user " +
            "searches for irrelevant data." +
            "TestListing_14")
    public void userSearchIrrelevantDataThenFunctionalityDoesNotWork(){
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.isDescriptionShowing();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.irrelevantSearch(Exams.IRRELAVENT_EXAM_NA);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST}, description = "Verify when any of the test is in progress" +
            " Timer appearing in test and respective test's card " +
            "in test listing page are appearing same." +
            "TestListing_37" +
            "TestListing_39")
    public void userShouldGetTimerAppearingShouldBeSame()throws ParseException{
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_REVISION_TEST);
        String testName1 = mockTestHomePage.getFirstTestName();
        mockTestHomePage.startTestWithDifferentTab(0);
        String testName2 = instructionPage.getTestName();
        instructionPage.verifyTestNameWrtTestListingPage(testName1, testName2);
        instructionPage.dismissInstructions();
        mockTestHomePage.verifyTimerShouldBeSameForTestListAndQuesPage();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify In a Test card(Full test, previous test...)- " +
            "on clicking on Show All link - available test's list is displayed." +
            "TestListing_30")
    public void userShouldGetAllTestCardsWrtTestTypesForMedical() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MEDICAL);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyTestCardsCountForAllTestTypes();
    }
}