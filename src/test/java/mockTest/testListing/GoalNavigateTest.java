package mockTest.testListing;

import constants.Goals;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class GoalNavigateTest extends TestBase {

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
            "fo SSC")
    public void verifyGoalNavigationForSSC() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_SSC);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_SSC);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario"+
            "for Bank")
    public void verifyGoalNavigationForBank() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_BANK);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_BANK);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify on changing goal, " +
            "user is navigating to respective test listing page." +
            "TestListing_7" +
            "TestListing_9")
    public void verifyGoalNavigationForEighthFoundation() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_8th_FOUNDATION);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_8th_FOUNDATION);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Verify on changing goal, " +
            "user is navigating to respective test listing page." +
            "TestListing_7" +
            "TestListing_9")
    public void verifyGoalNavigationForNinethFoundation() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_9th_FOUNDATION);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_9th_FOUNDATION);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
            "for Railway")
    public void verifyGoalNavigationForRailway() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_Railway);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_Railway);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario"+
            "for Lectureship")
    public void verifyGoalNavigationForLectureship(){
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_LECTURESHIP);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_LECTURESHIP);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario"+
            "for Management")
    public void verifyGoalNavigationForManagement() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_MANAGEMENT);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_MANAGEMENT);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
            "for Insurance")
    public void verifyGoalNavigationForInsurance() throws Exception{
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_INSURANCE);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_INSURANCE);
    }


    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
            "Defence")
    public void verifyGoalNavigationForDefence() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_DEFENCE);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_DEFENCE);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario" +
            "for CBSE 12")
    public void verifyGoalNavigationForCBSETwelth() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_CBSE_12);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_CBSE_12);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_TAKE_A_TEST_TEST_LISTING}, description = "Goal navigation and End to end scenario"+
            "for CBSE")
    public void verifyGoalNavigationForCBSE() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_CBSE);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_CBSE);
    }


    @Test(description = "Goal navigation and End to end scenario"+
            "for 10th foundation")
    public void verifyGoalNavigationForTenthFoundation()throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_10th_FOUNDATION);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserNavigateToRespectiveGoal(Goals.GOAL_10th_FOUNDATION);
    }
}
