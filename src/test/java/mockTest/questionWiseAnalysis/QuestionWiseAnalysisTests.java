package mockTest.questionWiseAnalysis;

import builders.MockTestBuilder;
import constants.*;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;
import static constants.Badge.*;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class QuestionWiseAnalysisTests extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS},description = "User should able to view all Questions " +
            "when clicked on View All Question button" +
            "Question-Wise Analysis_1")
    public void userShouldBeAbleToViewAllQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2 + 1, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickViewAllQuestionsTab();
        questionWiseAnalysis.checkAllQuestionIsPresentOrNot(mockTestPlanner);
        questionWiseAnalysis.verifyQuestionWithOptions(mockTestPlanner);
        questionWiseAnalysis.verifyUserSelectedAnswerDisplayed(mockTestPlanner);
        questionWiseAnalysis.verifyRightAnswerDisplayed(mockTestPlanner);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS},description = "User should able to view all Over Time Correct Questions " +
            "when clicked on Over Time Correct button" +
            "Question-Wise Analysis_2")
    public void userShouldBeAbleToViewAllOvertimeCorrectQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_OVERTIME_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickOverTimeCorrectTab();
        questionWiseAnalysis.checkAllQuestionIsPresentOrNot(mockTestPlanner);
        questionWiseAnalysis.verifyQuestionWithOptions(mockTestPlanner);
        questionWiseAnalysis.verifyUserSelectedAnswerDisplayed(mockTestPlanner);
        questionWiseAnalysis.verifyRightAnswerDisplayed(mockTestPlanner);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS},description = "User should able to view all Over Time Correct Questions " +
            "when clicked on wasted attempt button" +
            "Question-Wise Analysis_3")
    public void userShouldBeAbleToViewAllWastedAttemptsQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickWastedAttemptsTab();
        questionWiseAnalysis.checkAllQuestionIsPresentOrNot(mockTestPlanner);
        questionWiseAnalysis.verifyQuestionWithOptions(mockTestPlanner);
        questionWiseAnalysis.verifyUserSelectedAnswerDisplayed(mockTestPlanner);
        questionWiseAnalysis.verifyRightAnswerDisplayed(mockTestPlanner);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS},description = "User should able to view all Over Time Correct Questions " +
            "when clicked on wasted attempt button" +
            "Question-Wise Analysis_4")
    public void userShouldBeAbleToViewAllOvertimeIncorrectQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_INCORRECT, BADGE_OVERTIME_INCORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickOverTimeIncorrectTab();
        questionWiseAnalysis.checkAllQuestionIsPresentOrNot(mockTestPlanner);
        questionWiseAnalysis.verifyQuestionWithOptions(mockTestPlanner);
        questionWiseAnalysis.verifyUserSelectedAnswerDisplayed(mockTestPlanner);
        questionWiseAnalysis.verifyRightAnswerDisplayed(mockTestPlanner);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS},description = "User should able to view all Over Time Correct Questions " +
            "when clicked on incorrect attempt button" +
            "Question-Wise Analysis_5")
    public void userShouldBeAbleToViewAllIncorrectAttemptsQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickIncorrectAttemptsTab();
        questionWiseAnalysis.checkAllQuestionIsPresentOrNot(mockTestPlanner);
        questionWiseAnalysis.verifyQuestionWithOptions(mockTestPlanner);
        questionWiseAnalysis.verifyUserSelectedAnswerDisplayed(mockTestPlanner);
        questionWiseAnalysis.verifyRightAnswerDisplayed(mockTestPlanner);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Chapter-wise performance section, User should able to view chapters which user needs to improve" +
            " when clicked on Chapters you need to improve button." +
            "Question_Wise_Analysis_6" +
            "Question_Wise_Analysis_20")
    public void userShouldAbleToViewChapterNeedToImproveInChapterWisePerformance(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()), CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT,getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyChapterWisePerformanceDisplayed();
        questionWiseAnalysis.clickChapterNeedsToImprove();
        questionWiseAnalysis.verifyChaptersNeedsToImproveDisplayed(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS);
        questionWiseAnalysis.verifyPracticeLinkWorking(Links.PRACTICE);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Chapter-wise performance section, User should able to view chapters which user needs to improve" +
            " when clicked on Chapters you could do well button." +
            "Question_Wise_Analysis_7" +
            "Question_Wise_Analysis_20")
    public void userShouldAbleToViewChapterYouCouldDoWellInChapterWisePerformance(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ())/2, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT,getmockTestResponse ()));
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ())/2 + 1, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT,getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyChapterWisePerformanceDisplayed();
        questionWiseAnalysis.clickChapterYouCouldDoWell();
        questionWiseAnalysis.verifyChaptersYouCouldDoWellDisplayed(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS);
        questionWiseAnalysis.verifyPracticeLinkWorking(Links.PRACTICE);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Chapter-wise performance section, User should able to view chapters which user has done good" +
            " when clicked on Chapters you did good button." +
            "Question_Wise_Analysis_8" +
            "Question_Wise_Analysis_20")
    public void userShouldAbleToViewChapterYouDidWellInChapterWisePerformance(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()) - 25, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT,getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyChapterWisePerformanceDisplayed();
        questionWiseAnalysis.clickChapterYouDidWell();
        questionWiseAnalysis.verifyChaptersYouDidWellDisplayed(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS);
        questionWiseAnalysis.verifyPracticeLinkWorking(Links.PRACTICE);
    }

    @Test(description = "In Concept-wise Performance section, User should able to view weak concepts" +
            "when clicked on Weak concepts button." +
            "Question_Wise_Analysis_9" +
            "Question_Wise_Analysis_20")
    public void userShouldAbleToViewWeakConcept(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()), CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyConceptWisePerformanceDisplayed();
        questionWiseAnalysis.clickWeakConcept();
        questionWiseAnalysis.verifyConceptDisplaying();
        questionWiseAnalysis.verifyPracticeLinkWorking(Links.PRACTICE);
    }

    @Test(description = "In Concept-wise Performance section, User should able to view weak concepts" +
            "when clicked on strong concepts button." +
            "Question_Wise_Analysis_10" +
            "Question_Wise_Analysis_20")
    public void userShouldAbleToViewStrongConcept(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_MATHEMATICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_MATHEMATICS_TRIOGONOMETRY).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPT_HEIGHTS_AND_DISTANCES).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyConceptWisePerformanceDisplayed();
        questionWiseAnalysis.clickStrongConcept();
        questionWiseAnalysis.verifyConceptDisplaying();
        questionWiseAnalysis.verifyPracticeLinkWorking(Links.PRACTICE);
    }
}
