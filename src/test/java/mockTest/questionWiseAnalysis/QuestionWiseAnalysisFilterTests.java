package mockTest.questionWiseAnalysis;

import builders.MockTestBuilder;
import constants.*;
import entities.MockTest;
import entities.MockTestData;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import testBase.TestBase;
import entities.MockTestPlanner;
import utils.Categories;
import utils.Properties;
import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static constants.Badge.BADGE_WASTED_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class QuestionWiseAnalysisFilterTests extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Filter question by section, user should be able to see particular subjects question displayed" +
            "when selects the subject in the 1st dropdown." +
            "Question_Wise_Analysis_11")
    public void userShouldGetQuestionsWrtDropDownSelected(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2 + 1, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        driver.findElement(By.cssSelector(".css-1hwfws3")).click();
        questionWiseAnalysis.selectSubjectInLeftFilter(Subjects.SUBJECT_CHEMISTRY);
        questionWiseAnalysis.verifyQuestionsWithSolutionsFromLeftFilter();
        driver.navigate().refresh();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        driver.findElement(By.cssSelector(".css-1hwfws3")).click();
        questionWiseAnalysis.selectSubjectInLeftFilter(Subjects.SUBJECT_PHYSICS);
        questionWiseAnalysis.verifyQuestionsWithSolutionsFromLeftFilter();
        driver.navigate().refresh();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        driver.findElement(By.cssSelector(".css-1hwfws3")).click();
        questionWiseAnalysis.selectSubjectInLeftFilter(Subjects.SUBJECT_MATHEMATICS);
        questionWiseAnalysis.verifyQuestionsWithSolutionsFromLeftFilter();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Filter question by section, user should be able to see particular subjects question displayed" +
            "when selects the subject in the 1st dropdown." +
            "Question_Wise_Analysis_12")
    public void userShouldAbleToViewAnsweredQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickQuestionStatusDropDown();
        questionWiseAnalysis.selectQuestionStatusInLeftFilter(QuestionStatus.Answered);
        questionWiseAnalysis.verifyAnsweredQuestions();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Filter question by section, user should be able to see particular subjects question displayed" +
            "when selects the subject in the 1st dropdown." +
            "Question_Wise_Analysis_13")
    public void userShouldAbleToViewNotAnsweredQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        driver.findElement(By.cssSelector(".css-1hwfws3")).click();
        questionWiseAnalysis.selectSubjectInLeftFilter(Subjects.SUBJECT_CHEMISTRY);
        questionWiseAnalysis.clickQuestionStatusDropDown();
        questionWiseAnalysis.selectQuestionStatusInLeftFilter(QuestionStatus.NotAnswered);
        questionWiseAnalysis.verifyNotAnsweredQuestions();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Filter question by section, user should be able to see particular subjects question displayed" +
            "when selects the subject in the 1st dropdown." +
            "Question_Wise_Analysis_14")
    public void userShouldAbleToViewNotVisitedQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        driver.findElement(By.cssSelector(".css-1hwfws3")).click();
        questionWiseAnalysis.selectSubjectInLeftFilter(Subjects.SUBJECT_CHEMISTRY);
        questionWiseAnalysis.clickQuestionStatusDropDown();
        questionWiseAnalysis.selectQuestionStatusInLeftFilter(QuestionStatus.NotVisited);
        questionWiseAnalysis.verifyNotVisitedQuestions();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Filter question by section, user should be able to see particular subjects question displayed" +
            "when selects the subject in the 1st dropdown." +
            "Question_Wise_Analysis_15")
    public void userShouldAbleToViewInCorrectAnsweredQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_INCORRECT,BADGE_WASTED_ATTEMPT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickQuestionStatusDropDown();
        questionWiseAnalysis.selectQuestionStatusInLeftFilter(QuestionStatus.InCorrectAnswer);
        questionWiseAnalysis.verifyInCorrectAnswer();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Filter question by section, user should be able to see particular subjects question displayed" +
            "when selects the subject in the 1st dropdown." +
            "Question_Wise_Analysis_16")
    public void userShouldAbleToViewCorrectAnsweredQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()) / 2, CHOICE_CORRECT,BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        driver.findElement(By.cssSelector(".css-1hwfws3")).click();
        questionWiseAnalysis.selectSubjectInLeftFilter(Subjects.SUBJECT_CHEMISTRY);
        questionWiseAnalysis.clickQuestionStatusDropDown();
        questionWiseAnalysis.selectQuestionStatusInLeftFilter(QuestionStatus.CorrectAnswer);
        questionWiseAnalysis.verifyCorrectAnswer();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Filter question by section, user should be able to see particular subjects question displayed" +
            "when selects the subject in the 1st dropdown." +
            "Question_Wise_Analysis_17")
    public void userShouldAbleToViewAnsweredAndMarkedForReviewQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.jumpToQuestionUsingQuestionPad(3);
        questionsHomePage.clickOnAnyOption();
        questionsHomePage.clickOnNextButton();
        questionsHomePage.clickOnPreviousButton();
        questionsHomePage.clickMarkForReviewQuestion();
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickQuestionStatusDropDown();
        questionWiseAnalysis.selectQuestionStatusInLeftFilter(QuestionStatus.AnsweredAndMarkedForReview);
        questionWiseAnalysis.verifyAnsAndMarkedForReview();
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "In Filter question by section, user should be able to see particular subjects question displayed" +
            "when selects the subject in the 1st dropdown." +
            "Question_Wise_Analysis_18")
    public void userShouldAbleToViewMarkedForReviewQuestions(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        MockTest exam = new MockTestBuilder().withExamName(Exams.EXAM_JEE_MAIN).build();
        leftFilter.selectExamName(exam);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_MINI_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        questionsHomePage.jumpToQuestionUsingQuestionPad(3);
        questionsHomePage.clickMarkForReviewQuestion();
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.clickQuestionStatusDropDown();
        questionWiseAnalysis.selectQuestionStatusInLeftFilter(QuestionStatus.MarkedForReview);
        questionWiseAnalysis.verifyMarkedForReview();
    }
}
