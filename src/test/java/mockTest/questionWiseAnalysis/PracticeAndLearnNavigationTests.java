package mockTest.questionWiseAnalysis;

import builders.MockTestBuilder;
import constants.*;
import entities.MockTest;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;
import static constants.Badge.*;
import static constants.Badge.BADGE_PERFECT_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class PracticeAndLearnNavigationTests extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "" +
            "Question_Wise_Analysis_19")
    public void userShouldBeAbleToNavigateToLearnTabForChapterNeedsToImprove(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()) - 25, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT,getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyChapterWisePerformanceDisplayed();
        questionWiseAnalysis.clickChapterNeedsToImprove();
        questionWiseAnalysis.verifyLearnLinkWorking(Links.LEARN);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "" +
            "Question_Wise_Analysis_19")
    public void userShouldBeAbleToNavigateToLearnTabForChapterYouCouldDoWell(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ())/2, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT,getmockTestResponse ()));
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ())/2 + 1, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT,getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyChapterWisePerformanceDisplayed();
        questionWiseAnalysis.clickChapterYouCouldDoWell();
        questionWiseAnalysis.verifyLearnLinkWorking(Links.LEARN);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "" +
            "Question_Wise_Analysis_19")
    public void userShouldBeAbleToNavigateToLearnTabForChaptersYouDidWell(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()) - 25, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT,getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyChapterWisePerformanceDisplayed();
        questionWiseAnalysis.clickChapterYouDidWell();
        questionWiseAnalysis.verifyLearnLinkWorking(Links.LEARN);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "" +
            "Question_Wise_Analysis_19")
    public void userShouldBeAbleToNavigateToLearnTabForWeakConcept(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_PHYSICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_PHYSICS_MODERNPHYSICS).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPTS_EXPERIMENTAL_PHYSICS).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()), CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT, getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyConceptWisePerformanceDisplayed();
        questionWiseAnalysis.clickWeakConcept();
        questionWiseAnalysis.verifyLearnLinkWorking(Links.LEARN);
    }

    @Test(groups = {Categories.TRP_TAKE_A_TEST, Categories.TRP_QUESTION_WISE_ANALYSIS}, description = "" +
            "Question_Wise_Analysis_19")
    public void userShouldBeAbleToNavigateToLearnTabForStrongConcept(){
        mockTestPlanner = new MockTestPlanner();
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_MAIN);
        mockTestHomePage.verifyUserNavigatesWithRespectToSearch(Exams.EXAM_JEE_MAIN);
        MockTest physics = new MockTestBuilder().withSubjectName(Subjects.SUBJECT_MATHEMATICS).build();
        leftFilter.selectSubjectName(physics);
        MockTest subSubject1 = new MockTestBuilder().withSubSubject(SubSubjects.SUB_SUBJECT_MATHEMATICS_TRIOGONOMETRY).build();
        leftFilter.selectSubSubjectName(subSubject1);
        MockTest concept1 = new MockTestBuilder().withConcept(Concepts.CONCEPT_HEIGHTS_AND_DISTANCES).build();
        leftFilter.selectConceptName(concept1);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.verifySelectedTestCardIsDisplayed(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        mockTestHomePage.startTest();
        instructionPage.dismissInstructions();
        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse ()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse ()));
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
        examSummaryPage.clickOnShowMeFeedBackButton();
        questionWiseAnalysis.clickQuestionWiseAnalysisTab();
        questionWiseAnalysis.verifyConceptWisePerformanceDisplayed();
        questionWiseAnalysis.clickStrongConcept();
        questionWiseAnalysis.verifyLearnLinkWorking(Links.LEARN);
    }
}
