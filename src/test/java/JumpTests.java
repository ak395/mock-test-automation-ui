import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import page.jumpUPPage.*;
import testBase.TestBase;
import utils.Properties;
import verification.TestFailures;

import java.io.IOException;

public class JumpTests extends TestBase
{
    @Test(description ="User clicks on the get jump button and verifies the header")
    public void shouldClickOnGetJumpButton()
    {
        driver.get(Properties.jumpUrl);
       // driver.manage().window().maximize();
        NavigatetoJump navigatetoJump = new NavigatetoJump(driver);
        navigatetoJump.clickOnJumpHeader();
        TestFailures.reportIfAny();
    }

    @Test(description = "User should be able to click on the GET JUMP button.")
    public void clickOnGetJumpButton() {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        GetJumpSection jps = new GetJumpSection(driver);
        jps.clickOnGetJump();
        TestFailures.reportIfAny();
    }

    @Test(description = "Assertion for Get Jump Button")
    public void getJumpAssertion() {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        GetJumpSection jps = new GetJumpSection(driver);
        jps.getJumpAssertion();
        TestFailures.reportIfAny();
    }

    @Test(description = "Clicking on Test Taking MRI Tab in jump-up--features")
    public void clickOnTesttakingMrITab() {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        TabSection TabSection = new TabSection(driver);
        TabSection.clickOnTesttakingMRITab();
        TestFailures.reportIfAny();
    }

    @Test(description = "Clicking on Compare Like Never Before Tab in jump-up--features")
    public void clickOnCompareLikeNeverBeforeTab() {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        TabSection TabSection = new TabSection(driver);
        TabSection.clickOnCompareLikeNeverBeforeTab();
        TestFailures.reportIfAny();
    }

    @Test(description = "Click On Experience The Real Test Tab in jump-up--features")
    public void clickOnExperienceTheRealTestTab() {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        TabSection TabSection = new TabSection(driver);
        TabSection.clickOnExperienceTheRealTestTab();
        TestFailures.reportIfAny();
    }

    @Test(description = "Click On Unbeatable Academic Analysis Tab in jump-up--features")
    public void clickOnUnbeatableAcademicAnalysisTab() {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        TabSection TabSection = new TabSection(driver);
        TabSection.clickOnUnbeatableAcademicAnalysisTab();
        TestFailures.reportIfAny();
    }

    @Test(description = "Click on Read their story in Improvement Story Section")
    public void clickOnReadTheirStory() throws InterruptedException {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        ImprovementStory improvementStory = new ImprovementStory(driver);
        improvementStory.clickOnReadTheirStory();
        TestFailures.reportIfAny();
    }

    @Test(description = "Click on Learn More in Science Behind The Packs Section")
    public void clickOnLearnMore() throws InterruptedException {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        ScienceBehindThePacks scienceBehindThePacks = new ScienceBehindThePacks(driver);
        scienceBehindThePacks.clickOnLearnMore();
        TestFailures.reportIfAny();
    }

    @Test(description = "Click on Explore RankUp in RANKUP = JUMP + Personalization section")
    public void clickOnExploreRankup() throws InterruptedException {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        ExploreRankUp exploreRankUp = new ExploreRankUp(driver);
        exploreRankUp.clickOnExploreRankUp();
        TestFailures.reportIfAny();
    }

    @Test(description = "Click on Unlock Button in Fix your knowledge gaps and strategy to become the top ranker  section")
    public void clickOnUnlock() {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        JumpDidYouKnow jumpDidYouKnow = new JumpDidYouKnow(driver);
        jumpDidYouKnow.clickOnUnlockButton();
        TestFailures.reportIfAny();
    }

    @Test(description = "User clicks on Engineering Goal Class 11 and May2019 and clicks on Pack1")
    public void clickOnEngineeringGoalClass11May2019Pack1() throws IOException {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        JumpLogin jumpLogin = new JumpLogin(driver);
        jumpLogin.clickOnJumpLogin();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.cssSelector("[class='jump-up--buy-packs__title ']"));
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
        JumpUpPacks JumpUpPacks = new JumpUpPacks(driver);
        JumpUpPacks.ClickEngineeringGoalClass112019Goal();
        JumpUpPacks.ClickOnUnlockPack1();
    }

    @Test(description = "User clicks on Engineering Goal Class 11 and May2019 and clicks on Pack2")
    public void clickOnEngineeringGoalClass11May2019Pack2() throws IOException {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        JumpLogin jumpLogin = new JumpLogin(driver);
        jumpLogin.clickOnJumpLogin();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.cssSelector("[class='jump-up--buy-packs__title ']"));
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
        JumpUpPacks JumpUpPacks = new JumpUpPacks(driver);
        JumpUpPacks.ClickEngineeringGoalClass112019Goal();
        JumpUpPacks.ClickOnUnlockPack2();
    }

    @Test(description = "User clicks on Engineering Goal Class 11 and May2019 and clicks on Pack3")
    public void clickOnEngineeringGoalClass11May2019Pack3() throws IOException {
        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        JumpLogin jumpLogin = new JumpLogin(driver);
        jumpLogin.clickOnJumpLogin();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.cssSelector("[class='jump-up--buy-packs__title ']"));
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
        JumpUpPacks JumpUpPacks = new JumpUpPacks(driver);
        JumpUpPacks.ClickEngineeringGoalClass112019Goal();
        JumpUpPacks.ClickOnUnlockPack3();
        JumpUpPacks.ClickOnUnlockPack3();
    }

    @Test(description = "User clicks on Embibe Chat Button")
    public void ClickOnEmbibeChatButton() {

        driver.get(Properties.jumpUrl);
        driver.manage().window().maximize();
        ChatButton chatButton = new ChatButton(driver);
        chatButton.clickOnChatButton();
    }
}
