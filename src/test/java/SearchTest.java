import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import page.Landing.LandingPageStartSection;
import page.Search.*;
import testBase.TestBase;

import java.io.IOException;
@Getter
@Setter
public class SearchTest extends TestBase
{
    private LandingPageStartSection landingPageStartSection;
    @Test(description = "User verifies the Title of the Search Page along with the URL of the Search Page")
    public void shouldVerifyTitleOfSearchPage()
    {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SearchPageTitleSection searchPageTitleSection=searchHomePage.getSearchPageTitleSection();
        searchPageTitleSection.getSearchTitleElement();
        searchPageTitleSection.assertHeaderToBe("What would you like to STUDY today?");
        searchPageTitleSection.assertUrlHasBeenResetTo("https://www.embibe.com/");
    }

    @Test(description = "User clicks on Start Practice Button and verifies the URL along with the header of the practice section ")
    public void shouldClickOnPracticeButton()
    {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        StartPracticeSection startPracticeSection =searchHomePage.getSearchPracticeSection();
        StartPracticeSectionHeaderSection startPracticeSectionHeaderSection=searchHomePage.getStartPracticeSectionHeaderSection();
        startPracticeSection.clickOnStartPracticeButton();
        startPracticeSection.assertUrlHasBeenResetTo("https://www.embibe.com/engineering/practice/solve");
        startPracticeSectionHeaderSection.assertHeaderToBe("Guided practice with hints and detailed solutions.\n" + "Always get questions right within time!");
    }

    @Test(description = "User clicks on the Take A Test Button and Verifies the URL along with the Header section")
    public void shouldClickOnTakeATestButton()
    {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        TakeATestSection takeATestSection=searchHomePage.getTakeATestSection();
        takeATestSection.clickOnTakeATestButton();
        takeATestSection.assertUrlHasBeenResetTo("https://www.embibe.com/engineering/test");
        TakeATestHeaderSection takeATestHeaderSection=searchHomePage.getTakeATestHeaderSection();
        takeATestHeaderSection.assertHeaderToBe("Take a mock test, review performance of past tests");
    }

    @Test(description = "User clicks on Find Something Cool Link and verifies the URL along with the header section")
    public void shouldClickOnFindSomethingCoolLink()
    {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        FindSomethingCoolSection findSomethingCoolSection=searchHomePage.getFindSomethingCoolSection();
        FindSomethingCoolHeaderSection findSomethingCoolHeaderSection=searchHomePage.getFindSomethingCoolHeaderSection();
        findSomethingCoolSection.scrollToFindSomethingCoolLink();
        driver.navigate().refresh();
        findSomethingCoolSection.clickOnFindSomethingCoolLink();
        for(String winHandle : driver.getWindowHandles())
        {
            driver.switchTo().window(winHandle);
        }

        findSomethingCoolSection.assertUrlHasBeenResetTo("https://www.embibe.com/ai");
        findSomethingCoolHeaderSection.assertHeaderToBe("Personalized Learning Outcomes for everyone through the world's best Artificial Intelligence platform in Education");
    }

    @Test(description = "User clicks on the SearchField Text box")
    public void shouldClickOnSearchTextField()
    {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SearchPageTextFieldSection searchPageTextFieldSection=searchHomePage.getSearchPageTextFieldSection();
        searchPageTextFieldSection.clickOnSearchTextField();
    }

    @Test(description = "User clicks on the Ambiguity Keywords")
    public void shouldClickOnAnyKeywords() throws IOException {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SearchPageTextFieldSection searchPageTextFieldSection=searchHomePage.getSearchPageTextFieldSection();
        searchPageTextFieldSection.enterAmbiguityKeyWord();
        driver.findElement(By.cssSelector("[class*='ta3']")).click();
    }

    @Test(description = "User enters  the DisAmbiguity Keywords")
    public void shouldClickOnDisAmbiguityKeywords() throws IOException {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SearchPageTextFieldSection searchPageTextFieldSection=searchHomePage.getSearchPageTextFieldSection();
        searchPageTextFieldSection.enterDisAmbiguityKeyWord();
    }

    @Test(description = "User searches for random ambiguity keywords and then user clicks on all the Auto-populated words that are displayed in the search page")
    public void shouldSelectRandomKeywords()
    {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SearchPageTextFieldSection searchPageTextFieldSection=searchHomePage.getSearchPageTextFieldSection();
        searchPageTextFieldSection.enterRandomKeywords();

    }

    @Test(description = "User enters the Random Disambiguited Words and finds response code of all links displayed in the search resultant page")
    public void shouldSelectRandomDisambiguitedWords() throws IOException {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SearchPageTextFieldSection searchPageTextFieldSection=searchHomePage.getSearchPageTextFieldSection();
        searchPageTextFieldSection.enterRandomDisambiguityKeywords();

    }

    @Test(description = "User clicks on the Learn More Link")
    public void shouldClickOnLearnMoreLink() throws IOException {
        driver.get("https://www.embibe.com");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SearchPageTextFieldSection searchPageTextFieldSection=searchHomePage.getSearchPageTextFieldSection();
        searchPageTextFieldSection.clickOnLearnMoreLinkDisplayedInStudyResultantPage();

    }


}
