package reporting;

import lombok.Getter;

import java.util.List;

@Getter
public class TestResultsFailedGroupRow {
    private List<String> testCases;
    private String errorMessage;
    private int count;
    private double failPercentage;

    public TestResultsFailedGroupRow(List<String> testCases, String errorMessage, int count, double failPercentage) {
        this.testCases = testCases;
        this.errorMessage = errorMessage;
        this.count = count;
        this.failPercentage = failPercentage;
    }

}
