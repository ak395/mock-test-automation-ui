package reporting;

import lombok.Getter;

import java.util.List;

@Getter
public class TestResultsData {
    private String testFullName;
    private String testName;
    private List<String> testGroups;
    private String testStatus;
    private String errorMessage;
    private String description;


    public TestResultsData(String testFullName, String testName, List<String> testGroups, String testStatus, String errorMessage, String description) {
        this.testFullName = testFullName;
        this.testName = testName;
        this.testGroups = testGroups;
        this.testStatus = testStatus;
        this.errorMessage = errorMessage;
        this.description = description;
    }
}
