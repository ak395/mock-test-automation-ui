package reporting;

import org.apache.commons.lang3.StringUtils;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.xml.XmlSuite;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ch.lambdaj.Lambda.*;
import static org.hamcrest.Matchers.equalTo;

public class GroupwiseReporter implements IReporter {

    private PrintWriter mOut;
    private HtmlHelper htmlHelper;


    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outdir) {

        htmlHelper = new HtmlHelper();

        createWriter(outdir);

        htmlHelper.startHtml(mOut);

        TestResultsDataContainer testResultsDataContainer = new TestResultsDataContainer(suites.get(0));

        generateAggregatorReport(testResultsDataContainer);

        generateFeatureWiseSummaryReport(testResultsDataContainer);

        generateFailureWiseSummaryReport(testResultsDataContainer);

        generateTestcaseWiseReport(testResultsDataContainer);

        htmlHelper.endHtml(mOut);

        mOut.flush();
        mOut.close();

    }

    public void generateAggregatorReport(TestResultsDataContainer container) {

        mOut.println("<br><b>Aggregated Results</b>");

        htmlHelper.tableStart("param", mOut, Arrays.asList("Total", "Passed", "Failed", "Skipped", "Pass%"));

        List<TestResultsData> resultsData = container.getTestResultsData();
        int total = resultsData.size();
        int passed = select(resultsData, having(on(TestResultsData.class).getTestStatus(), equalTo("PASSED"))).size();
        int failed = select(resultsData, having(on(TestResultsData.class).getTestStatus(), equalTo("FAILED"))).size();
        int skipped = select(resultsData, having(on(TestResultsData.class).getTestStatus(), equalTo("SKIPPED"))).size();
        double passPercentage = (100 * passed) / total;

        mOut.println("<tr>");
        mOut.println(String.format("<td>%s</td>", total));

        mOut.println(String.format("<td>%s</td>", passed));
        mOut.println(String.format("<td>%s</td>", failed));
        mOut.println(String.format("<td>%s</td>", skipped));
        mOut.println(String.format("<td>%s</td>", passPercentage));
        mOut.println("</tr>");

        htmlHelper.tableEnd(mOut);
    }


    public void generateFeatureWiseSummaryReport(TestResultsDataContainer container) {

        mOut.println("<b>FeatureWise Results</b>");
        htmlHelper.tableStart("param", mOut, Arrays.asList("Features", "Total", "Passed", "Failed", "Skipped", "Pass%"));

        for (int i = 0; i < container.getTestResultsGroupSummaryRows().size(); i++) {
            TestResultsSummaryRow row = container.getTestResultsGroupSummaryRows().get(i);

            mOut.println("<tr>");
            mOut.println(String.format("<td>%s</td>", row.getGroupName()));

            mOut.println(String.format("<td>%s</td>", row.getNumberTotal()));
            mOut.println(String.format("<td>%s</td>", row.getNumberPassed()));
            mOut.println(String.format("<td>%s</td>", row.getNumberFailed()));
            mOut.println(String.format("<td>%s</td>", row.getNumberSkipped()));
            mOut.println(String.format("<td>%s</td>", (100 * (row.getNumberPassed())) / row.getNumberTotal()));
            mOut.println("</tr>");
        }

        htmlHelper.tableEnd(mOut);
    }

    public void generateFailureWiseSummaryReport(TestResultsDataContainer container) {

        htmlHelper.tableStart("param", mOut, Arrays.asList("Tests", "ErrorMessage", "Count", "Fail%"));

        mOut.println("<b>FailureWise Results</b>");

        for (int i = 0; i < container.getTestResultsFailedGroupRows().size(); i++) {
            TestResultsFailedGroupRow row = container.getTestResultsFailedGroupRows().get(i);

            mOut.println("<tr><td>");
            for (String tc : row.getTestCases()) {
                mOut.println(String.format("%s<br>", tc));
            }
            mOut.println(String.format("<td>%s</td>", row.getErrorMessage()));
            mOut.println(String.format("</td><td>%s</td>", row.getCount()));
            mOut.println(String.format("<td>%s</td>", row.getFailPercentage()));
            mOut.println("</tr>");
        }

        htmlHelper.tableEnd(mOut);
    }

    public void generateTestcaseWiseReport(TestResultsDataContainer container) {

        mOut.println("<br><b>TestcaseWise Results</b>");

        htmlHelper.tableStart("param", mOut, Arrays.asList("Testcase Name", "Groups", "Status", "Description", "Coverage", "Bugs"));

        List<TestResultsData> resultsData = container.getTestResultsData();
        List<String> descriptionList = new ArrayList<>();

        for (TestResultsData data : resultsData) {
            descriptionList = Arrays.asList(data.getDescription().split(";"));

            mOut.println("<tr>");
            mOut.println(String.format("<td>%s</td>", data.getTestFullName()));

            mOut.println(String.format("<td>%s</td>", StringUtils.join(data.getTestGroups(), ',')));
            mOut.println(String.format("<td>%s</td>", data.getTestStatus()));
            mOut.println(String.format("<td>%s</td>", descriptionList.get(0)));
            try {
                mOut.println(String.format("<td>%s</td>", descriptionList.get(1)));
            }catch (IndexOutOfBoundsException ignore){
                mOut.println(String.format("<td>%s</td>", ""));
            }
            try {
                List<String> tickets = Arrays.asList(descriptionList.get(2).split(","));
                mOut.println("<td>");
                tickets.forEach(x -> mOut.println("<a target=\"_blank\" href=https://embibe-delivery.atlassian.net/browse/" + x + ">" + x + "</a>"));
            }catch (IndexOutOfBoundsException ignore){
                mOut.println(String.format("<td>%s</td>", ""));
            }
            mOut.println("</td>");
            mOut.println("</tr>");
        }


        htmlHelper.tableEnd(mOut);
    }


    private void createWriter(String outdir) {
        try {
            new File(outdir).mkdirs();
            mOut = new PrintWriter(new BufferedWriter(new FileWriter(new File(outdir, "groupwise-report.html"))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}