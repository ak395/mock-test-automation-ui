package reporting;

import ch.lambdaj.Lambda;
import ch.lambdaj.group.Group;
import org.testng.*;


import java.util.*;

import static ch.lambdaj.Lambda.*;
import static org.hamcrest.Matchers.*;

public class TestResultsDataContainer {

    ISuite suite;
    List<TestResultsData> testResultsData;
    List<TestResultsSummaryRow> testResultsGroupSummaryRows;
    List<TestResultsFailedGroupRow> testResultsFailedGroupRows;

    public List<TestResultsData> getTestResultsData() {
        return testResultsData;
    }

    public List<TestResultsSummaryRow> getTestResultsGroupSummaryRows() {
        return testResultsGroupSummaryRows;
    }

    public List<TestResultsFailedGroupRow> getTestResultsFailedGroupRows() {
        return testResultsFailedGroupRows;
    }

    public TestResultsDataContainer(ISuite suite) {
        this.suite = suite;
        testResultsData = new ArrayList<TestResultsData>();
        extractData();
        testResultsGroupSummaryRows = extractGroupWiseSummary();
        testResultsFailedGroupRows = extractFailedTestsAsGroups();
    }

    public List<String> getGroupNames(ISuite suite) {

        List<String> groupNames = new ArrayList<String>();

        for (Map.Entry<String, Collection<ITestNGMethod>> entry : suite.getMethodsByGroups().entrySet()) {
            groupNames.add(entry.getKey());
        }

      // groupNames.removeAll(Arrays.asList(Categories.PRP_PRACTICE_SMOKE));

        return groupNames;
    }


    public List<TestResultsFailedGroupRow> extractFailedTestsAsGroups() {

        // Filter out passed tests
        List<TestResultsData> tests = select(testResultsData, having(on(TestResultsData.class).getTestStatus(), not("PASSED")));

        // Group by error/exception message
        Group<TestResultsData> groupedTests = Lambda.group(tests, Lambda.by(on(TestResultsData.class).getErrorMessage()));

        // Put it to data structure

        List<TestResultsFailedGroupRow> rows = new ArrayList<TestResultsFailedGroupRow>();

        for (Group<TestResultsData> subgroup : groupedTests.subgroups()) {

            List<TestResultsData> all = subgroup.findAll();

            List<String> testnames = new ArrayList<String>();
            for (TestResultsData one : all) {
                testnames.add(one.getTestFullName() + "." + one.getTestName());
            }

            rows.add(new TestResultsFailedGroupRow(testnames, all.get(0).getErrorMessage(), all.size(), (100 * all.size()) / tests.size()));
        }

        Collections.sort(rows, new Comparator<TestResultsFailedGroupRow>() {
            @Override
            public int compare(TestResultsFailedGroupRow o1, TestResultsFailedGroupRow o2) {
                return Double.compare(o1.getFailPercentage(), o2.getFailPercentage());
            }
        });
        Collections.reverse(rows);
        return rows;

    }

    public List<TestResultsSummaryRow> extractGroupWiseSummary() {

        List<TestResultsSummaryRow> rows = new ArrayList<TestResultsSummaryRow>();

        for (String group : getGroupNames(suite)) {
            List<TestResultsData> testResultsForGroup = getTestResultsForGroup(group);
            int total = testResultsForGroup.size();
            int passed = select(testResultsForGroup, having(on(TestResultsData.class).getTestStatus(), equalTo("PASSED"))).size();
            int failed = select(testResultsForGroup, having(on(TestResultsData.class).getTestStatus(), equalTo("FAILED"))).size();
            int skipped = select(testResultsForGroup, having(on(TestResultsData.class).getTestStatus(), equalTo("SKIPPED"))).size();
            double passPercentage = (100 * passed) / total;

            rows.add(new TestResultsSummaryRow(group, total, passed, failed, skipped, passPercentage));
        }

        Collections.sort(rows, new Comparator<TestResultsSummaryRow>() {
            @Override
            public int compare(TestResultsSummaryRow o1, TestResultsSummaryRow o2) {
                return Double.compare(o1.getPassPercentage(), o2.getPassPercentage());
            }
        });

        return rows;

    }

    public List<TestResultsData> getTestResultsForGroup(String groupName) {
        return select(testResultsData, having(on(TestResultsData.class).getTestGroups(), hasItem(groupName)));
    }


    private void extractData() {

        Map<String, ISuiteResult> results = suite.getResults();

        for (ISuiteResult r : results.values()) {

            ITestContext testContext = r.getTestContext();

            addTestResultsData(testContext.getFailedTests());
            addTestResultsData(testContext.getPassedTests());
            addTestResultsData(testContext.getSkippedTests());

            System.out.println();

        }
    }

    private void addTestResultsData(IResultMap tests) {

        for (ITestNGMethod method : tests.getAllMethods()) {

            String message = "";
            String status = "PASSED";

            Set<ITestResult> testResults = tests.getResults(method);

            for (ITestResult iTestResult : testResults) {
                if (iTestResult.getStatus() == 2) {
                    message = iTestResult.getThrowable().getMessage();
                    status = "FAILED";
                }
                if (iTestResult.getStatus() == 3) {
                    status = "SKIPPED";
                }
            }


            TestResultsData failed = new TestResultsData(
                    method.getTestClass().getName(),
                    method.getMethodName(),
                    Arrays.asList(method.getGroups()),
                    status,
                    message,
                    method.getDescription());


            testResultsData.add(failed);

        }
    }

}
