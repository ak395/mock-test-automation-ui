package reporting;

import lombok.Getter;

@Getter
public class TestResultsSummaryRow {
    private String groupName;
    private int numberTotal;
    private int numberPassed;
    private int numberFailed;
    private int numberSkipped;
    private double passPercentage;

    public TestResultsSummaryRow(String groupName, int numberTotal, int numberPassed, int numberFailed, int numberSkipped, double passPercentage) {
        this.groupName = groupName;
        this.numberTotal = numberTotal;
        this.numberPassed = numberPassed;
        this.numberFailed = numberFailed;
        this.numberSkipped = numberSkipped;
        this.passPercentage = passPercentage;
    }
}
