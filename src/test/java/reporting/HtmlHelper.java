package reporting;

import java.io.PrintWriter;
import java.util.List;

public class HtmlHelper {

    protected void startHtml(PrintWriter out) {
        out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
        out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        out.println("<head>");
        out.println("<title>TestNG:  Unit Test</title>");
        out.println("<style type=\"text/css\">");
        out.println("table caption,table.info_table,table.param,table.passed,table.failed {margin-bottom:10px;border:1px solid #000099;border-collapse:collapse;empty-cells:show;}");
        out.println("table.info_table td,table.info_table th,table.param td,table.param th,table.passed td,table.passed th,table.failed td,table.failed th {");
        out.println("border:1px solid #000099;padding:.25em .5em .25em .5em");
        out.println("}");
        out.println("table.param th {vertical-align:bottom}");
        out.println("td.numi,th.numi,td.numi_attn {");
        out.println("text-align:right");
        out.println("}");
        out.println("tr.total td {font-weight:bold}");
        out.println("table caption {");
        out.println("text-align:center;font-weight:bold;");
        out.println("}");
        out.println("table.passed tr.stripe td,table tr.passedodd td {background-color: #00AA00;}");
        out.println("table.passed td,table tr.passedeven td {background-color: #33FF33;}");
        out.println("table.passed tr.stripe td,table tr.skippedodd td {background-color: #cccccc;}");
        out.println("table.passed td,table tr.skippedodd td {background-color: #dddddd;}");
        out.println("table.failed tr.stripe td,table tr.failedodd td,table.param td.numi_attn {background-color: #FF3333;}");
        out.println("table.failed td,table tr.failedeven td,table.param tr.stripe td.numi_attn {background-color: #DD0000;}");
        out.println("tr.stripe td,tr.stripe th {background-color: #E6EBF9;}");
        out.println("p.totop {font-size:85%;text-align:center;border-bottom:2px black solid}");
        out.println("div.shootout {padding:2em;border:3px #4854A8 solid}");
        out.println("</style>");
        out.println("</head>");
        out.println("<body>");
    }

    protected void endHtml(PrintWriter out) {
        out.println("</body></html>");
    }

    public void tableStart(String cssclass, PrintWriter out, List<String> headers) {
        out.println("<table cellspacing=0 cellpadding=0"
                + (cssclass != null ? " class=\"" + cssclass + "\""
                : " style=\"padding-bottom:2em\"") + ">");

        out.print("<tr>");

        for (String header : headers) {
            out.print(String.format("<th class=\"numi\">" + "%s" + "</th>", header));
        }
        out.println("</tr>");
    }

    public void tableEnd(PrintWriter out) {
        out.println("</table>");
        out.println("<br>");
        out.println("<br>");

    }

}
