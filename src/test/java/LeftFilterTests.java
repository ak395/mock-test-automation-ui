import builders.AMockOrPracticeTestBuilder;
import constants.*;
import entities.AMockOrPracticeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.sections.leftFilter.LeftFilterSection;
import testBase.TestBase;
import verification.TestFailures;
import verification.TestListener;

@Listeners({TestListener.class})
public class LeftFilterTests extends TestBase {

    @Test (description = "User filters a mock test from left pane", groups = {"smoke"})
    public void shouldFilterFromLeftPaneTests() {
        driver.get("https://www.embibe.com/engineering/test");

        AMockOrPracticeTest test = new AMockOrPracticeTestBuilder().withGoal(Goals.GOAL_ENGINEERING).build();

        mockTestHomePage.selectTest(test);

        mockTestHomePage.getLeftFilterSection().assertTestHasBeenSelected(test);

        TestFailures.reportIfAny();

    }

    @Test (description = "User clears filters from left pane", groups = {"smoke"})
    public void shouldResetFiltersInLeftPaneTests() {
        driver.get("https://www.embibe.com/engineering/test/jee-main/chapterwise-test/physics/rotation");

        LeftFilterSection leftFilterSection = mockTestHomePage.getLeftFilterSection();

        leftFilterSection.resetFilters();
        leftFilterSection.assertUrlHasBeenResetTo("https://www.embibe.com/engineering");
        leftFilterSection.assertThatFiltersHaveBeenReset();


        TestFailures.reportIfAny();

    }

    @Test (description = "User Click on Start Solving Button from left pane", groups = {"smoke"})
    public void shouldStartSolvingInLeftPaneTests(){
        shouldFilterFromLeftPaneTests();
        mockTestHomePage.getLeftFilterSection().clickOnStartSolvingButton();
        TestFailures.reportIfAny();
    }


    @Test (description = "User filters a practice test from left pane", groups = {"smoke"})
    public void shouldFilterFromLeftPaneTestsOnPracticePage(){

        driver.get("https://www.embibe.com/engineering/practice/solve");

        AMockOrPracticeTest test = new AMockOrPracticeTestBuilder().withGoal(Goals.GOAL_ENGINEERING).withDropDown1(Exams.EXAM_JEE_MAIN).withDropDown2(Subjects.SUBJECT_CHEMISTRY).withDropDown3(Units.UNIT_PHYSICAL_CHEMISTRY).withDropDown4(Chapters.CHAPTER_CHEMICAL_EQUILIBRIUM).build();

        practiceHomePage.selectTest(test);

        TestFailures.reportIfAny();


    }

}
