
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.rankUpLoginPage.LoginFormSection;
import page.rankUpPage.rankUpHeader.RankUpHeaderSection;
import page.rankUpPage.rankUpTabs.RankUpTabSection;
import page.rankUpPage.rankUpVideoSection.RankUpVideoSection;
import testBase.TestBase;
import utils.Properties;
import verification.TestFailures;
import verification.TestListener;

@Listeners({TestListener.class})
public class RankUpTests extends TestBase {

    @Test(description = "User should Able to Apply to Rank Up", groups = {"smoke"})
    public void shouldAbleToApplyOnRankUp() {
        driver.get(Properties.rankUpUrl);

        RankUpHeaderSection rankUpHeaderSection = rankUpHomePage.getRankUpHeaderSection();
        rankUpHeaderSection.applyToRankUp();
        rankUpHeaderSection.assertUrlHasSetTo(Properties.rankUpSignUpUrl);

        TestFailures.reportIfAny();

    }

    @Test(description = "should display error message when tries to login without registration", groups = {"smoke"})
    public void shouldDisplayValidationMessageLoginWithoutRegistering() {
        driver.get(Properties.rankUpUrl);
        RankUpHeaderSection rankUpHeaderSection = rankUpHomePage.getRankUpHeaderSection();
        rankUpHeaderSection.clickOnLogin();
        LoginFormSection loginFormSection = rankUpLoginHomePage.getLoginFormSection();
        loginFormSection.enterUserName(Properties.invalidUserName);
        loginFormSection.enterPassword(Properties.invalidPassword);
        loginFormSection.clickOnLogin();
        loginFormSection.displayInvalidLoginMessage();
        TestFailures.reportIfAny();
    }

    @Test(description = "should able to login with valid credentials")
    public void shouldAbleToLogin() {
        driver.get(Properties.rankUpUrl);
        RankUpHeaderSection rankUpHeaderSection = rankUpHomePage.getRankUpHeaderSection();
        rankUpHeaderSection.clickOnLogin();
        LoginFormSection loginFormSection = rankUpLoginHomePage.getLoginFormSection();
        loginFormSection.loginAsRankUpUser(Properties.validUserName,Properties.validPassword);
        TestFailures.reportIfAny();

    }

    @Test (description = "Page is displaying properly on each tab click")
    public void checkAllTabsOnRankUp() {

        driver.get(Properties.rankUpUrl);
        RankUpTabSection rankUpTabSection =rankUpHomePage.getRankUpTabSection();
        rankUpTabSection.clickOnTab1();
        rankUpTabSection.clickOnTab2();
        rankUpTabSection.clickOnTab3();
        rankUpTabSection.clickOnTab4();
        TestFailures.reportIfAny();
    }

    @Test (description = "Should land on Jump Page When clicks on get jump button")
    public void navigateToJumpPage() {

        driver.get(Properties.rankUpUrl);
        RankUpVideoSection rankUpVideoSection = rankUpHomePage.getRankUpVideoSection();
        rankUpVideoSection.navigateToJumPage();
    }
}
