import org.testng.annotations.Test;
import page.Ask.AskPAgeSearchContainerSection;
import page.Ask.AsksTrendingConceptsPaneSection;
import page.Ask.SidebarSocialLinksSection;
import page.Ask.TakeATourWrapperSection;
import testBase.TestBase;

public class AskTests extends TestBase
{
    @Test(description = "User verifies the title of the Ask Page")
    public void shouldVerifyTitleOfAskPage()
    {
        driver.get("https://www.embibe.com/ask");
        driver.manage().window().maximize();
        AskPAgeSearchContainerSection askPAgeTitleSection=new AskPAgeSearchContainerSection(driver);
        askPAgeTitleSection.assertHeaderToBe("Ask a Question");

    }

    @Test(description = "User clicks on the Search Field")
    public void shouldClickOnSearchField()
    {
        driver.get("https://www.embibe.com/ask");
        driver.manage().window().maximize();
        AskPAgeSearchContainerSection askPAgeSearchContainerSection=new AskPAgeSearchContainerSection(driver);
        askPAgeSearchContainerSection.clickOnSearchField();
        askPAgeSearchContainerSection.searchFieldDisplayed();
    }

    @Test(description = "User clicks on the Explore Study Button and verifies the redirected URL")
    public void shouldClickOnExploreStudyButton()
    {
        driver.get("https://www.embibe.com/ask");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        TakeATourWrapperSection takeATourWrapperSection=new TakeATourWrapperSection(driver);
        takeATourWrapperSection.clickOnExploreStudyButton();
        takeATourWrapperSection.assertUrlHasBeenResetTo("https://www.embibe.com/landing");
    }

    @Test(description = "User clicks on the Search Button")
    public void shouldClickOnSearchButton()
    {
        driver.get("https://www.embibe.com/ask");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        AskPAgeSearchContainerSection askPAgeSearchContainerSection=new AskPAgeSearchContainerSection(driver);
        askPAgeSearchContainerSection.clickOnSearchButton();
        askPAgeSearchContainerSection.assertUrlHasBeenResetTo("https://www.embibe.com/ask/question/add");

    }

    @Test(description = "User clicks on the Facebook Link and verifies the URL")
    public void shouldClickOnFacebookLink()
    {
        driver.get("https://www.embibe.com/ask");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SidebarSocialLinksSection sidebarSocialLinks=new SidebarSocialLinksSection(driver);
        sidebarSocialLinks.clickOnFacebookIcon();
        for(String winHandle : driver.getWindowHandles())
        {
            driver.switchTo().window(winHandle);
        }
        sidebarSocialLinks.assertUrlHasBeenResetTo("https://www.facebook.com/embibe.me/");
    }

    @Test(description = "User clicks on the Twitter Link and verifies the URL")
    public void shouldClickOnTwitterLink()

    {
        driver.get("https://www.embibe.com/ask");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SidebarSocialLinksSection sidebarSocialLinks=new SidebarSocialLinksSection(driver);
        sidebarSocialLinks.clickOnTwitterLink();
        for (String winHandle: driver.getWindowHandles())
        {
            driver.switchTo().window(winHandle);
        }
        sidebarSocialLinks.assertUrlHasBeenResetTo("https://twitter.com/embibe");
    }

    @Test(description = "User clicks on the Social Medium Link and verifies the URL")
    public void shouldClickOnSocialMediumLink()
    {
        driver.get("https://www.embibe.com/ask");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        SidebarSocialLinksSection sidebarSocialLinks=new SidebarSocialLinksSection(driver);
        sidebarSocialLinks.clickOnSocialMediumLink();
        for(String winHandle : driver.getWindowHandles())
        {
            driver.switchTo().window(winHandle);
        }
        sidebarSocialLinks.assertUrlHasBeenResetTo("https://medium.com/tag/embibe/");
    }

    @Test(description = "User clicks on all the links displayed in the  Trendinng concepts pane")
    public void shouldClickOnAllLinksOfConceptsPane()
    {
        AsksTrendingConceptsPaneSection asksTrendingConceptsPaneSection=new AsksTrendingConceptsPaneSection(driver);
        asksTrendingConceptsPaneSection.findAllLinks();
    }
}
