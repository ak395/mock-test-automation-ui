import org.testng.annotations.Test;
import page.institute.*;
import testBase.TestBase;

import java.io.IOException;

public class InstituteTests extends TestBase
{
    @Test(description = "User Fetches all links and clicks on all Links")
    public void shouldFetchAllLinks() throws IOException {
        driver.get("https://www.embibe.com/institute");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        InstituteLinks instituteLinks=new InstituteLinks(driver);
        instituteLinks.fetchAllLinks();
    }

    @Test(description = "User clicks on the ContactUS Button")
    public void shouldClickOnContactUSButton()
    {
        driver.get("https://www.embibe.com/institute");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        InstituteHeaderSection instituteHeaderSection=new InstituteHeaderSection(driver);
        instituteHeaderSection.clickOnContactUsButton();
        instituteHeaderSection.assertHeaderToBe("Contact Us");
    }
@Test(description = "User clicks on the LOGIN Button")
    public void shouldClickOnLoginButton()
{
    driver.get("https://www.embibe.com/institute");
    driver.manage().window().maximize();
    driver.navigate().refresh();
    InstituteHeaderSection instituteHeaderSection=new InstituteHeaderSection(driver);
    instituteHeaderSection.clickOnLoginButton();
    instituteHeaderSection.assertUrlHasBeenResetTo("https://grow.embibe.com/institute/login");

}

@Test(description = " User clicks on the SignUp For Free Button")
    public void shouldClickOnSignUpForFreeButton()
{
    driver.get("https://www.embibe.com/institute");
    driver.manage().window().maximize();
    driver.navigate().refresh();
    InstituteBannerSection instituteBannerSection=new InstituteBannerSection(driver);
    instituteBannerSection.clickOnSingUpForFreeButton();
    InstituteNavContainerSection instituteNavContainerSection=new InstituteNavContainerSection(driver);
    instituteNavContainerSection.assertHeaderToBe("INSTITUTES");
}

@Test(description = "User clicks on the Request A Demo Button")
    public void shouldClickOnRequestADemoButton()
{
    driver.get("https://www.embibe.com/institute");
    driver.manage().window().maximize();
    driver.navigate().refresh();
    InstituteBannerSection instituteBannerSection=new InstituteBannerSection(driver);
    instituteBannerSection.clickOnRequestADemoButton();
    RequestADemoPopUp requestADemoPopUp=new RequestADemoPopUp(driver);
    requestADemoPopUp.assertHeaderToBe("Request a demo");

}
}
