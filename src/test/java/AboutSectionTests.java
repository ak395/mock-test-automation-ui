import org.testng.annotations.Test;
import page.landingPage.leftContainer.AboutSection;
import testBase.TestBase;
import utils.Properties;

public class AboutSectionTests extends TestBase {

    @Test
    public void shouldLandOnStudyPage() {
        driver.get("https://www.embibe.com/landing");
        AboutSection aboutSection = landingHomePage.getAboutSection();
        aboutSection.clickStartNowButton();
        aboutSection.assertUrlHasSetTo(Properties.baseUrl);

    }
}
