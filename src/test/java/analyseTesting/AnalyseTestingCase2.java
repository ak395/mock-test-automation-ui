package analyseTesting;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;

import static utils.AttemptType.TOO_FAST_CORRECT;
import static utils.AttemptType.WASTED_ATTEMPT;

public class AnalyseTestingCase2 extends TestBase {

    private PracticePlanner planner;
    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 25, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(26, 26, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(27, 30, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(31, 31, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(32, 35, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(36, 36, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(37, 40, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(41, 41, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(42, 45, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(46, 46, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(47, 50, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(51, 52, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(53, 55, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(56, 57, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(36, 38, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(39, 40, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(41, 43, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(44, 45, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(46, 47, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(48, 50, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(51, 52, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(53, 55, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(56, 57, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(58, 60, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(61, 62, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(63, 65, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(66, 67, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(68, 70, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(71, 72, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(73, 75, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(76, 78, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(79, 80, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(81, 83, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(84, 85, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(86, 88, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(89, 90, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(91, 93, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(94, 95, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(96, 97, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(98, 103, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(104, 104, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(105,105,WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(106, 109, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(110, 110, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(111, 114, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(114, 114, TOO_FAST_CORRECT));

        planner.addPracticeData(new PracticeData(115, 118, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(119, 119, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(120, 120, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(121, 123, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(124, 150, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(151, 151, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(152, 155, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(156, 156, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(157, 160, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(161, 161, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(162, 165, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(166, 166, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(167, 170, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(171, 171, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(172, 175, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(1, 3, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(1, 2, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(1, 2, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(1, 3, WASTED_ATTEMPT));
    }
    @Test
    public void verifyFlukeBehaviourMeterIsDisplayed() throws Exception {
        navigateTo("https://www.embibe.com/questions/jee-main/mathematics/coordinate-geometry/circle-questions");
//        driver.findElement(By.cssSelector(".btn-container.round.login-modal-loginBtn ")).click();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("academic_testing_eng6@embibe.com","embibe1234");
        Thread.sleep(5000);
//        searchHomePage.clickOnStartPracticeButton(planner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswers(planner);
//        driver.findElement(By.cssSelector(".form-control.emailField")).sendKeys("academic_testing_eng6@embibe.com");
//        driver.findElement(By.cssSelector(".form-control.passwordField")).sendKeys("embibe1234");
//        driver.findElement(By.cssSelector(".login-logout-button")).click();
//        practiceQuestionHomePage.selectAnswers(planner);
        planner.getQuestionDetails();
    }
}
