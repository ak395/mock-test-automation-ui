package analyseTesting;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;

import static utils.AttemptType.TOO_FAST_CORRECT;
import static utils.AttemptType.WASTED_ATTEMPT;

public class AnalyseTestingCase3 extends TestBase {
    private PracticePlanner planner;
    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        planner = new PracticePlanner();
//        planner.addPracticeData(new PracticeData(50, 84, TOO_FAST_CORRECT));
//
//        planner.addPracticeData(new PracticeData(85, 86, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(87, 87, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(88, 88, WASTED_ATTEMPT));
//        planner.addPracticeData(new PracticeData(89, 89, TOO_FAST_CORRECT));
//        planner.addPracticeData(new PracticeData(90, 93, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(146, 169, TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(170, 170, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(171,171,TOO_FAST_CORRECT));
        planner.addPracticeData(new PracticeData(172, 172, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(173, 175, TOO_FAST_CORRECT));

    }
    @Test
    public void verifyFlukeBehaviourMeterIsDisplayed() throws Exception {
        navigateTo("https://www.embibe.com/questions/jee-main/chemistry/physical-chemistry/atomic-structure-questions");
//        driver.findElement(By.cssSelector(".btn-container.round.login-modal-loginBtn ")).click();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("academic_testing_eng6@embibe.com","embibe1234");
        Thread.sleep(5000);
//        searchHomePage.clickOnStartPracticeButton(planner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswers(planner);
//        driver.findElement(By.cssSelector(".form-control.emailField")).sendKeys("academic_testing_eng6@embibe.com");
//        driver.findElement(By.cssSelector(".form-control.passwordField")).sendKeys("embibe1234");
//        driver.findElement(By.cssSelector(".login-logout-button")).click();
//        practiceQuestionHomePage.selectAnswers(planner);
        planner.getQuestionDetails();
    }
}
