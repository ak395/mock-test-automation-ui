package analyseTesting;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Properties;

import static utils.AttemptType.TOO_FAST_CORRECT;

public class analyseTestingForPractice extends TestBase {

    private PracticePlanner practicePlanner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData() throws Exception {
        practicePlanner = new PracticePlanner();
        practicePlanner.addPracticeData(new PracticeData(1, 5, TOO_FAST_CORRECT, false));
        practicePlanner.addPracticeData(new PracticeData(6, 10, TOO_FAST_CORRECT, false));
        practicePlanner.addPracticeData(new PracticeData(11, 15, TOO_FAST_CORRECT, false));
        practicePlanner.addPracticeData(new PracticeData(16, 20, TOO_FAST_CORRECT, false));
        practicePlanner.addPracticeData(new PracticeData(21, 25, TOO_FAST_CORRECT, false));
        practicePlanner.addPracticeData(new PracticeData(25, 30, TOO_FAST_CORRECT, false));

    }
    @Test
    public void analyseTestForPractice() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("analysetesting4@embibe.com","embibe1234");
        Thread.sleep(2000);
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswers(practicePlanner);
    }
}
