package analyseTesting;

import constants.Exams;
import constants.TestTypes;
import entities.MockTestData;
import entities.MockTestPlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static constants.Badge.BADGE_TOO_FAST_CORRECT;
import static constants.Badge.BADGE_WASTED_ATTEMPT;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;

public class AnalyseTesting  extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 5, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(6, 10, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(11, 15, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(16, 20, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(21, 25, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(26, 30, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
    }

    @Test(groups = {Categories.TRP_ANALYSE_TESTING},
            description = "Verify the Time Zone Issue for Analyse" )
    public void analyseTestForTest() throws InterruptedException {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("dsl_testing33demo@embibe.com","embibe1234");
        Thread.sleep(2000);
        searchHomePage.clickOnTakeATestButton();
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_NEET);
        mockTestHomePage.selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        mockTestHomePage.startTestWithDifferentTab();
        instructionPage.dismissInstructions();
        questionsHomePage.selectAnswers(mockTestPlanner);
        questionsHomePage.finishTheTest();
        questionsHomePage.submitTheTest();
    }

}
