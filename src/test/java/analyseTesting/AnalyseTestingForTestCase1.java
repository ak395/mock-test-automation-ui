package analyseTesting;

import entities.MockTestData;
import entities.MockTestPlanner;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;

import static constants.Badge.*;
import static mockTestFactory.MyChoice.CHOICE_CORRECT;
import static mockTestFactory.MyChoice.CHOICE_INCORRECT;
import static mockTestFactory.MyChoice.CHOICE_UNATTEMPTED;

public class AnalyseTestingForTestCase1 extends TestBase {

    private MockTestPlanner mockTestPlanner;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForTest() {
        mockTestPlanner = new MockTestPlanner();
        mockTestPlanner.addMockTestData(new MockTestData(1, 1, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(2, 2, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(3, 3, CHOICE_INCORRECT, BADGE_OVERTIME_INCORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(4, 4, CHOICE_CORRECT, BADGE_OVERTIME_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(5, 5, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(6, 6, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(7, 7, CHOICE_UNATTEMPTED, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(8, 11, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(12, 13, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(14, 14, CHOICE_INCORRECT, BADGE_WASTED_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(15, 15, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(16, 16, CHOICE_UNATTEMPTED, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(17, 21, CHOICE_CORRECT, BADGE_OVERTIME_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(22, 22, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(23, 25, CHOICE_INCORRECT, BADGE_OVERTIME_INCORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(26, 27, CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT));
        mockTestPlanner.addMockTestData(new MockTestData(28, 28, CHOICE_INCORRECT, BADGE_INCORRECT_ATTEMPT));
        mockTestPlanner.addMockTestData(new MockTestData(29, 30, CHOICE_CORRECT, BADGE_PERFECT_ATTEMPT));

    }

    @Test
    public void verifyHundredAccInTestWithNewUser() throws Exception{
        navigateTo("https://www.embibe.com/");
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("academic_testing_eng7@embibe.com", "embibe1234");
        Thread.sleep(3000);
        navigateTo("https://www.embibe.com/mock-test/jee-main/physics/electrostatics-chapter-test/electric-potential-and-capacitance-04-1/session");
        driver.findElement(By.cssSelector(".checkmark  ")).click();
        driver.findElement(By.cssSelector(".btn-container.round.instruction-next-button.common-button ")).click();
//        mockTestPlanner.addMockTestData(new MockTestData(1, mockTestClient.getQuestionCount(getmockTestResponse()), CHOICE_CORRECT, BADGE_TOO_FAST_CORRECT, getmockTestResponse()));
        questionsHomePage.selectAnswers(mockTestPlanner);
    }
}
