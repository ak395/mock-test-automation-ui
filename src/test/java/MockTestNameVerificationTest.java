import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.testDataService.TestDataService;
import testBase.TestBase;
import utils.App;
import utils.Categories;

public class MockTestNameVerificationTest extends TestBase {

    private TestDataService data;

    @BeforeTest(alwaysRun = true)
    private void prepareTestDataForMockTest() {
        data = new TestDataService(App.WEB);
    }

    @Test(groups = {Categories.MOCKTEST})
    public void verifyTestName() {
        navigateTo(data.getMockTestTab().getExam());
        mockTestHomePage.verifyTheNameOfTheExam(data);
    }

}
