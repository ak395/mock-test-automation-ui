import org.testng.annotations.Test;
import testBase.TestBase;

public class SampleTest3 extends TestBase {

    @Test
    public void someTest() {
        driver.get("https://www.embibe.com/engineering/test");
        mockTestHomePage.getPhoenixCandyContainerSection().assertHeaderToBe("Take a mock test, review performance of past tests");
    }

}
