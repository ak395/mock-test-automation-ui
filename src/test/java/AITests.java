import org.testng.annotations.Test;
import page.AI.*;
import testBase.TestBase;
import utils.Properties;

public class AITests extends TestBase
{
    //Tc 1 = pass
    @Test(groups = {"test1"},description = "User clicks on the Research Button and verifies the header of the respective section along with the title of the content section")
    public void shouldClickOnResearchButton()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AIBannerContentSection aiBannerContentSection=new AIBannerContentSection(driver);
    AITabSections aiTabSections=new AITabSections(driver);
    aiBannerContentSection.clickOnResearchButton();
    aiTabSections.assertHeaderToBe(Properties.aiTabHeader);
    aiTabSections.assertMentorIntelligenceTabContentHeaderToBe(Properties.mentorIntelligenceTabHeader);
}

//Tc 2 =pass
@Test(description = "User clicks on the Open Problems Button and Verifies the header of the respective section along with the title of the respective content section")
    public void shouldClickOnOpenProblemsButton()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AIBannerContentSection aiBannerContentSection=new AIBannerContentSection(driver);
    AITabSections aiTabSections=new AITabSections(driver);
    aiBannerContentSection.clickOnOpenProblemsButton();
    aiTabSections.assertHeaderToBe(Properties.aiTabHeader);
    aiTabSections.assertOpenProblemsTableftContentTitle(Properties.openProblemsTableftContentTitle);
    aiTabSections.assertOpenProblemsRightContentTitle(Properties.openProblemsRightContentTitle);

}
//TC 3 = pass
@Test(description = "User clicks on the AI Banner Carousel Card Section and user verifies the URL along with the header section")
    public void shouldClickOnAIBannerCarouselCardSection()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AIBannerCarouselCardSection aiBannerCarouselCardSection=new AIBannerCarouselCardSection(driver);
    aiBannerCarouselCardSection.clickOnAIBannerCarouselCardSection();
    aiBannerCarouselCardSection.assertUrlHasBeenResetTo(Properties.aiCardRedirectedUrl);
    aiBannerCarouselCardSection.assertHeaderToBe(Properties.aiCardContentHeader);
}

//Tc 4 = pass
@Test(description = "User clicks on the Read More Link displayed in the Card section and user verifies the URL along with the Header part")
public void shouldClickOnReadMoreLink()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AIBannerCarouselCardSection aiBannerCarouselCardSection=new AIBannerCarouselCardSection(driver);
    aiBannerCarouselCardSection.scrollToReadMoreLink();
    aiBannerCarouselCardSection.clickOnReadMoreLink();
    for(String winHandle : driver.getWindowHandles())
    {
        driver.switchTo().window(winHandle);
    }
    aiBannerCarouselCardSection.assertUrlHasBeenResetTo(Properties.aiCardRedirectedUrl);
    aiBannerCarouselCardSection.scrollToAIBannerCarouselCardContentSection();
    aiBannerCarouselCardSection.assertHeaderToBe(Properties.aiCardContentHeader);
}

//TC 5 =pass
@Test(description = "User clicks on the Mentor Intelligence Tab and verifies whether the respective tab is clicked")
    public void shouldClickOnMentorIntelligenceTab()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AITabSections aiTabSections=new AITabSections(driver);
    aiTabSections.scrollToTabSections();
    aiTabSections.clickOnMentorIntelligenceTab();
    aiTabSections.assertMentorIntelligenceTabContentHeaderToBe(Properties.mentorIntelligenceTabHeader);
}

//TC 6 = pass
@Test(description = "User clicks on the Content Intelligence Tab and verifies title of the respective content Section")
    public void shouldClickOnContentIntelligenceTab()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AITabSections aiTabSections=new AITabSections(driver);
    aiTabSections.scrollToTabSections();
    aiTabSections.clickOnContentIntelligenceTab();
    aiTabSections.assertContentIntelligenceTabContentHeaderToBe(Properties.ContentIntelligenceTabContentHeader);
}

//TC 7 = pass
@Test(description = "User clicks on the Student Intelligence Tab and verifies the Title of the Content section")
    public void shouldClickOnStudentIntelligenceTab()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AITabSections aiTabSections=new AITabSections(driver);
    aiTabSections.scrollToTabSections();
    aiTabSections.clickOnStudentIntelligenceTab();
    aiTabSections.assertStudentIntelligenceTabContentHeaderToBe(Properties.StudentIntelligenceTabContentHeader);
}

//Tc 8 =pass
@Test(description = "User clicks on the Open Problems Tab and verifies the title of the respective content section")
    public void shouldClickOnOpenProblemsTab()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AITabSections aiTabSections=new AITabSections(driver);
    aiTabSections.scrollToTabSections();
    aiTabSections.scrollToOpenProblemsTabSections();
    aiTabSections.clickOnOpenProblemsTab();
    aiTabSections.assertOpenProblemsTableftContentTitle(Properties.openProblemsTableftContentTitle);
    aiTabSections.assertOpenProblemsRightContentTitle(Properties.openProblemsRightContentTitle);
}

//Tc 9 = pass
@Test(description = "User clicks on the Research Blog Tab and verifies the Title of respective content section")
    public void shouldClickOnResearchBlogTab()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AITabSections aiTabSections=new AITabSections(driver);
    aiTabSections.scrollToTabSections();
    aiTabSections.scrollToResearchBlogTabSections();
    aiTabSections.clickOnResearchBlogTab();
    aiTabSections.assertResearchBlogContentTitle(Properties.researchBlogContentTitle);
}

//Tc 10 = pass
@Test(description = "User scrolls down to the Contact Us Section")
    public void shouldScrollToContactUsSection()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    ContactUsContainerSection contactUsContainerSection=new ContactUsContainerSection(driver);
    contactUsContainerSection.ScrollToContactUsSection();
    contactUsContainerSection.clickOnContactUsSection();
}

//Tc 11 = pass
@Test(description = "User scrolls down to the Video Section and clicks on the Video Button")
    public void shouldClickOnVideoSection()
{
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AIVideoSection aiVideoSection=new AIVideoSection(driver);
    aiVideoSection.clickOnVideoPlayButton();
}

//Tc 12 =pass
@Test(description = "User scrolls down to the Media and Awards Section")
    public void shouldScrollDownTOMediaSection() throws Exception {
    driver.get(Properties.aiUrl);
    driver.manage().window().maximize();
    driver.navigate().refresh();
    AIMediaMentionsAwards aiMediaMentionsAwards=new AIMediaMentionsAwards(driver);
    aiMediaMentionsAwards.scrollToMediaAndAwardsSection();
    aiMediaMentionsAwards.assertMediaSectionHeaderToBe(Properties.mediaSectionHeader);
  //  aiMediaMentionsAwards.clickAllLinks();
    aiMediaMentionsAwards.getLinks();
}
}
