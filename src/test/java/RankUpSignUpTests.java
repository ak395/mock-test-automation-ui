import org.testng.annotations.Test;
import page.rankUpPage.rankUpHeader.RankUpHeaderSection;
import page.rankUpPage.rankUpTabs.RankUpTabSection;
import page.rankUpSignUpPage.academicDetails.AcademicsDetailsSection;
import page.rankUpSignUpPage.rankUpSignUpHeader.RankUpSignUpHeaderSection;
import testBase.TestBase;
import utils.Properties;
import verification.TestFailures;

public class RankUpSignUpTests extends TestBase {

    @Test (description ="User should able to open Rank Up registration form")
    public void shouldOpenRankUpRegistrationForm() {
        driver.get(Properties.rankUpUrl);
        RankUpHeaderSection rankUpHeaderSection = rankUpHomePage.getRankUpHeaderSection();
        RankUpSignUpHeaderSection rankUpSignUpHeaderSection = rankUpSignUpHomePage.getRankUpSignUpHeaderSection();
        rankUpHeaderSection.applyToRankUp();
        rankUpHeaderSection.assertUrlHasSetTo(Properties.rankUpSignUpUrl);
        rankUpSignUpHeaderSection.assertHeaderToBe(Properties.rankUpHeaderText);
        TestFailures.reportIfAny();

    }

    @Test (description = "Validation message should appear when user clicks on Continue Button without entering the details")
    public void shouldShowValidationMessageOnSignUpPage () {
        driver.get(Properties.rankUpSignUpUrl);
        AcademicsDetailsSection academicsDetailsSection = rankUpSignUpHomePage.getAcademicsDetailsSection();
        academicsDetailsSection.checkValidation();

        TestFailures.reportIfAny();

    }



}

