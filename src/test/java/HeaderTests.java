import org.testng.annotations.Test;
import page.Ask.AskPAgeSearchContainerSection;
import page.Header.*;
import page.Search.SearchPageTitleSection;
import testBase.TestBase;

import java.io.IOException;

public class HeaderTests extends TestBase {


    @Test(description = "Click on the Embibe Logo displayed in the Header Section")
    public void shouldClickOnEmbibeLogo()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        HeaderSection headerSection=new HeaderSection(driver);
        headerSection.clickOnEmbibeLogo();
        SearchPageTitleSection searchPageTitleSection=new SearchPageTitleSection(driver);
        searchPageTitleSection.assertHeaderToBe("What would you like to STUDY today?");
        searchPageTitleSection.assertUrlHasBeenResetTo("https://www.embibe.com/");

    }

    @Test(description = "Click on the Ask Link at the header section")
    public void shouldClickOnAskLink()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        HeaderSection headerSection=new HeaderSection(driver);
        headerSection.clickOnAskLink();
        AskPAgeSearchContainerSection askPAgeTitleSection=new AskPAgeSearchContainerSection(driver);
        askPAgeTitleSection.assertHeaderToBe("Ask a Question");
        AskPAgeSearchContainerSection askPAgeSearchContainerSection=new AskPAgeSearchContainerSection(driver);
        askPAgeSearchContainerSection.assertUrlHasBeenResetTo("https://www.embibe.com/ask");

    }

    @Test(description = "Click on the Jump Link displayed at the header section")
    public void shouldClickOnTheStudyLink()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        HeaderSection headerSection=new HeaderSection(driver);
        headerSection.clickOnStudyLink();
        SearchPageTitleSection searchPageTitleSection=new SearchPageTitleSection(driver);
        searchPageTitleSection.assertHeaderToBe("What would you like to STUDY today?");
        searchPageTitleSection.assertUrlHasBeenResetTo("https://www.embibe.com/");
    }

    @Test(description = "User clicks on the Jump Link displayed at the header section")
    public void shouldClickOnJumpLink()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        HeaderSection headerSection=new HeaderSection(driver);
        headerSection.clickOnJumpLink();
        headerSection.assertUrlHasBeenResetTo("https://www.embibe.com/jump");
    }

    @Test(description = "User clicks on the Rankup Link displayed at the header section")
    public void shouldClickOnRankupLink()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        HeaderSection headerSection=new HeaderSection(driver);
        headerSection.clickOnRankupLink();
        headerSection.assertUrlHasBeenResetTo("https://www.embibe.com/rankup");
    }

    @Test(description = "User clicks on the AI Link displayed in the header section")
    public void shouldClickOnAILink()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        HeaderSection headerSection=new HeaderSection(driver);
        headerSection.clickOnAILink();
        headerSection.assertUrlHasBeenResetTo("https://www.embibe.com/ai");
    }


    @Test(description = "User clicks on the Institute Link displayed in the header section")
    public void shouldClickOnInstituteLink()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        HeaderSection headerSection=new HeaderSection(driver);
        headerSection.clickOnInstituteLink();
        headerSection.assertUrlHasBeenResetTo("https://www.embibe.com/institute");
    }

    @Test(description = "User clicks on the Login Button displayed at the header section")
    public void shouldClickOnLoginButton()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        HeaderSection headerSection=new HeaderSection(driver);
        headerSection.clickOnLoginButton();
    }

    @Test(description = "Login with different credentials")
    public void shouldLogin() throws IOException {

        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        EmbibeLogin embibeLogin=new EmbibeLogin(driver);
        embibeLogin.loginForEmbibeUsers();
    }

    @Test(description = "User clicks on the Facebook Login Button and verifies the Title of the Facebook Page")
    public void shouldClickOnFaceBookLogin()
    {
        driver.get("https://www.embibe.com/");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        FaceBookLogin faceBookLogin=new FaceBookLogin(driver);
        faceBookLogin.clickOnFaceBookLoginButton();
        FaceBookPageTitleSection faceBookPageTitleSection=new FaceBookPageTitleSection(driver);
        faceBookPageTitleSection.assertFaceBookTitle("Log in to Facebook");
    }

@Test(description = "User clicks on the Google Login Button")
    public void shouldClickOnGoogleLoginButton()
{
    driver.get("https://www.embibe.com/");
    driver.manage().window().maximize();
    driver.navigate().refresh();
    GoogleLogin googleLogin=new GoogleLogin(driver);
    googleLogin.clickOnGoogleButton();
    GooglePageTitleSection googlePageTitleSection=new GooglePageTitleSection(driver);
    googlePageTitleSection.assertTitleOfGooglePage("Sign in with Google");
}

}