package testBase;

import builders.MockTestBuilder;
import com.browserstack.local.Local;
import com.google.common.collect.ImmutableMap;
import driver.DriverInitializer;
import driver.DriverProvider;
import driver.driverFactory.DriverEnvironment;
import entities.MockTest;
import io.qameta.allure.Attachment;
import io.restassured.response.ResponseBody;
import mockTestService.MockTestClient;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.Command;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.Response;
import org.testng.IHookCallBack;
import org.testng.IHookable;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import page.*;
import page.Search.TakeATestSection;
import page.examSummary.ExamSummaryPage;
import page.examSummary.overallPerformanceSection.OverallPerformanceHomePage;
import page.examSummary.overallPerformanceSection.SubjectWiseAnalysisSection;
import page.learnMorePage.LearnMoreHomePage;
import page.mockTestPage.*;
import page.mockTestPage.chatBot.*;
import page.mockTestQuestionsPage.LegendsAndStatsSection;
import page.mockTestQuestionsPage.QuestionStatusSection;
import page.mockTestQuestionsPage.QuestionsHomePage;
import page.practicePage.PractiseHomePage;
import page.practicePage.practiceSessionSummary.SessionSummaryHomePage;
import page.practiceQuestionsPage.PracticeQuestionHomePage;
import page.rankUpLoginPage.RankUpLoginHomePage;
import page.rankUpPage.RankUpHomePage;
import page.rankUpSignUpPage.RankUpSignUpHomePage;
import page.sections.ForgotPasswordSection;
import page.sections.leftFilter.LeftFilter;
import page.studyPage.StudyHomePage;
import page.studyResultantPage.StudyResultantHomePage;
import services.testDataService.Exam;
import utils.Properties;
import verification.TestEnvironment;
import verification.TestFailures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TestBase implements IHookable {

    protected WebDriver driver;
    private Local local;
    protected LeftFilter leftFilter;
    protected MockTestHomePage mockTestHomePage;
    protected JumpHomePage jumpHomePage;
    protected HeaderHomePage headerHomePage;
    protected LandingHomePage landingHomePage;
    protected SearchHomePage searchHomePage;
    protected PractiseHomePage practiceHomePage;
    protected StudyHomePage studyHomePage;
    protected RankUpHomePage rankUpHomePage;
    protected RankUpSignUpHomePage rankUpSignUpHomePage;
    protected LearnMoreHomePage learnMoreHomePage;
    protected InstructionPage instructionPage;
    protected QuestionsHomePage questionsHomePage;
    protected ExamSummaryPage examSummaryPage;
    protected FeedbackHomePage feedbackHomePage;
    protected RankUpLoginHomePage rankUpLoginHomePage;
    protected PracticeQuestionHomePage practiceQuestionHomePage;
    protected SessionSummaryHomePage sessionSummaryHomePage;
    protected ForgotPasswordSection forgotPasswordSection;
    protected StudyResultantHomePage studyResultantHomePage;
    protected TakeATestSection takeATestSection;
    protected OverallMarksWidgetPage overallMarksWidgetPage;
    protected LegendsAndStatsSection legendsAndStatsSection;
    protected QuestionStatusSection questionStatusSection;
    protected ChatBotPage chatBotPage;
    protected MockTestEmbiumPoints mockTestEmbiumPoints;
    protected SectionsWiseMarks sectionsWiseMarks;
    protected SubjectWiseAttempts subjectWiseAttempts;
    protected MockTestBuilder mockTestBuilder;
    protected MockTest mockTest;
    protected MockTestClient mockTestClient;
    protected TestFeedBackHeader testFeedBackHeader;
    protected TestStatusContainerSection testStatusContainerSection;
    protected TestListContainerSection testListContainerSection;
    protected TrueScorePage trueScorePage;
    protected SectionWiseTime sectionWiseTime;
    protected QuestionWiseAnalysis questionWiseAnalysis;
    protected OverallPerformanceHomePage overallPerformanceHomePage;
    protected SubjectWiseAnalysisSection subjectWiseAnalysisSection;
    protected CollagePredictor collagePredictor;
    protected AccuracyVsDifficultyWidget accuracyVsDifficultyWidget;
    protected MarksPerTimeInterval marksPerTimeInterval;
    protected SubjectSwapsPage subjectSwapsPage;

    protected TimeUtilizationWidget timeUtilizationWidget;
    protected FirstLookVsReattempted firstLookVsReattempted;
    protected AttemptTypesWidget attemptTypesWidget;

    protected TestOnTestAnalysisHomePage testOnTestAnalysisPage;
    protected RankUpPage rankUpPage;

    protected String url;

    private void startLocalTestingOnBrowserStack() throws Exception {
        if (System.getProperty("driverEnvironment").equals(DriverEnvironment.BROWSERSTACK)) {
            local = new Local();
            Map<String, String> options = new HashMap<String, String>();
            options.put("key", Properties.BROWSER_STACK_KEY);
            System.out.println("port number 45691 is busy " + isPortBusy(45691));
            if (isPortBusy(45691)) {
                killThePort();
            }

            local.start(options);
        }
    }

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() throws Exception {
        System.out.println(" before suite");
        startLocalTestingOnBrowserStack();
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() throws Exception {
        System.out.println(" after suite");
        if (local != null) {
            local.stop();
            if (isPortBusy(45691))
                killThePort();

        }

    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method, ITestResult result) throws Exception {
        //startLocalTestingOnBrowserStack();
        driver = new DriverInitializer(System.getProperty("driver")).init();
        TestEnvironment.setMethodName(method.getName());
        mockTestHomePage = new MockTestHomePage();
        practiceHomePage = new PractiseHomePage();
        studyHomePage = new StudyHomePage();
        rankUpHomePage = new RankUpHomePage();
        landingHomePage = new LandingHomePage();
        rankUpSignUpHomePage = new RankUpSignUpHomePage();
        learnMoreHomePage = new LearnMoreHomePage();
        jumpHomePage = new JumpHomePage();
        headerHomePage = new HeaderHomePage();
        searchHomePage = new SearchHomePage();
        instructionPage = new InstructionPage();
        questionsHomePage = new QuestionsHomePage();
        examSummaryPage = new ExamSummaryPage();
        feedbackHomePage = new FeedbackHomePage();
        rankUpLoginHomePage = new RankUpLoginHomePage();
        practiceQuestionHomePage = new PracticeQuestionHomePage();
        sessionSummaryHomePage = new SessionSummaryHomePage();
        forgotPasswordSection = new ForgotPasswordSection();
        studyResultantHomePage = new StudyResultantHomePage();
        overallMarksWidgetPage = new OverallMarksWidgetPage();
        legendsAndStatsSection = new LegendsAndStatsSection();
        questionStatusSection = new QuestionStatusSection();
        chatBotPage = new ChatBotPage(driver);
        mockTestEmbiumPoints = new MockTestEmbiumPoints(driver);
        sectionsWiseMarks = new SectionsWiseMarks(driver);
        subjectWiseAttempts = new SubjectWiseAttempts(driver);
        mockTestBuilder = new MockTestBuilder();
        mockTest = new MockTest();
        leftFilter = new LeftFilter(driver);
        mockTestClient = new MockTestClient();
        testFeedBackHeader = new TestFeedBackHeader(driver);
        testStatusContainerSection = new TestStatusContainerSection(driver);
        testListContainerSection = new TestListContainerSection(driver);
        trueScorePage = new TrueScorePage();
        sectionWiseTime = new SectionWiseTime();
        questionWiseAnalysis = new QuestionWiseAnalysis(driver);
        overallPerformanceHomePage = new OverallPerformanceHomePage();
        subjectWiseAnalysisSection = new SubjectWiseAnalysisSection();
        collagePredictor = new CollagePredictor();
        accuracyVsDifficultyWidget = new AccuracyVsDifficultyWidget();
        marksPerTimeInterval = new MarksPerTimeInterval();
        subjectSwapsPage = new SubjectSwapsPage();
        timeUtilizationWidget = new TimeUtilizationWidget();
        firstLookVsReattempted = new FirstLookVsReattempted();
        attemptTypesWidget=new AttemptTypesWidget();

        testOnTestAnalysisPage = new TestOnTestAnalysisHomePage(driver);


    }


    protected void navigateTo(Exam exam) {

        StringBuilder url = new StringBuilder();
        url.append(Properties.baseUrl);
        url.append(getValueForUrl(exam.getGoal()));
        url.append("test/");
        url.append(getValueForUrl(exam.getName()));
        url.append(getValueForUrl(exam.getCategory()));

        if (exam.getSubject() != null) url.append(getValueForUrl(exam.getSubject()));
        url.append(getValueForUrl(exam.getTopic()));

        navigateTo(url.toString());

    }


    protected void disconnectInternet() throws IOException {
        Map map = new HashMap();
        map.put("offline", true);
        map.put("latency", 5);
        map.put("download_throughput", 500);
        map.put("upload_throughput", 1024);


        CommandExecutor executor = ((ChromeDriver) driver).getCommandExecutor();
        Response response = executor.execute(
                new Command(((ChromeDriver) driver).getSessionId(),
                        "setNetworkConditions",
                        ImmutableMap.of("network_conditions", ImmutableMap.copyOf(map))));
    }

    protected void connectToInternet() throws IOException {
        Map map = new HashMap();
        map.put("offline", false);
        map.put("latency", 5);
        map.put("download_throughput", 5000);
        map.put("upload_throughput", 5000);


        CommandExecutor executor = ((ChromeDriver) driver).getCommandExecutor();
        Response response = executor.execute(
                new Command(((ChromeDriver) driver).getSessionId(),
                        "setNetworkConditions",
                        ImmutableMap.of("network_conditions", ImmutableMap.copyOf(map))));
    }


    protected ResponseBody getmockTestResponse() {
        return mockTestClient.getMockTestData(getCookies(), getMockTestBundlePath());
    }

    protected void getSummery() {
        mockTestClient.getSummary(getCookies(), getMockTestBundlePath());
    }

    protected String getCookies() {
        String cookiesname = System.getProperty("env") + "-embibe-token";
        if (System.getProperty("env").equalsIgnoreCase("production"))
            cookiesname = "embibe-token";
        return driver.manage().getCookieNamed(cookiesname).getValue();
    }

    protected String getMockTestBundlePath() {
        return driver.getCurrentUrl().replace(Properties.baseUrl, "/").
                replace("/session", "").replace("/analyze", "");


    }

    protected void navigateTo(String url) {
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    protected void navigateToRefresh() {
        driver.navigate().refresh();
    }

    protected String getCurrentURl() {
        return driver.getCurrentUrl();
    }

    private String getValueForUrl(String value) {
        return value.toLowerCase().replace(" ", "-") + "/";
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) {

        boolean hasErrors = TestFailures.isTestFailed();

        TestFailures.clearFailures();
        TestEnvironment.clear();

        if (null != driver) {
            driver.quit();
            driver = null;
        }
    }

    @Override
    public void run(IHookCallBack callBack, ITestResult testResult) {

        callBack.runTestMethod(testResult);
        if (testResult.getThrowable() != null) {
            try {
                takeScreenShot(testResult.getMethod().getMethodName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Attachment(value = "Failure in method {0}", type = "image/png")
    private byte[] takeScreenShot(String methodName) throws IOException {
        try {
            return ((TakesScreenshot) DriverProvider.getDriver()).getScreenshotAs(OutputType.BYTES);
        } catch (UnhandledAlertException e) {
            return ((TakesScreenshot) DriverProvider.getDriver()).getScreenshotAs(OutputType.BYTES);
        }
    }

    private boolean isPortBusy(int portNumber) {
        boolean isBusy = false;
        try (Socket socket = new Socket("localhost", portNumber)) {
            isBusy = true;
        } catch (IOException e) {
            isBusy = false;
        }
        return isBusy;
    }


    public void killThePort() {
        //lsof -i :45691 to find out  the process id by port number
        //kill -9 98206
        try {
            Runtime run = Runtime.getRuntime();
            Process process = run.exec("pgrep BrowserSt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String processId;
            while ((processId = reader.readLine()) != null) {
                int exitVal = process.waitFor();
                if (exitVal == 0) {
                    System.out.print("browserStack process is running on this process id ");
                    System.out.println(processId);
                    run.exec("kill -9 " + processId);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void editCookies() {
        String cookiesname = "preprod-ab_version";
        if (System.getProperty("env").equalsIgnoreCase("production"))
            cookiesname = "ab_version";

        Cookie cookie = driver.manage().getCookieNamed(cookiesname);
        driver.manage().deleteCookie(cookie);
        driver.manage().addCookie(
                new Cookie.Builder(cookie.getName(), "6")
                        .domain(cookie.getDomain())
                        .expiresOn(cookie.getExpiry())
                        .path(cookie.getPath())
                        .isSecure(cookie.isSecure())
                        .build()
        );

    }


}

