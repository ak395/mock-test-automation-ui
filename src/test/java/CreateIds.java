import constants.Goals;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Properties;

public class CreateIds extends TestBase {


    @Test
    public void createIds() throws InterruptedException {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        for (int i = 1; i<=5; i++) {
            landingHomePage.clickOnLoginButton();
            landingHomePage.clickOnRegisterHereLink();
            String email = landingHomePage.createUserInEmbibeFormat(Goals.GOAL_MEDICAL, i);
            System.out.println(email);
            landingHomePage.clickOnGoalLoginButton();
            landingHomePage.logout();
            landingHomePage.verifyUserHasLoggedOut();

        }

    }
}
