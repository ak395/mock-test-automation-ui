package practice.practiceSession;

import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class VerifySubjectTest extends TestBase {

    @Test(groups = {Categories.PRP_PRACTICE_SMOKE, Categories.PRP_PRACTICE_SEARCH}, description = "verify practice test contains question and answer\n " +
            "practice_web_page_1,\n" +
            "practice_web_page_5,\n" +
            "PRP-507_1\n")
    public void verifyPracticeTestContainsQuestionAndAnswer() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.loginAs("engineer@embibe.com", "embibe1234");
        landingHomePage.verifyUserIsLoggedIn();
        searchHomePage.searchByUnAmbiguousKeyWord("Chemistry");
        studyHomePage.clickOnPracticeLink();
        practiceQuestionHomePage.verifyTheQuestionIsRelatedToSubjectSearched("Chemistry");
    }
}
