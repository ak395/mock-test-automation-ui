package practice.practiceSession;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static utils.AttemptType.*;

public class PracticePageTests extends TestBase {

    private PracticePlanner practicePlanner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        practicePlanner = new PracticePlanner();
        practicePlanner.addPracticeData(new PracticeData(1, 1, PERFECT_ATTEMPT));
        practicePlanner.addPracticeData(new PracticeData(2, 2, WASTED_ATTEMPT));
        practicePlanner.addPracticeData(new PracticeData(3, 3, OVERTIME_CORRECT));
        practicePlanner.addPracticeData(new PracticeData(4, 4, OVERTIME_INCORRECT));
        practicePlanner.addPracticeData(new PracticeData(5, 5, TOO_FAST_CORRECT));
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_PRACTICE_ATTEMPTS},
            description = "Student attempts, verifies attempts and quits practice with different attempt types\n;" +
                    "practice_web_page_21\n" +
                    "practice_web_page_22\n," +
                    "practice_web_page_26\n," +
                    "Practice_web_page_27\n," +
                    "Practice_web_page_29\n," +
                    "practice_web_page_28\n," +
                    "practice_web_page_35\n," +
                    "practice_web_page_52\n," +
                    "practice_web_page_30\n" +
                    "practice_web_page_41\n;")
    public void studentAttemptsAndQuitsPracticeWith() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser();
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswersAndVerifyAttempts(practicePlanner);
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifySessionAccuracy(practicePlanner);
        sessionSummaryHomePage.clickOnEndSession();
        practiceHomePage.verifyNumberOfQuestionsAttempted(practicePlanner);
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_RESUME_PRACTICE, Categories.PRP_QUIT_PRACTICE_SESSION, Categories.PRP_DIFFERENT_ATTEMPTS_PRACTICE, Categories.PRP_PRACTICE_END_TO_END},
            description = "User Resumes a Practice Test and Verifies the Questions Attempted\n;" +
                    "Practice_web_page_39,\n" +
                    "Practice_web_page_38,\n" +
                    "Practice_web_page_23,\n" +
                    "Practice_web_page_25,\n" +
                    "Practice_web_page_37,\n" +
                    "Practice_web_page_40,\n" +
                    "Practice_web_page_42,\n" +
                    "Practice_web_page_43,\n" +
                    "Practice_web_page_50,\n" +
                    "practice_web_page_22,\n" +
                    "practice_web_page_26,\n" +
                    "Practice_web_page_27,\n" +
                    "Practice_web_page_29,\n" +
                    "practice_web_page_28,\n" +
                    "practice_web_page_35,\n" +
                    "practice_web_page_52,\n" +
                    "practice_web_page_30\n;")
    public void resumePracticeTest() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser();

        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();

        practiceQuestionHomePage.selectAnswers(practicePlanner);
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifyNumberOfQuestionsAttemptedAs(practicePlanner);
        sessionSummaryHomePage.verifyQuestionStatus(practicePlanner);
        sessionSummaryHomePage.clickOnResumeSessionButton();
        practiceQuestionHomePage.verifySkipButtonIsVisible();

        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifyNumberOfQuestionsAttemptedAs(practicePlanner);
        sessionSummaryHomePage.clickOnEndSession();
        practiceHomePage.verifyNumberOfQuestionsAttempted(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.verifySkipButtonIsVisible();
    }
}
