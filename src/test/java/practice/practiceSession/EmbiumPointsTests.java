package practice.practiceSession;

import constants.Exams;
import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static utils.AttemptType.*;

public class EmbiumPointsTests extends TestBase {

    private PracticePlanner practicePlanner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        practicePlanner = new PracticePlanner();
        practicePlanner.addPracticeData(new PracticeData(1, 30, TOO_FAST_CORRECT, false));
//        practicePlanner.addPracticeData(new PracticeData(2, 2, OVERTIME_CORRECT, true));
//        practicePlanner.addPracticeData(new PracticeData(3, 3, TOO_FAST_CORRECT, true));
//        practicePlanner.addPracticeData(new PracticeData(4, 4, PERFECT_ATTEMPT, true));
//        practicePlanner.addPracticeData(new PracticeData(5, 5,OVERTIME_INCORRECT,true));
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_EMBIUM_POINTS,
            Categories.PRP_PRACTICE_ATTEMPTS}, description = "Verify Embium Points for the new registered user \n " +
            "Embium_Points_1,\n" +
            "Embium_Points_2,\n" +
            "Embium_Points_3,\n" +
            "Embium_Points_4,\n" +
            "Embium_Points_5,\n" )
    public void embiumRewardPointsForFirstTimeUSer() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser();
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.assertSilverEmbiumCoinsCountToBeZero();
        practiceQuestionHomePage.selectAnswersAndVerifyEmbiumCoins(practicePlanner);

    }


    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_EMBIUM_POINTS},
            description = "Verify Embium Points for the new features explored for the first time \n " +
            "Embium_Points_6,\n" +
            "Embium_Points_10,\n"+
            "Embium_Points_11,\n" )
    public void verifyEmbiumPointsForNewFeaturesExplored() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser();
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.assertSilverEmbiumCoinsCountToBeZero();
        practiceQuestionHomePage.useKnowledgeTreeForFirstTimeAndVerifyEmbiumCoins();
        practiceQuestionHomePage.bookmarkQuestionAndVerifyEmbiumCoins(true);
        practiceQuestionHomePage.checkAndUseHintFirstTime();
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_EMBIUM_POINTS},
    description = " Verify the Embium Coins for the Guest User \n" +
            "Embium_Points_For_Guest_User_1,\n"+
            "Embium_Points_For_Guest_User_2,\n"+
            "Embium_Points_For_Guest_User_4,\n"+
            "Embium_Points_For_Guest_User_5,\n")
    public void verifyEmbiumPointsForGuestUser() throws InterruptedException {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        instructionPage.continueAsGuest();
        practiceQuestionHomePage.assertSilverEmbiumCoinsCountToBeZero();
        practiceQuestionHomePage.verifyTheEmbiumCoinsForGuestUser();
        navigateTo(Properties.baseUrl);
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        mockTestHomePage.clearSearchField();
        mockTestHomePage.searchForExamSubjectAndTopic(Exams.EXAM_JEE_ADVANCED);
        Thread.sleep(5000);
        practiceHomePage.clickOnStartPracticeTestButton();
        navigateToRefresh();
        instructionPage.continueAsGuest();
        practiceQuestionHomePage.assertSilverEmbiumCoinsCountToBeZero();

    }




}
