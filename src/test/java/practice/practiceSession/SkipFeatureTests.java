package practice.practiceSession;

import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class SkipFeatureTests extends TestBase {
    private PracticePlanner practicePlanner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        practicePlanner = new PracticePlanner();
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_SKIP_PRACTICE_QUESTION, Categories.PRP_PRACTICE_HINT},
            description = "Student takes a Practice test And Quits Practice Test Without Answering any Question, Views hint, and Quits practice session;" +
            "practice_web_page_6,\n" +
            "practice_web_page_2,\n" +
            "practice_web_page_15,\n" +
            "practice_web_page_16,\n" +
            "practice_web_page_17,\n" +
            "practice_web_page_18,\n" +
            "practice_web_page_19,\n" +
            "practice_web_page_20,\n" +
            "practice_web_page_36,\n" +
            "practice_web_page_34,\n" +
            "practice_web_page_41\n" +
            "PRP-507_2\n;")
    public void userAttemptsAPracticeTest() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.assertSilverEmbiumCoinsCountToBeZero();
        practiceQuestionHomePage.skipPracticeQuestion();
        practiceQuestionHomePage.verifySkipOptionsAreShownToTheUser();
        practiceQuestionHomePage.clickOnNextButton(practicePlanner);

        practiceQuestionHomePage.skipPracticeQuestion();
        practiceQuestionHomePage.verifyHintIsVisible();
        practiceQuestionHomePage.closeSkipSection();
        practiceQuestionHomePage.verifySkipButtonIsVisible();

        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifySkippedQuestionCount(practicePlanner);
        sessionSummaryHomePage.clickOnEndSession();
        practiceHomePage.isStartPracticeButtonVisible();
    }
}
