package practice.practiceSession;

import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class BookmarksVerificationTest extends TestBase {

    private PracticePlanner practicePlanner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData() {
        practicePlanner = new PracticePlanner();
    }

    @Test(groups = {Categories.PRP_PRACTICE_SMOKE, Categories.PRP_BOOKMARKS}, description = "Verify Logged in user is able to Bookmark a Question and verify the bookmarks\n;" +
            "Practice_web_page_51\n;")
    public void bookmarkAQuestionAndVerifyTheBookmarks() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser();
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.clickOnBookmarkLogo();
        practiceQuestionHomePage.verifyMessageIsDisplayedAfterBookMarkAs("Hey there! This question has been bookmarked.");
        navigateTo(Properties.baseUrl);
        searchHomePage.clickOnBookMarksTab();
        searchHomePage.verifyTheQuestionIsBookMarked();
    }

    @Test(groups = {Categories.PRP_PRACTICE_SMOKE, Categories.PRP_BOOKMARKS}, description = "Verify Guest user is not able to Bookmark a Question;" +
            "Practice_web_page_51,\n" +
            "PRP-476_1,\n" +
            "PRP-507_4\n;")
    public void VerifyGuestUserIsNotAbleToBookmarkAQuestion() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.clickOnBookmarkLogo();
        practiceQuestionHomePage.verifyMessageIsDisplayedAfterBookMarkAs("You need to log in/sign up to be able to bookmark your question");
    }
}
