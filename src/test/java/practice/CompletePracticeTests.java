package practice;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static utils.AttemptType.TOO_FAST_CORRECT;
import static utils.AttemptType.WASTED_ATTEMPT;

public class CompletePracticeTests extends TestBase {

    private PracticePlanner planner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData(){
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 10, WASTED_ATTEMPT));
        planner.addPracticeData(new PracticeData(11, 20, TOO_FAST_CORRECT));
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_PRACTICE_QUESTIONS}, description = "Verify questions are not repeated in Practice For Guest User;" +
            "NQE_TEST_3\n," +
            "NQE_TEST_4\n;" +
            "QA-273")
    public void GuestStudentCompletesPractice() throws Exception {
        navigateTo(Properties.baseUrl + Properties.diceQuestionsUrl);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswers(planner);
        practiceQuestionHomePage.verifyQuestionsAreNotRepeated(planner);
    }
}
