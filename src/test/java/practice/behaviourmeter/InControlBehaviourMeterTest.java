package practice.behaviourmeter;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static utils.AttemptType.PERFECT_ATTEMPT;

public class InControlBehaviourMeterTest extends TestBase {
    private PracticePlanner planner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData(){
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 5, PERFECT_ATTEMPT));
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_BEHAVIOUR_METER},
            description = "Verify InControl Behaviour Meter Is Displayed If Student has Attempted Few Questions;" +
                    "PRP_444_3,\n" +
                    "PRP_444_2,\n" +
                    "PRP_444_7;")
    public void verifyInControlBehaviourMeterIsDisplayed() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(planner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswers(planner);
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifyInControlBehaviourMeterIsDisplayed();
    }
}
