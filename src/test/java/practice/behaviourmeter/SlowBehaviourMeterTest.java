package practice.behaviourmeter;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static utils.AttemptType.OVERTIME_CORRECT;

public class SlowBehaviourMeterTest extends TestBase {
    private PracticePlanner planner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData(){
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 3, OVERTIME_CORRECT));
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_BEHAVIOUR_METER},
            description = "Verify Slow Behaviour Meter Is Displayed If Student has Attempted Few Questions;" +
                    "PRP_444_3,\n" +
                    "PRP_444_2,\n" +
                    "PRP_444_4;" +
                    "QA-274")
    public void verifySlowBehaviourMeterIsDisplayed() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(planner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswers(planner);
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifySlowBehaviourMeterIsDisplayed();
    }
}
