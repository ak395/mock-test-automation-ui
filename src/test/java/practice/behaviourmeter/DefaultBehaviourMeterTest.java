package practice.behaviourmeter;

import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class DefaultBehaviourMeterTest extends TestBase {
    private PracticePlanner planner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData(){
        planner = new PracticePlanner();
    }

    @Test(groups = {Categories.PRP_PRACTICE_SMOKE, Categories.PRP_BEHAVIOUR_METER},
    description = "Verify Default Behaviour Meter Is Displayed If Student has not Attempted Any Questions;" +
            "PRP_444_3\n;")
    public void verifyDefaultBehaviourMeterIsDisplayed(){
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(planner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifyDefaultBehaviourMeterIsDisplayed();
    }
}
