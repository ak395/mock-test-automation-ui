package practice.behaviourmeter;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static utils.AttemptType.WASTED_ATTEMPT;

public class FlukeBehaviourMeterTest extends TestBase {
    private PracticePlanner planner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData(){
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 5, WASTED_ATTEMPT));
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_BEHAVIOUR_METER},
            description = "Verify Fluke Behaviour Meter Is Displayed If Student has Attempted Few Questions;" +
                    "PRP_444_3,\n" +
                    "PRP_444_2,\n" +
                    "PRP_444_5;" +
                    "QA-274")
    public void verifyFlukeBehaviourMeterIsDisplayed() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(planner);
        practiceHomePage.clickOnStartPracticeTestButton();
        instructionPage.continueAsGuest();
        practiceQuestionHomePage.selectAnswers(planner);
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifyFlukesBehaviourMeterIsDisplayed();
    }
}
