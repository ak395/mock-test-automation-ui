package practice.nqe;

import constants.Goals;
import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Properties;
import static utils.AttemptType.WASTED_ATTEMPT;

public class NQE extends TestBase {

    private PracticePlanner planner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestData(){
        planner = new PracticePlanner();
        planner.addPracticeData(new PracticeData(1, 30, WASTED_ATTEMPT));
    }

    @Test(description = "Verify if student is achiever difficulty level of the upcoming questions should be tough")
    public void verifyAchieverShouldGetDifficultQuestions() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        landingHomePage.clickOnLoginButton();
        landingHomePage.clickOnRegisterHereLink();
        landingHomePage.registerANewUser(Goals.GOAL_ENGINEERING);
        editCookies();
        navigateTo("https://www.embibe.com/questions/jee-main/physics/mechanics-1/units-and-dimensions-questions/session");
        practiceQuestionHomePage.selectAnswers(planner);
        planner.getDifficultyLevelOfQuestion();
        double a1 = planner.getAverageDifficultyLaval(0,5);
        double a2 = planner.getAverageDifficultyLaval(5,10);
        double a3 = planner.getAverageDifficultyLaval(10,15);
        double a4 = planner.getAverageDifficultyLaval(15,20);
        double a5 = planner.getAverageDifficultyLaval(20,25);
        double a6 = planner.getAverageDifficultyLaval(25,30);
//        practiceQuestionHomePage.verifyDifficultyLevelForFighter(a1, a2, a3, a4, a5, a6);
    }
}
