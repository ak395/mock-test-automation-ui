package practice.skills;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static utils.AttemptType.PERFECT_ATTEMPT;
import static utils.AttemptType.WASTED_ATTEMPT;

public class PracticeSkillsTest extends TestBase {

    private PracticePlanner practicePlanner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestDataForPractice() {
        practicePlanner = new PracticePlanner();
    	practicePlanner.addPracticeData(new PracticeData(1, 5, PERFECT_ATTEMPT));
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_PRACTICE_SKILLS}, description = "Verify Skills Acquired For A PerfectAnswer\n;" +
            "practice_web_page_48,\n" +
            "practice_web_page_49,\n" +
            "PRP_485_3\n;")
    public void verifySkillsAddedForAnsweringMultipleQuestions() throws Exception {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(practicePlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.selectAnswers(practicePlanner);
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifySkillsAcquired(practicePlanner);
    }
}
