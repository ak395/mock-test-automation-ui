package practice.skills;

import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

import static utils.AttemptType.PERFECT_ATTEMPT;

public class SkillsAcquiredVerificationTest extends TestBase {

    private PracticePlanner practiceDataPlanner;

    @BeforeTest(alwaysRun = true)
    public void prepareTestDataForPractice() {
        practiceDataPlanner = new PracticePlanner();
        practiceDataPlanner.addPracticeData(new PracticeData(1, 2, PERFECT_ATTEMPT));
    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_PRACTICE_SKILLS}, description = "Verify Skills Acquired For A PerfectAnswer\n;" +
            "practice_web_page_48,\n" +
            "practice_web_page_49,\n;")
    public void verifySkillsAcquiredForAPerfectAnswer() throws Exception {
        navigateTo(Properties.baseUrl);
        System.out.println("Test Completed");
        landingHomePage.clickOnStartNowButton();
        searchHomePage.clickOnStartPracticeButton(practiceDataPlanner);
        practiceHomePage.clickOnStartPracticeTestButton();
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifyNoSkillsAreAcquired();
        sessionSummaryHomePage.clickOnResumeSessionButton();
        practiceQuestionHomePage.selectAnswers(practiceDataPlanner);
        practiceQuestionHomePage.clickOnSessionSummaryButton();
        sessionSummaryHomePage.verifySkillsAcquired(practiceDataPlanner);
    }

}

