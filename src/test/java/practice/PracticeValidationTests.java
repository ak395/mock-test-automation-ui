package practice;

import org.testng.annotations.Test;
import testBase.TestBase;
import utils.Categories;
import utils.Properties;

public class PracticeValidationTests extends TestBase {


    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_ERROR_SCENARIOS}, description = "Verify different Error Scenarios while doing SignUp;" +
            "PRP_ERROR_SCENARIOS_3\n," +
            "PRP_ERROR_SCENARIOS_4\n;" +
            "PRP_ERROR_SCENARIOS_5\n;" +
            "PRP_ERROR_SCENARIOS_6\n;" +
            "PRP_ERROR_SCENARIOS_7")
    public void verifySignUpErrorScenarios() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        studyHomePage.getHeaderWrapperSection().clickOnLoginButton();
        landingHomePage.getLoginFormSection().clickOnRegisterHereLink();
        studyHomePage.getLoginFormSection().verifyAllFieldsAreBlank();
        landingHomePage.getLoginFormSection().verifyInvalidEmail();
        studyHomePage.getLoginFormSection().verifyMobileNumberMoreThanTenDigits();
        studyHomePage.getLoginFormSection().verifyMobileNumberLessThanTenDigits();
        studyHomePage.getLoginFormSection().verifyPasswordMismatch();

    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_ERROR_SCENARIOS}, description = "Verify different Error Scenarios while performing Login;" +
            "PRP_ERROR_SCENARIOS_8\n," +
            "PRP_ERROR_SCENARIOS_9\n;" +
            "PRP_ERROR_SCENARIOS_10\n;" +
            "PRP_ERROR_SCENARIOS_11\n;" +
            "PRP_ERROR_SCENARIOS_12\n;" +
            "PRP_ERROR_SCENARIOS_13\n;" +
            "PRP_ERROR_SCENARIOS_14")
    public void verifyLoginScenarios() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        studyHomePage.getHeaderWrapperSection().clickOnLoginButton();
        studyHomePage.getLoginFormSection().verifyAllLoginFieldsAreBlank();
        studyHomePage.getLoginFormSection().verifyInvalidEmail();
        studyHomePage.getLoginFormSection().verifyMobileNumberLessThanTenDigits();
        studyHomePage.getLoginFormSection().verifyMobileNumberMoreThanTenDigits();
        studyHomePage.getLoginFormSection().verifyUnregisteredEmailId();
        studyHomePage.getLoginFormSection().verifyUnregisteredPhoneNo();
        studyHomePage.getLoginFormSection().verfiyUnamePaswordNotMatching();

    }

    @Test(groups = {Categories.PRP_PRACTICE_REGRESSION, Categories.PRP_ERROR_SCENARIOS}, description = "Verify different Error Scenarios while performing Login;" +
            "PRP_ERROR_SCENARIOS_8\n," +
            "PRP_ERROR_SCENARIOS_9\n;" +
            "PRP_ERROR_SCENARIOS_10\n;" +
            "PRP_ERROR_SCENARIOS_11\n;" +
            "PRP_ERROR_SCENARIOS_12\n;" +
            "PRP_ERROR_SCENARIOS_13\n;" +
            "PRP_ERROR_SCENARIOS_14")
    public void verifyForgotPasswordScenarios() {
        navigateTo(Properties.baseUrl);
        landingHomePage.clickOnStartNowButton();
        studyHomePage.getHeaderWrapperSection().clickOnLoginButton();
        studyHomePage.getLoginFormSection().clickOnForgotPasswordLink();
        forgotPasswordSection.verifyForgotPasswordPopUpIsDisplayed();
        forgotPasswordSection.verifyforgetPassowrdWithUnregisteredID();
        forgotPasswordSection.verifyForgetPasswordWithUnregisteredPhNo();
    }
}
