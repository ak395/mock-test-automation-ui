package readExcelpoiji;

import com.poiji.bind.Poiji;
import org.testng.annotations.Test;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class ReadExamDifficulty {
    HashMap<String,ExamDifficultyModel>  map= new HashMap<String,ExamDifficultyModel>();
    @Test
    public void storeDifficultyLevelInMap() {

        List<ExamDifficultyModel> examDetails = Poiji.fromExcel(new File("Difficulty_Level_Exam_Name.xlsx"), ExamDifficultyModel.class);
        System.out.println(examDetails.size());
        for (ExamDifficultyModel examDifficultyModel : examDetails) {

            map.put(examDifficultyModel.getExamName(),examDifficultyModel);
        }

    }
    public ExamDifficultyModel getExamDifficultyModel(String examName) {

        storeDifficultyLevelInMap();
        if (map.containsKey(examName)) {
            System.out.println("Map Details:"+ map.get(examName));
           return map.get(examName);
        }
        return null;
    }
}