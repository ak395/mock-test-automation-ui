package readExcelpoiji;

import com.poiji.annotation.ExcelCellName;
import com.poiji.annotation.ExcelRow;

public class ExamDifficultyModel {

    @ExcelRow
    private int rowIndex;

    @ExcelCellName("exam_name")
    private String examName;

    @ExcelCellName("easy")
    private String easy;

    @ExcelCellName("medium")
    private String medium;

    @ExcelCellName("hard")
    private String hard;

    @Override
    public String toString() {
        return "Answer {" +
                "rowIndex=" + rowIndex +
                ", Exam_name=" + examName +
                ", Easy='" + easy + '\'' +
                ", Medium='" + medium + '\'' +
                ", hard='" + hard + '\'' +
                '}';
    }
    public String getExamName() {
        return examName;
    }
    public String getEasyDifficultyLevel() {
        return easy;
    }
    public String getMediumDifficultyLevel() {
        return medium;
    }
    public String getHardDifficultyLevel() {
        return hard;
    }
}
