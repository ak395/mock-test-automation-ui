package utilityClass;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

public class ReadCSV {

    CSVReader reader = new CSVReader(new FileReader("/Users/shrutinuwal/Downloads/MyReport.csv"));

    // this will load content into list

    public ReadCSV() throws Exception {

    }

    public ArrayList<String> getKeywords() throws  IOException{
        List<String[]> list =reader.readAll();
        ArrayList<String> keywords = new ArrayList<>();
        for (String [] keyword : list) {
            keywords.add(keyword[0]);
        }
        return keywords;

    }
}
