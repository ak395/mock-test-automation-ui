package services.testDataService;

import lombok.Getter;
import utils.JsonHelper;

import java.io.InputStream;

public class TestDataService {

    private String app;

    @Getter
    private TestData testData;

    public TestDataService(String app) {
        this.app = app;
        this.testData = buildTestData();
    }

    private TestData buildTestData() {

        String fileName = String.format("%s/testData/testData.json", app);
        String fileContent = getFileContentsAsString(fileName);
        return (TestData) JsonHelper.getResponseObject(fileContent, TestData.class);

    }

    private String getFileContentsAsString(String fileName) {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(fileName);
        return JsonHelper.getFileContentsAsString(inputStream);
    }

    public Tab getPracticeTab() {
        return testData.getPracticeTab();
    }

    public Tab getMockTestTab() {
        return testData.getMockTestTab();
    }

}
