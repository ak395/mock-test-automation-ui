package services.testDataService;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class S3 {
    private String folder;
}
