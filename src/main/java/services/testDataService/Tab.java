package services.testDataService;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Tab {

    private String name;

    private List<Exam> exam;

    public Exam getExam() {
        return exam.get(0);
    }

}
