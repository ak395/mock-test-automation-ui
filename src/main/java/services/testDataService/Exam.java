package services.testDataService;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Exam {

    private String name;
    private String goal;

    private String subject;
    private String category;

    private String topic;
    private String xpath;

    private String practiceId;
    private String testId;


}
