package services.testDataService;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TestData {
    private S3 s3;
    private List<Tab> tab;

    public Tab getPracticeTab() {
        List<Tab> filetredTab = tab.stream()
                .filter(t -> t.getName().equalsIgnoreCase("practice"))
                .collect(Collectors.toList());

        return filetredTab.get(0);
    }

    public Tab getMockTestTab() {
        List<Tab> filetredTab = tab.stream()
                .filter(t -> t.getName().equalsIgnoreCase("mockTest"))
                .collect(Collectors.toList());

        return filetredTab.get(0);
    }
}
