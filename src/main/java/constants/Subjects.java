package constants;

public class Subjects {
    public static final String SUBJECT_PHYSICS = "Physics";
    public static final String SUBJECT_CHEMISTRY = "Chemistry";
    public static final String SUBJECT_MATHEMATICS = "Mathematics";
    public static final String SUBJECT_BIOLOGY = "Biology";
}
