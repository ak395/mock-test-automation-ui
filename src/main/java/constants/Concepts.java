package constants;

public class Concepts {
    public static final String CONCEPTS_EXPERIMENTAL_PHYSICS = "Experimental Physics";
    public static final String CONCEPTS_PROPERTIES_OF_TRIANGLE = "Properties of Triangle";
    public static final String CONCEPT_D_AND_F_BLOCK_ELEMENTS = "d and f Block elements";
    public static final String CONCEPT_P_BLOCK_ELEMENTS = "p Block Elements";
    public static final String CONCEPT_HEIGHTS_AND_DISTANCES = "Heights and Distances";
}
