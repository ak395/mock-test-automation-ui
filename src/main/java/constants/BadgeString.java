package constants;

public class BadgeString {

    public static final String BADGE_PERFECT_ATTEMPT = "Perfect Attempt";
    public static final String BADGE_WASTED_ATTEMPT = "Wasted Attempt";
    public static final String BADGE_TOO_FAST_CORRECT = "Too fast Correct";
    public static final String BADGE_INCORRECT_ATTEMPT = "Incorrect Attempt";
    public static final String BADGE_OVERTIME_CORRECT_LEVEL_ONE = "OverTime Correct (Level 1)";
    public static final String BADGE_OVERTIME_CORRECT_LEVEL_TWO = "OverTime Correct (Level 2)";
    public static final String BADGE_OVERTIME_CORRECT_LEVEL_THREE = "OverTime Correct (Level 3)";
    public static final String BADGE_OVERTIME_CORRECT_LEVEL_FOUR = "OverTime Correct (Level 4)";
    public static final String BADGE_OVERTIME_CORRECT_LEVEL_FIVE = "OverTime Correct (Level 5)";
    public static final String BADGE_OVERTIME_CORRECT = "Overtime Correct";
    public static final String BADGE_OVERTIME_INCORRECT_LEVEL_ONE = "OverTime In-Correct (Level 1)";
    public static final String BADGE_OVERTIME_INCORRECT_LEVEL_TWO = "OverTime In-Correct (Level 2)";
    public static final String BADGE_OVERTIME_INCORRECT_LEVEL_THREE = "OverTime In-Correct (Level 3)";
    public static final String BADGE_OVERTIME_INCORRECT_LEVEL_FOUR = "OverTime In-Correct (Level 4)";
    public static final String BADGE_OVERTIME_INCORRECT_LEVEL_FIVE = "OverTime In-Correct (Level 5)";
    public static final String BADGE_OVERTIME_INCORRECT = "OverTime In-Correct";

    public static final String BADGE_UNATEMPTED_LEVEL_ONE = "Unattempted (Level 1)";
    public static final String BADGE_UNATEMPTED_LEVEL_TWO = "Unattempted (Level 2)";
    public static final String BADGE_UNATEMPTED_LEVEL_THREE = "Unattempted (Level 3)";
    public static final String BADGE_UNATEMPTED_LEVEL_FOUR = "Unattempted (Level 4)";
    public static final String BADGE_UNATEMPTED_LEVEL_FIVE = "Unattempted (Level 5)";
    public static final String BADGE_UNATEMPTED_LEVEL_SIX = "Unattempted (Level 6)";
    public static final String BADGE_UNATEMPTED_LEVEL_SEVEN = "Unattempted (Level 7)";

    public static final String BADGE_UNATTEMPTED = "Unattempted";
    public static final String BADGE_NOT_OPENED_QUESTION = "Not Opened Questions";
    public static final String EMPTYCELL = "";

}
