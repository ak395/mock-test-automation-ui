package constants;

public class Branches {

    public static final String BRANCH_COMPUTER_SCIENCE = "Comp. Science";
    public static final String BRANCH_MECHANICAL = "Mech.";
    public static final String BRANCH_CIVIL = "Civil";
}

