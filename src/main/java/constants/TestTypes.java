package constants;

public class TestTypes {
    public static final String TEST_TYPE_CHAPTERWISE_TEST = "Chapterwise Test";
    public static final String TEST_TYPE_PART_TEST = "Part Test";
    public static final String TEST_TYPE_FULL_TEST = "Full Test";
    public static final String TEST_TYPE_CUSTOM_TEST = "Custom Test";
    public static final String TEST_TYPE_REVISION_TEST = "Revision Test";
    public static final String TEST_TYPE_LIVE_TEST = "Live Test";
    public static final String TEST_TYPE_PREVIOUS__YEAR_TEST = "Previous Year Test";
    public static final String TEST_TYPE_MINI_TEST = "Mini Test";

}
