package constants;

import com.amazonaws.services.dynamodbv2.xspec.S;

public class Goals {
    public static final String GOAL_ENGINEERING = "Engineering";
    public static final String GOAL_MEDICAL = "Medical";
    public static final String GOAL_BANK = "Banking & Clerical";
    public static final String GOAL_SSS = "sss";
    public static final  String GOAL_10th_FOUNDATION = "10th Foundation";
    public static final  String GOAL_9th_FOUNDATION = "9th Foundation";
    public static final  String GOAL_8th_FOUNDATION = "8th Foundation";
    public static final  String GOAL_MANAGEMENT = "Management";
    public static final  String GOAL_CBSE_12 = "Cbse-12";
    public static final  String GOAL_Railway = "Railway";
    public static final  String GOAL_INSURANCE = "Insurance";
    public static final  String GOAL_DEFENCE = "Defence";
    public static final  String GOAL_CBSE = "CBSE";
    public static final  String GOAL_LECTURESHIP = "Lectureship";
    public static final  String GOAL_SSC = "SSC";
}
