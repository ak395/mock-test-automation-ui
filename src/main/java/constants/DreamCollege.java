package constants;

public class DreamCollege {

    public static final String COLLEGE_BITS_PILANI = "BITS Pilani";
    public static final String COLLEGE_BITS_HYDERABAD = "BITS Hyderabad";
    public static final String COLLEGE_BITS_GOA = "BITS Goa";
}
