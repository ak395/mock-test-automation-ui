package constants;

public class SubSubjects {
    public static final String SUB_SUBJECT_PHYSICS_MODERNPHYSICS = "Modern Physics";
    public static final String SUB_SUBJECT_MATHEMATICS_TRIOGONOMETRY = "Trigonometry";
    public static final String SUB_SUBJECT_CHEMISTRY_INORGANIC = "Inorganic Chemistry";

}
