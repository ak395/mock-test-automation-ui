package constants;

public class QuestionStatus {

    public static final String CorrectAnswer = "Correct Answers (1)";
    public static final String InCorrectAnswer = "Incorrect Answers (2)";
    public static final String AnsweredAndMarkedForReview = "Answered and Marked for Review";
    public static final String MarkedForReview = "Marked for Review";
    public static final String Answered = "Answered";
    public static final String NotAnswered = "Not Answered";
    public static final String NotVisited = "Not Visited";
}
