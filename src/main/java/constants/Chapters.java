package constants;

public class Chapters {
    public static final String CHAPTER_ROTATIONAL_MOTION= "Rotational Motion";
    public static final String CHAPTER_CIRCULAR_MOTION = "Circular Motion";
    public static final String CHAPTER_WAVE_OPTICS = "Wave Optics";
    public static final String CHAPTER_CHEMICAL_EQUILIBRIUM = "Chemical Equilibrium";
}
