package constants;

public class TestPerformanceFilter {

    public static final String TOO_FAST_CORRECT_FILTER = "Too Fast Correct";
    public static final String WASTED_ATTEMPT_FILTER = "Wasted Attempt";
    public static final String OVERTIME_ATTEMPT_FILTER = "Overtime Attempt";
    public static final String ACCURACY_FILTER = "Accuracy";
    public static final String PERFECT_ATTEMPT_FILTER = "Perfect Attempt";
    public static final String FIRST_LOOK_ACCURACY = "First Look Accuracy";
    public static final String INCORRECT_ATTEMPT = "Incorrect Attempt";
    public static final String TIME_SPENT_FILTER = "Time Spent (mins)";
    public static final String EFFORT_FILTER = "Effort";
    public static final String MARKS = "Marks";
}
