package constants;

public class Exams {
    public static final String EXAM_JEE_MAIN = "JEE Main";
    public static final String EXAM_BITSAT = "BITSAT";
    public static final String EXAM_JEE_ADVANCED = "JEE Advanced";
    public static final String EXAM_AP_EAMCET = "AP EAMCET";
    public static final String EXAM_TS_EAMCET = "TS EAMCET";
    public static final String EXAM_GUJCET = "GUJCET";
    public static final String EXAM_ASSAM_CEE = "Assam CEE";
    public static final String EXAM_VITEEE = "VITEEE";
    public static final String EXAM_KCET = "KCET";
    public static final String EXAM_UPSEE = "UPSEE";
    public static final String EXAM_KEAM = "KEAM";
    public static final String EXAM_JCECE = "JCECE";
    public static final String EXAM_MHT_CET = "MHT-CET";
    public static final String EXAM_CUSAT = "CUSAT";
    public static final String EXAM_MET = "MET";
    public static final String EXAM_NATA = "NATA";
    public static final String EXAM_JEE_MAIN_PAPER_2 = "JEE Main Paper 2";
    public static final String EXAM_NEET = "NEET";
    public static final String IRRELAVENT_EXAM_NA = "ewfwfwf";
    public static final String EXAM_AIIMS = "AIIMS";
    public static final String EXAM_JIPMER = "JIPMER";

}
