package utils;

public class Properties {

    private static final PropertiesReader propertiesReader = new PropertiesReader();
    public static final String baseUrl = propertiesReader.getBaseUrl();
    public static final String engineeringMockTestUrl = propertiesReader.getMockTestUrl();
    public static final String practicePageUrl = propertiesReader.getPracticePageUrl();
    public static final String mockTestWithOutNegativeMarking = propertiesReader.getMockTestUrlWithOutNegativeMarking();
    public static final String mockTestWithNegativeMarking = propertiesReader.getMockTestUrlWithNegativeMarking();
    public static final String miniMockTest = propertiesReader.getMiniMockTest();
    public static final String mockTestJEEAdvanced = propertiesReader.getJEEAdvancedUrl();

    public static final String BROWSER_STACK_USERNAME = propertiesReader.getBrowserStackUsername();
    public static final String BROWSER_STACK_KEY = propertiesReader.getBrowserStackKey();
    public static final String rankUpUrl = propertiesReader.getRankUpUrl();
    public static final String rankUpSignUpUrl = propertiesReader.getRankUpSignUpUrl();
    public static final String invalidUserName = propertiesReader.getInvalidUserName();
    public static final String invalidPassword = propertiesReader.getInvalidPassword();
    public static final String rankUpHeaderText = propertiesReader.getRankUpHeaderText();
    public static final String rankUpLoginUrl = propertiesReader.getRankUpLoginUrl();
    public static final String rankUpValidationCount = propertiesReader.getRankUpSignUpValidationCount();

    public static final String aiUrl = propertiesReader.getAIPageUrl();
    public static final String aiTabHeader = propertiesReader.getAITabHeader();
    public static final String mentorIntelligenceTabHeader = propertiesReader.getMentorIntelligenceTabHeader();
    public static final String openProblemsTableftContentTitle = propertiesReader.getOpenProblemsTableftContentTitle();
    public static final String openProblemsRightContentTitle = propertiesReader.getOpenProblemsRightContentTitle();
    public static final String aiCardRedirectedUrl = propertiesReader.getAICardRedirectedUrl();
    public static final String aiCardContentHeader = propertiesReader.getAiCardContentHeader();
    public static final String ContentIntelligenceTabContentHeader = propertiesReader.getContentIntelligenceTabContentHeader();
    public static final String StudentIntelligenceTabContentHeader = propertiesReader.getStudentIntelligenceTabContentHeader();
    public static final String researchBlogContentTitle = propertiesReader.getResearchBlogContentTitle();
    public static final String mediaSectionHeader = propertiesReader.getMediaSectionHeader();

    public static final String jumpUrl = propertiesReader.getJumpUrl();
    public static final String validUserName = propertiesReader.getValidUserName();
    public static final String validPassword = propertiesReader.getValidPassword();

    public static String skillsClientAPI = propertiesReader.getSkillsClientAPI();
    public static String questionsClientAPI = propertiesReader.getQuestionsClientAPI();
    public static String secret_key = propertiesReader.getSecretKey();

    public static String invalidEmailId = propertiesReader.getInvalidEmailId();
    public static String invalidConfirmPassword = propertiesReader.getInvalidConfirmPassword();
    public static String phoneNoMoreThanTenDigits = propertiesReader.getPhoneNoMoreThanTenDigits();
    public static String phoneNoLessThanTenDigits = propertiesReader.getPhoneNoLessThanTenDigits();
    public static String unregisteredEmailId = propertiesReader.getUnregisteredEmailId();
    public static String unregisteredMobileNo = propertiesReader.getUnregisteredMobileNo();

    public static String diceQuestionsUrl = propertiesReader.getDiceQuestionsUrl();
    public static String testListingUrl = propertiesReader.getTestListingUrl();
    public static String jeeMainMockTestUrl = propertiesReader.getJeeMainMockTestUrl();
    public static String twoMinutesTestUrl = propertiesReader.getTwoMinutesTestUrl();
    public static String getMockTestCode = propertiesReader.getmocktestcode();
    public static String getMockTestApi = propertiesReader.getmocktestApi();
    public static String getMockTestFullInfo = propertiesReader.getMockTestFullInfo();

}
