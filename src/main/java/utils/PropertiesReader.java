package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class PropertiesReader {
    private Properties prop = new Properties();

    PropertiesReader() {
        String environment = System.getProperty("env");
        String propertiesFilePath = environment + ".properties";
        InputStream inputStream;
        inputStream = getInputStream(propertiesFilePath);

        try {
            prop.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private InputStream getInputStream(String propertiesFilePath) {
        return this.getClass().getClassLoader().getResourceAsStream(propertiesFilePath);
    }

    String getBaseUrl() {
        return prop.getProperty("baseUrl");
    }

    String getMockTestUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("mockTestUrl");
    }

    String getMockTestUrlWithOutNegativeMarking() {
        return prop.getProperty("mockTestUrlWithoutNegativeMarking");
    }

    String getMockTestUrlWithNegativeMarking() {
        return prop.getProperty("mockTestUrlWithNegativeMarking");
    }

    String getMiniMockTest() {
        return prop.getProperty("miniMockTest");
    }

    String getPracticePageUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("practiceUrl");
    }

    String getBrowserStackUsername() {
        return prop.getProperty("browserStackUsername");
    }

    String getBrowserStackKey() {
        return prop.getProperty("browserStackKey");
    }

    String getJumpUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("jumpUrl");
    }

    String getValidUserName() {
        return prop.getProperty("validUserName");
    }

    String getValidPassword() {
        return prop.getProperty("validPassword");
    }


    String getRankUpUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("rankUpUrl");
    }

    String getRankUpSignUpUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("rankUpSignUpUrl");
    }

    String getRankUpLoginUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("rankUpLoginUrl");
    }

    String getInvalidUserName() {
        return prop.getProperty("invalidUserName");
    }

    String getInvalidPassword() {
        return prop.getProperty("invalidPassword");
    }

    String getRankUpHeaderText() {
        return prop.getProperty("rankUpSignUpHeaderText");
    }

    String getRankUpSignUpValidationCount() {
        return prop.getProperty("rankUpSignUpValidationCount");
    }

    String getAIPageUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("aiUrl");
    }

    String getAICardRedirectedUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("aiCardRedirectedUrl");
    }

    String getAITabHeader() {
        return prop.getProperty("aiTabHeader");
    }

    String getMentorIntelligenceTabHeader() {
        return prop.getProperty("mentorIntelligenceTabHeader");
    }

    String getOpenProblemsTableftContentTitle() {
        return prop.getProperty("openProblemsTableftContentTitle");
    }

    String getOpenProblemsRightContentTitle() {
        return prop.getProperty("openProblemsRightContentTitle");
    }

    String getAiCardContentHeader() {
        return prop.getProperty("aiCardContentHeader");
    }

    String getContentIntelligenceTabContentHeader() {
        return prop.getProperty("ContentIntelligenceTabContentHeader");
    }

    String getStudentIntelligenceTabContentHeader() {
        return prop.getProperty("StudentIntelligenceTabContentHeader");
    }

    String getResearchBlogContentTitle() {
        return prop.getProperty("researchBlogContentTitle");
    }

    String getMediaSectionHeader() {
        return prop.getProperty("mediaSectionHeader");
    }

    String getSkillsClientAPI() {
        return prop.getProperty("skillsClient");
    }

    String getQuestionsClientAPI() {
        return prop.getProperty("questionsClient");
    }

    String getSecretKey() {
        return prop.getProperty("secret_key");
    }


    String getPhoneNoMoreThanTenDigits() {
        return prop.getProperty("phoneNoMoreThanDigits");
    }

    String getPhoneNoLessThanTenDigits() {
        return prop.getProperty("phoneNoLessThan10Digits");
    }

    String getInvalidEmailId() {
        return prop.getProperty("invalidEmailId");
    }

    String getInvalidConfirmPassword() {
        return prop.getProperty("invalidConfirmPassword");
    }

    String getUnregisteredEmailId() {
        return prop.getProperty("unregisteredEmailId");
    }

    String getUnregisteredMobileNo() {
        return prop.getProperty("unregisteredMobileNo");
    }

    String getDiceQuestionsUrl() {
        return prop.getProperty("dice_questions");
    }

    String getJEEAdvancedUrl() {
        return prop.getProperty("mockTestJEEAdvanced");
    }

    String getTestListingUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("testListingUrl");
    }

    String getJeeMainMockTestUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("jeeMainMockTestUrl");
    }

    String getTwoMinutesTestUrl() {
        return prop.getProperty("baseUrl") + prop.getProperty("twoMinutesTestUrl");
    }

    String getmocktestcode() {
        return prop.getProperty("mocktestcodeApi");
    }

    String getmocktestApi() {
        return prop.getProperty("mocktestApi");
    }

    String getMockTestFullInfo() {
        return prop.getProperty("mocktestFullInfo");
    }

}
