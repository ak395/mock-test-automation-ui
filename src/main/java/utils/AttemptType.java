package utils;

public class AttemptType {
    public static final String PERFECT_ATTEMPT = "Perfect Attempt";
    public static final String WASTED_ATTEMPT = "Wasted Attempt";
    public static final String OVERTIME_CORRECT = "Overtime Correct";
    public static final String OVERTIME_INCORRECT = "Overtime Incorrect";
    public static final String TOO_FAST_CORRECT = "Too Fast Correct";
    public static final String INCORRECT_ATTEMPT = "Incorrect Attempt";

}
