package excelHyginen;
import utilityClass.Xls_Reader;

public class VisitAndRoundMappingTest {
    Xls_Reader reader;


    public void visitMappingTest(String file, String sheet) {
        reader = new Xls_Reader(file);
        int col = reader.getColumnCount(sheet);
        int roundNo = col - 1;
        int visitNo = col - 2;
        int visit = 4;
        int round = 5;
        int row = reader.getRowCount(sheet);

        while (visit <= visitNo && round <= roundNo) {
            for (int i = 2; i <= row; i++) {
                String visit1 = reader.getCellData(sheet, visit, i);
                String round1 = reader.getCellData(sheet, round, i);
                verifyVisitMappingWithRound(visit1, round1);
            }
            visit = visit + 2;
            round = round + 2;
        }
    }
    private void verifyVisitMappingWithRound(String visit, String round) {
        if (visit == "" && round != "" || round == "" && visit != "")
            throw new AssertionError("Visit and rounds mapping is not correct");

    }
}
