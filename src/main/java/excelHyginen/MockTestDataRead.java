package excelHyginen;

import constants.Badge;
import entities.MockTestPlanner;
import mockTestFactory.MyChoice;
import utilityClass.Xls_Reader;

import java.util.Arrays;

public class MockTestDataRead {

    public BadgeMapping badgeMapping;
    public ChoiceMapping choiceMapping;
    public MockTestPlanner planner;
    public String[] rounds = new String[]{"Round 1", "Round 2", "Round 3", "Round 4", "Round 5", "Round 6", "Round 7"};


    public MockTestPlanner builder(String path, String sheet) {
        planner = new MockTestPlanner();
        Xls_Reader reader = new Xls_Reader(path);
        badgeMapping = new BadgeMapping();
        choiceMapping = new ChoiceMapping();
        int row = reader.getRowCount(sheet);

        Arrays.stream(rounds).forEach(round -> {
            for (int i = 2; i <= row; i++) {
                String attempted = reader.getCellData(sheet, round, i);
                if (attempted != "") {
                    Badge typeBadge = badgeMapping.badgeType(attempted);
                    MyChoice typeOfChoice = choiceMapping.myChoice(attempted);
                    int section = (int) Double.parseDouble(reader.getCellData(sheet, "Section", i));
                    int questionNum = (int) Double.parseDouble(reader.getCellData(sheet, "Q. No.", i));
                    planner.addMockTestData(new entities.MockTestData(questionNum, questionNum, typeOfChoice, typeBadge, section));

                }
            }
        });
        return planner;

    }

    public static void main(String[] args) {
        MockTestDataRead buil = new MockTestDataRead();
        MockTestPlanner b = buil.builder("src/AutomationDemo1.xlsx", "Automation");
        b.getMockTestDataList().forEach(x -> System.out.println(x.getStartIndex() + " " +
                x.getEndIndex() + " " + x.getSection() + " " + x.getMyChoice().toString() + " " + x.getBadge().toString()));


    }

}

