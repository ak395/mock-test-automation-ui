package excelHyginen;

import constants.BadgeString;
import mockTestFactory.MyChoice;

public class ChoiceMapping {


    public MyChoice myChoice(String badge) {
        switch (badge) {
            case BadgeString.BADGE_TOO_FAST_CORRECT:
            case BadgeString.BADGE_PERFECT_ATTEMPT:
            case BadgeString.BADGE_OVERTIME_CORRECT:
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_THREE:
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_FOUR:
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_FIVE:
                return MyChoice.CHOICE_CORRECT;

            case BadgeString.BADGE_WASTED_ATTEMPT:
            case BadgeString.BADGE_OVERTIME_INCORRECT:
            case BadgeString.BADGE_INCORRECT_ATTEMPT:
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_THREE:
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_FOUR:
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_FIVE:
                return MyChoice.CHOICE_INCORRECT;

            case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
            case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
            case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:
            case BadgeString.BADGE_UNATEMPTED_LEVEL_FOUR:
            case BadgeString.BADGE_UNATEMPTED_LEVEL_FIVE:
            case BadgeString.BADGE_UNATEMPTED_LEVEL_SIX:
            case BadgeString.BADGE_UNATEMPTED_LEVEL_SEVEN:
            case BadgeString.BADGE_UNATTEMPTED:
                return MyChoice.CHOICE_UNATTEMPTED;

            default:
                return MyChoice.CHOICE_NOT_VISITED;
        }
    }
}
