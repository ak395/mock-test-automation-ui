package excelHyginen;

import constants.BadgeString;
import utilityClass.Xls_Reader;

import java.util.HashSet;
import java.util.Set;


public class HygieneTestForBadgeSequence {

    public void BadgeSequenceTest(String path, String sheet) {
        Xls_Reader reader = new Xls_Reader(path);
        int round = 5;

        while (round <= 21) {
            for (int i = 2; i <= reader.getRowCount(sheet); i++) {
                String round1AttemptType = reader.getCellData(sheet, round, i);
                String round2AttemptType = reader.getCellData(sheet, round + 2, i);
                validateBadgeSequence(round1AttemptType, round2AttemptType);
            }
            round = round + 2;
        }
    }

    public static void validateBadgeSequence(String round1, String round2) {
        switch (round1) {
            case BadgeString.BADGE_PERFECT_ATTEMPT:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                            throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        switch (round1) {
            case BadgeString.BADGE_INCORRECT_ATTEMPT:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:

                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        switch (round1) {
            case BadgeString.BADGE_WASTED_ATTEMPT:
                switch (round2){
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:

                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        switch (round1) {
            case BadgeString.BADGE_TOO_FAST_CORRECT:
                switch (round2) {
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:

                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        switch (round1) {
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:

                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        switch (round1) {
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_FOUR:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        switch (round1) {
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_THREE:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_FOUR:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_FIVE:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }

        switch (round1) {
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_FOUR:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_THREE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_THREE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_FOUR:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_FIVE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_SIX:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }

        switch (round1) {
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_FIVE:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_THREE:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_FOUR:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_THREE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_FOUR:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_FOUR:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_FIVE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_SIX:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }


        switch (round1) {
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:

                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        switch (round1) {
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        switch (round1) {
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_THREE:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }

        switch (round1) {
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_FOUR:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_THREE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_THREE:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }

        switch (round1) {
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_FIVE:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_WASTED_ATTEMPT:
                    case BadgeString.BADGE_INCORRECT_ATTEMPT:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_THREE:
                    case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_FOUR:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_THREE:
                    case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_FOUR:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }

        switch (round1) {
            case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }

        switch (round1) {
            case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }

        switch (round1) {
            case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }

        switch (round1) {
            case BadgeString.BADGE_UNATEMPTED_LEVEL_FOUR:
                switch (round2) {
                    case BadgeString.BADGE_TOO_FAST_CORRECT:
                    case BadgeString.BADGE_PERFECT_ATTEMPT:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                    case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:
                        throw new IllegalArgumentException(String.format("we can not attempt %s after  %s", round2, round1));
                }
                break;
        }
        if (BadgeString.EMPTYCELL.equals(round1)) {

        }
    }


    public void visitHygieneTest(String path, String sheet) throws IllegalArgumentException {
        Xls_Reader reader = new Xls_Reader(path);
        Set<Integer> set = new HashSet<>();
        int colIndex = 2;
        int round = 9;
        for (int j = 0; j < round; j++) {
            colIndex += 2;
            for (int i = 2; i <= reader.getRowCount(sheet); i++) {
                String data = reader.getCellData(sheet, colIndex, i);
                if (!data.equalsIgnoreCase("")) {
                    int visit = (int) Double.parseDouble(data);
                    if (set.contains(visit))
                        throw new IllegalArgumentException(String.format("duplicate visit count is display %s visit already exist", visit));
                    set.add(visit);
                }

            }
        }


    }
}
