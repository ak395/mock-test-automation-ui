package excelHyginen;

import constants.Badge;
import constants.BadgeString;

public class BadgeMapping {


    public Badge badgeType(String badge) {

        switch (badge) {
            case BadgeString.BADGE_TOO_FAST_CORRECT:
                return constants.Badge.BADGE_TOO_FAST_CORRECT;
            case BadgeString.BADGE_PERFECT_ATTEMPT:
                return constants.Badge.BADGE_PERFECT_ATTEMPT;
            case BadgeString.BADGE_WASTED_ATTEMPT:
                return constants.Badge.BADGE_WASTED_ATTEMPT;
            case BadgeString.BADGE_OVERTIME_CORRECT:
                return constants.Badge.BADGE_OVERTIME_CORRECT;
            case BadgeString.BADGE_OVERTIME_INCORRECT:
                return constants.Badge.BADGE_OVERTIME_INCORRECT;
            case BadgeString.BADGE_UNATTEMPTED:
                return constants.Badge.BADGE_UNATTEMPTED;
            case BadgeString.BADGE_INCORRECT_ATTEMPT:
                return constants.Badge.BADGE_INCORRECT_ATTEMPT;
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_ONE:
                return constants.Badge.BADGE_OVERTIME_CORRECT_LEVEL_ONE;
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_TWO:
                return constants.Badge.BADGE_OVERTIME_CORRECT_LEVEL_TWO;
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_THREE:
                return constants.Badge.BADGE_OVERTIME_CORRECT_LEVEL_THREE;
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_FOUR:
                return constants.Badge.BADGE_OVERTIME_CORRECT_LEVEL_FOUR;
            case BadgeString.BADGE_OVERTIME_CORRECT_LEVEL_FIVE:
                return constants.Badge.BADGE_OVERTIME_CORRECT_LEVEL_FIVE;
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
                return constants.Badge.BADGE_OVERTIME_INCORRECT_LEVEL_ONE;
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
                return constants.Badge.BADGE_OVERTIME_INCORRECT_LEVEL_TWO;
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_THREE:
                return constants.Badge.BADGE_OVERTIME_INCORRECT_LEVEL_THREE;
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_FOUR:
                return constants.Badge.BADGE_OVERTIME_INCORRECT_LEVEL_FOUR;
            case BadgeString.BADGE_OVERTIME_INCORRECT_LEVEL_FIVE:
                return constants.Badge.BADGE_OVERTIME_INCORRECT_LEVEL_FIVE;
            case BadgeString.BADGE_UNATEMPTED_LEVEL_ONE:
                return constants.Badge.BADGE_UNATEMPTED_LEVEL_ONE;
            case BadgeString.BADGE_UNATEMPTED_LEVEL_TWO:
                return constants.Badge.BADGE_UNATEMPTED_LEVEL_TWO;
            case BadgeString.BADGE_UNATEMPTED_LEVEL_THREE:
                return constants.Badge.BADGE_UNATEMPTED_LEVEL_THREE;
            case BadgeString.BADGE_UNATEMPTED_LEVEL_FOUR:
                return constants.Badge.BADGE_UNATEMPTED_LEVEL_FOUR;
            case BadgeString.BADGE_UNATEMPTED_LEVEL_FIVE:
                return constants.Badge.BADGE_UNATEMPTED_LEVEL_FIVE;
            case BadgeString.BADGE_UNATEMPTED_LEVEL_SIX:
                return constants.Badge.BADGE_UNATEMPTED_LEVEL_SIX;
            case BadgeString.BADGE_UNATEMPTED_LEVEL_SEVEN:
                return constants.Badge.BADGE_UNATEMPTED_LEVEL_SEVEN;

            default:
                return constants.Badge.EMPTYCELL;
        }

    }
}
