package testData.practice;

import org.testng.annotations.DataProvider;

import static utils.AttemptType.*;

public class PracticeTestData {
    @DataProvider
    public Object[][] attemptTypes() {
        return new Object[][]{
                {PERFECT_ATTEMPT},
                {WASTED_ATTEMPT},
                {OVERTIME_CORRECT},
                {OVERTIME_INCORRECT}
        };
    }
}
