package globalSearchApi.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import globalSearchApi.pojo.GlobalSearchResponsePojo;
import globalSearchApi.pojo.Results;
import globalSearchApi.properties.PropertyRetriever;
import globalSearchApi.webService.GlobalSearchService;
import io.restassured.response.Response;
import java.io.IOException;
import java.util.List;

public class MapperClass {
    public GlobalSearchResponsePojo readJson(String keyword) {

        GlobalSearchService service = new GlobalSearchService();

        Response response = service.getWidgetDetailsBySearchQuery(keyword);
        String res = response.asString();

        ObjectMapper objectMapper = new ObjectMapper();

        GlobalSearchResponsePojo globalSearchResponsePojo = null;


            try {
                globalSearchResponsePojo = objectMapper.readValue(res, GlobalSearchResponsePojo.class);
//                System.out.println("Current Goal "+ globalSearchResponsePojo.getCurrent_goal());
//                System.out.println("Current Widget " + globalSearchResponsePojo.getResults()[0].getWidget());
                System.out.println("Number of Widgets Present " + globalSearchResponsePojo.getCount_all_widgets());
                if (Integer.parseInt(globalSearchResponsePojo.getCount_all_widgets()) < Integer.parseInt(PropertyRetriever.getInstance().getValueForPropertyName("widgetCount"))) {
                    System.out.println(keyword + " is having widget count less than 5");
                }
                Results[] results = globalSearchResponsePojo.getResults();
//                for (int i=0; i<results.length ; i++) {
//
//                    System.out.println(results[i].getWidget());


//
//


                System.out.println("Widget at First Index " + results[1].getWidget());
                System.out.println("List of all Widgets : ");




            } catch (IOException e) {
                e.printStackTrace();
            }

            return  globalSearchResponsePojo;


    }

}