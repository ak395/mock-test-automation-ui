package globalSearchApi.properties;
import org.testng.annotations.Test;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@Test
public class PropertyRetriever {

    private static PropertyRetriever propertyRetriever = null;
    Properties properties = new Properties();
    InputStream input = null;

    private PropertyRetriever () {

        try {
            input = new FileInputStream("/Users/shrutinuwal/IdeaProjects/master/src/main/resources/metadata.properties");
            properties.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static PropertyRetriever getInstance() {
        if (propertyRetriever == null)
            propertyRetriever = new PropertyRetriever();

        return  propertyRetriever;
    }


    public String getValueForPropertyName(String propertyName) {
        return (properties.getProperty(propertyName));

    }

}
