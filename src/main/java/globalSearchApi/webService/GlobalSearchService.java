package globalSearchApi.webService;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class GlobalSearchService {

    public   Response getWidgetDetailsBySearchQuery(String keyword)

    {
        RestAssured.baseURI = "https://staging1ms.embibe.com/";

        Response response = given()
                .param("query",keyword)
                .header("embibe-token", "eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MjM4NzMzNDUsImVtYWlsIjoiZ3Vlc3RfMTU0NzExOTAyNDI1OTE4MkBlbWJpYmUuY29tIiwiaXNfZ3Vlc3QiOnRydWUsInJvbGUiOiJzdHVkZW50IiwidGltZV9zdGFtcCI6IjIwMTktMDEtMTBUMTE6MTc6MDQuNzIxWiJ9.FX4eGJM2FF89HfsfCOXiwBiPxblZW7zZtreKfrAyWwg4mnKbhjw7qmOi5KvUTfm0t4qWjOm1stJ9DYiWjQ01jA")
                .get("horizontal_ms/v1/embibe/en/global_search")
                .then().
                assertThat().statusCode(200)
                .extract().response();
        System.out.println(response.asString());
        return response;
    }

}

