package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Disambiguation {

    private Top_result top_result;

    private String is_disambiguated;

    private String autofill;
}
