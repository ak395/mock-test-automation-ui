package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ranker_info {

    private String name;

    private String year;

    private String user_id;

    private String profile_pic;

    private String exam;

}
