package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Que_rankers_wrong {

    private String practice_url;

    private String widget_name;

    private Ranker_info ranker_info;

    private String question_text;

    private String question_code;

}
