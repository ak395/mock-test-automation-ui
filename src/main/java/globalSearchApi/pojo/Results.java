package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Results {

    private String learn_path;

    private Lmap lmap;

    private String index;

    private String goal_code;

//    private Actionables actionables;

    private String recommended;

    private String code;

    private String final_score;

    private String url;

    private String is_visible;

    private String seo_subject_name;

    private String name;

    @JsonProperty("widget")
    private String widget;

    private String old_id;
}
