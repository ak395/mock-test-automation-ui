package globalSearchApi.pojo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TopPrevYearQue {

    private String practice_url;

    private String widget_name;

    private String question_text;

    private String question_code;

}
