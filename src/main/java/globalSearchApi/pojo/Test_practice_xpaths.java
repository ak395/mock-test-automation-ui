package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Test_practice_xpaths {


    private String test_xpaths;

    private String practice_xpaths;
}
