package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class GlobalSearchResponsePojo {

    @JsonProperty("Test_practice_xpaths")
    private Test_practice_xpaths test_practice_xpaths;

    private String[] valid_goals;

    @JsonProperty("current_goal")
    private String current_goal;

    private Dym dym;

    private String[] valid_exams;

    private Suggestions[] suggestions;

    private String search_query;

    private String count_overall;

    private Related_searches[] related_searches;

    private Disambiguation disambiguation;

    @JsonProperty("results")
    private Results[] results;

    private String count_all_widgets;

    private String current_exam;

    private String count_suggestions;

    private String success;

    private String count_results;

}
