package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Lmap {

    private String ex;

    private String gl;

    private String cn;

    private String sb;

    private String ch;

    private String un;

    private String qn;
}
