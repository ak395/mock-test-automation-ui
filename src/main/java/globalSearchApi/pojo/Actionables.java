package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Actionables {

    private Que_rankers_wrong queRankersWrong;

    private ImpQue impQue;

    private TopPrevYearQue topPrevYearQue;

}
