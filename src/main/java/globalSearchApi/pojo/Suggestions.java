package globalSearchApi.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Suggestions {


    private String is_visible;

    private String seo_subject_name;

//    private Lmap lmap;

    private String learn_path;

    private String index;

    private String desc;

    private String valid_goals;

    private String name;

    private String recommended;

    private String valid_exams;

    private String code;

    private String final_score;

    private String old_id;
}
