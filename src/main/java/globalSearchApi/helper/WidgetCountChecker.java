package globalSearchApi.helper;

import globalSearchApi.mapper.MapperClass;
import globalSearchApi.pojo.GlobalSearchResponsePojo;
import globalSearchApi.properties.PropertyRetriever;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class WidgetCountChecker {

public void checkNoOfWidgets(GlobalSearchResponsePojo globalSearchResponsePojo, String keyword) {

    if(Integer.parseInt(globalSearchResponsePojo.getCount_all_widgets()) < Integer.parseInt(PropertyRetriever.getInstance().getValueForPropertyName("widgetCount"))) {
        System.out.println(keyword + " is having widget count less than 5");
    }

}
    public void verifyWidgetCountFor(GlobalSearchResponsePojo globalSearchResponsePojo, String keyword) {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(Integer.parseInt(globalSearchResponsePojo.getCount_all_widgets())< Integer.parseInt(PropertyRetriever.getInstance().getValueForPropertyName("widgetCount")));
    }

}


