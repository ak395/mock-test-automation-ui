package verification;

import org.testng.Assert;

import java.util.Optional;

public class Verify {

    public static void assertEquals(Object actual, Object expected, String message, Optional<Boolean> proceed) {

        boolean p = proceed.isPresent() ? proceed.get() : false;

        try {
            Assert.assertEquals(actual, expected, message);
        } catch (AssertionError e) {
            TestFailures.handleAssertionException(e, p);
        }

    }

    public static void assertTrue(boolean condition, String message, Optional<Boolean> proceed) {

        boolean p = proceed.isPresent() ? proceed.get() : false;

        try {
            Assert.assertTrue(condition, message);
        } catch (AssertionError e) {
            TestFailures.handleAssertionException(e, p);
        }

    }

    public static void assertFalse(boolean condition, String message, Optional<Boolean> proceed) {

        boolean p = proceed.isPresent() ? proceed.get() : false;

        try {
            Assert.assertFalse(condition, message);
        } catch (AssertionError e) {
            TestFailures.handleAssertionException(e, p);
        }

    }
}
