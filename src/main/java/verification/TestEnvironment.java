package verification;

public class TestEnvironment {

    private static ThreadLocal<String> methodName = new ThreadLocal<>();

    public static void setMethodName(String name) {
        methodName.set(name);
    }

    public static String getMethodName() {
        return methodName.get();
    }

    public static String getScreenShotRootPath() {
        return "build/";
    }

    public static void clear() {
        methodName.set("");
    }
}
