package verification;

import driver.DriverProvider;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestFailures {

    private static ThreadLocal<List<String>> failures = new ThreadLocal<>();

    private static List<String> getFailures() {

        if (null == failures.get()) {
            failures.set(new ArrayList<>());
        }
        return failures.get();
    }

    public static boolean isTestFailed() {
        return getFailures().size() > 0;
    }

    public static void clearFailures() {
        getFailures().clear();
    }

    public static void reportIfAny() {
        if (isTestFailed()) {
            Assert.fail(getFailureMessages());
        }
    }


    public static void handleAssertionException(AssertionError e, boolean proceed) {

        StringBuffer f = new StringBuffer();
        f.append("\r\nERROR MESSAGE: " + e.getMessage() + "\r\n");

        String screenShotPath = captureScreenshot();
        f.append("SCREENSHOT PATH: " + screenShotPath + "\r\n");

        f.append(getStackTrace(e));
        getFailures().add(f.toString());

        if (!proceed) {
            reportIfAny();
        }

    }


    private static String captureScreenshot() {

        String screenShotRootPath = TestEnvironment.getScreenShotRootPath();
        int screenShotNumber = getFailures().size() + 1;

        String fileName = TestEnvironment.getMethodName() + "_" + screenShotNumber + ".png";

        String fileWithPath = screenShotRootPath + fileName;

        TakesScreenshot screenshot = ((TakesScreenshot) DriverProvider.getDriver());

        File srcFile = screenshot.getScreenshotAs(OutputType.FILE);
        File destFile=new File(fileWithPath);


        try {
            FileUtils.copyFile(srcFile, destFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            cs(TestEnvironment.getMethodName());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";


    }

    @Attachment
            (value = "Failure in assertion : method {0}", type = "image/png")
    private static byte[] cs(String methodName) throws IOException {
        return ((TakesScreenshot) DriverProvider.getDriver()).getScreenshotAs(OutputType.BYTES);
    }


    private static String getStackTrace(AssertionError e) {
        String t = "";
        StackTraceElement[] stackTrace = e.getStackTrace();

        for (StackTraceElement stackTraceElement : stackTrace) {
            t += "\r\n";
            t += stackTraceElement.toString();
        }
        return t;

    }



    private static String getFailureMessages() {
        String newLine = "\r\n";
        String failureMessages = newLine + String.format("TOTAL NUMBER OF ASSERTION FAILURES: %s", getFailures().size()) + newLine + newLine;

        for (String failure : getFailures()) {
            failureMessages += failure + newLine;
        }

        return failureMessages + newLine;
    }

}
