package skillService;

import entities.SkillClientResponse;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.testng.Reporter;
import org.testng.annotations.Test;
import page.practicePage.practiceSessionSummary.SkillClientRequest;
import utils.Properties;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class SkillClient {

    public SkillClientResponse getSkillsByQuestionCode(List<String> questionCodes) throws IOException {
        SkillClientRequest request = new SkillClientRequest();
        request.setQuestion_codes(questionCodes);

        Response response = given()
                .header(new Header("Content-Type", "application/json"))
                .body(getJson(request))
                .post(Properties.skillsClientAPI);

        Reporter.log(String.format("Skill Service Response: %s", response.asString()));
        return getSkillsAcquiredForQuestion(response);

    }

    private SkillClientResponse getSkillsAcquiredForQuestion(Response response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response.asString(), SkillClientResponse.class);
    }

    private String getJson(Object o){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        try {
            return objectMapper.writeValueAsString(o);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Test(groups = {"api"})
    private void testSkillsClientAPI() throws IOException {
        List<String> questionCodes = new ArrayList<>();
        questionCodes.add("EM0035999");
        questionCodes.add("EM0054941");
        questionCodes.add("EM0026217");

        SkillClientResponse skillClientResponse = getSkillsByQuestionCode(questionCodes);
        assertNotNull(skillClientResponse.getResult());
    }
}
