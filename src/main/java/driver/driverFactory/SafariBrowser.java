package driver.driverFactory;

import driver.BrowserStackDriver;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.net.URL;

public class SafariBrowser implements BrowserDriver {

    @Override
    public WebDriver getDriver() throws Exception {
        SafariOptions safariOptions = new SafariOptions();
        safariOptions.setUseTechnologyPreview(true);

        switch (driverEnvironment) {

            case DriverEnvironment.BROWSERSTACK:
                return new BrowserStackDriver().getDriver(driver);

            default:
                return new SafariDriver();
        }
    }

    @Override
    public void setDriverBinaryPath() {

    }
}
