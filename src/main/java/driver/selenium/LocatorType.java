package driver.selenium;

public enum LocatorType {
    ID,
    CLASSNAME,
    CSS,
    XPATH,
    LINKTEXT,
    TAG,
    PARTIALLINKTEXT

}
