package entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MockTest {
    private String exam;
    private String subject;
    private String subSubject;
    private String concept;
}


