package entities;

import constants.Badge;
import io.restassured.response.ResponseBody;
import lombok.Getter;
import mockTestFactory.MyChoice;

@Getter
public class MockTestData {
    private int startIndex;
    private int endIndex;
    private MyChoice myChoice;
    private Badge badge;
    private int count;
    private ResponseBody responseBody;
    private int section;


    public MockTestData(int startIndex, int endIndex, MyChoice myChoice, Badge badge, ResponseBody responseBody) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.myChoice = myChoice;
        this.badge = badge;
        this.count = endIndex - startIndex + 1;
        this.responseBody = responseBody;

    }

    public MockTestData(int startIndex, int endIndex, MyChoice myChoice, Badge badge) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.myChoice = myChoice;
        this.badge = badge;
        this.count = endIndex - startIndex + 1;

    }

    public MockTestData(int startIndex, int endIndex, MyChoice myChoice, Badge badge, int section, ResponseBody responseBody) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.myChoice = myChoice;
        this.badge = badge;
        this.count = endIndex - startIndex + 1;
        this.responseBody = responseBody;
        this.section = section;

    }

    public MockTestData(int startIndex, int endIndex, MyChoice myChoice, Badge badge, int section) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.myChoice = myChoice;
        this.badge = badge;
        this.count = endIndex - startIndex + 1;
        this.section = section;

    }


}
