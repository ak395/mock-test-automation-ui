package entities;

import constants.Badge;
import entities.questionTypes.mockTest.MockTestQuestion;
import lombok.Getter;
import lombok.Setter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;
import services.testDataService.TestDataService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class MockTestPlanner {
    private List<MockTestData> mockTestDataList;
    private List<MockTestQuestion> mockTestQuestions;
    private TestDataService testDataService;
    private String testId;

    public MockTestPlanner() {
        mockTestDataList = new ArrayList<>();
        mockTestQuestions = Collections.synchronizedList(new ArrayList<>());
    }

    public void addMockTestData(MockTestData mockTestData) {
        this.mockTestDataList.add(mockTestData);
    }

    public int getNumberOfQuestionsFor(Badge badge, String subject) {
        return mockTestQuestions.stream()
                .filter(q -> q.getSubject().equals(subject))
                .filter(q -> badge.equals(q.getBadge()))
                .collect(Collectors.toList())
                .size();
    }

    public int getNumberOfQuestionsFor(MyChoice choice, String subject) {
        return mockTestQuestions.stream()
                .filter(q -> q.getSubject().equalsIgnoreCase(subject))
                .filter(q -> choice.equals(q.getMyChoice()))
                .collect(Collectors.toList())
                .size();
    }

    public int getNumberOfQuestionsFor(Badge badge) {
        return mockTestQuestions.stream()
                .filter(q -> badge.equals(q.getBadge()))
                .collect(Collectors.toList())
                .size();
    }

    public int getNumberOfQuestionsAccordingToDifficulty(List<Integer> difficultyLevel ) {
        return mockTestQuestions.stream()
                .filter(q->difficultyLevel.contains(q.getDifficultyLevel()))
                .collect((Collectors.toList()))
                .size();
    }

    public int getIdealTimeAccordingToDifficulty(List<Integer> difficultyLevel) {
        return getMockTestQuestions().stream()
                .filter(q->difficultyLevel.contains(q.getDifficultyLevel()))
                .mapToInt(q->q.getIdealTime()).sum();
    }


    public int getNumberOfQuestionAttemptTypeFor(Set<MyChoice> choices) {
        return mockTestQuestions.stream()
                .filter(q -> choices.contains(q.getMyChoice()))
                .collect(Collectors.toList()).size();
    }

    public void addQuestion(MockTestQuestion mockTestQuestion) {
        this.mockTestQuestions.add(mockTestQuestion);
    }

    public int getNumberOfQuestionForAttemptTypes(Set<AttemptType> attemptTypes) {
        return (int) getMockTestQuestions().stream()
                .filter(q -> attemptTypes.contains(q.getAttemptType())).count();

    }

    public int getTotalMarksForTest() {
        return getMockTestQuestions().stream().mapToInt(mockTestQuestion -> mockTestQuestion.getMarksForQuestion()).sum();

    }

    public void totalTime() {
        int sum = getMockTestQuestions().stream().mapToInt(mockTestQuestions -> mockTestQuestions.getQuestionWaitTime()).sum();
        System.out.println("total time  " + sum
        );

    }

    public int subjectWiseTime(String subject) {
        return getMockTestQuestions().stream().filter(m -> m.getSubject().equalsIgnoreCase(subject)).mapToInt(m -> m.getQuestionWaitTime()).sum();

    }


    public List<String> getUniqueSubjects() {
        return getMockTestQuestions().stream()
                .map(MockTestQuestion::getSubject)
                .distinct()
                .collect(Collectors.toList());

    }

    public int getIdealTimeForSubject(String subject) {

        return getMockTestQuestions().stream()
                .filter(q -> q.getSubject().equalsIgnoreCase(subject))
                .mapToInt(q->q.getIdealTime()).sum();

    }
}
