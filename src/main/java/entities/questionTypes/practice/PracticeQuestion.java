package entities.questionTypes.practice;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PracticeQuestion {
    private String questionCode;
    private String questionType;
    private String attemptType;
    private String subject;
    private String correctAnswer;
    private String inCorrectAnswer;
    private int idealTime;
    private int difficultyLevel;


    public PracticeQuestion(String questionCode, String questionType, String attemptType, String subject, String correct, String inCorrectAnswer, int idealTime, int difficultyLevel) {
        this.questionCode = questionCode;
        this.questionType = questionType;
        this.attemptType = attemptType;
        this.subject = subject;
        this.correctAnswer = correct;
        this.inCorrectAnswer = inCorrectAnswer;
        this.idealTime = idealTime;
        this.difficultyLevel = difficultyLevel;
    }
}
