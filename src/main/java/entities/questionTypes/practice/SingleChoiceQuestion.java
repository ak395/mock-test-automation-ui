package entities.questionTypes.practice;

import lombok.Getter;

import java.util.List;

@Getter
public class SingleChoiceQuestion extends PracticeQuestion {
    private List<String> correctAnswers;
    private List<String> incorrectAnswer;

    public SingleChoiceQuestion(String questionCode, String questionType, String attemptType, List<String> correctAnswers, List<String> incorrectAnswer, String subject, String correct, String incorrect, int idealTime, int difficultyLevel) {
        super(questionCode, questionType, attemptType, subject, correct, incorrect, idealTime, difficultyLevel);
        this.correctAnswers = correctAnswers;
        this.incorrectAnswer = incorrectAnswer;
    }
}
