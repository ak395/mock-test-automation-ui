package entities.questionTypes.mockTest;

import constants.Badge;
import lombok.Getter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;

import java.util.List;

@Getter
public class SingleChoiceQuestion extends MockTestQuestion {
    private String correctAnswer;
    private List<String> incorrectAnswers;

    public SingleChoiceQuestion(String questionType, MyChoice myChoice, AttemptType attemptType, Badge badge, String correctAnswer, List<String> incorrectAnswers, String subject, int marksForQuestion, String questionCode, int idealTime,int difficultyLevel) {
        super(questionType, myChoice, attemptType, badge, subject, marksForQuestion, questionCode, idealTime,difficultyLevel);
        this.correctAnswer = correctAnswer;
        this.incorrectAnswers = incorrectAnswers;

    }
}
