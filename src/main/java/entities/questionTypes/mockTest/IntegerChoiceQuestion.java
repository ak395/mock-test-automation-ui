package entities.questionTypes.mockTest;

import constants.Badge;
import lombok.Getter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;

import java.util.List;

@Getter
public class IntegerChoiceQuestion extends MockTestQuestion{

    private String correctAnswer;
    private List<String> incorrectAnswers;

    public IntegerChoiceQuestion(String questionType, MyChoice myChoice, AttemptType attemptType, Badge badge, String correctAnswer, String subject, int marksForQuestion, String questionCode, int questionWaitTime) {
        super(questionType, myChoice, attemptType, badge, subject, marksForQuestion, questionCode, questionWaitTime);
        this.correctAnswer = correctAnswer;
    }
}
