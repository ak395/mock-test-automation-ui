package entities.questionTypes.mockTest;

import constants.Badge;
import lombok.Getter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;

import java.util.List;
@Getter
public class MatrixChoiceQuestion extends MockTestQuestion {
    private List<String> correctAnswers;
    private List<String> incorrectAnswers;

    public MatrixChoiceQuestion(String questionType, MyChoice myChoice, AttemptType attemptType, Badge badge, List<String> correctAnswers, List<String> inCorrectAnswers, String subject, int marksForQuestion, String questionCode, int questionWaitTime) {
        super(questionType, myChoice, attemptType, badge, subject, marksForQuestion, questionCode, questionWaitTime);
        this.correctAnswers = correctAnswers;
        this.incorrectAnswers = inCorrectAnswers;
    }
}
