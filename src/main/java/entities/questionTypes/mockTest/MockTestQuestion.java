package entities.questionTypes.mockTest;

import constants.Badge;
import lombok.Getter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;

@Getter
public abstract class MockTestQuestion {
    private MyChoice myChoice;
    private String questionType;
    private AttemptType attemptType;
    private Badge badge;
    private String subject;
    private int marksForQuestion;
    private String questionCode;
    private int questionWaitTime;
    private int idealTime;
    private int difficultyLevel;

    public MockTestQuestion(String questionType, MyChoice myChoice, AttemptType attemptType, Badge badge, String subject, int marksForQuestion, String questionCode, int questionWaitTime) {
        this.questionType = questionType;
        this.myChoice = myChoice;
        this.attemptType = attemptType;
        this.badge = badge;
        this.subject = subject;
        this.marksForQuestion = marksForQuestion;
        this.questionCode = questionCode;
        this.questionWaitTime = questionWaitTime;
    }

    public MockTestQuestion(String questionType, MyChoice myChoice, AttemptType attemptType, Badge badge, String subject, int marksForQuestion, String questionCode, int idealTime,int difficultyLevel) {
        this.questionType = questionType;
        this.myChoice = myChoice;
        this.attemptType = attemptType;
        this.badge = badge;
        this.subject = subject;
        this.marksForQuestion = marksForQuestion;
        this.questionCode=questionCode;
        this.idealTime = idealTime;
        this.difficultyLevel = difficultyLevel;
    }
}
