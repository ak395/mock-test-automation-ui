package entities.practice;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PracticeData {
    private final int startIndex;
    private final int endIndex;
    private final String attemptType;
    private final int count;
    private boolean isFirstTimeAttempt = false;

    public PracticeData(int startIndex, int endIndex, String attemptType, boolean isFirstTimeAttempt) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.attemptType = attemptType;
        this.count = endIndex - startIndex + 1;
        this.isFirstTimeAttempt = isFirstTimeAttempt;
    }

    public PracticeData(int startIndex, int endIndex, String attemptType) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.attemptType = attemptType;
        this.count = endIndex - startIndex + 1;
    }
}
