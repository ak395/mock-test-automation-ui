package entities.practice;

import entities.questionTypes.practice.PracticeQuestion;
import lombok.Getter;
import lombok.Setter;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import static utils.AttemptType.*;

@Getter
@Setter
public class PracticePlanner {

    private String questionCode;
    private List<PracticeData> practiceDataList;
    private List<PracticeQuestion> practiceQuestionList;
    private List<String> questionCodesList;
    private int numberOfSkippedQuestions;

    public PracticePlanner() {
        this.practiceDataList = new ArrayList<>();
        this.practiceQuestionList = new ArrayList<>();
        this.questionCodesList = new ArrayList<>();
    }

    public void addPracticeData(PracticeData practiceData) {
        practiceDataList.add(practiceData);
    }

    public void addQuestion(PracticeQuestion question) {
        practiceQuestionList.add(question);
    }

    public int getNumberOfCorrectAnswers() {
        return getPracticeQuestionList().stream()
                .filter(x -> x.getAttemptType().equals(PERFECT_ATTEMPT) || x.getAttemptType().equals(OVERTIME_CORRECT) || x.getAttemptType().equals(TOO_FAST_CORRECT))
                .collect(Collectors.toList())
                .size();
    }

    public int getNumberOfIncorrectAnswers() {
        return getPracticeQuestionList().stream()
                .filter(x -> x.getAttemptType().equals(WASTED_ATTEMPT) || x.getAttemptType().equals(OVERTIME_INCORRECT))
                .collect(Collectors.toList())
                .size();
    }

    public String getSessionAccuracy() {
        double correctAnswers = getNumberOfCorrectAnswers();
        double totalAnswers = getPracticeQuestionList().size();
        double percentage = (correctAnswers * 100) / totalAnswers;
        return String.format("%.2f", percentage) + "%";
    }

    public void areQuestionsNotRepeatedInSession() {

        int actualSize = practiceQuestionList.size();
        int expectedSize = practiceQuestionList.stream()
                .map(PracticeQuestion::getQuestionCode)
                .distinct()
                .collect(Collectors.toList()).size();
        Assert.assertEquals(actualSize, expectedSize, "Questions are repeating");
    }

    public void getQuestionDetails() {
        getPracticeQuestionList().stream().forEach(e -> {
            System.out.print("Question code is " + e.getQuestionCode() + " - ");
            System.out.print("ideal time is " + e.getIdealTime() + "-");
            System.out.println();
        });
    }


    public int getNumberOfQuestion() {
        return getPracticeQuestionList().stream().collect(Collectors.toList()).size();

    }


    public void getDifficultyLevelOfQuestion() {
        getPracticeQuestionList().stream().forEach(e ->
        {
            System.out.println(e.getDifficultyLevel());
        });
    }


    public double getAverageDifficultyLaval(int start, int end) {
        OptionalDouble average = getPracticeQuestionList().stream().skip(start).limit(end).mapToInt(e -> e.getDifficultyLevel()).average();
        return average.orElse(-1);


    }

}
