package entities;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AMockOrPracticeTest {
    private String goal;
    private String name;
    private String category;
    private String subject;
    private String topic;

}
