package entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SkillClientResponse {
    private String success;
    private List<String> result;
}
