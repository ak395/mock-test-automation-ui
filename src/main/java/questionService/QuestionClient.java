package questionService;

import io.restassured.http.Header;
import io.restassured.response.Response;
import org.testng.Reporter;
import org.testng.annotations.Test;
import utils.Properties;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.testng.Reporter.log;

public class QuestionClient {

    public Response getQuestionDetailsWithAnswersByQuestionCode(String questionId) {
        log(String.format("Get Question Details by Id: %s", questionId), true);

        Response response = given()
                .header(new Header("Content-Type", "application/json"))
                .queryParam("secret_key", Properties.secret_key)
                .queryParam("view_solution", "true")
                .body(String.format("{\"questions_data\":[{\"code\":\"%s\"}]}", questionId))
                .post(Properties.questionsClientAPI);

        Reporter.log(String.format("Question Response: %s", response.asString()));
        return response;
    }

    public Response getQuestionDetailsWithAnswersByQuestionCode(String questionId, String version) {
        log(String.format("Get Question Details by Id: %s", questionId), true);

        Response response = given()
                .header(new Header("Content-Type", "application/json"))
                .queryParam("secret_key", Properties.secret_key)
                .queryParam("view_solution", "true")
                .body(String.format("{\"questions_data\":[{\"code\":\"%s\",\"version\":\"%s\"}]}", questionId, version))
                .post(Properties.questionsClientAPI);

        Reporter.log(String.format("Question Response: %s", response.asString()));
        return response;
    }

    public int getDifficultyLevelByQuestionCode(Response response, String questionCode) {
        return from(response.asString()).get(String.format("result.%s.difficulty_level", questionCode));
    }

    public String getSubjectByQuestionCode(Response response, String questionCode) {
        return from(response.asString()).get(String.format("result.%s.learning_map.subject_name", questionCode));
    }


    public List<String> getCorrectAnsFor(Response response, String questionCode) throws Exception {
        if (getQuestionCategoryFor(response, questionCode).equalsIgnoreCase("Matrix")) {
            String ans = from(response.asString()).get(String.format("result.%s.correct_answer_csv", questionCode)).toString();
            return Arrays.asList(ans.replaceAll(".+;;", "").split(";"));
        } else
            return Arrays.asList(from(response.asString()).get(String.format("result.%s.correct_answer_csv", questionCode)).toString().split(","));
    }

    public int getIdealTimeFor(Response response, String questionCode) {
        return from(response.asString()).get(String.format("result.%s.ideal_time", questionCode));
    }

    public List<String> getIncorrectAnswer(Response response, String questionCode) {
        List<String> answers = from(response.asString()).get(String.format("result.%s.answers", questionCode));
        String incorrectAnswer = "";
        if (getQuestionCategoryFor(response, questionCode).equalsIgnoreCase("Matrix"))
            for (int i = 0; i < answers.size(); i++){
                Boolean option = from(response.asString()).get(String.format("result.%s.answers[%s].is_correct", questionCode, i));
                if (!option) {
                    String ans = from(response.asString()).get(String.format("result.%s.answers[%s].body", questionCode, i));
                    return Arrays.asList(ans.replaceAll(".+;;", "").split(";"));
                }
            }
        else {
            for (int i = 0; i < answers.size(); i++){
                Boolean option = from(response.asString()).get(String.format("result.%s.answers[%s].is_correct", questionCode, i));
                if (!option)
                    incorrectAnswer = from(response.asString()).get(String.format("result.%s.answers[%s].code", questionCode, i));
            }
            return Collections.singletonList(incorrectAnswer);
        }
        return null;
    }

    public String getQuestionCategoryFor(Response response, String questionCode) {
        return from(response.asString()).get(String.format("result.%s.category", questionCode));
    }

    @Test
    public void testCorrectAnswers() throws Exception {

        Response response = getQuestionDetailsWithAnswersByQuestionCode("EM0032622DUP01");
        List<String> answers = getCorrectAnsFor(response, "EM0032622DUP01");

        answers.forEach(System.out::println);

    }
}
