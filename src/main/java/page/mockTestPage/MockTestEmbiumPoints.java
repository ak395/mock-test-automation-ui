package page.mockTestPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

public class MockTestEmbiumPoints extends BasePage {

    public MockTestEmbiumPoints(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".milestone-wrapper")
    private WebElement embiumPointPopup;
    @FindBy(css = ".modal-close")
    private WebElement embiumPointPopupClose;
    @FindBy(css = ".milestone-message")
    private WebElement embiumPointMessage;
    @FindBy(css = ".embium-coins-earned")
    private WebElement embiumCoinEarned;

    public void verifyEmbiumPointPopup() {
        Assert.assertTrue(embiumPointPopup.isDisplayed(), "embium Point Popup is not display");
        Assert.assertEquals(embiumPointPopup.findElement(By.cssSelector(" .milestone-ribbon")).getText(), "CONGRATULATIONS", "\"CONGRATULATIONS\" text is not display");
        Assert.assertEquals(embiumPointPopup.findElement(By.cssSelector(" .milestone-message")).getText(), "You earned some embium points", "\"You earned some embium points\" text is not display");
    }

    public void closeEmbibePointPopUp() {
        waitForElementTobeClickable(embiumPointPopupClose);
        jsClick(embiumPointPopupClose);
    }

    public int embibeCoinEarned() {
        waitForElementTobeClickable(embiumCoinEarned);
        return Integer.parseInt(embiumCoinEarned.getText());
    }

}
