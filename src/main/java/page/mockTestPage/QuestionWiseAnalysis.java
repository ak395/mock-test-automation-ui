package page.mockTestPage;

import entities.MockTestPlanner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import utils.Properties;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class QuestionWiseAnalysis extends BasePage {

    @FindBy(xpath = "//div[text()='Question-wise Analysis']")
    private WebElement questionAnalysisTab;

    @FindBy(xpath = "//div[text()='View All Questions']")
    private WebElement viewAllQuestions;

    @FindBy(xpath = "//div[text()='Overtime Correct']")
    private WebElement viewOvertimeCorrect;

    @FindBy(xpath = "//div[text()='Wasted Attempts']")
    private WebElement viewWastedAttempts;

    @FindBy(xpath = "//div[text()='Overtime Incorrect']")
    private WebElement viewOvertimeInCorrect;

    @FindBy(xpath = "//div[text()='Incorrect Attempts']")
    private WebElement viewIncorrectAttempts;

    @FindBy(xpath = "//div[text()='chapter-wise Performance']")
    private WebElement chapterWisePerformance;

    @FindBy(xpath = "//div[text()='concept-wise Performance']")
    private WebElement conceptWisePerformance;

    @FindBy(css = ".arrow-btn")
    private List<WebElement> chaptersPerformance;

    @FindBy(className = "test-wrapper")
    private List<WebElement> questionsContainer;

    @FindBy(css = ".mcq-container.view-mode")
    private List<WebElement> optionsContainer;

    @FindBy(css = ".answer-container .__img-height")
    private List<WebElement> rightAnswer;

    @FindBy(css = ".answer-container .user-selected-answer")
    private List<WebElement> userSelectedAnswer;

    @FindBy(css = ".chapter-name.chapter-desktop-view")
    private List<WebElement> chaptersNeedToImprove;

    @FindBy(css = ".chapter-name.chapter-desktop-view")
    private List<WebElement> chapterCouldDoWell;

    @FindBy(css = ".chapter-name.chapter-desktop-view")
    private List<WebElement> chapterDidWell;

    @FindBy(css = ".chapter-name.chapter-desktop-view")
    private List<WebElement> weakConcepts;

    @FindBy(css = ".questionlist-practice-legends-item")
    private List<WebElement> questionsOnLeftFilter;

    @FindBy(css = ".card-title.toolTip-parent")
    private List<WebElement> keyConcepts;

    @FindBy(css = ".css-1hwfws3")
    private List<WebElement> leftFilter;

    @FindBy(css = ".explanation-answers")
    private WebElement explanation;

    @FindBy(css = ".css-1pcexqc-container.question-status-filter-select-wrapper")
    private WebElement leftfilterSecond;

    @FindBy(css = ".question-status-filter .css-1hwfws3")
    private WebElement questionStatusDropdown;

    @FindBy(css = ".test-legend-feedback-items--image.not-answered")
    private WebElement notAnswered;

    @FindBy(css = ".test-legend-feedback-items--image.not-visited")
    private WebElement notVisited;

    @FindBy(css = ".test-legend-feedback-items--image.toofastcorrect.visitedanswered.answered ")
    private WebElement correctAnswer;

    @FindBy(css = ".test-legend-feedback-items--image.wasted.visitedanswered.answered ")
    private WebElement incorrectAnswer;

    @FindBy(css = ".test-legend-feedback-items--image.wasted.markedforreviewanswered.answered-and-marked-for-review ")
    private WebElement ansAndMarkedForReview;

    @FindBy(css = ".test-legend-feedback-items--image.review-now")
    private WebElement markedForReview;

    @FindBy(className = "learn-practice-link")
    private List<WebElement> learnAndPracticeLink;

    @FindBy(css = ".footer-text .title")
    private WebElement practiceTitle;

    @FindBy(css = ".chapter-name.chapter-desktop-view")
    private List<WebElement> cards;

    @FindBy(css = ".react-typeahead-input.react-typeahead-usertext")
    private WebElement learnTitle;

    private String subjectType = "//span[text()='%s']";
    private String questionStatus = "//span[text()='%s']";

    public QuestionWiseAnalysis(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickQuestionWiseAnalysisTab() {
        questionAnalysisTab.click();
    }

    public void clickViewAllQuestionsTab() {
        viewAllQuestions.click();
    }

    public void clickOverTimeCorrectTab() {
        viewOvertimeCorrect.click();
    }

    public void clickWastedAttemptsTab() {
        viewWastedAttempts.click();
    }

    public void clickOverTimeIncorrectTab() {
        viewOvertimeInCorrect.click();
    }

    public void clickIncorrectAttemptsTab() {
        viewIncorrectAttempts.click();
    }

    public void clickChapterNeedsToImprove() {
        chaptersPerformance.get(0).click();
    }

    public void clickChapterYouCouldDoWell() {
        chaptersPerformance.get(1).click();
    }

    public void clickChapterYouDidWell() {
        chaptersPerformance.get(0).click();
    }

    public void clickWeakConcept() {
        chaptersPerformance.get(1).click();
    }

    public void clickStrongConcept() {
        chaptersPerformance.get(1).click();
    }


    public void checkAllQuestionIsPresentOrNot(MockTestPlanner planner) {
        int questionCount = questionsContainer.size();
        int questionInTest = planner.getMockTestQuestions().stream().collect(Collectors.toList()).size();
        Assert.assertEquals(questionInTest, questionCount, String.format("user attempt the question in test is %s but in \"Question-wise Analysis\" its display %s question", questionInTest, questionCount));

    }

    public void verifyUserSelectedAnswerDisplayed(MockTestPlanner planner) {
        int selectedAnsCount = planner.getMockTestQuestions().stream().collect(Collectors.toList()).size();
        String examPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "");
        Assert.assertEquals(selectedAnsCount, userSelectedAnswer.size());
        questionsContainer.stream().forEach(x -> {
            WebElement userSelectedAnswer = x.findElement(By.cssSelector(".answer-container .user-selected-answer"));
            Assert.assertTrue(userSelectedAnswer.isDisplayed(), "user selected answer is not display on Question-wise Analysis page for exam path" + examPath);
        });
    }

    public void verifyQuestionWithOptions(MockTestPlanner planner) {
        int questionsCount = planner.getMockTestQuestions().stream().collect(Collectors.toList()).size();
        Assert.assertEquals(questionsCount, optionsContainer.size(), "Questions without options");
    }

    public void verifyRightAnswerDisplayed(MockTestPlanner planner) {
        int count1 = 0;
        int count2 = 0;
        int questionsCount = planner.getMockTestQuestions().stream().collect(Collectors.toList()).size();
        for (int i = 0; i < rightAnswer.size(); i++) {
            if (rightAnswer.get(i).getAttribute("alt").equalsIgnoreCase("right answer tick")) {
                count1++;
            }
            if (rightAnswer.get(i).getAttribute("alt").equalsIgnoreCase("wrong answer tick")) {
                count2++;
            }
        }
        Assert.assertEquals(questionsCount, count1);
        System.out.println(count1);
        System.out.println(count2);
    }

    public void verifyChapterWisePerformanceDisplayed() {
        Assert.assertTrue(chapterWisePerformance.isDisplayed(), "Chapter wise performance button is not displaying");
    }

    public void verifyConceptWisePerformanceDisplayed() {
        Assert.assertTrue(conceptWisePerformance.isDisplayed(), "Concept wise performance button is not displaying");
    }

    public void verifyChaptersNeedsToImproveDisplayed(String chapter) {
        boolean flag = false;
        for (int chapterName = 0; chapterName < chaptersNeedToImprove.size(); chapterName++) {
            if (chaptersNeedToImprove.get(chapterName).getText().equalsIgnoreCase(chapter)) {
                flag = true;
            }
        }
        if (flag) {
            System.out.println("Subject needs to improve is present");
        } else {
            Assert.fail("Chapter is not present");
        }
    }

    public void verifyChaptersYouCouldDoWellDisplayed(String chapter) {
        boolean flag = false;
        for (int chapterName = 0; chapterName < chapterCouldDoWell.size(); chapterName++) {
            if (chapterCouldDoWell.get(chapterName).getText().equalsIgnoreCase(chapter)) {
                flag = true;
            }
        }
        if (flag) {
            System.out.println("Subject could do well is present");
        } else {
            Assert.fail("Chapter could do well is not present");
        }
    }

    public void verifyChaptersYouDidWellDisplayed(String chapter) {
        boolean flag = false;
        for (int chapterName = 0; chapterName < chapterDidWell.size(); chapterName++) {
            if (chapterDidWell.get(chapterName).getText().equalsIgnoreCase(chapter)) {
                flag = true;
            }
        }
        if (flag) {
            System.out.println("Concept did well is present");
        } else {
            Assert.fail("Concept did well is not present");
        }
    }

    public void verifyConceptDisplaying() {
        List<String> concept = new ArrayList<>();
        List<String> keyConcept = new ArrayList<>();

        Assert.assertTrue(weakConcepts.size()>0, "concepts is not displaying");
        weakConcepts.stream().forEach(e -> {
            concept.add(e.getText());
        });

        concept.stream().forEach(e->{
            System.out.println(e);
        });

        questionsOnLeftFilter.get(0).click();

        System.out.println("-----------------------------------");

        keyConcepts.stream().forEach(k->{
            keyConcept.add(k.getText());
        });

        keyConcept.stream().forEach(l->{
            System.out.println(l);
        });

        int count = 0;
        for(int i=0; i<keyConcept.size(); i++){
            for(int j = 0; j<concept.size(); j++){
                if(keyConcept.get(i).equalsIgnoreCase(concept.get(j))){
                    count++;
                }
            }
        }

        if(count>0){
            System.out.println("Concept exist");
        }
        else {
            Assert.fail("Concept does not exist");
        }
    }

    public void selectSubjectInLeftFilter(String subject){
        waitForElementTobeClickable(driver.findElement(By.xpath(String.format(subjectType, subject))));
        jsClick(driver.findElement(By.xpath(String.format(subjectType, subject))));
        Assert.assertEquals(leftFilter.get(0).getText(), subject);
        //wait(5000);
    }

    public void verifyQuestionsWithSolutionsFromLeftFilter(){
        for(int i=0; i<questionsOnLeftFilter.size(); i++){
            waitForElementTobeClickable(questionsOnLeftFilter.get(i));
            jsClick(questionsOnLeftFilter.get(i));
            questionsContainer.stream().forEach(x -> {
                WebElement userSelectedAnswer = x.findElement(By.cssSelector(".answer-container .user-selected-answer"));
                Assert.assertTrue(userSelectedAnswer.isDisplayed(), "user selected answer is not display on Question-wise Analysis page for exam path");
            });
            questionsContainer.stream().forEach(y -> {
                WebElement optionsContainer = y.findElement(By.cssSelector(".mcq-container.view-mode"));
                Assert.assertTrue(optionsContainer.isDisplayed(), "user selected answer is not display on Question-wise Analysis page for exam path");
        });
            Assert.assertTrue(explanation.isDisplayed(), "Question's explanation is not displaying");
            waitForPageToLoad();
      }
    }

    public void selectQuestionStatusInLeftFilter(String questionStatusType){
            waitForElementTobeClickable(driver.findElement(By.xpath(String.format(questionStatus, questionStatusType))));
            //jsClick(driver.findElement(By.xpath(String.format(questionStatus, questionStatusType))));
        driver.findElement(By.xpath(String.format(questionStatus, questionStatusType))).click();
    }

    public void clickQuestionStatusDropDown(){
        questionStatusDropdown.click();
    }

    public void verifyAnsweredQuestions(){
        for(int i=0; i<questionsOnLeftFilter.size(); i++){
            waitForElementTobeClickable(questionsOnLeftFilter.get(i));
            jsClick(questionsOnLeftFilter.get(i));
            questionsContainer.stream().forEach(x -> {
                WebElement userSelectedAnswer = x.findElement(By.cssSelector(".answer-container .user-selected-answer"));
                Assert.assertTrue(userSelectedAnswer.isDisplayed(), "user selected answer is not display on Question-wise Analysis page for exam path");
            });
            waitForPageToLoad();
        }
    }

    public void verifyNotAnsweredQuestions(){
        Assert.assertTrue(notAnswered.isDisplayed(), "Not answered questions is not displaying");
    }

    public void verifyNotVisitedQuestions(){
        Assert.assertTrue(notVisited.isDisplayed(), "Not visited questions is not displaying");
    }

    public void verifyCorrectAnswer(){
        Assert.assertTrue(correctAnswer.isDisplayed(), "Correct answer is displaying");
    }

    public void verifyInCorrectAnswer(){
        Assert.assertTrue(incorrectAnswer.isDisplayed(), "In correct answer is not displaying");
    }

    public void verifyAnsAndMarkedForReview(){
        Assert.assertTrue(ansAndMarkedForReview.isDisplayed(), "Answered and marked for review is not displaying");
    }

    public void verifyMarkedForReview(){
        Assert.assertTrue(markedForReview.isDisplayed(),  "Marked for review is not displaying");
    }

    public void verifyPracticeLinkWorking(String practiceLink){
        String parentWindow = driver.getWindowHandle();
        int cardsCount = cards.size();
        int practiceLinksCount = 0;
        int countOpenTabs = 0;
        List<String> practiceCardsName = new ArrayList<>();

        for(int links = 0; links<learnAndPracticeLink.size(); links++){
            if(learnAndPracticeLink.get(links).getText().equalsIgnoreCase(practiceLink)){
                practiceCardsName.add(learnAndPracticeLink.get(links).getText());
                practiceLinksCount++;
                learnAndPracticeLink.get(links).click();
            }
            else{
                continue;
            }
        }

        Assert.assertTrue(practiceLinksCount>0, "Practice links are not displaying");

          for(String childWindow : driver.getWindowHandles()){
              if(!childWindow.equals(parentWindow)){
                  countOpenTabs++;
                  driver.switchTo().window(childWindow);
                  Assert.assertTrue(practiceTitle.isDisplayed(), " link is not working for " + practiceLink);
                  for(String cardValue : practiceCardsName){
                      learnTitle.click();
                      String searchValue = learnTitle.getAttribute("value");
                      System.out.println(searchValue);
                      if(cardValue.equalsIgnoreCase(searchValue)){
                          Assert.assertTrue(cardValue.equalsIgnoreCase(searchValue), "Card name is not matching with search value");
                      }
                      else{
                          continue;
                      }
                  }
              }

          }
        Assert.assertEquals(practiceLinksCount, cardsCount, "Learn links counts are not matching with cards count , expecting " + practiceLinksCount + " but found " + cardsCount);
        Assert.assertEquals(practiceLinksCount, countOpenTabs, "Learn links counts are not matching with number of tabs opened, expecting " + practiceLinksCount + " but found " + countOpenTabs);

    }

    public void verifyLearnLinkWorking(String learnLink){
        String parentWindow = driver.getWindowHandle();
        int cardsCount = cards.size();
        int learnLinksCount = 0;
        int countOpenTabs = 0;
        List<String> learnCardsName = new ArrayList<>();

        for(int links = 0; links<learnAndPracticeLink.size(); links++){
            if(learnAndPracticeLink.get(links).getText().equalsIgnoreCase(learnLink)){
                learnCardsName.add(learnAndPracticeLink.get(links).getText());
                learnLinksCount++;
                learnAndPracticeLink.get(links).click();
            }
            else{
                continue;
            }
        }

        Assert.assertTrue(learnLinksCount>0, "Learn links are not displaying");

        for(String childWindow : driver.getWindowHandles()){
            if(!childWindow.equals(parentWindow)){
                countOpenTabs++;
                driver.switchTo().window(childWindow);
                Assert.assertTrue(driver.getTitle().contains(learnLink), "User is not landed on learn page");
                for(String cardValue : learnCardsName){
                    learnTitle.click();
                    String searchValue = learnTitle.getAttribute("value");
                    System.out.println(searchValue);
                    if(cardValue.equalsIgnoreCase(searchValue)){
                        Assert.assertTrue(cardValue.equalsIgnoreCase(searchValue), "Card name is not matching with search value");
                    }
                    else{
                        continue;
                    }
                }
            }
        }

        Assert.assertEquals(learnLinksCount, cardsCount, "Learn links counts are not matching with cards count , expecting " + learnLinksCount + " but found " + cardsCount);
        Assert.assertEquals(learnLinksCount, countOpenTabs, "Learn links counts are not matching with number of tabs opened, expecting " + learnLinksCount + " but found " + countOpenTabs);
    }
}

