package page.mockTestPage;

import constants.TestTypes;
import driver.DriverProvider;
import entities.AMockOrPracticeTest;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import page.examSummary.AttemptsToMap;
import page.examSummary.ExamSummaryPage;
import page.mockTestPage.phoenixCandyContainer.PhoenixCandyContainerSection;
import page.mockTestQuestionsPage.TestGridNavSection;
import page.sections.leftFilter.LeftFilter;
import page.sections.leftFilter.LeftFilterSection;
import services.testDataService.TestDataService;
import utils.Properties;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;


@Getter
public class MockTestHomePage extends BasePage {

    private PhoenixCandyContainerSection phoenixCandyContainerSection;
    private LeftFilterSection leftFilterSection;
    private StartMockTestSection mockTestSection;
    private ExamSummaryPage examSummaryPage;
    private TestListContainerSection testListContainerSection;
    private LeftFilter leftFilter;
    private TestStatusContainerSection testStatusContainerSection;
    private MoreInfoPage moreInfoPage;
    private String urlText;
    private TestGridNavSection testGridNavSection;
    private TestFeedBackHeader testFeedBackHeader;
    private TestListingAttemptDetailsSection testListingAttemptDetailsSection;
    private AttemptsToMap attemptsToMap;

    @FindBy(css = ".footer-text > .title")
    private WebElement mockTestPageTitle;

    @FindBy(css = ".react-typeahead-input.react-typeahead-usertext")
    private WebElement searchField;

    @FindBy(className = "common-padding")
    private WebElement descriptionWidget;

    @FindBy(css = ".link-more-component")
    private WebElement showMoreORLessButton;

    @FindBy(css = ".snack-bar-normal-alert")
    private WebElement offlineSnackbar;

    @FindBy(css = ".snack-bar-success-alert")
    private WebElement onlineSnackbar;

    @FindBy(css = ".yt-option")
    private WebElement selectBySearch;

    @FindBy(css = ".btn-box")
    private WebElement resume;
    @FindBy(css = ".test-details-card-list a[target='_blank']")
    private List<WebElement> startTest;


    @FindBy(css = ".title.secondary-heading ")
    private WebElement testName;

    @FindBy(css = ".toggle-btn")
    private WebElement showAllORHide;

    @FindBy(css = ".css-s3m8b3-indicatorContainer")
    private WebElement testFilter;

    @FindBy(css = ".test-questions-filter-label")
    private WebElement selectTestType;

    @FindBy(className = "test-details-card-wrapper")
    private List<WebElement> testCards;

    @FindBy(className = "test-topic-details")
    private List<WebElement> testCounts;

    @FindBy(className = "test-list-card-heading")
    private List<WebElement> testCardHeadings;

    @FindBy(css = ".test-innerlist-show-more .link-more-component")
    private WebElement showMore;

    @FindBy(className = "test-details-card-wrapper")
    private WebElement testCard;

    @FindBy(className = "test-topic-name")
    private List<WebElement> testTopics;

    @FindBy(css = ".breadcrumb-node:nth-child(1)")
    private WebElement goalName;

    @FindBy(css = ".view-feedback-btn")
    private WebElement viewFeedBackButton;

    @FindBy(css = ".breadcrumb-node:nth-child(2)")
    private WebElement examLink;

    @FindBy(css = ".breadcrumb-node:nth-child(3)")
    private WebElement subjectLink;

    @FindBy(css = ".breadcrumb-node:nth-child(4)")
    private WebElement subSubjectLink;

    @FindBy(css = ".breadcrumb-node:nth-child(5)")
    private WebElement conceptLink;

    @FindBy(css = ".node-wrapper >.childTree .childName")
    private List<WebElement> exam;

    @FindBy(css = ".title.-inProgress")
    private List<WebElement> testInProgress;

    @FindBy(css = ".align-center > span > span")
    private List<WebElement> inProgressTimer;

    @FindBy(css = ".align-center > span > span")
    private WebElement inProgressTestTimer;

    @FindBy(css = ".test-detail-card-name")
    private List<WebElement> testCardsName;

    @FindBy(css = ".test-topic-details")
    private WebElement testCount;

    @FindBy(css = ".test-details-card-wrapper")
    private List<WebElement> testNumber;

    @FindBy(css = ".align-center > span > span")
    private WebElement examTimer;

    @FindBy(css = ".over-all-stats.small-common-spacing")
    private WebElement overAllWidget;

    public MockTestHomePage() {
        driver = DriverProvider.getDriver();

        PageFactory.initElements(driver, this);

        phoenixCandyContainerSection = new PhoenixCandyContainerSection(driver);
        leftFilterSection = new LeftFilterSection(driver);
        mockTestSection = new StartMockTestSection(driver);
        examSummaryPage = new ExamSummaryPage();
        testStatusContainerSection = new TestStatusContainerSection(driver);
        testListContainerSection = new TestListContainerSection(driver);
        moreInfoPage = new MoreInfoPage(driver);
        testListingAttemptDetailsSection = new TestListingAttemptDetailsSection(driver);
        attemptsToMap = new AttemptsToMap(driver);
    }

    public void selectTest(AMockOrPracticeTest test) {
        phoenixCandyContainerSection.selectGoal(test.getGoal());
        leftFilterSection.selectTest(test);
    }

    public void verifyTheNameOfTheExam(TestDataService data) {
        assertEquals(data.getMockTestTab().getExam().getTopic(), getMockTestSection().verifyTheTestName());
    }

    public void clickOnStartTestButton() {
        getMockTestSection().startTheTest();
    }

    public void verifyUserLandedOnTestListingPage() {
        System.out.println(mockTestPageTitle.getText());
        Assert.assertEquals(mockTestPageTitle.getText(), "Welcome to the New Test on Embibe!");
    }

    public void clearSearchField() {
        jsClick(searchField);
        searchField.clear();
    }

    public void searchForExamSubjectAndTopic(String searchContent) {
            searchField.sendKeys(searchContent);
            jsClick(selectBySearch);
    }

    public void irrelevantSearch(String searchKey){
        try{
            searchField.sendKeys(searchKey);
            jsClick(selectBySearch);
            Assert.fail("User search irrelevant exam/subject/concept" + searchKey +"but still it's working");
        }catch (NoSuchElementException ne){
            System.out.println("Irrelevant search functionality is working as expected");
        }
    }

    public void isDescriptionShowing() {
        Assert.assertTrue(descriptionWidget.isDisplayed(), "Description widget is not available");
    }

    public void isShowMoreORShowLessDisplayingAndWorking() {
        Assert.assertTrue(showMoreORLessButton.isDisplayed(), "Show more is not available");
        Assert.assertEquals(showMoreORLessButton.getText(), "SHOW MORE", "\"SHOW MORE\" button is not display");
        jsClick(showMoreORLessButton);
        isDescriptionShowing();
        Assert.assertEquals(showMoreORLessButton.getText(), "SHOW LESS", "\"SHOW LESS\" button is not display");
        jsClick(showMoreORLessButton);
        isDescriptionShowing();
    }

    public void verifyTestIsGettingAutoSubmitted() {
        examSummaryPage.testSummaryBoxIsDisplayed();
        Assert.assertTrue(examSummaryPage.showMeFeedbackButtonIsDisplayed(), "Show Me Feedback Button is not displayed ");

    }

    public void verifyUserIsNotConnectedToInternet() {
        Assert.assertTrue(offlineSnackbar.isDisplayed(), "Offline snackbar message is not displayed");
        Assert.assertEquals(offlineSnackbar.getText(), "Looks like you are no longer connected to the internet. Don't worry you can continue taking the test.\nDISMISS", "Snackbar Text is not matching");
    }

    public void verifyUserIsConnectedToInternet() {
        Assert.assertTrue(onlineSnackbar.isDisplayed(), "Online snackbar message is not displayed");
        Assert.assertEquals(onlineSnackbar.getText(), "You are online now. Please continue the test.\nDISMISS", "Snackbar Text is not matching");
    }


    public void verifySelectedTestCardIsDisplayed(String testType) {
//       testListContainerSection.selectTestType();
        testListContainerSection.verifySelectedTestTypeAppears(testType);
    }

    public void verifyTestCardDetailsIsDisplayed() {
        testListContainerSection.testTopicDetailsIsShown();
        testListContainerSection.showAllLinkIsDisplayed();
    }

    public void verifyUserNavigatesWithRespectToSearch(String search) {
        Assert.assertTrue(driver.getCurrentUrl().toLowerCase().replace("-", " ").contains(search.toLowerCase()), " user is not land on" + search + "page");
    }

    public void verifyClearSearchIsWorking() {
        Assert.assertEquals(searchField.getText(), "", "Clear search is not working");
    }


    public void clickResume() {
        jsClick(resume);
    }


    public void selectTestType(String testType) {
        testListContainerSection.selectTestType(testType);
    }

    public void selectLanguage(String language) {
        testListContainerSection.selectLanguageFromDropDown(language);
    }

    public void verifyLanguageLabel(String language) {
        testListContainerSection.verifySelectedLanguageAppears(language);
    }

    public void startTest() {
        waitForElementTobeClickable(startTest.get(0));
        jsClick(startTest.get(0));
        switchToActiveTab();
    }

    public void startTest(int i) {
        click_show_more();
        try {
            Thread.sleep(3000);
        }catch (Exception e){}
        waitForElementTobeClickable(startTest.get(i));
        jsClick(startTest.get(i));
        switchToActiveTab();
    }

    public void click_show_more() {
        List<WebElement> sm;
        do {
            sm = driver.findElements(By.cssSelector("div.test-innerlist-show-more > .link-more-component "));
            if(sm.size()>0){
                sm.get(0).click();
            }
            try{
                Thread.sleep(3000);
            }catch(Exception e){}
        }while(sm.size()>0);
    }

    public void startTestWithIndex(int i) {
        waitForElementTobeClickable(startTest.get(i - 1));
        jsClick(startTest.get(i));
        switchToActiveTab();
    }

    public void verifyTitleOfTheExam(String testName) {
        testStatusContainerSection.verifyTheTestTitleRunningInProgress(testName);
    }

    public void leaveTheTestPage() {
        acceptAlertPopUp();
    }

    public void verifySearchNavigationWithTestName(String search) {
        Assert.assertEquals(testName.getText(), search + " " + "Tests");
    }

    public void verifyHideLink() {
        exam.stream().map(ex -> ex.getText()).collect(Collectors.toList()).forEach(el -> {
            WebElement element = driver.findElement(By.xpath(String.format("//div[text()='%s']", el)));
            waitForElementTobeClickable(element);
            jsClick(element);
            testListContainerSection.getTheTestType().stream().forEach(e -> {
                testListContainerSection.selectTestType(e);
                Assert.assertTrue(testCard.isDisplayed());
                Assert.assertEquals(showAllORHide.getText(), "Hide");
                showAllORHide.click();
                Assert.assertEquals(showAllORHide.getText(), "Show All");
            });
        });

    }

    public void clickTestTypeFilter() {
        jsClick(testFilter);
    }

    public void selectAnyTestType() {
        jsClick(selectTestType);
    }

    public void verifyTestCardsNumber() {
        exam.stream().map(ex -> ex.getText()).collect(Collectors.toList()).forEach(el -> {
            WebElement element = driver.findElement(By.xpath(String.format("//div[text()='%s']", el)));
            waitForElementTobeClickable(element);
            jsClick(element);
            testListContainerSection.getTheTestType().stream().forEach(e -> {
                testListContainerSection.selectTestType(e);
                Assert.assertTrue(testCards.size() <= 10);
            });
            clickShowAllOrHideButton();
        });
    }

    public void verifyShowMoreLink() {
        exam.stream().map(ex -> ex.getText()).collect(Collectors.toList()).forEach(el -> {
            WebElement element = driver.findElement(By.xpath(String.format("//div[text()='%s']", el)));
            waitForElementTobeClickable(element);
            jsClick(element);
            testListContainerSection.getTheTestType().stream().forEach(e -> {
                testListContainerSection.selectTestType(e);
                if (testCards.size() > 10) {
                    Assert.assertTrue(showMore.isDisplayed(), "show more is not  displaying");
                } else if (testCards.size() < 10) {
                    try {
                        Assert.assertFalse(showMore.isDisplayed(), "show more is not displaying");
                    } catch (NoSuchElementException exception) {
                        System.out.println(String.format("Show more button is not displaying because test cards count is %s is less then 10", testCards.size()));
                    }
                }
            });
        });
    }

    public void clickShowAllOrHideButton() {
        showAllORHide.click();
    }


    public void verifyMoreInfoFunctionality() {
        waitForPageToLoad();
        String testName = testListContainerSection.getTheTestName();
        testListContainerSection.clickOnMoreInfoLink();
        testListContainerSection.verifyUserHasLandedOnMoreInfoPage("more-info");
        moreInfoPage.testFeaturesIsDisplayed();
        moreInfoPage.verifyUserIsLandedOnSameTestInfo(testName);
        moreInfoPage.startOrResumeTestIsDisplayed();
        moreInfoPage.testFeaturesIsDisplayed();
        moreInfoPage.testSummaryWidgetIsDisplayed();
        moreInfoPage.goBackToListingPage();
        waitForPageToLoad();
        assertUrlHasResetTo(Properties.testListingUrl);

    }

    public void clickMoreInfo(){
        testListContainerSection.clickOnMoreInfoLink();
    }

    public void verifyUserNavigateToRespectiveGoal(String goal) {
        Assert.assertEquals(goalName.getText(), goal, "User navigates to wrong goal");

    }

    public void clickExamLink(String examName) {
        refreshPage();
        waitForElementTobeClickable(examLink);
        driver.findElement(By.cssSelector(".breadcrumb-node:nth-child(2)")).click();
        Assert.assertEquals(examLink.getText(), examName);

    }

    public void verifyDescriptionWrtSearch(String searchContent) {
        Assert.assertTrue(descriptionWidget.getText().contains(searchContent));
    }


    public void verifyBreadCrumb(String goals, String exam, String subject, String subSubject, String concept) {
        waitForPageToLoad();
        Assert.assertEquals(goalName.getText(), goals, "Navigated to wrong goals");
        Assert.assertEquals(examLink.getText(), exam, "Navigated to wrong exam");
        Assert.assertEquals(subjectLink.getText(), subject, "Navigated to wrong subject");
        Assert.assertEquals(subSubjectLink.getText(), subSubject, "Navigated to wrong sub subject");
        Assert.assertEquals(conceptLink.getText(), concept, "Navigated to wrong concept");
    }


    public void clickOnViewFeedbackButton() {
        waitForElementToBeVisible(viewFeedBackButton);
        jsClick(viewFeedBackButton);
    }

    public void verifySubjectAvailableForExam(String subject) {
        Assert.assertEquals(subjectLink.getText(), subject, "Subject is not matching");
    }

    public void startTestWithDifferentTab() {
        wait(2000);
        waitForElementTobeClickable(startTest.get(0));
        jsClick(startTest.get(0));
        switchToParentTab();
    }

    public void startTestWithDifferentTab(int i) {
        wait(2000);
        waitForElementTobeClickable(startTest.get(i));
        jsClick(startTest.get(i));
        //switchToParentTab();
        switchToActiveTab();
    }

    public void startSecondTest() {
        wait(2000);
        waitForElementTobeClickable(startTest.get(1));
        jsClick(startTest.get(1));
        swicthMultipleTab();
    }

    public void verifyUserNavigateToTestListing() {
        closeChildTab();
        switchParentTab();
    }

    public void verifyInProgressTestForMultipleTest(String firstTest, String secondTest) {
        Assert.assertEquals(testInProgress.get(0).getText().toLowerCase(), firstTest.toLowerCase(), "Test Names are not matching, expected " + firstTest + " " + "but found " + testInProgress.get(0).getText());
        Assert.assertEquals(testInProgress.get(1).getText().toLowerCase(), secondTest.toLowerCase(), "Test Names are not matching, expected " + secondTest + " " + "but found " + testInProgress.get(1).getText());
    }

    public void verifyInprogressTest(String test) {
        Assert.assertEquals(testInProgress.get(0).getText().toLowerCase(), test.toLowerCase(), "Test name is not matching. Expected is" + testInProgress.get(0).getText() + " but found " + test);
    }

    public void verifyTimerWorkingForInProgressTest(int waitForSecond, int i) throws ParseException {
        String beforeTime = inProgressTimer.get(i).getText();
        wait(waitForSecond * 1000);
        String afterTime = inProgressTimer.get(i).getText();
        DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        Date date = sdf.parse(beforeTime);
        Date date2 = sdf.parse(afterTime);
        long difference = Math.abs(date2.getTime() - date.getTime());
        long timeDifference = difference / 1000 % 60;
        Boolean diff = timeDifference - waitForSecond <= 2;
        Assert.assertTrue(diff, "timer is not working properly during test its expected " + waitForSecond + " second " + " but found " + timeDifference + " second during test in progress");
    }

    public void verifyTimerShouldBeSameForTestListAndQuesPage()throws ParseException{
//        dismissAlertPopUp();
        switchToActiveTab();
        String timeOnTestPage = examTimer.getText();
        DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        Date date = sdf.parse(timeOnTestPage);
        switchParentTab();
        refreshPage();
        String timeOnTestListing = inProgressTestTimer.getText();
        Date date2 = sdf.parse(timeOnTestListing);
        long difference = Math.abs(date.getTime() - date2.getTime());
        long timeDifference = difference / 1000 % 60;
        Boolean diff = timeDifference <= 20;
        Assert.assertTrue(diff, "timer is not working properly");
    }

    public String getFirstTestName() {
        return testCardsName.get(0).getText();
    }

    public void verifyTestTypeAndLanguageDropdown() {
        testListContainerSection.verifyTestTypeDropDownDisplaying();
        testListContainerSection.verifyLanguageDropDownDisplaying();
    }

    public void clickShowAllForAllTestCards() {
        while (testCards.size() >= 10) {
            try {
                if (showMore.isDisplayed()) {
                    jsClick(showMore);
                }
            } catch (NoSuchElementException noSuchElement) {
                break;
            }
        }
    }

    public int getTestCardsNumber() {
        return testCards.size();

    }

    public String getTestCount() {
        return testCount.getText().replaceAll(".+:", "");
    }

    public void verifyTestCountWrtTestCards(int testCount, int testCards) {
        Assert.assertEquals(testCount, testCards, "Test cards count are not matching");
    }

    @Step("For an attempted Test Card: Test Score, Attempt Count and View Feedback button is displayed")
    public void assertThatTestDetailsIsDisplayed() {
        testListContainerSection.assertThatAttemptTypesAndFeedbackButtonIsDisplayed();
    }

    public void compareTestListingAndFeedBackPageData() {

    }

    public void verifyTestCardsCountForAllTestTypes() {
        exam.stream().map(ex -> ex.getText()).collect(Collectors.toList()).forEach(el -> {
            WebElement element = driver.findElement(By.xpath(String.format("//div[text()='%s']", el)));
            waitForElementTobeClickable(element);
            jsClick(element);
            testListContainerSection.getTheTestType().stream().forEach(e -> {
                testListContainerSection.selectTestType(e);
                clickShowAllForAllTestCards();
                int testCards = getTestCardsNumber();
                String testCounts = getTestCount().trim();
                int tests = Integer.parseInt(testCounts);
                verifyTestCountWrtTestCards(testCards, tests);
            });
        });
    }


    public void verifyTestDataIsSameOnFeedbackAndTestListingPage() {
        switchParentTab();
        refreshPage();
        selectTestType(TestTypes.TEST_TYPE_FULL_TEST);
        assertThatTestDetailsIsDisplayed();
        HashMap testListingMap = testListingAttemptDetailsSection.storeAttemptsAndValuesInMap();
        clickOnViewFeedbackButton();
        switchToActiveTab();
        examSummaryPage.verifyFeedbackPage();
        examSummaryPage.clickOnOverallPerformanceSection();
        HashMap feedbackPageMap = attemptsToMap.getAttemptTypesAndValues();
        Assert.assertTrue(testListingMap.equals(feedbackPageMap),"The test data on test listing page and feedback page are not same");
    }
    public void verifyTheDetailsOfInProgressSection(String testName) {
        switchParentTab();
        refreshPage();
        verifyTitleOfTheExam(testName);
        testStatusContainerSection.verifyResumeButtonIsDisplayed();
    }
    public void verifyTestCardsWrtLanguage(){
        exam.stream().map(ex -> ex.getText()).collect(Collectors.toList()).forEach(el -> {
            WebElement element = driver.findElement(By.xpath(String.format("//div[text()='%s']", el)));
            waitForElementTobeClickable(element);
            jsClick(element);
            testListContainerSection.getTheTestType().stream().forEach(e-> {
                testListContainerSection.selectTestType(e);
                testListContainerSection.getTheLanguageType().stream().forEach(l ->{
                    testListContainerSection.selectLanguageType(l);
                    clickShowAllForAllTestCards();
                    testCardsName.stream().forEach(cardName ->{
                        if(l == "Hindi") {
                            boolean isHindi = cardName.getText().chars().anyMatch(c -> Character.UnicodeBlock.of(c) == Character.UnicodeBlock.DEVANAGARI);
                            Assert.assertTrue(isHindi, "Test cards hindi language is not matching with language drop down filter");
                        }
                        else {
                            boolean isEnglish = true;
                            Assert.assertTrue(isEnglish, "Test cards english language is not matching with language drop down filter");
                        }
                    });
                });
            });
        });
    }

    public void verifySubjectLevelDescription(String searchContent, String exam){
        try{
        Assert.assertTrue(descriptionWidget.getText().contains(searchContent), "search subject content is not available");
        Assert.assertTrue(showMoreORLessButton.isDisplayed(), "");
    }catch (NoSuchElementException exception){
            Assert.fail("Subject level description is not available in"+ " " + exam + " " +  "exam for " + searchContent);
        }
    }

    public void startTestAndBackToTestListing(int i) {
        wait(2000);
        waitForElementTobeClickable(startTest.get(i));
        jsClick(startTest.get(i));
        switchToParentTab();
    }

    public void overAllwidgetDisplaying(){
        Assert.assertTrue(overAllWidget.isDisplayed(), "Over all widget is not displaying on test listing page");
    }
}
