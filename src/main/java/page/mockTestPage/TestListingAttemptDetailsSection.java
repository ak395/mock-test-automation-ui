package page.mockTestPage;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TestListingAttemptDetailsSection extends BasePage {

    @FindBy(css = ".attempt-wrapper")
    private List<WebElement> attemptDetails;

    public TestListingAttemptDetailsSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public HashMap storeAttemptsAndValuesInMap() {
        waitForListOfToBeVisible(attemptDetails);
        HashMap map = new HashMap();

        if (System.getProperty("env").equalsIgnoreCase("production")) {
            attemptDetails.stream().forEach(element -> {
                String attemptType = StringUtils.substringBetween(element.findElement(By.cssSelector(".attempt-type-badge")).getAttribute("src"), "assets/images/", ".svg");
                List<String> attemptTypeCount = Arrays.asList(element.findElement(By.cssSelector(".attempt-score")).getText().split("/"));

                map.put(attemptType, attemptTypeCount.get(0));
            });
        }
        else {
            attemptDetails.stream().forEach(element -> {
                String attemptType = StringUtils.substringBetween(element.findElement(By.cssSelector(".attempt-type-badge")).getAttribute("src"), "divum/images/", ".svg");
                List<String> attemptTypeCount = Arrays.asList(element.findElement(By.cssSelector(".attempt-score")).getText().split("/"));

                map.put(attemptType, attemptTypeCount.get(0));
            });
        }
        System.out.println("TestListing Map Details");
        map.forEach((key, value) -> System.out.println(key + " : " + value));

        return map;

    }

    public void userIsGettingAllAttemptsDetails() {
        Assert.assertEquals(attemptDetails.size(), Integer.parseInt("6"), "The count of attempts is less than actual");
    }
}
