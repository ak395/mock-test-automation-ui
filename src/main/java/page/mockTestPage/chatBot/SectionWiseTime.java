package page.mockTestPage.chatBot;

import driver.DriverProvider;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import page.BasePage;

import java.time.Duration;
import java.util.List;

public class SectionWiseTime extends BasePage {

    @FindBy(css = ".subject-wise-time-wrapper .widget-title")
    private WebElement subjectWiseTimeWidgetHeading;

    @FindBy(css = ".subject-wise-time-wrapper")
    private WebElement subjectWiseTimeWidget;

    @FindBy(css = ".subject-wise-time-wrapper .slider")
    private WebElement subjectWiseTimeCompareSlider;

    @FindBy(css = ".highcharts-legend-item")
    private List<WebElement> subjectWiseTimeData;

    public SectionWiseTime() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void verifySectionWiseTimeIsDisplay() {
        waitForElementToBeVisible(subjectWiseTimeWidget);
        waitForElementToBeVisible(subjectWiseTimeWidgetHeading);
        Assert.assertTrue(subjectWiseTimeWidget.isDisplayed(), "section wise time widget is not display");
        Assert.assertEquals(subjectWiseTimeWidgetHeading.getText().toLowerCase(), "section wise time", "section wise time widget heading is not correct");
    }

    public void clickOnCompareSlider() {
        waitForElementToBeDisplay(subjectWiseTimeCompareSlider);
        Assert.assertTrue(subjectWiseTimeCompareSlider.isDisplayed(), "Section Wise Time Widget comapre slider is not working");
        jsClick(subjectWiseTimeCompareSlider);
    }

    public void subjectWiseTimerData() {
        try {
            waitForListOfElementToBeDisplay(subjectWiseTimeData);
            for (int i = 0; i < subjectWiseTimeData.size(); i++) {
                String text = subjectWiseTimeData.get(i).getText();
                System.out.println(text);
            }
        } catch (Exception e) {
            System.out.println("inside catch block");
            Wait wait = new FluentWait(driver).withTimeout(Duration.ofSeconds(30)).ignoring(StaleElementReferenceException.class).pollingEvery(Duration.ofSeconds(2));
            wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOfAllElements(subjectWiseTimeData)));
        }


    }

}
