package page.mockTestPage.chatBot;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import utils.Properties;

import java.util.List;

public class ChatBotPage extends BasePage {

    @FindBy(css = ".start-chatting-btn")
    private WebElement startChatButton;
    @FindBy(className = "right-direction-msg")
    private WebElement clickOnHey;
    @FindBy(xpath = "//span[text()='Awesome! Looking forward to it.']")
    private WebElement clickOnawesome;
    @FindBy(xpath = "//span[text()='Sure \uD83D\uDE00']")
    private WebElement clickOnsure;
    @FindBy(xpath = "//span[text()='Nope \uD83D\uDE12']")
    private WebElement nope;
    @FindBy(xpath = "//span[text()='Skip']")
    private WebElement clickOnSkip;
    @FindBy(className = "test-feedback-image")
    private WebElement testFeedBackEmoji;
    @FindBy(name = "name")
    private WebElement nameField;
    @FindBy(name = "email")
    private WebElement emailField;
    @FindBy(xpath = "//div[text()='Embibe Guide']")
    private WebElement embibeGuide;
    @FindBy(className = "checkmark")
    private WebElement checkBox;
    @FindBy(css = ".save-btn")
    private WebElement signUp;
    @FindBy(className = "register-message")
    private WebElement signUpSuccessfullyMessage;
    @FindBy(xpath = "//span[text()='Done']")
    private WebElement doneBtn;
    @FindBy(css = "img[src*='in-control']")
    private WebElement inControlBehaviourEmoji;

    @FindBy(css = "img[src*='slow']")
    private WebElement slowBehaviourEmoji;

    @FindBy(css = "img[src*='struggling']")
    private WebElement strugglingBehaviourEmoji;

    @FindBy(css = ".message-widget-wrapper:nth-of-type(7) .content")
    private WebElement BehaviourEmojiMessage;

    @FindBy(xpath = "//span[text()='Show me more']")
    private WebElement showMeMoreButton;

    @FindBy(xpath = "//span[contains(text(),'Guess what?')]")
    private WebElement showMeMoreMessage;
    @FindBy(css = ".left-direction:nth-of-type(10) .content")
    private WebElement showMeMoreMessage1;

    @FindBy(xpath = "//span[contains(text(),'got')]")
    private WebElement okGotItButton;

    @FindBy(css = ".left-direction:nth-of-type(12) .content")
    private WebElement okGotItMessage;


    @FindBy(xpath = "//span[contains(text(),'View overall')]")
    private WebElement OverAllPerformanceButton;

    @FindBy(className = "attempt-analysis-wrapper")
    private WebElement OverAllPerformancePage;


    @FindBy(xpath = "//span[contains(text(),'Sure let')]")
    private WebElement sureLetSeeButton;

    @FindBy(xpath = "//span[contains(text(),'move on')]")
    private WebElement LetsMoveOnButton;
    @FindBy(xpath = "//span[contains(text(),'Do you know')]")
    private WebElement LetsMoveSubjectVerifyMessage;

    @FindBy(xpath = "//span[text()='Physics' and @class='content']")
    private WebElement clickOnPhysicsSubject;

    @FindBy(className = "options-wrapper")
    private WebElement choiceSubjectTabToCheckMarks;
    @FindBy(xpath = "//span[text()='Awesome! Yes, it is ']")
    private WebElement subjectMessage;
    @FindBy(xpath = "//span[contains(text(),'s move ahead')]")
    private WebElement thanksLetsMoveAhead;

    @FindBy(xpath = "//span[contains(text(),'time management')]")
    private WebElement timeManagementLeftSideMessage;
    @FindBy(css = ".test-feedback-image")
    private WebElement feedback;

    @FindBy(xpath = "//span[text()='No']")
    private WebElement noButton;
    @FindBy(xpath = "//span[text()='Yes']")
    private List<WebElement> yesButton;
    @FindBy(xpath = "//span[contains(text(),'Nope! Let')]")
    private WebElement nopeLetsMoveAhead;
    @FindBy(xpath = "//span[contains(text(),'Awesome! Let')]")
    private WebElement awesomeLetsMoveAheadButton;

    @FindBy(xpath = "//span[contains(text(),'Nice')]")
    private WebElement niceButton;

    @FindBy(xpath = "//span[contains(text(),'s proceed')]")
    private WebElement letsProcessed;

    @FindBy(css = ".message-widget-wrapper:nth-child(38) .content")
    private WebElement letsProcessedMessage;

    @FindBy(xpath = "//span[contains(text(),'really cool!')]")
    private WebElement yesItWasReallyCoolButton;

    @FindBy(xpath = "//span[contains(text(),'not really')]")
    private WebElement notReallyButton;
    @FindBy(xpath = "//span[contains(text(),'Okay. Let')]")
    private WebElement okLetsMoveOn;

    @FindBy(xpath = "//span[contains(text(),'see more \uD83D\uDE42')]")
    private WebElement yesLetsSeeMore;
    @FindBy(xpath = "//span[contains(text(),'Not anymore')]")
    private WebElement notAnyMore;

    @FindBy(xpath = "//span[contains(text(),'Show me more!')]")
    private WebElement showMeMoreFollowedByTimeUtilizationWidget;

    @FindBy(xpath = "//span[contains(text(),'s go!')]")
    private WebElement letsGoFollowedByFirstLookWidget;

    @FindBy(xpath = "//span[contains(text(),'Not Really')]")
    private WebElement notReallyFollowedByFirstLookWidget;

    @FindBy(xpath = "//span[contains(text(),'Yes. Show me')]")
    private WebElement yesShowMeFollowedByFirstLookWidget;

    @FindBy(xpath = "//span[contains(text(),'keep moving.')]")
    private WebElement keepMovingButton;


    public ChatBotPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void verifyStartChatButtonIsDisplay() {
        waitForElementTobeClickable(startChatButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(startChatButton.isDisplayed(), "start chat button is not display for test " + testPath);
    }

    public void clickOnStartChatBtn() {
        waitForElementToBeDisplay(startChatButton);
        jsClick(startChatButton);


    }

    public void verifyHeyButtonIsDisplay() {
        waitForElementTobeClickable(clickOnHey);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(clickOnHey.isDisplayed(), "Hey button is not display for test " + testPath);
    }


    public void clickOnHey() {
        jsClick(clickOnHey);

    }

    public void verifyAwesomeButtonIsDisplay() {
        waitForElementTobeClickable(clickOnawesome);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(clickOnawesome.isDisplayed(), "Awesome button is not display for test " + testPath);
    }

    public void clickOnAwesome() {
        jsClick(clickOnawesome);

    }

    public void clickOnSure() {
        jsClick(clickOnsure);

    }

    public void clickOnSkip() {
        jsClick(clickOnSkip);

    }

    public void clickFeedBackImage() {
        jsClick(feedback);
    }

    public void verifyTestFedBackEmojiIsDisplay() {
        waitForElementTobeClickable(testFeedBackEmoji);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(testFeedBackEmoji.isDisplayed(), "test feedback emoji is not display for test " + testPath);
    }

    public void clickOnTestFedBackButton() {
        jsClick(testFeedBackEmoji);

    }

    public void verifyUserNameIsNotDisplayForGuestUser() {
        Assert.assertEquals(clickOnHey.getText(), "Hey!", "Hey! is not display for guest User");

    }

    public void verifyOptionSureAndNopeIsDisplayForGuestUser() {
        Assert.assertTrue(clickOnsure.getText().contains("Sure "), "Sure  is not display for guest User");
        Assert.assertTrue(nope.getText().contains("Nope "), "Nope is not display for guest User");

    }


    public void verifyInControlBehaviourEmoji(){
        waitForElementTobeClickable(inControlBehaviourEmoji);
        Assert.assertTrue(inControlBehaviourEmoji.isDisplayed(), "in control Behaviour meter emoji is not display");
        jsClick(inControlBehaviourEmoji);
        waitForElementToBeVisible(BehaviourEmojiMessage);
        Assert.assertEquals(BehaviourEmojiMessage.getText(), "Love the confidence \uD83D\uDE04! Let me show you how you actually did.");
    }

    public void verifySlowBehaviourEmoji() {
        waitForElementTobeClickable(slowBehaviourEmoji);
        Assert.assertTrue(slowBehaviourEmoji.isDisplayed(), "in control Behaviour meter emoji is not display");
        jsClick(slowBehaviourEmoji);
        Assert.assertEquals(BehaviourEmojiMessage.getText(), "Ahh, take it easy! Let me show you how you actually did.");
    }

    public void verifyStrugglingBehaviourEmoji() {
        waitForElementTobeClickable(strugglingBehaviourEmoji);
        Assert.assertTrue(strugglingBehaviourEmoji.isDisplayed(), "in struggle Behaviour meter emoji is not display");
        jsClick(strugglingBehaviourEmoji);
        waitForElementToBeVisible(BehaviourEmojiMessage);
        Assert.assertEquals(BehaviourEmojiMessage.getText(), "That bad, huh? Let me show you how you actually did.");
    }

    public void verifyShowMeMoreButtonIsDisplay() {
        waitForElementTobeClickable(showMeMoreButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(showMeMoreButton.isDisplayed(), "show me more button is not display for test " + testPath);
    }

    public void verifyShowMeMoreMessageIsDisplay() {
        waitForElementToBeVisible(showMeMoreMessage1);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertEquals(showMeMoreMessage1.getText(), "Oh no! Looks like you did not spend enough time on this test. Our insights will only be helpful if you take the test seriously.", "after click on show me more button correct message is not display for test  " + testPath);
    }

    public void clickOnShowMeMoreButton() {
        waitForElementTobeClickable(showMeMoreButton);
        jsClick(showMeMoreButton);

    }

    public void verifyTrueScoreMessage() {
        waitForElementToBeDisplay(showMeMoreMessage);
        wait(1000);
        Assert.assertEquals(showMeMoreMessage.getText(), "Guess what? I can tell exactly how much more you could have scored in this test. We call it your \"True Score\". Want to see it?");

    }

    public void verifyOkGotItIsDisplay() {
        waitForElementTobeClickable(okGotItButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(okGotItButton.isDisplayed(), "ok got it button is not display for test " + testPath);
    }


    public void clickOnOkGotItButton() {
        waitForElementTobeClickable(okGotItButton);
        jsClick(okGotItButton);
    }

    public void verifyOkGotMessageIsDisplay() {
        waitForElementToBeVisible(okGotItMessage);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertEquals(okGotItMessage.getText(), "From next time, please attempt more than 10% of total questions and spend more than 10% of the test duration to view our insights.", "overall performance mars  message  is not display correctly for test " + testPath);
    }

    public void verifyOverAllPerformanceButtonIsDisplay() {
        waitForElementTobeClickable(OverAllPerformanceButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(OverAllPerformanceButton.isDisplayed(), "overall performance button is not display for test " + testPath);
    }


    public void clickOverAllPerformanceButton() {
        waitForElementTobeClickable(OverAllPerformanceButton);
        jsClick(OverAllPerformanceButton);
    }

    public void verifyOverAllPerformancePageIsDisplay() {
        waitForElementToBeVisible(OverAllPerformancePage);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(OverAllPerformancePage.isDisplayed(), "overall performance page is not display after click on Over All Performance button for test " + testPath);
    }

    public void verifySureLetsSeeButtonIsDisplay() {
        waitForElementTobeClickable(sureLetSeeButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(sureLetSeeButton.isDisplayed(), "Sure lets see button is not display for test " + testPath);
    }

    public void clickOnSureLetSeeButton() {
        waitForElementTobeClickable(sureLetSeeButton);
        jsClick(sureLetSeeButton);
    }


    public void verifyLetsMoveOnButtonIsDisplay() {
        waitForElementTobeClickable(LetsMoveOnButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(LetsMoveOnButton.isDisplayed(), "Lets Move On button is not display for test " + testPath);
    }

    public void clickOnLetsMoveOn() {
        waitForElementTobeClickable(LetsMoveOnButton);
        jsClick(LetsMoveOnButton);
        waitForElementToBeVisible(LetsMoveSubjectVerifyMessage);
        Assert.assertEquals(LetsMoveSubjectVerifyMessage.getText(), "Do you know in which subject you scored the highest?", "After click on Lets Move on Button subject wise report option is not display");
    }

    public void verifySubjectIsDisplay() {
        waitForElementToBeVisible(choiceSubjectTabToCheckMarks);
        Assert.assertTrue(choiceSubjectTabToCheckMarks.isDisplayed(), "subject is not display");
    }


    public void clickOnSubject() {
        waitForElementTobeClickable(clickOnPhysicsSubject);
        jsClick(clickOnPhysicsSubject);
    }

    public void verifyLeftSideSubjectMessageAfterClickOnSubject() {
        waitForElementToBeVisible(subjectMessage);
        Assert.assertTrue(subjectMessage.getText().contains(clickOnPhysicsSubject.getText()), "subject display in subject tab option and subject display in subject message tab is not same");

    }

    public void enterMobileNumber() {
        String mobileNumber = "98" + RandomStringUtils.randomNumeric(8);
        emailField.sendKeys(mobileNumber);
    }

    public void clickEmbibeGuideTab() {
        embibeGuide.click();
    }

    public void verifyThanksLetsMoveAheadButtonIsDisplay() {
        waitForElementToBeVisible(thanksLetsMoveAhead);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(thanksLetsMoveAhead.isDisplayed(), "Thanks Lets Move ahead button is not display for test " + testPath);
    }

    public void clickThanksLetsMoveAhead() {
        waitForElementTobeClickable(thanksLetsMoveAhead);
        jsClick(thanksLetsMoveAhead);
    }

    public void verifyTimeManagementMessage() {
        waitForElementToBeVisible(timeManagementLeftSideMessage);
        Assert.assertEquals(timeManagementLeftSideMessage.getText(), "Did you know that time management is the key to maximising your scoring potential. Would you like to see your time spent across subjects?", "time management message is not display");
    }

    public void verifyYesAndNoButtonIsDisplay() {
        waitForListOfToBeVisible(yesButton);
        waitForElementToBeVisible(noButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(yesButton.get(0).isDisplayed(), "yes button is not display after click on Thanks lest move ahead button for test " + testPath);
        Assert.assertTrue(noButton.isDisplayed(), "No button is not display after click on Thanks lest move ahead button for test " + testPath);
    }

    public void clickOnNoButton() {
        waitForElementTobeClickable(noButton);
        jsClick(noButton);
    }

    public void clickOnYesButton() {
        waitForElementTobeClickable(yesButton.get(0));
        jsClick(yesButton.get(0));
    }

    public void clickOnYesButtonAfterSectionWiseTime() {
        waitForListOfToBeVisible(yesButton);
        jsClick(yesButton.get(1));
    }

    public void verifyYesAndNopeLetsMoveAheadButtonIsDisplay() {
        waitForListOfToBeVisible(yesButton);
        waitForElementToBeVisible(nopeLetsMoveAhead);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(yesButton.get(1).isDisplayed(), "yes button is not display after click on Thanks lest move ahead button for test " + testPath);
        Assert.assertTrue(nopeLetsMoveAhead.isDisplayed(), "No button is not display after click on Thanks lest move ahead button for test " + testPath);
    }

    public void clickOnNopeLetsMoveAhead() {
        waitForElementTobeClickable(nopeLetsMoveAhead);
        jsClick(nopeLetsMoveAhead);
    }


    public void verifyAwesomeLetsMoveAheadIsDisplay() {
        waitForElementToBeVisible(awesomeLetsMoveAheadButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(awesomeLetsMoveAheadButton.isDisplayed(), "awesome Lets Move Ahead Button  is not display after click on save button on collage predictor widget  for test " + testPath);
    }


    public void clickAwesomeLetsMoveAhead() {
        waitForElementTobeClickable(awesomeLetsMoveAheadButton);
        jsClick(awesomeLetsMoveAheadButton);
    }


    public void verifyNiceButtonIsDisplay() {
        waitForElementToBeVisible(niceButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(niceButton.isDisplayed(), "Nice button is not display after click on awesome Lets Move Ahead Button after on collage predictor widget  for test " + testPath);
    }

    public void clickOnNiceButton() {
        waitForElementTobeClickable(niceButton);
        jsClick(niceButton);
    }


    public void verifyLetsProcessedButtonIsDisplay() {
        waitForElementToBeVisible(letsProcessed);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(letsProcessed.isDisplayed(), "lets Processed button is not display after click on Nice Button for test " + testPath);
    }

    public void clickOnLetsProcessedButton() {
        waitForElementTobeClickable(letsProcessed);
        jsClick(letsProcessed);
    }

    public void verifyLetsProcessedMessage() {
        waitForElementToBeVisible(letsProcessedMessage);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertEquals(letsProcessedMessage.getText(), "Did you find the above information helpful?", "After click on lest processed correct message is not display");

    }


    public void verifyNoReallyAndYesItWasCoolButtonIsDisplay() {
        waitForElementToBeVisible(yesItWasReallyCoolButton);
        waitForElementToBeVisible(notReallyButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(yesItWasReallyCoolButton.isDisplayed(), " \"Yes, it was really cool!\" button is not display after click on Lets Processed Button for test " + testPath);
        Assert.assertTrue(notReallyButton.isDisplayed(), "\"No not really\"button is not display after click on Lets Processed  Button for test " + testPath);
    }


    public void clickOnNoReallyButton() {
        waitForElementTobeClickable(notReallyButton);
        jsClick(notReallyButton);
    }

    public void clickOnYesItWasCoolProcessedButton() {
        waitForElementTobeClickable(yesItWasReallyCoolButton);
        jsClick(yesItWasReallyCoolButton);

    }

    public void verifyOkLetsMoveOnButtonIsDisplay() {
        waitForElementToBeVisible(okLetsMoveOn);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(okLetsMoveOn.isDisplayed(), " \"Okay. Let's move on\" button is not display after click on \"Yes, it was really cool!\"  Button for test " + testPath);
    }

    public void clickOnOkLetsMoveOnButton() {
        waitForElementTobeClickable(okLetsMoveOn);
        jsClick(okLetsMoveOn);
    }

    public void verifyYesLetsSeeMoreAndNotAnyMoreButtonIsDisplay() {
        waitForElementToBeVisible(yesLetsSeeMore);
        waitForElementToBeVisible(notAnyMore);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(yesLetsSeeMore.isDisplayed(), " \"Yes let's see more \uD83D\uDE42\" button is not display followed by Subject swaps  widget for test " + testPath);
        Assert.assertTrue(notAnyMore.isDisplayed(), "\"Not anymore, I'm bored. \uD83D\uDE1E\"button is not display followed by Subject swaps  widget for test " + testPath);
    }


    public void clickOnYesLetsSeeMoreButton() {
        waitForElementTobeClickable(yesLetsSeeMore);
        jsClick(yesLetsSeeMore);
    }

    public void clickOnNotAnyMoreButton() {
        waitForElementTobeClickable(notAnyMore);
        jsClick(notAnyMore);

    }


    public void verifyShowMeMoreFollowedBYTimeUtilizationWidgetButtonIsDisplay() {
        waitForElementToBeVisible(showMeMoreFollowedByTimeUtilizationWidget);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(showMeMoreFollowedByTimeUtilizationWidget.isDisplayed(), " \"Show me more!\" button is not display after click on \"Yes let's see more \uD83D\uDE42\"  Button followed by time utilization widget for test " + testPath);
    }


    public void clickOnShowMeMoreFollowedBYTimeUtilizationWidget() {
        waitForElementTobeClickable(showMeMoreFollowedByTimeUtilizationWidget);
        jsClick(showMeMoreFollowedByTimeUtilizationWidget);

    }

    public void verifyLetsGoFollowedByFirstLookWidgetButtonIsDisplay() {
        waitForElementToBeVisible(letsGoFollowedByFirstLookWidget);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(letsGoFollowedByFirstLookWidget.isDisplayed(), " \"Let's go!\" button is not display after click on \"Show me more!\"  Button followed by First Look widget for test " + testPath);
    }


    public void clickOnLetsGoFollowedByFirstLookWidget() {
        waitForElementTobeClickable(letsGoFollowedByFirstLookWidget);
        jsClick(letsGoFollowedByFirstLookWidget);

    }


    public void verifyYesShowMeAndNotReallyButtonIsDisplay() {
        waitForElementToBeVisible(yesShowMeFollowedByFirstLookWidget);
        waitForElementToBeVisible(notReallyFollowedByFirstLookWidget);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(yesShowMeFollowedByFirstLookWidget.isDisplayed(), " \"Yes. Show me. \uD83D\uDE03\" button is not display followed by First Look widget for test " + testPath);
        Assert.assertTrue(notReallyFollowedByFirstLookWidget.isDisplayed(), "\"Not Really. \uD83D\uDE12\"button is not display followed by First Look  widget for test " + testPath);
    }


    public void clickOnYesShowMeFollowedByFirstLookWidgetButton() {
        waitForElementTobeClickable(yesShowMeFollowedByFirstLookWidget);
        jsClick(yesShowMeFollowedByFirstLookWidget);
    }

    public void clickOnNotReallyFollowedByFirstLookWidgetAnyMoreButton() {
        waitForElementTobeClickable(notReallyFollowedByFirstLookWidget);
        jsClick(notReallyFollowedByFirstLookWidget);

    }


    public void verifyKeepMovingButtonIsDisplay() {
        waitForElementToBeVisible(keepMovingButton);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(keepMovingButton.isDisplayed(), " \"Let's keep moving.\" button is not display after click on \"Show me more!\"  Button followed by First Look widget for test " + testPath);
    }
    public void clickOnKeepMovingButton() {
        waitForElementTobeClickable(keepMovingButton);
        jsClick(keepMovingButton);

    }

}

