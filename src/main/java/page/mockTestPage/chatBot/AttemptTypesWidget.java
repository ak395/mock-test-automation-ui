package page.mockTestPage.chatBot;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.List;

public class AttemptTypesWidget extends BasePage {

    @FindBy(css = ".question-attempt-type-chart-wrapper")
    private List<WebElement> attemptTypeCharts;

    @FindBy(css = ".question-attempt-type-chart-wrapper .widget-heading")
    private List<WebElement> attemptTypeChartsHeading;

    public AttemptTypesWidget() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }


    public void verifyAttemptTypeChartsIsDisplay() {
        waitForListOfToBeVisible(attemptTypeCharts);
        Assert.assertFalse(attemptTypeCharts.isEmpty(), "attemptType Chart is not display");
        attemptTypeChartsHeading.stream().forEach(e -> Assert.assertTrue(e.isDisplayed(), e.getText() + " widget is not display"));

    }


}
