package page.mockTestPage.chatBot;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

public class MarksPerTimeInterval extends BasePage {

    @FindBy(css = ".marks-spent-per-min-wrapper")
    private WebElement marksPerTimeWidget;
    @FindBy(css = ".marks-spent-per-min-wrapper .widget-title")
    private WebElement marksPerTimeWidgetTitle;

    public MarksPerTimeInterval() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void verifyMarksPerTimeWidgetIsDisplay() {
        waitForElementToBeVisible(marksPerTimeWidget);
        waitForElementToBeVisible(marksPerTimeWidgetTitle);
        Assert.assertTrue(marksPerTimeWidget.isDisplayed(), "Marks per time interval widget is not display after click on nice button");
        Assert.assertEquals(marksPerTimeWidgetTitle.getText().toLowerCase(), "marks per time interval", "Marks per time interval widget heading is not correct after click on nice button");
    }
}
