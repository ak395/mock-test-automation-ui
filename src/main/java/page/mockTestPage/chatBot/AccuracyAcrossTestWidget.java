package page.mockTestPage.chatBot;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import utils.Properties;

public class AccuracyAcrossTestWidget extends BasePage {
    @FindBy(css = ".first-look-vs-reattempt-wrapper")
    private WebElement firstLookVsReattemptedWidget;

    @FindBy(css = ".first-look-vs-reattempt-wrapper .widget-heading")
    private WebElement firstLookVsReattemptedWidgetHeading;

    public AccuracyAcrossTestWidget() {

        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void verifyTimeUtilizationWidgetIsDisplay() {
        waitForElementToBeDisplay(firstLookVsReattemptedWidget);
        waitForElementToBeVisible(firstLookVsReattemptedWidgetHeading);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(firstLookVsReattemptedWidget.isDisplayed(), "first-look-vs-reattempt-wrapper is not display for test " + testPath);
        Assert.assertEquals(firstLookVsReattemptedWidgetHeading.getText(), "First look vs Re-attempt accuracy".toUpperCase(), "first-look-vs-reattempt-wrapper heading should be First look vs Re-attempt accuracy but found " + firstLookVsReattemptedWidgetHeading.getText());
    }

}
