package page.mockTestPage.chatBot;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.List;

public class TrueScorePage extends BasePage {


    @FindBy(xpath = "//div[text()='True Score']")
    private WebElement trueScoreWidget;

    @FindBy(css = ".true-score-wrapper .round")
    private WebElement trueScoreCompareSlider;
    @FindBy(css = ".true-score-wrapper .rankers-name")
    private List<WebElement> TrueScoreRankerMarks;

    @FindBy(css = ".true-score-wrapper .marks-obtained")
    private WebElement trueScoreMarks;

    public TrueScorePage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);

    }

    public void clickOnTrueScoreCompareSlider() {
        waitForElementTobeClickable(trueScoreCompareSlider);
        jsClick(trueScoreCompareSlider);
    }


    public void verifyTrueScoreMarksCompared() {
        waitForListOfToBeVisible(TrueScoreRankerMarks);
        Assert.assertTrue(TrueScoreRankerMarks.size() > 2, " Marks is not compared  with other user");

    }


    public void verifyTrueScoreWidgetDisplay() {
        waitForElementToBeVisible(trueScoreWidget);
        Assert.assertEquals(trueScoreWidget.getText(), "TRUE SCORE");
    }

    public int marksObtainByUser(){
        return Integer.parseInt(trueScoreMarks.getText());
    }


}
