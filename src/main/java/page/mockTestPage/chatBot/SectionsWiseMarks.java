package page.mockTestPage.chatBot;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.List;
import java.util.stream.Collectors;

public class SectionsWiseMarks extends BasePage {

    public SectionsWiseMarks(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".subject-wise-marks-outer-wrapper")
    private WebElement sectionWiseMarksWidget;
    @FindBy(css = ".subject")
    private List<WebElement> subjectName;
    @FindBy(css = ".marks")
    private List<WebElement> subjectActualMarks;
    @FindBy(css = ".positive-stepper-label>span +span")
    private List<WebElement> subjectExpectedMarks;

    public void verifySectionWiseMarksIsDisplay() {

        waitForElementToBeVisible(sectionWiseMarksWidget);
        Assert.assertTrue(sectionWiseMarksWidget.isDisplayed(), "section wise marks is not display");
        Assert.assertEquals(sectionWiseMarksWidget.findElement(By.cssSelector(" .widget-heading")).getText(), "SECTIONS WISE MARKS", "SECTIONS WISE MARKS heading is not display");

    }

    public void verifySubjectDisplayInSectionsWiseMarks(List<String> subject) {
        waitForListOfToBeVisible(subjectName);
        subjectName.forEach(t -> System.out.println(t.getText()));
        Assert.assertEquals(subjectName.stream().map(t -> t.getText().toUpperCase()).collect(Collectors.toList()), subject, "Subject display in section wise marks is not same as subject display in test");
    }

    public void verifySubjectMarksInSectionsWiseMarksForAllQuestionCorrect() {
        Assert.assertEquals(subjectActualMarks.stream().map(t -> t.getText()).collect(Collectors.toList()), subjectExpectedMarks.stream().map(t -> t.getText()).collect(Collectors.toList()), "Subject marks display in \"section wise marks\" is not same as actual marks");
    }
}
