package page.mockTestPage.chatBot;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import utils.Properties;

import java.util.List;

public class CollagePredictor extends BasePage {

    @FindBy(css = ".edit-details-widget")
    private WebElement collagePredictorWidget;
    @FindBy(css = ".edit-details-widget .widget-heading")
    private WebElement collagePredictorHeading;
    @FindBy(css = ".edit-widget-body-items:nth-child(1)")
    private WebElement dreamCollage;
    @FindBy(css = ".css-pduewl-option")
    private List<WebElement> dropDownList;
    @FindBy(css = ".edit-widget-body-items:nth-child(2)")
    private WebElement interestedBranch;
    @FindBy(css = ".edit-widget-body-items:nth-child(3)")
    private WebElement selectState;
    @FindBy(css = ".edit-widget-body-items:nth-child(4)")
    private WebElement selectCategory;
    @FindBy(css = ".save-btn")
    private WebElement saveButton;
    @FindBy(css = ".college-predictor-wrapper")
    private WebElement predictedCollageWidget;
    @FindBy(css = ".college-predictor-list-item")
    private List<WebElement> predictedCollageList;
    @FindBy(css = ".show-more")
    private WebElement showMoreButton;

    public CollagePredictor() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void verifyCollagePredictorIsDisplay() {
        waitForElementToBeVisible(collagePredictorWidget);
        waitForElementToBeVisible(collagePredictorHeading);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(collagePredictorWidget.isDisplayed(), "collage predictor is not display for test " + testPath);
        Assert.assertEquals(collagePredictorHeading.getText(), "PROVIDE DETAILS FOR COLLEGE PREDICTOR", "collage predictor heading should be PROVIDE DETAILS FOR COLLEGE PREDICTOR but found " + collagePredictorHeading.getText());
    }


    public void selectDreamCollage() {
        waitForElementTobeClickable(dreamCollage);
        dreamCollage.click();
        dropDownList.get(0).click();

    }

    public void printAllCollage() {
        waitForElementTobeClickable(dreamCollage);
        dreamCollage.click();
        dropDownList.forEach(e -> System.out.println(e.getText()));
        System.out.println();
        String totalCollage = dropDownList.get(dropDownList.size() - 1).getAttribute("id");
        System.out.println("total collage is " + totalCollage);

    }

    public void selectBranch() {
        waitForElementTobeClickable(interestedBranch);
        interestedBranch.click();
        dropDownList.get(0).click();

    }

    public void selectState() {
        waitForElementTobeClickable(selectState);
        selectState.click();
        dropDownList.get(0).click();

    }

    public void selectCategory() {
        waitForElementTobeClickable(selectCategory);
        selectCategory.click();
        dropDownList.get(0).click();
    }

    public void clickOnSaveButton() {
        waitForElementTobeClickable(saveButton);
        saveButton.click();
    }


    public void verifyPredictedCollageWidgetDisplay() {
        waitForElementToBeVisible(predictedCollageWidget);
        Assert.assertTrue(predictedCollageWidget.isDisplayed(), " predicted collage widget is not display");
    }

    public void verifyPredictedCollageListDisplay() {
        waitForListOfToBeVisible(predictedCollageList);
        for (WebElement el : predictedCollageList) {
            Assert.assertTrue(el.isDisplayed(), " predicted collage is not display");

        }
    }

    public void verifyShowMoreButtonIsDisplay() {
        waitForElementToBeVisible(showMoreButton);
        Assert.assertTrue(showMoreButton.isDisplayed(), " show more button is not display in predicted collage widget");
    }
}
