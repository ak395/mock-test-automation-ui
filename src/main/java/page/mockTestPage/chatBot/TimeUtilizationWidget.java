package page.mockTestPage.chatBot;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import utils.Properties;

public class TimeUtilizationWidget extends BasePage {

    @FindBy(css = ".time-utilization-widget-wrapper")
    private WebElement timeUtilizationWidget;

    @FindBy(css = ".time-utilization-widget-wrapper .widget-heading")
    private WebElement timeUtilizationWidgetHeading;

    public TimeUtilizationWidget() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void verifyTimeUtilizationWidgetIsDisplay() {
        waitForElementToBeDisplay(timeUtilizationWidget);
        waitForElementToBeVisible(timeUtilizationWidgetHeading);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(timeUtilizationWidget.isDisplayed(), "time-utilization-widget-wrapper is not display for test " + testPath);
        Assert.assertEquals(timeUtilizationWidgetHeading.getText(), "Last 10 min utilization".toUpperCase(), "time-utilization-widget-wrapper heading should be Last 10 min utilization but found " + timeUtilizationWidgetHeading.getText());
    }


}
