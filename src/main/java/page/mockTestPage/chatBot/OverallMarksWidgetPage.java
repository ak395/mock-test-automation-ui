package page.mockTestPage.chatBot;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import page.examSummary.ExamSummaryPage;
import utils.Properties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Getter
public class OverallMarksWidgetPage extends BasePage {
    private ChatBotPage chatBotPage;
    private ExamSummaryPage examSummaryPage;

    @FindBy(className = "widget-title")
    private WebElement WidgetHeading;

    @FindBy(css = ".marks-obtained")
    private WebElement marksObtainByUser;

    @FindBy(className = "total-marks")
    private WebElement maximumMarks;

    @FindBy(css = ".step:nth-of-type(1) .chart-label")
    private WebElement averageMarks;

    @FindBy(css = ".step:nth-of-type(2) .chart-label")
    private WebElement topScore;
    @FindBy(css = ".user-score-message")
    private WebElement userScoreMessage;
    @FindBy(css = ".min-max-label.min")
    private WebElement minScore;

    @FindBy(css = ".overall-score-wrapper .slider")
    private WebElement compareSlider;

    @FindBy(css = ".rankers-name")
    private List<WebElement> rankerName;

    @FindBy(css = ".rankers-stepper-inner-wrapper .chart-label:nth-child(1)")
    private List<WebElement> rankerMarks;

    @FindBy(css = ".mark-obtained-wrapper .marks-obtained")
    private WebElement overallMarksObtainByUser;
    @FindBy(css = ".mark-obtained-wrapper .total-marks")
    private WebElement overAllTotalMarks;


    public OverallMarksWidgetPage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        chatBotPage = new ChatBotPage(driver);
        examSummaryPage = new ExamSummaryPage();

    }


    public void verifyOverAllMarksWidgetCellIsDisplay() {
        waitForElementTobeClickable(WidgetHeading);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(WidgetHeading.isDisplayed(), "start chat button is not display for test " + testPath);
        Assert.assertEquals(WidgetHeading.getText(), "OVERALL MARKS", String.format("Overall Marks", "OverAll Marks cell title is expected Overall Marks but found %s for test %s ", WidgetHeading.getText(), testPath));
    }

    public void navigateToOverallMarksWidgetForGuestUser() {
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnSure();
        chatBotPage.clickOnSkip();
        chatBotPage.clickOnTestFedBackButton();
        Assert.assertTrue(WidgetHeading.isDisplayed());
    }

    public void navigateToOverallMarksWidgetForLoginUser() {
        chatBotPage.clickOnStartChatBtn();
        chatBotPage.clickOnHey();
        chatBotPage.clickOnAwesome();
        chatBotPage.clickOnTestFedBackButton();
        Assert.assertTrue(WidgetHeading.isDisplayed());
    }


    public void verifyUserScoreIsDisplay() {
        Assert.assertTrue(marksObtainByUser.isDisplayed(), "marks obtain By user is not display");
    }

    public void verifyTotalMarksIsDisplay() {
        Assert.assertTrue(maximumMarks.isDisplayed(), "Maximum marks is not display");
    }

    public void verifyAverageMarksIsDisplay() {
        Assert.assertTrue(averageMarks.isDisplayed(), "average marks is not display");
    }

    public void verifyTopScoreIsDisplay() {
        Assert.assertTrue(topScore.isDisplayed(), "Top Scorers marks is not display");
    }

    public void verifyMinScoreIsDisplay() {
        Assert.assertTrue(minScore.isDisplayed(), "Minimum marks is not display");
    }

    public void verifyUserScoreMessageIsDisplayForAllQuestionCorrect() {
        Assert.assertEquals(userScoreMessage.getText(), "Hurray :)! You've secured rank 1 in this exam");
    }

    public void verifyUserScoreMessageIsDisplayForNegativeScore() {

        Assert.assertEquals(userScoreMessage.getText(), String.format("Oh No ! You lost %s marks in wrong attempts. Make sure to revise your answers next time", Math.abs(marksObtainByUser())));
    }

    public String averageMarks() {
        return averageMarks.getText();
    }

    public int minimumMarks() {
        return Integer.parseInt(minScore.getText());
    }

    public String topScorerMarks() {
        return topScore.getText();
    }

    public void verifyOverallMarksWRTOverallPerformance() {
        int userScore = marksObtainByUser();
        String averageMarks = averageMarks();
        int minimumScore = minimumMarks();
        String topScorer = topScorerMarks();
        int totalMarks = totalMarks();
        driver.manage().window().fullscreen();
        examSummaryPage.clickOnOverallPerformanceSection();
        Assert.assertEquals(examSummaryPage.getScoreObtainByUser(), userScore);
        Assert.assertEquals(examSummaryPage.getTotalScore(), totalMarks);
        Assert.assertEquals(examSummaryPage.minimumMarks(), minimumScore);
        Assert.assertEquals(examSummaryPage.averageMarks(), averageMarks);
        Assert.assertEquals(examSummaryPage.topScorerMarks(), topScorer);
    }


    public void verifyCompareSliderIsDisplay() {
        waitForElementTobeClickable(compareSlider);
        String testPath = driver.getCurrentUrl().replaceAll(Properties.baseUrl, "").replaceAll("/", "-").replaceAll("analyze", "");
        Assert.assertTrue(compareSlider.isDisplayed(), "start chat button is not display for test " + testPath);
    }

    public void clickOnCompareSlider() {
        jsClick(compareSlider);

    }

    public void verifyUserMarksComparedWithRanker() {
        for (WebElement name : rankerName)
            Assert.assertTrue(name.isDisplayed());
    }


    public void verifyUserMarksCompareWithMaximum5Ranker() {
        int size = rankerName.size();
        Assert.assertEquals(size, 5, "More then 5 ranker is present");

    }


    public void verifyRankerNameAndMarksUnique() {
        Map<String, Integer> map = new HashMap<>();
        IntStream.range(0, rankerName.size()).forEach(i -> {
            int marks = Integer.parseInt(rankerMarks.get(i).getText());
            String name = rankerName.get(i).getText();
            map.put(name, marks);
        });
        Assert.assertEquals(map.size(), 5, "5 uniques Ranker's Marks is not display with unique name");

    }

    public int marksObtainByUser(){
        return Integer.parseInt(overallMarksObtainByUser.getText());
    }

    public int totalMarks(){
        return Integer.parseInt(overAllTotalMarks.getText().replace("/",""));
    }

    public void verifyOverAllMarks(){
        Assert.assertEquals(marksObtainByUser(),totalMarks(),"marks obtain by user and total marks is not correct");

    }

}
