package page.mockTestPage.chatBot;

import entities.MockTestPlanner;
import mockTestFactory.MyChoice;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SubjectWiseAttempts extends BasePage {
    public SubjectWiseAttempts(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".subject-wise-attempts-wrapper")
    private WebElement subjectWiseAttemptsWrapper;

    @FindBy(css = ".subject-name")
    private List<WebElement> subjectName;

    @FindBy(xpath = ".//tr[not(position()=0)]")
    private WebElement firstSubjectAttempts;

    @FindBy(xpath = ".//tr[not(position()=1)]")
    private WebElement secondSubjectAttempts;

    @FindBy(xpath = ".//tr[(position()=3)]")
    private WebElement thirdSubjectAttempts;

    @FindBy(xpath = "//td[text()='Physics']/following-sibling::td/child::div/child::span[2]")
    private List<WebElement> physicsAttemptType;
    @FindBy(xpath = "//td[text()='Physics']/following-sibling::td/child::div/child::span[2]")
    private List<WebElement> chemistryAttemptType;
    @FindBy(xpath = "//td[text()='Physics']/following-sibling::td/child::div/child::span[2]")
    private List<WebElement> MathematicsAttemptType;


    public void verifySubjectWiseAttemptsWrapper() {
        waitForElementToBeVisible(subjectWiseAttemptsWrapper);
        Assert.assertTrue(subjectWiseAttemptsWrapper.isDisplayed(), "subject wise marks is not display");
        Assert.assertEquals(subjectWiseAttemptsWrapper.findElement(By.cssSelector(" .widget-heading")).getText(), "SUBJECT WISE ATTEMPTS", "\"SUBJECT WISE ATTEMPTS\" heading is not display");

    }

    public void verifySubjectDisplayInSubjectWiseAttempts(List<String> subject) {
        waitForListOfToBeVisible(subjectName);
        Assert.assertEquals(subjectName.stream().map(t -> t.getText().toUpperCase()).collect(Collectors.toList()), subject, "Subject display in \"SUBJECT WISE ATTEMPTS\" is not same as subject display in test");
    }


    public void verifySubjectWiseQuestionAttempted(MockTestPlanner planner) {
        waitForElementToBeVisible(firstSubjectAttempts);
        for(WebElement element: subjectName){
            String subject = element.getText();
            List<WebElement> attemptCount = driver.findElements(By.xpath(String.format("//td[text()='%s']/following-sibling::td/*[1]",subject)));
            List<WebElement> attempt = driver.findElements(By.xpath(String.format("//td[text()='%s']/following-sibling::td/span",subject)));
            List<Integer> attemptType = attemptCount.stream().map(e -> Integer.parseInt(e.getText())).collect(Collectors.toList());
            int correctAnswerCount = planner.getNumberOfQuestionsFor(MyChoice.CHOICE_CORRECT,subject);
            int InCorrectAnswerCount = planner.getNumberOfQuestionsFor(MyChoice.CHOICE_INCORRECT,subject);
            int UnAttemptAnswerCount = planner.getNumberOfQuestionsFor(MyChoice.CHOICE_UNATTEMPTED,subject);
            List<Integer> list = new ArrayList();
            list.add(correctAnswerCount);
            list.add(InCorrectAnswerCount);
            list.add(UnAttemptAnswerCount);
            for(int i = 0;i < list.size();i++){
                Assert.assertEquals(list.get(i),attemptType.get(i),String.format("for subject %s %s question should be  %s but in subject wise widget its showing %s ",subject,attempt.get(i),list.get(i),attemptType.get(i)));
            }

        }


    }


}
