package page.mockTestPage.chatBot;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

public class SubjectSwapsPage extends BasePage {

    public SubjectSwapsPage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".subject-swaps-wrapper")
    private WebElement subjectSwapsWidget;
    @FindBy(css = ".subject-swaps-wrapper .widget-heading")
    private WebElement subjectSwapsWidgetHeading;

    public void verifySubjectSwapsWidgetIsDisplay() {
        waitForElementToBeDisplay(subjectSwapsWidget);
        waitForElementToBeDisplay(subjectSwapsWidgetHeading);
        Assert.assertTrue(subjectSwapsWidget.isDisplayed(), "Subject Swaps  widget is not display");
        Assert.assertEquals(subjectSwapsWidgetHeading.getText().toLowerCase(), "subject swaps", "Subject Swaps  widget heading is not correct");
    }

}
