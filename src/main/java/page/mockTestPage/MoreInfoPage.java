package page.mockTestPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MoreInfoPage extends BasePage {

    @FindBy(css = ".back-to-listing > a")
    private WebElement backToListing;

    @FindBy(css = ".feature .sub-title")
    private List<WebElement> testFeatures;

    @FindBy(css = ".heading-sub-wrapper")
    private WebElement testName;

    @FindBy(css = ".start-test-btn")
    private WebElement startOrResumeTest;

    @FindBy(css = ".summary-wrapper")
    private WebElement testSummaryWidet;

    public MoreInfoPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void goBackToListingPage() {

        backToListing.click();
    }

    protected void testFeaturesIsDisplayed() {
        int i = 0;
        List<String> questionTypeInATest = new ArrayList<>();
        questionTypeInATest.add("Total Marks");
        questionTypeInATest.add("No. of Questions");
        questionTypeInATest.add("Test Duration");
        questionTypeInATest.add("Quality Score");
        List<String> testFeaturesOnPage = null;
        testFeatures.forEach(e -> System.out.println(e.getText()));

        while (i < testFeatures.size()) {
            i++;
            testFeaturesOnPage = testFeatures.stream().map(e -> e.getText()).
                    distinct().collect(Collectors.toList());
        }
        assert testFeaturesOnPage != null;
        Assert.assertTrue(questionTypeInATest.retainAll(testFeaturesOnPage), "All the Test Features are not displayed to the user on More Info Page");

    }

    protected void verifyUserIsLandedOnSameTestInfo(String expectedTestName) {
        Assert.assertTrue(testName.getText().contains(expectedTestName), "The test opened on more-info page: "+ testName.getText() +"does not contain the expected test name: "+ expectedTestName);

    }

    protected void startOrResumeTestIsDisplayed() {
        Assert.assertTrue(startOrResumeTest.isDisplayed(), "Start Test or Resume Test button is not displayed on More Info Page");
    }

    protected void testSummaryWidgetIsDisplayed() {
        Assert.assertTrue(testSummaryWidet.isDisplayed(), "Test Summary Widget is not displayed on More Info Page ");
    }
}