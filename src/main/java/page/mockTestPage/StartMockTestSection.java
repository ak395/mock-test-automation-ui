package page.mockTestPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class StartMockTestSection extends BasePage {

    @FindBy(css = ".test_start")
    private WebElement startTestButton;

    @FindBy(css = ".test_name")
    private WebElement testNameElement;

    public StartMockTestSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void startTheTest(){
        waitForElementToBeVisible(startTestButton);
        startTestButton.click();
    }

    public String verifyTheTestName(){
        waitForElementToBeVisible(testNameElement);
        return testNameElement.getText();
    }
}
