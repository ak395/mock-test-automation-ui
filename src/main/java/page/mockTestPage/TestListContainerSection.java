package page.mockTestPage;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import page.BasePage;

import java.util.List;
import java.util.stream.Collectors;

public class TestListContainerSection extends BasePage {


    SoftAssert softAssert;

    @FindBy(css = ".test-type-wrapper:nth-child(1) .language-tt-drop-down")
    private WebElement testTypeDropDown;

    @FindBy(css = ".test-type-wrapper:nth-child(2) .language-tt-drop-down")
    private WebElement languageTypeDropdown;

    @FindBy(css = ".toggle-btn")
    private WebElement showTest;

    @FindBy(css = ".test-type-wrapper")
    private List<WebElement> dropDownList;

    @FindBy(css = ".test-type-wrapper > .title")
    private List<WebElement> DropDownListName;

    @FindBy(css = ".test-topic-name")
    private WebElement testTopicNameDisplayed;

    @FindBy(css = ".test-topic-details")
    private WebElement testTopicDetails;

    @FindBy(css = ".more-info")
    private List<WebElement> moreInfoLink;

    @FindBy(className = "css-13r65w3-menu")
    private List<WebElement> testTypes;

    @FindBy(css = ".test-detail-card-name")
    private List<WebElement> testName;

    @FindBy(css = ".language-tt-drop-down")
    private List<WebElement> testTypeAndLanguageDropdown;

    @FindBy(className = "css-13r65w3-menu")
    private List<WebElement> languageTypes;

    @FindBy(css = ".test-questions-filter-label")
    private WebElement languageLabel;

    @FindBy(css = "div[tabindex='-1']")
    private List<WebElement> dropDownTestType;

    private String testTypeOption = "//span[text()='%s']";

    private String languageTypeOption = "//span[text()='%s']";

    @FindBy(css = ".test-score")
    private WebElement testScore;

    @FindBy(css = ".attempt-details-wrapper")
    private WebElement testAttemptDetails;

    @FindBy(css = ".view-feedback-btn")
    private WebElement viewFeedBackButton;

    @FindBy(css = ".test-detail-card-details")
    private List<WebElement> testCardDetails;


    public TestListContainerSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        softAssert = new SoftAssert();
    }

    public List<String> getTheTestType() {
        testTypeDropDown.click();
        return dropDownTestType.stream().map(e -> e.getText()).collect(Collectors.toList());
    }

    public List<String> getTheLanguageType(){
        languageTypeDropdown.click();
         return dropDownTestType.stream().map(e -> e.getText()).collect(Collectors.toList());
    }

    public void selectTestType(String testType) {
        refreshPage();
        waitForPageToLoad();
        waitForElementTobeClickable(testTypeDropDown);
        testTypeDropDown.click();
        try {
            waitForElementTobeClickable(driver.findElement(By.xpath(String.format(testTypeOption, testType))));
            jsClick(driver.findElement(By.xpath(String.format(testTypeOption, testType))));

        } catch (NoSuchElementException exception) {
            dropDownTestType.get(0).click();
        }
        waitForElementTobeClickable(showTest);
        jsClick(showTest);

    }

    public void selectLanguageType(String languageType){
        refreshPage();
        waitForPageToLoad();
        waitForElementTobeClickable(languageTypeDropdown);
        languageTypeDropdown.click();
        try {
            waitForElementTobeClickable(driver.findElement(By.xpath(String.format(languageTypeOption, languageType))));
            jsClick(driver.findElement(By.xpath(String.format(languageTypeOption, languageType))));

        } catch (NoSuchElementException exception) {
            dropDownTestType.get(0).click();
        }
        waitForElementTobeClickable(showTest);
        jsClick(showTest);
    }

    public void selectLanguageFromDropDown(String language) {
        testTypeAndLanguageDropdown.get(1).click();
        waitForElementTobeClickable(driver.findElement(By.xpath(String.format(languageTypeOption, language))));
        jsClick(driver.findElement(By.xpath(String.format(languageTypeOption, language))));
    }

    public void verifyTwoDropDownOnListingPage() {
        softAssert.assertEquals(dropDownList.size(), "2", "No. of Dropdowns are not same as expected");
        softAssert.assertEquals(DropDownListName.get(0).getText(), "Test Type:", "First Dropdown name is not matching");
        softAssert.assertEquals(DropDownListName.get(1).getText(), "Language:", "Second Dropdown name is not matching");
        softAssert.assertAll();
    }

    protected void verifySelectedTestTypeAppears(String testType) {
        Assert.assertEquals(testTopicNameDisplayed.getText(), testType, "Test card selected is: "+testType + "But test card appeared in UI is: "+testTopicNameDisplayed.getText());
    }

    protected void testTopicDetailsIsShown() {
        Assert.assertTrue(testTopicDetails.isDisplayed());
    }

    protected void showAllLinkIsDisplayed() {
        Assert.assertTrue(showTest.isDisplayed());
    }

    protected void clickOnShowAll() {
        showTest.click();
    }

    protected void clickOnMoreInfoLink() {
        wait(2000);
        waitForElementTobeClickable(moreInfoLink.get(0));
        jsClick(moreInfoLink.get(1));
        wait(2000);
    }

    protected void verifyUserHasLandedOnMoreInfoPage(String urlText) {
        wait(2000);
        Assert.assertTrue(urlContains(urlText),"User is not Landed on More-Info Page");

    }

    protected void clickAllTestType() {
        for (WebElement testType : testTypes) {
            testTypeDropDown.click();
            testType.click();
        }
    }

    protected String getTheTestName() {
        return testName.get(1).getText();
    }

    public void verifyTestTypeDropDownDisplaying() {
        Assert.assertTrue(testTypeAndLanguageDropdown.get(0).isDisplayed(), "Test type dropdown is not displaying");
    }

    public void verifyLanguageDropDownDisplaying() {
        Assert.assertTrue(testTypeAndLanguageDropdown.get(1).isDisplayed(), "Language dropdown is not displaying");
    }

    public void verifySelectedLanguageAppears(String language) {
        Assert.assertEquals(languageLabel.getText(), language, "Selected language doesn't match");
    }

    protected void assertThatAttemptTypesAndFeedbackButtonIsDisplayed() {
        softAssert.assertTrue(testScore.isDisplayed(), "Test Score is not visible for the attempted test type on Test Listing Page");
        softAssert.assertTrue(testAttemptDetails.isDisplayed(), "Different test attempt details is not present for the attempted test type on Test Listing");
        softAssert.assertTrue(viewFeedBackButton.isDisplayed(), "View Feeback Buttopn is Displayed");
        softAssert.assertAll();
    }

    public void testCardDetailsIsDisplayed() {
        testCardDetails.stream().forEach(((element -> Assert.assertTrue(element.getText().contains("Questions:") & element.getText().contains("Duration:")))));
    }

}
