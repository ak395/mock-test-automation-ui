package page.mockTestPage;

import entities.MockTestPlanner;
import mockTestFactory.AttemptType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.assertEquals;

public class TestFeedBackHeader extends BasePage {

    @FindBy(css = ".verification-text")
    private WebElement testFeedBackHeader;
    @FindBy(className = "answered")
    private WebElement badgeAnsweredNumber;

    @FindBy(className = "not-answered")
    private WebElement badgeNotAnsweredNumber;

    @FindBy(css = ".question-map__not-visited .ng-binding")
    private WebElement badgeNotVisitedNumber;

    @FindBy(css = ".marked-for-review")
    private WebElement badgeReviewLaterNumber;

    @FindBy(className = "answered-and-marked-for-review")
    private WebElement badgeAnsweredAndReviewLaterNumber;

    public TestFeedBackHeader(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void testFeedBackHeadingIsDisplayed() {
        waitForElementToBeVisible(testFeedBackHeader);
        Assert.assertTrue(testFeedBackHeader.isDisplayed(), "Test Feedback Heading is not displayed");

    }

    public int getNumberOfAnsweredQuestions() {
        waitForElementToBeVisible(badgeAnsweredNumber);
        return Integer.parseInt(badgeAnsweredNumber.getText());
    }

    public int getNumberOfNotAnsweredQuestions() {
        waitForElementToBeVisible(badgeNotAnsweredNumber);
        return Integer.parseInt(badgeNotAnsweredNumber.getText());
    }

    public int getNumberOfNotVisitedQuestions() {
        waitForElementToBeVisible(badgeNotVisitedNumber);
        return Integer.parseInt(badgeNotVisitedNumber.getText());
    }

    public int getNumberOfReviewLaterQuestions() {
        waitForElementToBeVisible(badgeReviewLaterNumber);
        return Integer.parseInt(badgeReviewLaterNumber.getText()) + Integer.parseInt(badgeAnsweredAndReviewLaterNumber.getText());
    }


    private void verifyBadgeSummaryForAnsweredQuestions(MockTestPlanner mockTestPlanner) {
        Set<AttemptType> answered = new HashSet<>();
        answered.add(AttemptType.ATTEMPT_TYPE_ANSWERED);
        assertEquals(getNumberOfAnsweredQuestions(), mockTestPlanner.getNumberOfQuestionForAttemptTypes(answered));

    }

    public void verifyBadgeSummaryForNotAnsweredQuestions(MockTestPlanner mockTestPlanner) {
        Set<AttemptType> notAnswered = new HashSet<>();
        notAnswered.add(AttemptType.ATTEMPT_TYPE_NOT_ANSWERED);
        int actualNumberOfQuestions = mockTestPlanner.getNumberOfQuestionForAttemptTypes(notAnswered) + 1;
        assertEquals(getNumberOfNotAnsweredQuestions(), actualNumberOfQuestions);

    }

    public void verifyBadgeSummaryForReviewLater(MockTestPlanner mockTestPlanner) {
        Set<AttemptType> reviewLater = new HashSet<>();
        reviewLater.add(AttemptType.ATTEMPT_TYPE_REVIEW_LATER);
        reviewLater.add(AttemptType.ATTEMPT_TYPE_ANSWERED_REVIEW_LATER);
        assertEquals(getNumberOfReviewLaterQuestions(), mockTestPlanner.getNumberOfQuestionForAttemptTypes(reviewLater));
    }

    private void verifyBadgeSummaryForNotVisited(MockTestPlanner mockTestPlanner) {
        Set<AttemptType> notVisited = new HashSet<>();
        notVisited.add(AttemptType.ATTEMPT_TYPE_NOT_VISITED);
        int actualNumberOfQuestions = mockTestPlanner.getNumberOfQuestionForAttemptTypes(notVisited) - 1;
        assertEquals(getNumberOfNotVisitedQuestions(), actualNumberOfQuestions);
    }

    public void verifyBadgeSummary(MockTestPlanner mockTestPlanner) {
        wait(2000);
        verifyBadgeSummaryForAnsweredQuestions(mockTestPlanner);
        verifyBadgeSummaryForReviewLater(mockTestPlanner);
        verifyBadgeSummaryForNotAnsweredQuestions(mockTestPlanner);
        verifyBadgeSummaryForNotVisited(mockTestPlanner);
    }


}
