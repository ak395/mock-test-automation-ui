package page.mockTestPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import java.util.List;


public class RankUpPage extends BasePage {

    @FindBy(css = ".rank-up-header--text-btn")
    private WebElement signUpRankUp;

    @FindBy(xpath = "//span[text() = ' LOGIN']")
    private List<WebElement> login;

    @FindBy(css  = ".md-input-element.ng-pristine")
    private List<WebElement> loginField;

    @FindBy(css = ".md-input-element.ng-untouched.ng-pristine.ng-valid")
    private WebElement password;

    @FindBy(css = ".btn.login-btn")
    private WebElement loginButton;

    @FindBy(css = ".profile-icon")
    private WebElement dropDown;

    @FindBy(linkText = "Profile")
    private WebElement profileLink;

    @FindBy(css = ".profilename")
    private WebElement profileName;

    @FindBy(linkText = "Pack History")
    private WebElement packHistory;

    @FindBy(css = ".page-header")
    private WebElement packHistoryHeader;

    @FindBy(css = ".pack-widget-state-wrapper  ")
    private List<WebElement> packCards;

    @FindBy(css = ".test-feedback__packs--content")
    private WebElement recommendedPackWidget;

    @FindBy(css = ".unlockpackmodal")
    private WebElement unlockPopUpWidget;

    @FindBy(css = ".pack-btn.emcoin.test-feedback__packs--content-text")
    private WebElement recommendedPackButton;

    @FindBy(css = ".paymentpopup.center-block")
    private WebElement unlockNowButton;

    @FindBy(css = ".embiumbank")
    private WebElement embiumBankWidget;

    @FindBy(css = ".modal-close ")
    private WebElement closeEmbiumBankWidget;


    public RankUpPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void verifyApplyToRankUpDisplaying(){
        Assert.assertTrue(signUpRankUp.isDisplayed(), "Sign up button is not displaying");
    }

    public void clickAndVerifyNavigationForSignUpPage(){
        signUpRankUp.click();
        switchToActiveTab();
        String signUpUrl = getCurrentUrl();
        Assert.assertTrue(signUpUrl.contains("signup"));
    }

    public void loginRankUp(){
        login.get(0).click();
        loginField.get(0).sendKeys("annusrat07@gmail.com");
        password.sendKeys("123456789");
        loginButton.click();
    }

    public void loginRankUpWithWrongInput(){
        login.get(0).click();
        loginField.get(0).sendKeys("annurat07@gmail.com");
        password.sendKeys("12345678");
        loginButton.click();
    }


    public void verifySuccessfullyLoggedIn(){
        String url = driver.getCurrentUrl();
        Assert.assertTrue(url.contains("rankup/tour"));
    }

    public void clickOnProfileLink(){
        dropDown.click();
        profileLink.click();
    }

    public void clickOnProfile(){
        profileName.click();
        packHistory.click();
    }

    public void clickUnlockNowButton(){
        unlockNowButton.click();
    }
    public void verifyUserNavigateToPackHistory(){
        Assert.assertEquals(packHistoryHeader.getText(), "Pack History", "Pack history page is not displaying");
    }

    public void verifyPackCardsAreDisplaying(){
        Assert.assertTrue(packCards.size()>0, "Pack cards are not displaying under pack history");
    }

    public void recommendedPackDisplay(){
        Assert.assertTrue(recommendedPackWidget.isDisplayed(), "Recommended pack doesn't display on test on test analysis page");
    }

    public void clickRecommendedPackAndunlockPopUpDisplay(){
        recommendedPackButton.click();
        Assert.assertTrue(unlockPopUpWidget.isDisplayed(), "Unlock popup doesn't display on test on test analysis page");
    }

    public void unlockNowButtonDisplayed(){
        Assert.assertTrue(unlockNowButton.isDisplayed(), "Unlock now button is not displaying");
    }

    public void embiumBankDisplaying(){
        Assert.assertTrue(embiumBankWidget.isDisplayed(), "Embium bank widget is not displaying");
        closeEmbiumBankWidget.click();
    }
}
