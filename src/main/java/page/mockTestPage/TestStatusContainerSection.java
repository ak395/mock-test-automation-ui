package page.mockTestPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import page.mockTestQuestionsPage.QuestionsHomePage;

import java.util.List;


public class TestStatusContainerSection extends BasePage {

    private QuestionsHomePage questionsHomePage;

    @FindBy(css = ".test-running-status-container")
    private WebElement runningTestStatusContainer;

    @FindBy(css = ".content-container > div")
    private List<WebElement> testTitleInProgress;

    @FindBy(css = ".btn-box")
    private WebElement resumeButton;

    public TestStatusContainerSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        questionsHomePage = new QuestionsHomePage();
    }

    protected void testRunningStatusIsDisplayed() {
        Assert.assertTrue(runningTestStatusContainer.isDisplayed());
    }

    protected void verifyTheTestTitleRunningInProgress(String testName) {
        Assert.assertEquals(testTitleInProgress.get(0).getText().toLowerCase(),testName.toLowerCase(),"The test started is: "+ testName.toLowerCase() + "But the test shown in Test Listing Page Progress Section is: "+ testTitleInProgress.get(0).getText().toLowerCase());
    }

    public void verifyResumeButtonIsDisplayed() {

        Assert.assertTrue(resumeButton.isDisplayed(),"Resume Button is not Displayed on test Listing Page in In Progress Section");
    }
}
