package page.mockTestPage.phoenixCandyContainer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import page.BasePage;

import static org.testng.Assert.assertEquals;

public class PhoenixCandyContainerSection extends BasePage {

    private PhoenixCandyContainerSectionLocators locators;

    @FindBy(className = "phoenix-candy__container")
    private WebElement rootElement;

    public PhoenixCandyContainerSection(WebDriver driver) {
        this.driver = driver;
        locators = new PhoenixCandyContainerSectionLocators();
        PageFactory.initElements(driver, this);
    }

    private WebElement getHeaderElement() {
        return getElement(rootElement, locators.header);
    }

    private WebElement getGoalDropDownElement() {
        return getElement(rootElement, locators.selectGoal);
    }

    public void assertHeaderToBe(String expectedHeader) {
        assertEquals(getHeaderElement().getText().trim(), expectedHeader);
    }

    public void selectGoal(String goal) {

        WebElement element = getGoalDropDownElement();

        Select selectGoalElement = new Select(element);
        selectGoalElement.selectByVisibleText(goal.toLowerCase());
        waitForPageToLoad();

    }
}
