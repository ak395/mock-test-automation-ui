package page.mockTestPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

public class GuestUserWarningPopup extends BasePage {

    @FindBy(css = ".login-modal-continueBtn")
    private WebElement continueWithoutLoginButton;

    @FindBy(css = ".login-modal-loginBtn")
    private WebElement LoginButton;

    @FindBy(id = "emailArea")
    private WebElement emailId;
    @FindBy(id = "passwordArea")
    private WebElement passwordId;

    @FindBy(css = ".login-logout-button")
    private WebElement loginButton;

    @FindBy(css = ".test-login-modal-wrapper")
    private WebElement loginModalPopup;

    public GuestUserWarningPopup(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void guestLogin() {
        waitForElementToBeVisible(continueWithoutLoginButton);
        continueWithoutLoginButton.click();
        waitForPageToLoad();
    }

    public void Login(String email, String password) {
        waitForElementToBeVisible(LoginButton);
        LoginButton.click();
        waitForPageToLoad();
        emailId.sendKeys(email);
        passwordId.sendKeys(password);
        loginButton.click();


    }

    public void verifyloginPopupAppears() {
        Verify.assertTrue(loginModalPopup.isDisplayed(), "Login Popup is not dislayed", Optional.of(true));
    }
}
