package page.mockTestPage;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

@Getter
public class InstructionPage extends BasePage {

    @FindBy(css = ".instruction-next-button")
    private WebElement startTestButton;

    @FindBy(css = "span.checkmark")
    private WebElement termsAndConditionsCheckBox;

    @FindBy(css = ".btn-container")
    private WebElement startTestButtonDisabled;

    @FindBy(css = ".instruction-text")
    private WebElement instructionPageTitle;

    @FindBy(css = ".test-header-title.toolTip-parent")
    private WebElement testName;

    private GuestUserWarningPopup guestUserWarningPopup;

    public InstructionPage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        guestUserWarningPopup = new GuestUserWarningPopup(driver);
    }

    public void dismissInstructions() {
        waitForElementToBeVisible(startTestButton);
        termsAndConditionsCheckBox.click();
        startTestButton.click();
    }

    public void continueAsGuest() {
        getGuestUserWarningPopup().guestLogin();
    }

    public void continueAsLoginUser(String email, String password) {
        getGuestUserWarningPopup().Login(email, password);
    }

    public void verifyLoginPopupIsDisplayed() {
        getGuestUserWarningPopup().verifyloginPopupAppears();
    }

    public void verifyTermsAndConditionUncheckedThenUserNotAllowedToTakeATest() {
        Assert.assertFalse(termsAndConditionsCheckBox.isSelected());
        Assert.assertTrue(startTestButtonDisabled.getAttribute("class").contains("disabled"));
    }

    public void clickTermsAndConditionCheckbox() {
        termsAndConditionsCheckBox.click();
    }

    public void verifyInstructionPageTitleDisplaying() {
        Assert.assertTrue(instructionPageTitle.isDisplayed());
    }

    public void verifyUserLandedOnInstructionPageWrtTest(String test) {
        Assert.assertEquals(test.toLowerCase(), testName.getText().toLowerCase(), "User is not landed on instruction page wrt to test, expected " + test.toLowerCase() + " but found " + testName.getText().toLowerCase());
        verifyInstructionPageTitleDisplaying();
    }

    public String getTestName() {
        return testName.getText();
    }

    public void verifyTestNameWrtTestListingPage(String testNameTestListing, String testNameIns) {
        Assert.assertTrue(testNameTestListing.toLowerCase().equalsIgnoreCase(testNameIns.toLowerCase()), "TestName is not matching, expected " + testNameTestListing.toLowerCase() + " but found " + testNameIns.toLowerCase());
    }
}
