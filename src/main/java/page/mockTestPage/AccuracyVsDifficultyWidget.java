package page.mockTestPage;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

public class AccuracyVsDifficultyWidget extends BasePage {


    @FindBy(css = ".accuracy-vs-difficulty-chart-wrapper")
    private WebElement accuracyWidget;
    @FindBy(css = ".accuracy-vs-difficulty-chart-wrapper .widget-heading")
    private WebElement accuracyWidgetHeading;


    public AccuracyVsDifficultyWidget() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void verifyAccuracyWidgetIsDisplay() {
        waitForElementToBeVisible(accuracyWidget);
        waitForElementToBeVisible(accuracyWidgetHeading);
        Assert.assertTrue(accuracyWidget.isDisplayed(), "accuracy Widget is not display after click on awesome lets move ahead button");
        Assert.assertEquals(accuracyWidgetHeading.getText(), "ACCURACY VS DIFFICULTY", "Accuracy vs Difficulty widget heading is incorrect");
    }
}
