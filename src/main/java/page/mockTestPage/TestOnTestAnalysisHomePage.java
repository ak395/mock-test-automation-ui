package page.mockTestPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import java.util.List;
import java.util.Date;

public class TestOnTestAnalysisHomePage extends BasePage {

    private MockTestHomePage mockTestHomePage;
    private RankUpPage rankUpPage;


    @FindBy(xpath = "//div[text()='Test-on-Test Analysis']")
    private WebElement testOnTestAnalysis;

    @FindBy(css = ".unavailable-wrapper.widget-wrapper .desc-1")
    private WebElement validationMessage;

    @FindBy(css = ".unavailable-wrapper.widget-wrapper .desc-2")
    private WebElement validation;

    @FindBy(css = ".tot-heading-wrapper.widget-name")
    private WebElement testPerformanceSection;

    @FindBy(css = ".chapter-wise-accuracy-wrapper .widget-name")
    private WebElement chapterWiseSection;

    @FindBy(css = ".css-1oepry8-singleValue")
    private WebElement filterTypeDropDown;

    @FindBy(css = ".highcharts-axis-title")
    private List<WebElement> graphAxixText;

    @FindBy(css = ".highcharts-background")
    private List<WebElement> graphs;

    @FindBy(className = "filter-option-elements")
    private List<WebElement> filterOptions;

    @FindBy(css = ".highcharts-series-0.highcharts-line-series")
    private List<WebElement> graphCoordinates;

    @FindBy(css = ".highcharts-series-0.highcharts-line-series  .highcharts-point")
    private List<WebElement> pointsText;

    @FindBy(css = ".highcharts-axis-title")
    private List<WebElement> axixTitle;

    @FindBy(css = ".bottom-wrapper")
    private WebElement rankUpLink;

    @FindBy(css = ".rank-up-wrapper.widget-bottom-spacing")
    private WebElement rankUpSection;

    @FindBy(css = ".sub-title")
    private WebElement examType;

    @FindBy(css = ".go-to-tlp")
    private WebElement testListingButton;

    @FindBy(css = ".legend-subtitle")
    private List<WebElement> testNames;

    @FindBy(css = ".legend-title")
    private List<WebElement> dates;

    @FindBy(css = ".chapter-wise-accuracy-footer")
    private WebElement practiceWidget;


    public TestOnTestAnalysisHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        mockTestHomePage = new MockTestHomePage();
        rankUpPage = new RankUpPage(driver);

    }

    public void clickTestOnTestAnalysis() {
        testOnTestAnalysis.click();
    }

    public void verifyValidationMessageWhenAttemptedOnlyOneTest() {
        Assert.assertEquals(validationMessage.getText(), "Looks, like you've taken only 1 test", "Message validation is not displaying when user attempted only one test");
        Assert.assertEquals(validation.getText(), "You need to attempt atleast 2 tests to see the analysis", "Message validation is not displaying when user attempted only one test");
    }

    public void verifyTestPerformanceSectionDisplaying() {
        Assert.assertTrue(testPerformanceSection.isDisplayed(), "Test performance section is not displayed");
    }

    public void verifyChapterWisePerformanceDisplaying() {
        Assert.assertTrue(chapterWiseSection.isDisplayed(), "Chapter wise performance is not displayed");
    }

    public void clickFilterTypeDropDown() {
        filterTypeDropDown.click();
    }

    public void clickOnRankUpAndVerifyNavigation() {
        rankUpLink.click();
        switchToActiveTab();
        String currentURL = driver.getCurrentUrl();
        Assert.assertTrue(currentURL.contains("rankup"), "User is not redirecting to rank up page");
    }

    public void verifyRankUpSectionDisplaying(){
        Assert.assertTrue(rankUpSection.isDisplayed(), "Rank up section is not displaying");
    }

    public void selectAndVerifyByFilterType(String accuracyType) {
        wait(5000);
        for (int i = 0; i < filterOptions.size(); i++) {
            if (accuracyType.equalsIgnoreCase(filterOptions.get(i).getText())) {
                filterOptions.get(i).click();
                break;
            }
        }
        Assert.assertTrue(accuracyType.equalsIgnoreCase(axixTitle.get(1).getText()), "Filter type and axis title is not matching");
        wait(5000);
    }

    public void hoverMouseOnTestPerformanceGraph() {
        Actions actions = new Actions(driver);
        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver, graphCoordinates.get(1));
        wait(2000);
        actions.moveToElement(pointsText.get(1)).clickAndHold().build().perform();
        String text = pointsText.get(1).getText();
        System.out.println(text);

    }

    public void verifyExamType(String testType){
        Assert.assertTrue(testType.equalsIgnoreCase(examType.getText()), "Exam type is not matching expected " + testType + "but found " + examType.getText());
    }

    public void verifyTestNamesMatchingWrtTest(String fTestName, String sTestName){
        Assert.assertTrue(testNames.get(0).getText().toLowerCase().contains(fTestName.toLowerCase()), "Test name is not matching, expected " +fTestName + " but found " + testNames.get(0).getText());
        Assert.assertTrue(testNames.get(1).getText().toLowerCase().contains(sTestName.toLowerCase()), "Test name is not matching, expected " + sTestName + " but found " + testNames.get(1).getText());
    }

    public void verifyDateWithExam(){
        Date date=java.util.Calendar.getInstance().getTime();
        Assert.assertTrue(date.toString().contains(dates.get(0).getText()), "Dates with exam is not matching with system date expected " + dates.get(0).getText() + " but found " + date.toString());
        Assert.assertTrue(date.toString().contains(dates.get(1).getText()), "Dates with exam is not matching with system date expected " + dates.get(1).getText() + " but found " + date.toString());

    }

    public void verifyTestListingButtonDisplayingAndNavigatingToTlp() {
        Assert.assertTrue(testListingButton.isDisplayed());
        testListingButton.click();
        mockTestHomePage.verifyUserLandedOnTestListingPage();
    }

    public void verifyRankUpSignUpDisplaying(){
        rankUpPage.verifyApplyToRankUpDisplaying();
    }

    public void verifySignupNavigation(){
        rankUpPage.clickAndVerifyNavigationForSignUpPage();
    }

    public void verifyPracticeWidgetDisplaying(){
        Assert.assertTrue(practiceWidget.isDisplayed(), "Practice widget is not displaying on Test on test analysis page");
    }

    public void rankUpLogIn(){
        rankUpPage.loginRankUp();
    }

    public void rankUpLoginWithWrongCredential(){
        rankUpPage.loginRankUpWithWrongInput();
    }

    public void verifyUserLoggedInSuccessfullyForrankUp(){
        rankUpPage.verifySuccessfullyLoggedIn();
    }

    public void clickOnRankUpDropDownForProfileLink(){
        rankUpPage.clickOnProfileLink();
    }

    public void verifyProfileForPack(){
        rankUpPage.clickOnProfile();
    }

    public void verifyNavigationPackHistoryPage(){
        rankUpPage.verifyUserNavigateToPackHistory();
    }

    public void verifyUserShouldAbleToSeePackCards(){
        rankUpPage.verifyPackCardsAreDisplaying();
    }

    public void verifyOverallWidgetDisplaying(){
        mockTestHomePage.overAllwidgetDisplaying();
    }

    public void verifyRecommendedPackDisplayed(){
        rankUpPage.recommendedPackDisplay();
    }

    public void clickAndVerifyUnlockPopUpkDisplayed(){
        rankUpPage.clickRecommendedPackAndunlockPopUpDisplay();
    }

    public void verifyUnlockNowButtonDisplayingAndClickable(){
        rankUpPage.unlockNowButtonDisplayed();
        rankUpPage.clickUnlockNowButton();
    }

    public void verifyEmbiumBankWidgetDisplaying(){
        rankUpPage.embiumBankDisplaying();
    }
}
