package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

class RegistrationSection extends BasePage {

    @FindBy(name = "email")
    private List<WebElement> emailField;

    @FindBy(name = "password")
    private List<WebElement> passwordField;

    @FindBy(name = "passwordConfirm")
    private WebElement passwordConfirmField;

    @FindBy(css = "span.Dropdown-arrow")
    private WebElement goalSelectField;

    @FindBy(css = ".login-container:nth-child(1) .loginBtn")
    private WebElement signupButton;

    @FindBy(css = "span.checkmark")
    private WebElement termsAndConditions;

    private String selectGoal = "//div[contains(text(), '%s')]";


    RegistrationSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    void registerAs(String email, String password) {
        waitForElementToBeVisible(emailField.get(1));
        emailField.get(1).sendKeys(email);
        passwordField.get(1).sendKeys(password);
        passwordConfirmField.sendKeys(password);
        termsAndConditions.click();
        waitForElementTobeClickable(signupButton);
        signupButton.click();
    }

    void registerAs(String email, String password, String goal) {
        waitForElementToBeVisible(emailField.get(1));
        emailField.get(1).sendKeys(email);
        selectGoalAs(goal);
        passwordField.get(1).sendKeys(password);
        passwordConfirmField.sendKeys(password);
        jsClick(termsAndConditions);
        waitForElementTobeClickable(signupButton);
        jsClick(signupButton);
    }

    private void selectGoalAs(String goal) {
        goalSelectField.click();
        waitForElementTobeClickable(driver.findElement(By.xpath(String.format(selectGoal, goal))));
        jsClick(driver.findElement(By.xpath(String.format(selectGoal, goal))));
    }
}
