package page.studyPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;

class PracticeLinkSection extends BasePage {

    @FindBy(css = ".custom-button-component.practice-question-button.default.capsular.filled.onhover-style")
    private List<WebElement> practiceLinksList;

    @FindBy(css = ".custom-button-component.practice-question-button.default.capsular.filled.onhover-style")
    private WebElement practiceLink;

    PracticeLinkSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    void clickOnFirstPracticeLink() {
        waitForElementToBeVisible(practiceLink);
        practiceLinksList.get(0).click();
        switchToActiveTab();
    }
}
