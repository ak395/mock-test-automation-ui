package page.studyPage.searchWrapper;

import org.openqa.selenium.By;

public class SearchWrapperLocators {

    By searchContainer = By.className("react-typeahead-usertext");
    By searchResultsSuggestions = By.cssSelector("div[class = 'search_results'] a");

}
