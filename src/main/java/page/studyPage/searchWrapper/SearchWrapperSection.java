package page.studyPage.searchWrapper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.learnMorePage.leftTreeWrapper.LeftTreeWrapperSection;
import page.studyResultantPage.TopicWrapperSection;
import utilityClass.LinkUtil;
import verification.Verify;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


public class SearchWrapperSection extends BasePage {

    private SearchWrapperLocators locators;

    @FindBy(className = "study-search-container")
    private WebElement rootElement;

    public SearchWrapperSection(WebDriver driver) {
        this.driver = driver;
        locators = new SearchWrapperLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchContainerElement() {

        return getElement(rootElement,locators.searchContainer);

    }

    public List<WebElement> getSearchResultSuggestionsElements() {

        return getElements(rootElement,locators.searchResultsSuggestions);
    }

    public void keywordToSearch(String key) {

        WebElement searchTextBox = getSearchContainerElement();
        searchTextBox.sendKeys(key);
        wait(5000);
        waitForPageToLoad();
       gatherAllSuggestionsOnSearchAndClick();

    }

    private void gatherAllSuggestionsOnSearchAndClick() {

        List<WebElement> elements = getSearchResultSuggestionsElements();

        Map<Integer, List<String>> map = elements

                .stream()
                .map(ele -> ele.getAttribute("href"))
                .map(String::trim)
                .distinct()
                .collect(Collectors.groupingBy(LinkUtil::getResponseCode));

        map.get(200)
                .stream()
           .forEach(System.out::println);

        for (Map.Entry<Integer, List<String>> mp : map.entrySet()) {
            System.out.print(mp.getKey() + " | ");
            for (String url : mp.getValue()) {
                driver.get(url);
                waitForPageToLoad();
                assertUrlToBe(url);
                wait(5000);
                TopicWrapperSection topicWrapperSection = new TopicWrapperSection(driver);
                topicWrapperSection.clickOnLearnMore();
                wait(5000);
                LeftTreeWrapperSection leftTreeWrapperSection = new LeftTreeWrapperSection(driver);
                leftTreeWrapperSection.matchingConceptName();

                            }
            System.out.println();
        }

    }

    private void assertUrlToBe(String url) {

        Verify.assertEquals(driver.getCurrentUrl(),url,"Url not Matching", Optional.of(true));
    }



}


