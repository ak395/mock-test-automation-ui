package page.studyPage;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.rankUpLoginPage.LoginFormSection;
import page.sections.headerWrapper.HeaderWrapperSection;
import page.studyPage.searchWrapper.SearchWrapperSection;
import verification.Verify;

import java.util.Optional;

@Getter
public class StudyHomePage extends BasePage {

    private HeaderWrapperSection headerWrapperSection;
    private SearchWrapperSection searchWrapperSection;
    private PracticeLinkSection practiceLinkSection;
    private LoginFormSection loginFormSection;

    public StudyHomePage() {
        driver = DriverProvider.getDriver();

        PageFactory.initElements(driver, this);

        headerWrapperSection = new HeaderWrapperSection(driver);
        searchWrapperSection = new SearchWrapperSection(driver);
        practiceLinkSection = new PracticeLinkSection(driver);
        loginFormSection = new LoginFormSection(driver);

    }

    public void assertThatStudyHomePageIsLoaded() {
        WebElement searchContainer = searchWrapperSection.getSearchContainerElement();
        boolean result = searchContainer.isDisplayed();
        Verify.assertTrue(result,"successful", Optional.of(true));
    }

    public void clickOnPracticeLink(){
        practiceLinkSection.clickOnFirstPracticeLink();
    }

}
