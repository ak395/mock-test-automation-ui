package page;

import driver.DriverProvider;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.Landing.LandingPageKnowledgeTreeSection;
import page.Landing.LandingPageSearchSection;
import page.Landing.LandingPageStartSection;
import page.Landing.LandingPageVideoSection;
import page.landingPage.leftContainer.AboutSection;
import page.rankUpLoginPage.LoginFormSection;
import utils.Properties;
import verification.Verify;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.testng.Assert.assertTrue;
import static utils.Randoms.getRandomEmail;

@Getter
@Setter
public class LandingHomePage extends BasePage
{
    private LandingPageStartSection landingPageStartSection;
    private LandingPageVideoSection landingPageVideoSection;
    private LandingPageSearchSection landingPageSearchSection;
    private LandingPageKnowledgeTreeSection landingPageKnowledgeTreeSection;
    private LoginFormSection loginFormSection;
    private AboutSection aboutSection;
    private RegistrationSection registrationSection;

    @FindBy(css = ".create-profile")
    private WebElement loginButton;

    @FindBy(css = ".user-name")
    private WebElement profileElement;

    @FindBy(css = ".button-common.close-mail")
    private WebElement closePopupButton;

    @FindBy(className = "Dropdown-option")
    private List<WebElement> goalsOptions;

    @FindBy(css = ".Dropdown-arrow-wrapper")
    private WebElement goalDropdown;

    @FindBy(css = "span.Dropdown-arrow")
    private WebElement goalSelectField;

    @FindBy(css = ".exam_goal_logo")
    private WebElement goalLoginButton;

    @FindBy(xpath = "//a[contains(text(),'Logout')]")
    private WebElement logoutButton;

    private String selectGoal = "//div[contains(text(), '%s')]";

    public LandingHomePage()
    {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver,this);
        landingPageStartSection = new LandingPageStartSection(driver);
        landingPageVideoSection = new LandingPageVideoSection(driver);
        landingPageSearchSection = new LandingPageSearchSection(driver);
        landingPageKnowledgeTreeSection =new LandingPageKnowledgeTreeSection(driver);
        aboutSection = new AboutSection(driver);
        loginFormSection = new LoginFormSection(driver);
        registrationSection = new RegistrationSection(driver);
    }

    public void clickOnStartNowButton()
    {
        driver.navigate().refresh();
        landingPageStartSection.assertUrlHasBeenResetTo(Properties.baseUrl);
    }

    public void clickOnMuteButton()
    {
        landingPageVideoSection.clickOnMuteButton();
    }

    public void clickOnUnMuteButton()
    {
        landingPageVideoSection.clickOnUnMuteButton();
    }

    public void clickOnSearchNowButton()
    {
        landingPageSearchSection.clickOnSearchNowButton();
    }
    public void hoverOnKnowledgeTree()
    {
        landingPageKnowledgeTreeSection.hoverOnKnowledgeTree();
    }
    public void assertUrlHasBeenResetTo(String url)
    {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }

    public void loginAs(String username, String password) {
//        clickOnLoginButton();
        getLoginFormSection().loginAs(username, password);
    }

    public void logIn(String username, String password){
        clickOnLoginButton();
        getLoginFormSection().loginAs(username, password);
    }

    public void clickOnLoginButton(){
        waitForElementTobeClickable(loginButton);
        loginButton.click();
    }

    public void clickOnGoalLoginButton() {
        waitForElementToBeVisible(goalLoginButton);
        goalLoginButton.click();

    }

    public void  logout() {
        waitForElementToBeVisible(logoutButton);
        logoutButton.click();
    }

    public void verifyUserHasLoggedOut() {
        assertUrlHasBeenResetTo("https://www.embibe.com/signup");
    }
    public void clickOnRegisterHereLink(){
        getLoginFormSection().clickOnRegisterHereLink();
    }

    public void verifyUserIsLoggedIn() {
    waitForElementToBeVisible(profileElement);
        assertTrue(profileElement.isDisplayed(), "Login is not Successful");
    }

    public String registerANewUser() {
        String email = getRandomEmail();
        String password = "password";
        getRegistrationSection().registerAs(email, password);
        closeThePopupAfterRegistration();
        return email;

    }

    public String registerANewUser(String goal) {
        String email = getRandomEmail();
        String password = "password";
        getRegistrationSection().registerAs(email, password, goal);
        closeThePopupAfterRegistration();
        return email;
    }

    public String createUserInEmbibeFormat(String goal,int i) {

            String email = "preprod_analyse_testing_"+ goal.substring(0,3).toLowerCase() + i + "@embibe.com";
            String password = "embibe1234";
            getRegistrationSection().registerAs(email,password,goal);
            closeThePopupAfterRegistration();
            return email;

    }

    public void closeThePopupAfterRegistration() {
        waitForElementToBeVisible(closePopupButton);
        closePopupButton.click();
    }

    public void clickOnGoals(){
        goalSelectField.click();
    }

    public List<String> getGoals(){
        return goalsOptions.stream().map(e->e.getText()).collect(Collectors.toList());

    }

    public void verifyAllGoalsAvailable(String desiredGoal){
        getGoals().stream().forEach(e->System.out.println(e));
        for (String goals:getGoals()) {
            if(goals.equalsIgnoreCase(desiredGoal)){
                System.out.println("Desired goal available");
                clickOnGoals();
                break;
            }
        }
    }
}
