package page.jumpUPPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.jumpUPPage.JumpPageSectionLocators;


public class ChatButton extends BasePage {

private JumpPageSectionLocators locators;

@FindBy(id = "intercom-container")
private WebElement rootElement;

    public ChatButton(WebDriver driver) {
        this.driver = driver;
        locators = new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement chatbutton(){ return getElement(rootElement,locators.chatbutton);}

     public void clickOnChatButton()
     {
         waitForPageToLoad();
         driver.switchTo().frame("intercom-launcher-frame");
         driver.findElement(By.cssSelector("[id='intercom-container']")).click();
     }


}
