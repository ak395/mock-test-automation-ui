package page.jumpUPPage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

public class TabSection extends BasePage {

    private JumpPageSectionLocators locators;
    @FindBy(className = "jump-up")
    private WebElement rootElement;

    public TabSection(WebDriver driver) {
        this.driver =  driver;
        locators = new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement Tab2() {
        return getElement(rootElement, locators.TesttakingMRI);
    }

    public void clickOnTesttakingMRITab() {
        waitForPageToLoad();
        JavascriptExecutor je = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.id("tabHeading"));
        je.executeScript("arguments[0].scrollIntoView(true);",element);
        Tab2().click();
        String actualtabscontent2 = driver.findElement(By.xpath("//*[text()='39% of your score depends on test-taking strategy']")).getText();
        Assert.assertEquals(actualtabscontent2,"39% of your score depends on test-taking strategy\n" +
                ">");
        waitForPageToLoad();
    }
    public WebElement Tab3() {
        return getElement(rootElement, locators.Comparelikeneverbefore);
    }

    public void clickOnCompareLikeNeverBeforeTab() {
        waitForPageToLoad();
        JavascriptExecutor je = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.id("tabHeading"));
        je.executeScript("arguments[0].scrollIntoView(true);",element);
        Tab3().click();
        String actualtabscontent3 = driver.findElement(By.id("tabTitle2")).getText();
        Assert.assertEquals(actualtabscontent3,"Compare like\n" +
                "never before");
        waitForPageToLoad();
    }

    public WebElement Tab4() {
        return getElement(rootElement, locators.experiencetherealtest);
    }

    public void clickOnExperienceTheRealTestTab() {
        waitForPageToLoad();
        Tab4().click();
        String actualtabscontent4 = driver.findElement(By.id("tabTitle3")).getText();
        Assert.assertEquals(actualtabscontent4,"Experience\n" +
                "the real test");
        waitForPageToLoad();
    }

    public WebElement Tab1() {
        return getElement(rootElement, locators.Unbeatableacademicanalysis);
    }

    public void clickOnUnbeatableAcademicAnalysisTab() {
        waitForPageToLoad();
        Tab1().click();
        String actualtabscontent1 = driver.findElement(By.id("tabTitle0")).getText();
        Assert.assertEquals(actualtabscontent1,"Unbeatable academic analysis\n" +
                "Fix knowledge gaps");
        waitForPageToLoad();
    }
}
