package page.jumpUPPage;

import org.openqa.selenium.By;
import page.BasePage;

public class JumpPageSectionLocators extends BasePage {

    By GetJump = By.cssSelector("button[class*='jump-up-header--text-btn']");
    By GetJumpAssertion = By.cssSelector("[class='jump-up-header--text-main']");
    By TabHeaderAssertion =  By.id("tabHeading");
    By Unbeatableacademicanalysis = By.cssSelector("#tabTitle0");
    By TesttakingMRI = By.cssSelector("#tabTitle1");
    By Comparelikeneverbefore = By.cssSelector("#tabTitle2");
    By experiencetherealtest = By.cssSelector("#tabTitle3");
    By readtheirstory = By.xpath("//a[contains(text(),'READ THEIR STORY >')]");
    By learnmore = By.cssSelector("p[class*='right-col']");
    By explorerankup =  By.cssSelector("[class='try-rank-up-button']");
    By engineering = By.cssSelector("img[src*='engineering-unselected.svg']");
    By medical = By.cssSelector("img[src*='medical-unselected.svg']");
    By class11 = By.cssSelector("img[src*='class11-unselected.png']");
    By class12 = By.cssSelector("img[src*='class12-unselected']");
    By class12plus = By.cssSelector("img[src*='class12plus-unselected.png']");
    By may2019 = By.cssSelector("img[src*='calender2-unselected']");
    By may2020 = By.cssSelector("img[src*='calender1-unselected']");
    By header1 = By.cssSelector("*[class='jump-up-header--text-main']>div");
    By header2 = By.cssSelector("[class='jump-up--buy-packs__title ']");
    By jumppack1 = By.cssSelector("[id='unlockButtonId1']");
    By jumppack2 = By.id("#unlockButtonId2");
    By jumppack3 = By.id("#unlockButtonId3");
    By unlock = By.cssSelector("[class='did-u-know-unlock ']");
    By jumpheader = By.cssSelector("[href='jump']");
    By login = By.xpath("//*[text()='Login']");
    By username = By.xpath("//div[@class='tooltip-modal']//input[@id='emailArea']");
    By password =  By.xpath("//div[@class='tooltip-modal']//input[@id='passwordArea']");
    By loginbutton =  By.xpath("//div[@class='tooltip-modal']//button[@class='btn btn btn-custom btn-block loginBtn BtnText'][contains(text(),'Login')]");
    By dropdown = By.xpath("//div[@class='user-dropdown']");
    By logout = By.xpath("//a[contains(text(),'Logout')]");
    By jumpupbuypacks = By.cssSelector("[class='jump-up--buy-packs__title ']");
    By chatbutton = By.cssSelector("[name='intercom-launcher-frame']");

}
