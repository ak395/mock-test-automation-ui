package page.jumpUPPage;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;


public class GetJumpSection extends BasePage {
    private JumpPageSectionLocators locators;

    @FindBy(className = "jump-up")
    private WebElement rootElement;

    public GetJumpSection(WebDriver driver)
    {
        this.driver =  driver;
        locators = new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement GetJumpButton()
    {
        return getElement(rootElement, locators.GetJump);
    }

    public WebElement jumpHeader() { return getElement(rootElement, locators.jumpheader);}

    public void clickOnGetJump() {
        waitForPageToLoad();
        GetJumpButton().click();
        waitForPageToLoad();
    }

    public WebElement getJumpButtonAssertion()
    {
        return getElement(rootElement, locators.GetJumpAssertion);
    }

    public WebElement jumpUpBuyPacksAssertion()
    {
        return getElement(rootElement, locators.jumpupbuypacks);
    }

    public void getJumpAssertion() {
        waitForPageToLoad();
        String actualgetjumpheader = getJumpButtonAssertion().getText();
        String expectedgetjumpheader = "JUMP to a higher score.";
        Assert.assertEquals(actualgetjumpheader, expectedgetjumpheader);
        String actualjumpupbuypacks = jumpUpBuyPacksAssertion().getText();
        String expectedjumpupbuypacks = "Get your unfair advantage. JUMP NOW.";
        Assert.assertEquals(actualjumpupbuypacks, expectedjumpupbuypacks);
        waitForPageToLoad();
    }
}