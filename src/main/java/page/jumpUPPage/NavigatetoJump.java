package page.jumpUPPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class NavigatetoJump extends BasePage {

    private JumpPageSectionLocators locators;
    @FindBy(className = "center-right-nav-wrapper")
    private WebElement rootElement;

    public NavigatetoJump(WebDriver driver) {
        this.driver = driver;
        locators = new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement jumpHeader() {
        return getElement(rootElement, locators.jumpheader);
    }

    public void clickOnJumpHeader() {
        waitForPageToLoad();
        jumpHeader().click();
        waitForPageToLoad();
    }
}
