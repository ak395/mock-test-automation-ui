package page.jumpUPPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class JumpLogin extends BasePage {

    private JumpPageSectionLocators locators;
    @FindBy(className = "center-right-nav-wrapper")
    private WebElement rootElement;

    public JumpLogin(WebDriver driver) {
        this.driver = driver;
        locators = new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement Login() {
        return getElement(rootElement, locators.login);
    }

    public WebElement ClickOnusername() {
        return getElement(rootElement, locators.username);
    }

    public WebElement ClickOnPassword() {
        return getElement(rootElement, locators.password);
    }

    public WebElement ClickOnLoginButton() {
        return getElement(rootElement, locators.loginbutton);
    }

    public WebElement DropDown() {
        return getElement(rootElement, locators.dropdown);
    }

    public WebElement Logout() {
        return getElement(rootElement, locators.logout);
    }

    public WebElement jumpHeader() {
        return getElement(rootElement, locators.jumpheader);
    }


    public void clickOnJumpLogin() {
        waitForPageToLoad();
        jumpHeader().click();
        Login().click();
        waitForPageToLoad();

    }

    public void clickOnJumpLogOut() throws IOException {
        Login().click();
        waitForPageToLoad();
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("C:\\Users\\Dell\\git\\master\\config.properties");
        prop.load(fis);
        ClickOnusername().sendKeys(prop.getProperty("embibeusername"));
        ClickOnPassword().sendKeys(prop.getProperty("embibepassword"));
        ClickOnLoginButton().click();
        waitForPageToLoad();
        DropDown().click();
        Logout().click();

    }
}
