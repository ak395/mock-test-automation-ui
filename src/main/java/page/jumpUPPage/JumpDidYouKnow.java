package page.jumpUPPage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class JumpDidYouKnow extends BasePage {
    private JumpPageSectionLocators locators;
    @FindBy(className = "badge-status ")
    private  WebElement rootElement;

    public JumpDidYouKnow(WebDriver driver)
    {
        this.driver=driver;
        locators=new JumpPageSectionLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement unLockButton()
    {
        return getElement(rootElement,locators.unlock);
    }

    public  void clickOnUnlockButton()
    {
        WebElement element = driver.findElement(By.cssSelector("[class='badge-status-content-sub ']"));
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].scrollIntoView(true);",element);
        unLockButton().click();
        waitForPageToLoad();
    }
}
