package page.jumpUPPage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;


public class JumpUpPacks extends BasePage {
    private JumpPageSectionLocators locators;

    @FindBy(className = "jump-up--buy-packs")
    private WebElement rootElement;

    public JumpUpPacks(WebDriver driver) {
        this.driver = driver;
        locators = new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement selectYourMedicalGoal() {waitForPageToLoad();return getElement(rootElement, locators.medical); }

    public void ClickMedicalGoalClass112019Goal() {
        // WebElement element2 = selectYourMedicalGoal();
        waitForPageToLoad();
        selectYourMedicalGoal().click();
        waitForPageToLoad();
    }

    public WebElement SelectYourEnggineeringGoal() {waitForPageToLoad();return getElement(rootElement, locators.engineering); }

    public void ClickEngineeringGoalClass112019Goal() {
        waitForPageToLoad();

    }

    public WebElement UnlockPack1() {
        waitForPageToLoad();
        return getElement(rootElement, locators.jumppack1);
    }

    public void ClickOnUnlockPack1() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        Actions act = new Actions(driver);
        act.moveToElement(driver.findElement(By.cssSelector("[id=\"unlockButtonId1\"]"))).click().build().perform();
        waitForPageToLoad();
    }

    public void ClickOnUnlockPack2() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        Actions act = new Actions(driver);
        act.moveToElement(driver.findElement(By.cssSelector("[id=\"unlockButtonId2\"]"))).click().build().perform();
        waitForPageToLoad();
    }

    public void ClickOnUnlockPack3() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        Actions act = new Actions(driver);
        act.moveToElement(driver.findElement(By.cssSelector("[id=\"unlockButtonId3\"]"))).click().build().perform();
        act.moveToElement(driver.findElement(By.cssSelector("[id=\"unlockButtonId3\"]"))).click().build().perform();
        waitForPageToLoad();
    }


    public WebElement Class12() {
        return getElement(rootElement, locators.class12);

    }

    public void SelectingClass12() {
        Class12().click();
        waitForPageToLoad();
    }

    public WebElement Class11() {
        return getElement(rootElement, locators.class11);

    }

    public void SelectingClass11() {
        Class11().click();
        waitForPageToLoad();
    }

    public WebElement Class12plus() {
        return getElement(rootElement, locators.class12plus);
    }

    public void SelectingClass12Plus() {
        Class12plus().click();
        waitForPageToLoad();

    }

    public WebElement May2020() {

        return getElement(rootElement, locators.may2020);
    }

    public void ClickOnMay2020() {

        May2020().click();
        waitForPageToLoad();
    }


    public WebElement May2019() {

        return getElement(rootElement, locators.may2019);
    }

    public void ClickOnMay2019() {

        May2019().click();
        waitForPageToLoad();
    }
}
