package page.jumpUPPage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class ExploreRankUp extends BasePage {
    private JumpPageSectionLocators locators;
    @FindBy(className = "content-section ")
    private WebElement rootElement;

    public ExploreRankUp(WebDriver driver) {
        this.driver = driver;
        locators = new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement exploreRankUp() {
        return getElement(rootElement, locators.explorerankup);
    }

    public void clickOnExploreRankUp() {
        WebElement element = driver.findElement(By.cssSelector("[class='try-rank-up']"));
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
        exploreRankUp().click();
        waitForPageToLoad();
    }
}