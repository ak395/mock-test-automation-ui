package page.jumpUPPage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.Iterator;
import java.util.Set;

public class ImprovementStory extends BasePage {

    private JumpPageSectionLocators locators;
    @FindBy(className = "jump-up-tab-content-bottom-left-col-part")
    private WebElement rootelement;

    public ImprovementStory(WebDriver driver) {
        this.driver = driver;
        locators = new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement ImprovementStory() {
        return getElement(rootelement, locators.readtheirstory);
    }

    public void clickOnReadTheirStory() {
        waitForPageToLoad();
        WebElement element = driver.findElement(By.cssSelector("[class='jump-up-tab-content-bottom-left-col-part-text-sub']"));
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
        ImprovementStory().click();
        String parent = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> I1 = s1.iterator();
        while (I1.hasNext()) {

            String child_window = I1.next();


            if (!parent.equals(child_window)) {
                driver.switchTo().window(child_window);

                String actualurl = driver.switchTo().window(child_window).getCurrentUrl();
                String expectedurl = "https://www.embibe.com/exams/arihant-jain-air-152-on-how-embibe-can-save-50-marks-in-jee-mains-success-story/";
                Assert.assertEquals(actualurl, expectedurl);
                waitForPageToLoad();
            }
        }
    }
}
