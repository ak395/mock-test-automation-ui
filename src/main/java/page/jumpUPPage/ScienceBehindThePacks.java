package page.jumpUPPage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class ScienceBehindThePacks extends BasePage {
    private JumpPageSectionLocators locators;
    @FindBy(className="jump-up-tab-content-bottom-right-col-part")
    private WebElement rootElement;

    public ScienceBehindThePacks(WebDriver driver){
        this.driver=driver;
        locators= new JumpPageSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement scienceBehindThePack()
    {
        return getElement(rootElement, locators.learnmore);
    }

    public void clickOnLearnMore() {
        WebElement element = driver.findElement(By.cssSelector("[class='jump-up-tab-content-bottom-right-col-part-text']"));
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
        scienceBehindThePack().click();
        waitForPageToLoad();
    }
}
