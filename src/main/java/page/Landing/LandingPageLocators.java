package page.Landing;
import org.openqa.selenium.By;
import page.BasePage;

public class LandingPageLocators extends BasePage
{
    By startNowButton =By.linkText("START NOW");
    By muteButton=By.cssSelector("img[class='mute-btn-image']");
    By unMuteButton=By.cssSelector("img[class='unmute_video_segment-btn-image']");
    By searchNowButton=By.xpath("//a[contains(text(),'SEARCH NOW')]");
    By knowledgeTree=By.cssSelector("div[class='vis-network']");
    By knowledgeTreeHeader=By.xpath("//div[@class='lava-link passage kt_header']");
}
