package page.Landing;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;
import java.util.Optional;

@Getter
public class LandingPageStartSection extends BasePage
{
    private LandingPageLocators locators;
    @FindBy(className = "content")
    private WebElement rootElement;

    @FindBy(css = "a.get-started")
    private WebElement startNowButton;

    public LandingPageStartSection(WebDriver driver)
    {
        this.driver = driver;
        locators = new LandingPageLocators();
        PageFactory.initElements(driver, this);
    }

    @Step("User finds the StartNow Button WebElement")
    public WebElement getStartButtonElement()
    {
        return getElement(rootElement, locators.startNowButton);
    }

    @Step("User clicks on the Start Now Button")
    public void clickOnStartNowButton()
    {
        waitForElementTobeClickable(startNowButton);
        jsClick(startNowButton);
        waitForPageToLoad();
    }

    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url)
    {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }
}
