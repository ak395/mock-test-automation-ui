package page.Landing;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

@Getter
public class LandingPageKnowledgeTreeSection extends BasePage
{
    private LandingPageLocators locators;
    @FindBy(className = "KTGraphContainer")
    private WebElement rootElement;

    public LandingPageKnowledgeTreeSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new LandingPageLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getKnowledgeTree()
    {
      return getElement(rootElement,locators.knowledgeTree);
    }

    public WebElement getKnowledgeTreeHeader()
    {
        return getElement(rootElement,locators.knowledgeTreeHeader);
    }
    @Step("User Scrolls to Knowledge Tree")
    public void scrollToKnowledgeTree()
    {
        try
        {
            wait(5000);
        }
        catch (Exception e)
        {

            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", locators.knowledgeTree);

        }
        }

    @Step("User mouse hovers on the Knowledge Tree")
    public void hoverOnKnowledgeTree()
    {
        waitForPageToLoad();
        System.out.print(getKnowledgeTree().getLocation());
        Actions actionsOnKTPage=new Actions(driver);
        actionsOnKTPage.moveToElement(getKnowledgeTree()).click(getKnowledgeTree()).build().perform();
        wait(500);
        getKnowledgeTree().click();
    }

    @Step("User verifies the title of the Search Page")
    public void assertHeaderToBe()
    {
        waitForPageToLoad();
        Assert.assertTrue(getKnowledgeTreeHeader().isDisplayed(), String.valueOf(true));
    }
}
