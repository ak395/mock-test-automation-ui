package page.Landing;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
@Getter
public class LandingPageVideoSection extends BasePage
{
    private LandingPageLocators locators;
    @FindBy(id = "landing")
    private WebElement rootElement;

    public LandingPageVideoSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new LandingPageLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getMuteButtonElement()
    {
        return getElement(rootElement,locators.muteButton);
    }
    public WebElement getUnMuteButtonElement()
    {
        return getElement(rootElement,locators.unMuteButton);
    }

    @Step("User clicks on the Mute Button")
    public void clickOnMuteButton()
    {
        waitForPageToLoad();
        getMuteButtonElement().click();
        waitForPageToLoad();
    }

    @Step("User clicks  on the UnMute Button")
    public void clickOnUnMuteButton()
    {
        waitForPageToLoad();
        getUnMuteButtonElement().click();
        waitForPageToLoad();
    }
}
