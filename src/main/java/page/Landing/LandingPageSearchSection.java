package page.Landing;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;
import java.util.Optional;

@Getter
public class LandingPageSearchSection extends BasePage
{

    private LandingPageLocators locators;
    @FindBy(id ="search")
    private WebElement rootElement;

    public LandingPageSearchSection(WebDriver driver)
    {
        this.driver=driver;
        locators = new LandingPageLocators();
        PageFactory.initElements(driver,this);
    }

    @Step("User finds the SearchNow Button WebElement")
    public WebElement getSearchNowButtonElement()
    {
        return getElement(rootElement,locators.searchNowButton);
    }

    @Step("User Scrolls to Knowledge Tree")
    public void scrollToSearchButton()
    {
        try
        {
            wait(5000);
        }
        catch (Exception e)
        {

            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", locators.searchNowButton);

        }
    }


    @Step("User clicks on the Search Now Button")
    public void clickOnSearchNowButton()
    {
        waitForPageToLoad();
        getSearchNowButtonElement();
        waitForPageToLoad();
        int x= getSearchNowButtonElement().getLocation().getX();
        int y= getSearchNowButtonElement().getLocation().getY();
        Actions actionOnSearchNowButton=new Actions(driver);
        actionOnSearchNowButton.moveToElement(getSearchNowButtonElement(),x,y).build().perform();
        getSearchNowButtonElement().click();
        waitForPageToLoad();
    }


    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }
}
