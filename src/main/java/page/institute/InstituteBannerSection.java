package page.institute;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class InstituteBannerSection extends BasePage
{
    private InstituteLocators locators;
    @FindBy(className = "institute-banner")
    private WebElement rootElement;

    public InstituteBannerSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new InstituteLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getSingUpFreeButton()
    {
        return getElement(rootElement,locators.signUpForFreeButton);
    }
    public WebElement getRequestADemoButton()
    {
        return getElement(rootElement,locators.requestADemoButton);
    }
    @Step("User clicks on the SignUp For Free Button")
    public void clickOnSingUpForFreeButton()
    {
        waitForPageToLoad();
        getSingUpFreeButton().click();
        waitForPageToLoad();
    }

    @Step("User clicks on the Request A Demo Button")
    public void clickOnRequestADemoButton()
    {
        waitForPageToLoad();
        getRequestADemoButton().click();
        waitForPageToLoad();
    }

}
