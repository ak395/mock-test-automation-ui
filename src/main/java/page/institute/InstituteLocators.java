package page.institute;

import org.openqa.selenium.By;
import page.BasePage;

public class InstituteLocators extends BasePage
{

    By  contactUsButton= By.xpath("//button[text()='CONTACT US']");
    By contactUsSectionTitle=By.xpath("//div[@class='con-head']");
    By loginButton=By.xpath("//button[text()='LOGIN']");
    By signUpForFreeButton=By.xpath("//button[@id='signUpButton']");
    By signUpPageTitle=By.xpath("//span[@class='hd-institute']");
    By requestADemoButton=By.xpath("//span[@class='requestDemo']");
    By requestADemoPopUpTitle=By.xpath("//p[@class='required-demo-title']");




}
