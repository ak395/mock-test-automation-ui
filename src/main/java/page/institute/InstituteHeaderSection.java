package page.institute;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

import static org.testng.Assert.assertEquals;

public class InstituteHeaderSection extends BasePage
{
    private InstituteLocators locators;
    @FindBy(className = "institute-header")
    private WebElement rootElement;

    public InstituteHeaderSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new InstituteLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getContactUSButton()
    {
        return getElement(rootElement,locators.contactUsButton);
    }

    public WebElement getContactUsTitleSection()
    {
        return getElement(rootElement,locators.contactUsSectionTitle);
    }
    public WebElement getLoginButton()
    {
        return getElement(rootElement,locators.loginButton);
    }
    @Step("User clicks on the Contact US Button")
    public void clickOnContactUsButton()
    {

        waitForPageToLoad();
        getContactUSButton().click();
        waitForPageToLoad();
    }

    @Step("User verifies the title of the Search Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        assertEquals(getContactUsTitleSection().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }

    @Step("User clicks on the Login Button")
    public void clickOnLoginButton()
    {
        waitForPageToLoad();
        getLoginButton().click();
        waitForPageToLoad();
    }

    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url)
    {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }
}
