package page.institute;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

public class InstituteLinks extends BasePage
{
    @FindBy(className = "divam-wrapper")
    private WebElement rootElement;


    public InstituteLinks(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @Step("User fetches all links and cllck on all links")
    public void fetchAllLinks() throws IOException {
        //local Variables:
        String homePage = "http://www.embibe.com";
        String url = "";
        HttpURLConnection huc = null;
        int respCode = 200;

        waitForPageToLoad();
        List<WebElement> allLinks=driver.findElements(By.tagName("a"));
        for(WebElement link:allLinks){
            System.out.println(link + " - " + link.getAttribute("href"));
            System.out.println(link+"-"+link.getText());
            //Obtain Iterator to traverse to the List
            Iterator<WebElement> iterator=allLinks.iterator();

            //Identifying and validating URL:
            url=iterator.next().getAttribute("href");
            huc = (HttpURLConnection)(new URL(url).openConnection());
            huc.setRequestMethod("HEAD");
            huc.connect();
            respCode = huc.getResponseCode();
            if(respCode == 200){
                System.out.println(url+" is a valid link");
            }
            else{
                System.out.println(url+" is a broken link");
            }


        }


    }

}
