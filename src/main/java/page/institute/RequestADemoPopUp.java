package page.institute;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static org.testng.Assert.assertEquals;

public class RequestADemoPopUp extends BasePage
{

    private InstituteLocators locators;
    @FindBy(className = "modal-wrap")
    private WebElement rootElement;


    public WebElement getRequestADemoTitle()
    {
        return getElement(rootElement,locators.requestADemoPopUpTitle);
    }
    public RequestADemoPopUp(WebDriver driver)
    {
        this.driver=driver;
        locators=new InstituteLocators();
        PageFactory.initElements(driver,this);
    }

    @Step("User verifies the Title of the SignUp Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        assertEquals(getRequestADemoTitle().getText().trim(),expectedHeader);
    }

}
