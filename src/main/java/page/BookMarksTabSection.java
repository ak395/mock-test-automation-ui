package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

class BookMarksTabSection extends BasePage {

    @FindBy(xpath = "//div[text()='Questions']")
    private WebElement questionsFilterElement;

    @FindBy(css = ".bookmark-questions-wrap .bookmark-question")
    private WebElement questionBodyElement;

    BookMarksTabSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    void clickOnQuestionsFilter(){
        waitForElementToBeVisible(questionsFilterElement);
        questionsFilterElement.click();
    }

    boolean isQuestionPresentInBookmarks(){
        waitForElementToBeVisible(questionBodyElement);
        return questionBodyElement.isDisplayed();
    }
}
