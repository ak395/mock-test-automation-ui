package page.studyResultantPage;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

@Getter
public class StudyResultantHomePage extends BasePage {

    private LeftWidgetSection leftWidgetSection;

    public StudyResultantHomePage() {
        driver = DriverProvider.getDriver();

        PageFactory.initElements(driver, this);

        leftWidgetSection = new LeftWidgetSection();


    }

}
