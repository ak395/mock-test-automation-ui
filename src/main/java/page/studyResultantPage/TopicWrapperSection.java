package page.studyResultantPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class TopicWrapperSection extends BasePage {

    private TopicWrapperSectionLocators locators;

    @FindBy(className = "topic-wrapper")
    private WebElement rootElement;

    public TopicWrapperSection(WebDriver driver) {
        this.driver = driver;
        locators = new TopicWrapperSectionLocators();
        PageFactory.initElements(driver, this);

    }

    public WebElement getLearnMoreElement() {
        return getElement(rootElement,locators.learnMore);
    }

    public void clickOnLearnMore() {
        getLearnMoreElement().click();
    }
}
