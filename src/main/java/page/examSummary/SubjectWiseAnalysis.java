package page.examSummary;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class SubjectWiseAnalysis extends BasePage {

    @FindBy(css = ".subjectwise-time-spent-chart .highcharts-column-series:nth-child(1) .highcharts-point:nth-child(1)")
    WebElement subjectWiseTime;

//    @FindBy(css = ".highcharts-series-hover")
//    WebElement subjectWiseTime2;


    @FindBy(css = "span[data-z-index='1'] >div:nth-of-type(1)")
    WebElement questionAttempted;

    @FindBy(css = "span[data-z-index='1'] >div:nth-of-type(2)")
    WebElement unattemptedTime;

    @FindBy(css = "span[data-z-index='1'] >div:nth-of-type(2)>div")
    WebElement attemptedTime;


    public SubjectWiseAnalysis() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void getEelText() {

        Actions action = new Actions(driver);
        action.moveToElement(subjectWiseTime).clickAndHold().build().perform();
//        String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
//            ((JavascriptExecutor) driver).executeScript(mouseOverScript, subjectWiseTime);


        //System.out.println(questionAttempted.getText()+"  questionAttempted");
        System.out.println(unattemptedTime.getText() + "  unattemptedTime");
        System.out.println("end the try211 block");
        System.out.println("    ");
        System.out.println(attemptedTime.getText() + "   attemptedTime ");


    }
}
