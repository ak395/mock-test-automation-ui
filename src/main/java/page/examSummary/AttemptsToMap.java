package page.examSummary;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.HashMap;
import java.util.List;

public class AttemptsToMap extends BasePage {

    @FindBy(css = ".attempts-type-widget > .attempts-type-widget-wrapper")
    private List<WebElement> attemptDetails;

    public AttemptsToMap(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public HashMap getAttemptTypesAndValues() {
        waitForListOfToBeVisible(attemptDetails);
        HashMap map = new HashMap();
        if (System.getProperty("env").equalsIgnoreCase("production")) {
            attemptDetails.stream().
                    forEach(element -> {
                        if (element.findElements(By.cssSelector(".attempt-image")).isEmpty() == false) {
                            String attemptType = StringUtils.substringBetween(element.findElement(By.cssSelector(".attempt-image")).getAttribute("src"), "assets/images/", ".svg");
                            String attemptTypeCount = element.findElement(By.cssSelector(".desc-wrapper >.count")).getText();

                            map.put(attemptType, attemptTypeCount);
                        }
                    });
        }

        else{
            attemptDetails.stream().
                    forEach(element -> {
                        if (element.findElements(By.cssSelector(".attempt-image")).isEmpty() == false) {
                            String attemptType = StringUtils.substringBetween(element.findElement(By.cssSelector(".attempt-image")).getAttribute("src"), "divum/images/", ".svg");
                            String attemptTypeCount = element.findElement(By.cssSelector(".desc-wrapper >.count")).getText();

                            map.put(attemptType, attemptTypeCount);
                        }
                    });
        }
        System.out.println("Feedback Page Map");
        map.forEach((key, value) -> System.out.println(key + " : " + value));
        return map;
    }
}
