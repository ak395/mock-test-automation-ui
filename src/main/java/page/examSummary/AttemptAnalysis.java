package page.examSummary;

import com.google.common.base.CharMatcher;
import constants.Badge;
import driver.DriverProvider;
import entities.MockTestPlanner;
import mockTestFactory.MyChoice;
import mockTestService.MockTestClient;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import page.BasePage;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AttemptAnalysis extends BasePage {
    MockTestClient mockTestClient;
    SoftAssert softAssert;
    @FindBy(css = ".heading")
    private WebElement attemptAnalysis;


    @FindBy(css = ".attempts-type-widget-wrapper")
    private List<WebElement> attemptsTypeOfQuestions;

    @FindBy(css = ".chart-title-wrapper")
    private List<WebElement> totalattemptTypeQuestions;


    public AttemptAnalysis() {
        softAssert = new SoftAssert();
        mockTestClient = new MockTestClient();
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }


    public boolean attemptTypeAnalysisHeadingIsDisplay() {
        return attemptAnalysis.isDisplayed();
    }

    public void attemptTypeQuestionIsDisplay() {
        SoftAssert softAssertion = new SoftAssert();

        attemptsTypeOfQuestions.stream().forEach(e ->
                softAssertion.assertTrue(e.isDisplayed(), e.getText() + "attempted  question type is not display"));

        softAssertion.assertAll();
    }

    public int getNumberOfAnsweredQuestions() {
        waitForElementToBeVisible(attemptAnalysis);
        List<String> list = Arrays.asList(attemptAnalysis.getText().split("\\|"));
        return Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(list.stream().filter(e -> e.contains("Attempted:")).findFirst().get()));


    }

    public int getTotalQuestions() {
        waitForElementToBeVisible(attemptAnalysis);
        List<String> list = Arrays.asList(attemptAnalysis.getText().split("\\|"));
        return Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(list.stream().filter(e -> e.contains("Total Questions:")).findFirst().get()));


    }

    public int getTotalAttemptTypeQuestionOfQuestion(String questionType) {
        waitForListOfToBeVisible(totalattemptTypeQuestions);
        return Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(totalattemptTypeQuestions.stream().
                filter(e -> e.getText().contains(questionType)).findFirst().get().getText()));


    }

    public int getNumberOfCorrectAnsweredQuestions() {
        return getTotalAttemptTypeQuestionOfQuestion("CORRECT");
    }

    public int getNumberOfIncorrectAnsweredQuestions() {
        return getTotalAttemptTypeQuestionOfQuestion("INCORRECT");
    }

    public int getNumberOfUnattemptsQuestions() {
        return getTotalAttemptTypeQuestionOfQuestion("UNATTEMPTS");
    }


    /**
     * @param questionType is question attempted by user like overtime correct or not visited
     * @return its return number question which is pass as a parameter
     */
    public int getTypeOfQuestion(String questionType) {
        waitForListOfToBeVisible(attemptsTypeOfQuestions);
        return Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(attemptsTypeOfQuestions.stream().
                filter(e -> e.getText().contains(questionType)).findFirst().get().getText()));


    }


    public int getNumberOfTooFastCorrectAnsweredQuestions() {
        return getTypeOfQuestion("Too Fast Correct");
    }

    public int getNumberOfPerfectAnsweredQuestions() {
        return getTypeOfQuestion("Perfect");
    }

    public int getNumberOfOveraTimeCorrectAnsweredQuestions() {
        return getTypeOfQuestion("Overtime Correct");
    }

    public int getNumberOfWastedAttemptedQuestion() {
        return getTypeOfQuestion("Wasted");
    }

    public int getNumberOfIncorrectAttemptedQuestion() {
        return getTypeOfQuestion("Incorrect");
    }

    public int getNumberOfOverTimeIncorrectQuestion() {
        return getTypeOfQuestion("Overtime Incorrect");
    }

    public int getNumberOfNotAnsweredQuestions() {
        return getTypeOfQuestion("Not Answered");
    }

    public int getNumberOfNotVisitedQuestions() {
        return getTypeOfQuestion("Not Visited");
    }

    public int getNumberOfReviewLaterQuestions() {
        return getTypeOfQuestion("Marked for Review");
    }


    public void verifyTotalCorrectAnsweredQuestions(MockTestPlanner planner) {
        Set<MyChoice> attemptType = new HashSet<>();
        attemptType.add(MyChoice.CHOICE_CORRECT);
        attemptType.add(MyChoice.CHOICE_ANSWERED_REVIEW_LATER);
        int correctAnsweredQuestions = planner.getNumberOfQuestionAttemptTypeFor(attemptType);
        softAssert.assertEquals(getNumberOfCorrectAnsweredQuestions(), correctAnsweredQuestions,
                String.format("total number of correct question in a test should be %s but found %s",
                        correctAnsweredQuestions, getNumberOfCorrectAnsweredQuestions()));
    }

    public void verifyTotalInCorrectAnsweredQuestions(MockTestPlanner planner) {
        Set<MyChoice> attemptType = new HashSet<>();
        attemptType.add(MyChoice.CHOICE_INCORRECT);
        int incorrectAnsweredQuestions = planner.getNumberOfQuestionAttemptTypeFor(attemptType);
        softAssert.assertEquals(getNumberOfIncorrectAnsweredQuestions(), incorrectAnsweredQuestions,
                String.format("total number of incorrect question in a test should be %s but found %s",
                        incorrectAnsweredQuestions, getNumberOfIncorrectAnsweredQuestions()));
    }

    public void verifyTotalNumberOfQuestion(MockTestPlanner planner) {
        int expectedQuestionCount = mockTestClient.getQuestionCount(planner.getMockTestDataList().get(0).getResponseBody());
        Assert.assertEquals(getTotalQuestions(), expectedQuestionCount, String.format("total number of question in a test should be %s but found %s", expectedQuestionCount, getTotalQuestions()));
    }

    public void verifyAnsweredQuestion(MockTestPlanner planner) {
        Set<MyChoice> attemptType = new HashSet<>();
        attemptType.add(MyChoice.CHOICE_CORRECT);
        attemptType.add(MyChoice.CHOICE_INCORRECT);
        attemptType.add(MyChoice.CHOICE_ANSWERED_REVIEW_LATER);
        int answeredQuestion = planner.getNumberOfQuestionAttemptTypeFor(attemptType);
        Assert.assertEquals(getNumberOfAnsweredQuestions(), answeredQuestion, String.format("total number of answered  question in a test should be %s but found %s", answeredQuestion, getNumberOfAnsweredQuestions()));


    }

    public void verifyNotAttemptedQuestion(MockTestPlanner planner) {
        Set<MyChoice> attemptType = new HashSet<>();
        attemptType.add(MyChoice.CHOICE_UNATTEMPTED);
        int unattemptedQuestion = planner.getNumberOfQuestionAttemptTypeFor(attemptType) + 1;
        Assert.assertEquals(getNumberOfNotAnsweredQuestions(), unattemptedQuestion, String.format("total number of answered  question in a test should be %s but found %s", unattemptedQuestion, getNumberOfNotAnsweredQuestions()));

    }

    public void verifyReviewLaterQuestion(MockTestPlanner planner) {
        Set<MyChoice> attemptType = new HashSet<>();
        attemptType.add(MyChoice.CHOICE_REVIEW_LATER);
        int reviewLaterQuestion = planner.getNumberOfQuestionAttemptTypeFor(attemptType) + 1;
        Assert.assertEquals(getNumberOfNotAnsweredQuestions(), reviewLaterQuestion, String.format("total number of answered  question in a test should be %s but found %s", reviewLaterQuestion, getNumberOfReviewLaterQuestions()));


    }

    public void verifyPerfectAttempt(MockTestPlanner planner) {
        int perfectAttemptQuestion = planner.getNumberOfQuestionsFor(Badge.BADGE_PERFECT_ATTEMPT);
        softAssert.assertEquals(getNumberOfPerfectAnsweredQuestions(), perfectAttemptQuestion,
                String.format("total number of perfect attempt type question should be %s but display %s ",
                        perfectAttemptQuestion, getNumberOfPerfectAnsweredQuestions()));

    }

    public void verifyTooFastCorrectAttempt(MockTestPlanner planner) {
        int tooFastCorrectAttemptQuestions = planner.getNumberOfQuestionsFor(Badge.BADGE_TOO_FAST_CORRECT);
        softAssert.assertEquals(getNumberOfTooFastCorrectAnsweredQuestions(), tooFastCorrectAttemptQuestions,
                String.format("total number of too fast attempt type question should be %s but display %s ",
                        tooFastCorrectAttemptQuestions, getNumberOfTooFastCorrectAnsweredQuestions()));
    }

    public void verifyOverTimeCorrectQuestion(MockTestPlanner planner) {
        int overTimeCorrectAttemptQuestions = planner.getNumberOfQuestionsFor(Badge.BADGE_OVERTIME_CORRECT);
        softAssert.assertEquals(getNumberOfOveraTimeCorrectAnsweredQuestions(), overTimeCorrectAttemptQuestions,
                String.format("total number of over time correct attempt type question should be %s but display %s ",
                        overTimeCorrectAttemptQuestions, getNumberOfOveraTimeCorrectAnsweredQuestions()));

    }

    public void verifyWastedAttempt(MockTestPlanner planner) {
        int wastedAttempt = planner.getNumberOfQuestionsFor(Badge.BADGE_WASTED_ATTEMPT);
        softAssert.assertEquals(getNumberOfWastedAttemptedQuestion(), wastedAttempt,
                String.format("total number of wasted attempt type question should be %s but display %s ",
                        wastedAttempt, getNumberOfWastedAttemptedQuestion()));

    }

    public void verifyOverTimeIncorrect(MockTestPlanner planner) {
        int overTimeIncorrect = planner.getNumberOfQuestionsFor(Badge.BADGE_OVERTIME_INCORRECT);
        softAssert.assertEquals(getNumberOfOverTimeIncorrectQuestion(), overTimeIncorrect,
                String.format("total number of over time incorrect attempt type question should be %s but display %s ",
                        overTimeIncorrect, getNumberOfOverTimeIncorrectQuestion()));


    }

    public void verifyInCorrectAttempt(MockTestPlanner planner) {
        int incorrectAttempt = planner.getNumberOfQuestionsFor(Badge.BADGE_INCORRECT_ATTEMPT);
        softAssert.assertEquals(getNumberOfIncorrectAttemptedQuestion(), incorrectAttempt,
                String.format("total number of incorrect attempt type question should be %s but display %s ",
                        incorrectAttempt, getNumberOfIncorrectAttemptedQuestion()));


    }


}
