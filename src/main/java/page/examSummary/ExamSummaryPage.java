package page.examSummary;

import com.google.common.base.CharMatcher;
import driver.DriverProvider;
import entities.MockTestData;
import entities.MockTestPlanner;
import entities.questionTypes.mockTest.MockTestQuestion;
import mockTestService.MockTestClient;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import page.BasePage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ExamSummaryPage extends BasePage {

    MockTestClient mockTestClient;
    AttemptAnalysis attemptAnalysis;

    @FindBy(css = ".perfect")
    private WebElement perfectAttemptTypeWidget;

    @FindBy(css = ".attempt-wrapper .wasted")
    private WebElement wastedAttemptTypeWidget;

    @FindBy(css = ".unattempted")
    private WebElement unattemptedAttemptTypeWidget;

    @FindBy(xpath = "//div[text()='Show Me Feedback']")
    private WebElement showfeedbackButton;

    @FindAll({
            @FindBy(css = ".section-wrapper:nth-child(2)"),
            @FindBy(xpath = "//div[text()='Overall Performance']")
    })
    private WebElement overallPerformanceSection;

    @FindBy(css = ".test-feedback-result-card:nth-child(1) .obtained-socre")
    private WebElement obtainedScore;

    @FindBy(css = ".test-feedback-result-card:nth-child(1) .total-score")
    private WebElement totalScore;

    @FindBy(css = ".test-feedback-result-card:nth-child(2) .obtained-socre")
    private WebElement percentagemarks;

    @FindBy(css = ".legend-item:nth-of-type(1)")
    private WebElement averageMarks;

    @FindAll({
            @FindBy(css = ".legend-item:nth-of-type(2)"),
            @FindBy(css = ".legend-item")})
    private WebElement cutoffMarks;


    @FindBy(css = ".legend-item:nth-of-type(3)")
    private WebElement topScore;


    @FindBy(css = ".mark-stepper-container .min")
    private WebElement minScore;

    @FindBy(css = ".mark-stepper-container .max")
    private WebElement maxScore;

    @FindBy(css = ".section-wrapper:nth-child(1)")
    private WebElement embibeGuideSection;

    @FindBy(className = "section-name")
    private List<WebElement> sectionName;

    @FindBy(css = ".divam-wrapper .testSummary-box")
    private WebElement testSummaryBox;

    @FindBy(css = ".test-submit-footer-text.failed")
    private WebElement offlineSubmitTestMessage;

    @FindBy(css = ".test-summary-footer.practice-legends-items-div")
    private WebElement submittingTestOffline;


    @FindBy(className = "tf-test-title")
    private WebElement OverallPerformanceHeader;

    @FindBy(css = ".behaviour-meter")
    private WebElement behaviourMeter;

    @FindBy(className = "learn-more-text")
    private WebElement learMoreText;


    public ExamSummaryPage() {
        attemptAnalysis = new AttemptAnalysis();
        mockTestClient = new MockTestClient();
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }


    public void testSummaryBoxIsDisplayed() {
        waitForElementToBeVisible(testSummaryBox);
        Assert.assertTrue(testSummaryBox.isDisplayed(), "Test Summary Box is not displayed");
    }

    public void clickOnShowMeFeedBackButton() {
        try {
            waitForElementTobeClickable(showfeedbackButton);
            jsClick(showfeedbackButton);
        } catch (NoSuchElementException ex) {
            waitForElementTobeClickable(showfeedbackButton);
            jsClick(showfeedbackButton);
        }


    }

    public boolean showMeFeedbackButtonIsDisplayed() {
        waitForElementToBeVisible(showfeedbackButton);
        Assert.assertTrue(showfeedbackButton.isDisplayed(), "Show Me Feedback Button is not displayed ");
        return showfeedbackButton.isDisplayed();
    }

    public void verifyUserIsNotAbleToSubmitTestOffline() {
        waitForElementToBeVisible(submittingTestOffline);
        Assert.assertTrue(submittingTestOffline.isDisplayed(), "Submitting Test Button is not displayed");
        Assert.assertEquals(offlineSubmitTestMessage.getText(), "Please make sure you don't close the tab and are connected to the internet to view your feedback.", "Text are not matching");
    }


    public void clickOnOverallPerformanceSection() {
//        waitForPageToLoad();
        try {
            waitForElementToBeDisplay(overallPerformanceSection);
        } catch (NoSuchElementException ex) {
            refreshPage();
            waitForElementToBeVisible(overallPerformanceSection);
        }
        Assert.assertTrue(overallPerformanceSection.isDisplayed(), "overall performance button is not display after click on show me feedback button");
        jsClick(overallPerformanceSection);

    }

    public int getScoreObtainByUser() {
        return Integer.parseInt(obtainedScore.getText());
    }

    public int getTotalScore() {
        return Integer.parseInt(totalScore.getText().replace("/", ""));

    }

    public int getCutOffMarks() {
        return Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(cutoffMarks.getText()));


    }


    public void verifiedCutOffMarks(MockTestPlanner planner) {
        Assert.assertEquals(getCutOffMarks(), getCutOffMarksForTest(planner), String.format("user expected %s cutoff marks but found %s ", getCutOffMarksForTest(planner), getCutOffMarks()));
    }

    public double getPercentage() {
        return Double.valueOf(percentagemarks.getText().replaceAll("%", ""));
    }

    public double calculatePercentage() {

        float percentage = getScoreObtainByUser() * 100 / (float) getTotalScore();
        return Math.round(percentage * 100.0) / 100.0;
    }


    public void verifiedPercentageMarks() {
        Assert.assertEquals(getPercentage(), calculatePercentage(), String.format("user expected %s percentage marks but found %s ", calculatePercentage(), getPercentage()));
    }

    public void verifyPerformanceForAllQuestionAttemptedCorrect(MockTestPlanner mockTestPlanner) {
        Assert.assertEquals(getScoreObtainByUser(), getTotalScore(), String.format("score obtain by user is %s and but expected score is  %s", getScoreObtainByUser(), getTotalScore()));
        Assert.assertEquals(getUserMarksForTest(mockTestPlanner), getTotalScore(), String.format("score obtain by user is %s and but expected score is  %s", getUserMarksForTest(mockTestPlanner), getTotalScore()));
    }

    public void verifyPerformanceForAllQuestionAttemptedIncorrectly() {
        Assert.assertTrue(getScoreObtainByUser() <= 0, String.format("score obtain by user is %s and but expected score is less then 0", getScoreObtainByUser()));
    }

    public void verifyPerformanceForZeroQuestionAttempted() {
        Assert.assertTrue(getScoreObtainByUser() <= 0);
    }

    public void verifyIntermediateCorrectness(MockTestPlanner mockTestPlanner) {
        Assert.assertEquals(getUserMarksForTest(mockTestPlanner), getScoreObtainByUser(), String.format("score obtain by user is %s and but expected score is  %s", getUserMarksForTest(mockTestPlanner), getScoreObtainByUser()));

    }


    public void verifyUserAttemptedAllTypeOfQuestion(MockTestPlanner mockTestPlanner) {
        int i = 0;
        List<String> questionTypeInATest = new ArrayList<>();
        questionTypeInATest.add("Single Choice");
        questionTypeInATest.add("Multiple Choice");
        questionTypeInATest.add("Integer");
        questionTypeInATest.add("Matrix");
        questionTypeInATest.add("Linked Comprehension");
        List<String> questionType = null;
        while (i < mockTestPlanner.getMockTestQuestions().size()) {
            i++;
            questionType = mockTestPlanner.getMockTestQuestions().stream().map(MockTestQuestion::getQuestionType).
                    distinct().collect(Collectors.toList());
        }


        assert questionType != null;
        questionType.forEach(System.out::println);
        Assert.assertTrue(questionTypeInATest.retainAll(questionType), "user not able to attempt all type question");
    }

    public String averageMarks() {
        waitForElementToBeVisible(averageMarks);
        return CharMatcher.inRange('0', '9').retainFrom(averageMarks.getText());

    }

    public int minimumMarks() {
        return Integer.parseInt(minScore.getText());
    }

    public String topScorerMarks() {
        return CharMatcher.inRange('0', '9').retainFrom(topScore.getText());
    }

    public void verifyFeedbackPage() {
        waitForElementToBeVisible(embibeGuideSection);
        List<String> section = new ArrayList<>();
        section.add("Embibe Guide");
        section.add("Overall Performance");
        section.add("Question-wise Analysis");
        section.add("Test-on-Test Analysis");
        Assert.assertEquals(sectionName.stream().map(WebElement::getText).collect(Collectors.toList()), section, "section name is not matching");


    }

    public int getUserMarksForTest(MockTestPlanner mockTestPlanner) {
        return mockTestPlanner.getTotalMarksForTest();


    }

    private int getTotalMarksForTest(MockTestPlanner planner) {
        AtomicInteger totalMarsForTest = new AtomicInteger();
        List<MockTestData> response = new ArrayList<>(planner.getMockTestDataList());
        response.stream().forEach(e ->
                totalMarsForTest.set(mockTestClient.getMaximumMarks(e.getResponseBody())));
        return totalMarsForTest.get();

    }

    private int getTimeForTest(MockTestPlanner planner) {
        AtomicInteger timeForTest = new AtomicInteger();
        List<MockTestData> response = new ArrayList<>(planner.getMockTestDataList());
        response.stream().forEach(e ->
                timeForTest.set(mockTestClient.getTestTime(e.getResponseBody())));
        return timeForTest.get();
    }

    private int getCutOffMarksForTest(MockTestPlanner planner) {
        AtomicInteger cutoffMarks = new AtomicInteger();
        List<MockTestData> response = new ArrayList<>(planner.getMockTestDataList());
        response.stream().forEach(e ->
                cutoffMarks.set(mockTestClient.getMockTestCutOffMarks(e.getResponseBody())));
        return cutoffMarks.get();
    }

    private int getAvarageGoodScoreForTest(MockTestPlanner planner) {
        AtomicInteger avarageGoodScore = new AtomicInteger();
        List<MockTestData> response = new ArrayList<>(planner.getMockTestDataList());
        response.stream().forEach(e ->
                avarageGoodScore.set(mockTestClient.getMockTestMinimumAvgScore(e.getResponseBody())));
        return avarageGoodScore.get();
    }

    private int getMockTestMinimumGoodScore(MockTestPlanner planner) {
        AtomicInteger minimumGoodScore = new AtomicInteger();
        List<MockTestData> response = new ArrayList<>(planner.getMockTestDataList());
        response.stream().forEach(e ->
                minimumGoodScore.set(mockTestClient.getMockTestMinimumGoodScore(e.getResponseBody())));
        return minimumGoodScore.get();
    }

    public void verifyOverAllPerformancePage(MockTestPlanner planner) {
        SoftAssert softAssertion = new SoftAssert();

        attemptAnalysis.attemptTypeQuestionIsDisplay();
        softAssertion.assertTrue(attemptAnalysis.attemptTypeAnalysisHeadingIsDisplay(), " attempt type analysis is not display");
        softAssertion.assertEquals(attemptAnalysis.getNumberOfAnsweredQuestions(), planner.getMockTestQuestions().stream().count());
        softAssertion.assertTrue(obtainedScore.isDisplayed(), "marks obtain by student is not display");
        softAssertion.assertTrue(totalScore.isDisplayed(), "total marks in a test is not display");
        softAssertion.assertTrue(topScore.isDisplayed(), "top scorer marks is not display");
        softAssertion.assertTrue(averageMarks.isDisplayed(), "average  marks is not display");
        softAssertion.assertTrue(minScore.isDisplayed(), "minimum scorer   marks is not display");
        softAssertion.assertTrue(maxScore.isDisplayed(), "maximum  marks is not display");
        softAssertion.assertTrue(percentagemarks.isDisplayed(), "percentage  marks is not display");
        softAssertion.assertTrue(cutoffMarks.isDisplayed(), "cutoff  marks is not display");
        softAssertion.assertTrue(perfectAttemptTypeWidget.isDisplayed(), "perfectAttemptTypeWidget is not display");
        softAssertion.assertTrue(wastedAttemptTypeWidget.isDisplayed(), "wastedAttemptTypeWidget is not display");
        softAssertion.assertTrue(unattemptedAttemptTypeWidget.isDisplayed(), "unattemptedAttemptTypeWidget is not display");
        softAssertion.assertEquals(learMoreText.getText(), "Understand how attempt types help improve learning outcomes.", learMoreText.getText() + "is not display");
        softAssertion.assertTrue(behaviourMeter.isDisplayed(), "behaviour meter is not display");
        softAssertion.assertAll();
    }


    public void verifyQuestionStatisticeFieldIsDisplay() {
        attemptAnalysis.attemptTypeQuestionIsDisplay();
    }

    public void verifyQuestionStatistics(MockTestPlanner planner) {
        attemptAnalysis.verifyTotalNumberOfQuestion(planner);
        attemptAnalysis.verifyAnsweredQuestion(planner);
        attemptAnalysis.verifyNotAttemptedQuestion(planner);
        attemptAnalysis.verifyReviewLaterQuestion(planner);
    }

    public void verifyQuestionStatisticsForCorrectAndIncorrectAttemptType(MockTestPlanner planner) {
        waitForPageToLoad();
        attemptAnalysis.verifyTotalCorrectAnsweredQuestions(planner);
        attemptAnalysis.verifyTotalInCorrectAnsweredQuestions(planner);
        attemptAnalysis.softAssert.assertAll();

    }

    public void verifyQuestionStatisticsForAllAttemptType(MockTestPlanner planner) {
        attemptAnalysis.verifyPerfectAttempt(planner);
        attemptAnalysis.verifyTooFastCorrectAttempt(planner);
        attemptAnalysis.verifyOverTimeCorrectQuestion(planner);
        attemptAnalysis.verifyWastedAttempt(planner);
        attemptAnalysis.verifyOverTimeIncorrect(planner);
        attemptAnalysis.verifyInCorrectAttempt(planner);
        attemptAnalysis.softAssert.assertAll();

    }


}