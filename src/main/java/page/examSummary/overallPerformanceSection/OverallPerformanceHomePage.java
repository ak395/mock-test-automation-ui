package page.examSummary.overallPerformanceSection;

import driver.DriverProvider;
import mockTestService.MockTestClient;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OverallPerformanceHomePage extends BasePage {

    SubjectWiseAnalysisSection subjectWiseAnalysisSection;
    SubjectWiseAttemptsSection subjectWiseAttemptsSection;
    OverallTimeAndAccuracyAnalysisSection overallTimeAndAccuracyAnalysisSection;
    MockTestClient mockTestClient;

    public OverallPerformanceHomePage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        subjectWiseAnalysisSection = new SubjectWiseAnalysisSection();
        subjectWiseAttemptsSection = new SubjectWiseAttemptsSection();
        overallTimeAndAccuracyAnalysisSection = new OverallTimeAndAccuracyAnalysisSection();

    }

    public void verifyTheDataForSubjectWiseTimeSpentGraph(WebDriver driver) {
//        subjectWiseAnalysisSection.verifyThatSubjectWiseAnalysisSectionIsDisplayed();
        subjectWiseAnalysisSection.hoverOnSubjectWiseTimeSpentBarGraph(driver);
    }

    public void verifyTheDataForSubjectWiseAccuracy(WebDriver driver) {
//
        subjectWiseAnalysisSection.hoverOnSubjectWiseAccuracyGraph(driver);
    }

    public void verifyTheDataForSubjectWiseTimeSpentInfoIcon(WebDriver driver){
        subjectWiseAnalysisSection.hoverOnSubjectWiseTimeSpentInfoIcon(driver);
    }

    public void verifyTheHeaderDefinitionsInSubjectWiseTimeSpent(WebDriver driver) {
        subjectWiseAnalysisSection.hoverOnIdealTimeIcon(driver);
        subjectWiseAnalysisSection.hoverOnAttemptedTimeIcon(driver);
        subjectWiseAnalysisSection.hoverOnUnAttemptedTimeIcon(driver);
    }

    public void verifyTimeSpentAndAccuracyGraphIsDisplayed() {
        subjectWiseAnalysisSection.verifyThatSubjectWiseAnalysisSectionIsDisplayed();
    }

    public void verifyAxisLabelsInSubjectWiseTimeSpentGraph(List<String> subjectNames) {
//        subjectWiseAnalysisSection.verifyXAxisLabelOfSubjectWiseTimeSpent(subjectNames);
        subjectWiseAnalysisSection.verifyYAxisLabelOfSubjectWiseTimeSpent();
    }


    public void verifyAxisLabelsInSubjectWiseAccuracyGraph(List<String> subjectNames) {
        subjectWiseAnalysisSection.verifyYAxisLabelOfSubjectWiseAccuracy();
//        subjectWiseAnalysisSection.verifyXAxisLabelOfSubjectWiseAccuracy(subjectNames);
    }

    public void verifyTheDataForSubjectWiseAccuracyInfoIcon(WebDriver driver) {
        subjectWiseAnalysisSection.hoverOnSubjectAccuracyInfoIcon(driver);
    }

    public void verifyTheHeadingAndSubjectNamesInSubjectWiseAttempts(int noOfSubjects, List<String> subjectNames) {
        subjectWiseAttemptsSection.verifyTheSubjectWiseAttemptsHeading();
        subjectWiseAttemptsSection.verifyNumberAndNameOfSubjectWiseAttemptsWidget(noOfSubjects,subjectNames);

    }
    public void verifyAxisLabelForSubjectWiseAttempts() {
        wait(2000);
        subjectWiseAttemptsSection.verifyXAxisLabel();
        subjectWiseAttemptsSection.verifyYAxisLabel();
    }

    public void verifyTheNumberOfCorrectQuestionsInSubjectWiseAttempts(WebDriver driver, List<Integer> correctQuestions, List<Integer> incorrectQuestions) {
        subjectWiseAttemptsSection.verifyTheDataForCorrectQuestions(driver,correctQuestions,incorrectQuestions);
    }

    public void verifyTheDataForIncorrectQuestionsInSubjectWiseAttempts(WebDriver driver, List<Integer> incorrectQuestions) {
        subjectWiseAttemptsSection.verifyTheDataForIncorrectQuestions(driver,incorrectQuestions);
    }

    public void verifyTheDataForUnAttemptedQuestions(WebDriver driver, List<Integer> unAttemptedQuestions) {
        subjectWiseAttemptsSection.verifyTheDataForUnAttemptedQuestions(driver,unAttemptedQuestions);

    }

    public void verifyOverallTimeAndAccuracyAnalysisFields() {
        overallTimeAndAccuracyAnalysisSection.verifyTheHeading();
        overallTimeAndAccuracyAnalysisSection.verifyTheSubWidgets();
        overallTimeAndAccuracyAnalysisSection.verifyTheHeadingOfSubWidgets();
    }

    public void verifyTheAxisLabelForAttemptedTimeSpent() {
        overallTimeAndAccuracyAnalysisSection.verifyXAxisDataOfAttemptedTimeSpent();
        overallTimeAndAccuracyAnalysisSection.verifyYAxisDataOfAttemptedTimeSpent();
    }

    public void verifyTheAxisLabelForAccuracyWithDifficulty() {
        wait(2000);
        overallTimeAndAccuracyAnalysisSection.verifyXAxisDataOfAccuracyWithDifficulty();
        overallTimeAndAccuracyAnalysisSection.verifyYAxisDataOfAccuracyWithDifficulty();
    }

    public void verifyTheDefinitionsOfSubHeadingInAttemptedTimeSpent(WebDriver driver){
        overallTimeAndAccuracyAnalysisSection.verifyIdealTimeToolTip(driver);
        overallTimeAndAccuracyAnalysisSection.verifyAttemptedTimeTooltip(driver);
    }

    public void verifyInfoIconOfAttemptedTimeSpent(WebDriver driver) {
        overallTimeAndAccuracyAnalysisSection.verifyAttemptedTimeSpentInfoIconToolTip(driver);
    }

    public void verifyInfoIconOfAccuracyWithDifficulty(WebDriver driver) {
        overallTimeAndAccuracyAnalysisSection.verifyAccuracyWithDifficultyInfoIconToolTip(driver);
    }

    public void verifyToolTipForEasyMeadiumHard(WebDriver driver) {
        overallTimeAndAccuracyAnalysisSection.hoverOnEasyBarGraph(driver);
    }

    public void verifyTheNoOfEasyQuestions(WebDriver driver, int noOfAttemptedEasyQuestions, int totalSeconds,float attemptedTimeSpent, int totalNumberOfEasyQuestions){



        String minutes = covertSecondsHoursAndMinutes(totalSeconds);
        String attemptedTimeSpentMinutes = convertFloatToHrsMinsAndSeconds(attemptedTimeSpent);
        String attemptedTimeSpentData = overallTimeAndAccuracyAnalysisSection.hoverOnEasyBarGraph(driver);
        String[] noOfAttemptedQuestions;
        if(attemptedTimeSpentData != null) {

            noOfAttemptedQuestions = attemptedTimeSpentData.split("/");
            String noOfEasyQuestionsAttempted = noOfAttemptedQuestions[0];
            System.out.println("The Questions are:" + noOfAttemptedQuestions[0]);

            String totalNoOfEasyQuestions = (noOfAttemptedQuestions[1].split("Question"))[0];
            System.out.println("The Total No. of Easy Questions:" + totalNoOfEasyQuestions);
            System.out.println("Number of Easy Question in Actual: "+ totalNumberOfEasyQuestions);

            Assert.assertEquals(Integer.parseInt(noOfEasyQuestionsAttempted.trim()), noOfAttemptedEasyQuestions, "The Number Medium Questions Attempted in Attempted Time Spent is not correct.");
            Assert.assertEquals(Integer.parseInt(totalNoOfEasyQuestions.trim()),totalNumberOfEasyQuestions,"..flrfl");
//            String mydata = "some string with 'the data i want' inside";
            Pattern patternIdealTime = Pattern.compile("Ideal Time:(.*?)\\n");
            Matcher matcher = patternIdealTime.matcher(attemptedTimeSpentData);

            Pattern patternAttemptedTime = Pattern.compile("Attempted Time:(.*?)$");
            Matcher attemptedTimeMAtcher = patternAttemptedTime.matcher(attemptedTimeSpentData);

            if (matcher.find())
            {    System.out.println("The Time in Tooltip: "+ matcher.group(1 )+ "The Actual Minutes: "+minutes);
                Assert.assertEquals(minutes,matcher.group(1).trim(), "Ideal Time is not same for Medium Questions in Attempted Time Spent Graph");
            }

            if(attemptedTimeMAtcher.find()){
                System.out.println("The Attempted Time in Tooltip: "+ attemptedTimeMAtcher.group(1 )+ "The Actual Minutes: "+attemptedTimeSpentMinutes);
                Assert.assertEquals(attemptedTimeSpentMinutes.trim(),attemptedTimeMAtcher.group(1).trim(),"Attempted Time is not same for Medium Questions");
            }


        }

        else {
            System.out.println("The Data is Zero");
        }

    }

    public void verifyTheNoOfMediumQuestions(WebDriver driver, int noOfMediumQuestions, int totalSeconds, float attemptedTimeSpent, int noOfMediumQuestion) {
        String minutes = covertSecondsHoursAndMinutes(totalSeconds);
        String attemptedTimeSpentMinutes = convertFloatToHrsMinsAndSeconds(attemptedTimeSpent);
        String attemptedTimeSpentData = overallTimeAndAccuracyAnalysisSection.hoverOnMediumBarGraph(driver);
        String[] noOfAttemptedQuestions;
        if(attemptedTimeSpentData != null) {

            noOfAttemptedQuestions = attemptedTimeSpentData.split("/");
            String noOfEasyQuestionsAttempted = noOfAttemptedQuestions[0];
            System.out.println("The Questions are:" + noOfAttemptedQuestions[0]);

            String totalNoOfMediumQuestions = (noOfAttemptedQuestions[1].split("Question"))[0];
            System.out.println("The Total No. of Medium Questions:" + totalNoOfMediumQuestions);
            System.out.println("Number of Medium Question in Actual: "+ noOfMediumQuestion);

            Assert.assertEquals(Integer.parseInt(noOfEasyQuestionsAttempted.trim()), noOfMediumQuestions, "The Number Medium Questions Attempted in Attempted Time Spent is not correct.");
            Assert.assertEquals(Integer.parseInt(totalNoOfMediumQuestions.trim()),noOfMediumQuestion,"..flrfl");
//            String mydata = "some string with 'the data i want' inside";
            Pattern patternIdealTime = Pattern.compile("Ideal Time:(.*?)\\n");
            Matcher matcher = patternIdealTime.matcher(attemptedTimeSpentData);

            Pattern patternAttemptedTime = Pattern.compile("Attempted Time:(.*?)$");
            Matcher attemptedTimeMAtcher = patternAttemptedTime.matcher(attemptedTimeSpentData);

            if (matcher.find())
            {
                Assert.assertEquals(matcher.group(1).trim(),minutes, "Ideal Time is not same for Medium Questions in Attempted Time Spent Graph");
            }

            if(attemptedTimeMAtcher.find()){
                Assert.assertEquals(attemptedTimeMAtcher.group(1).trim(),attemptedTimeSpentMinutes,"Attempted Time is not same for Medium Questions");
            }


        }

        else {
            System.out.println("The Data is Zero");
        }

    }

    public void verifyTheNoOfHardQuestions( WebDriver driver, int expectedNoOfAttemptedHardQuestions, int idealTimeInSeconds,float attemptedTimeSpentInSeconds, int expectedTotalNumberOfHArdQuestions,int totalQuestionsFromApi) {


        String expectedIdealTime = covertSecondsHoursAndMinutes(idealTimeInSeconds);
        String expectedAttemptedTime = convertFloatToHrsMinsAndSeconds(attemptedTimeSpentInSeconds);
        String attemptedTimeSpentData = overallTimeAndAccuracyAnalysisSection.hoverOnHardBarGraph(driver);
        String[] noOfAttemptedQuestions;
        if(attemptedTimeSpentData != null) {

            noOfAttemptedQuestions = attemptedTimeSpentData.split("/");
            String actualNoOfHardQuestionsAttempted = noOfAttemptedQuestions[0];
            System.out.println("The Questions are:" + noOfAttemptedQuestions[0]);

            String actualNoOfTotalHardQuestions = (noOfAttemptedQuestions[1].split("Question"))[0];
            System.out.println("The Total No. of Hard Questions:" + actualNoOfTotalHardQuestions);
            System.out.println("Number of Total Hard Questions Question: "+ expectedTotalNumberOfHArdQuestions);

            Assert.assertEquals(expectedNoOfAttemptedHardQuestions, Integer.parseInt(actualNoOfHardQuestionsAttempted.trim()), "The Number Hard Questions Attempted in Attempted Time Spent is not correct.");
            Assert.assertEquals(expectedTotalNumberOfHArdQuestions,Integer.parseInt(actualNoOfTotalHardQuestions.trim()),"The Total Number of Hard Questions in Attempted Time Spent is not same");
//
            Pattern patternIdealTime = Pattern.compile("Ideal Time:(.*?)\\n");
            Matcher actualIdealTime = patternIdealTime.matcher(attemptedTimeSpentData);

            Pattern patternAttemptedTime = Pattern.compile("Attempted Time:(.*?)$");
            Matcher actualAttemptedTime = patternAttemptedTime.matcher(attemptedTimeSpentData);

            if (actualIdealTime.find())
            {    System.out.println("The Time in Tooltip: "+ actualIdealTime.group(1 )+ "The Actual Minutes: "+expectedIdealTime);
                Assert.assertEquals(expectedIdealTime,actualIdealTime.group(1).trim(), "Ideal Time is not same for Medium Questions in Attempted Time Spent Graph");
            }

            if(actualAttemptedTime.find()){
                System.out.println("The Attempted Time in Tooltip: "+ actualAttemptedTime.group(1 )+ "The Actual Minutes: "+expectedAttemptedTime);
                Assert.assertEquals(expectedAttemptedTime.trim(),actualAttemptedTime.group(1).trim(),"Attempted Time is not same for Medium Questions");
            }


        }

        else {
            Assert.assertEquals(expectedTotalNumberOfHArdQuestions,totalQuestionsFromApi,"The Total No. of Hard Questions are not Equal");
            Assert.assertEquals(expectedAttemptedTime,"0", String.format("The Expected Attempted Time should be zero but found %s",expectedAttemptedTime));
        }

    }


}