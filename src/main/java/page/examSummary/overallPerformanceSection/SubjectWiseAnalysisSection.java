package page.examSummary.overallPerformanceSection;

import driver.DriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import page.BasePage;
import page.mockTestQuestionsPage.QuestionsHomePage;

import java.util.ArrayList;
import java.util.List;

public class SubjectWiseAnalysisSection extends BasePage {

    SoftAssert softAssert;
    QuestionsHomePage questionsHomePage;

    @FindBy(css = ".subjectwise-time-spent-wrapper")
    private WebElement subjectWiseTimeSpent;

    @FindBy(css = ".subject-wise-accuracy-wrapper")
    private WebElement subjectWiseAccuracy;

    @FindBy(xpath = ".//*[text()='Subject-Wise Analysis']")
    private WebElement subjectWiseAnalysisHeader;

    @FindBy(css = ".subjectwise-time-spent-chart .highcharts-series-group .highcharts-series-1 .highcharts-point")
    private List<WebElement> subjectWiseTimeSpentGraphs;

    @FindBy(css = ".subject-wise-accuracy-wrapper .highcharts-series-group .highcharts-series-0 .highcharts-point")
    private List<WebElement> subjectWiseAccuracyGraph;

    @FindBy(css = ".subjectwise-time-spent-wrapper .chart-info-icon")
    private WebElement subjectWiseTimeSpentInfoIcon;

    @FindBy(css = ".subject-wise-accuracy-wrapper .chart-info-icon")
    private WebElement subjectWiseAccuracyInfoIcon;

    @FindBy(css = ".subjectwise-time-spent-wrapper  .bottomLeft  .toolTip-text")
    private WebElement subjectWiseTimeSpentInfoIconText;

    @FindBy(css = ".subject-wise-accuracy-wrapper .bottomLeft  .toolTip-text")
    private WebElement subjectWiseAccuracyInfoIconText;

    @FindBy(css = ".subjectwise-time-spent-wrapper .chart-legend")
    private List<WebElement> subjectWiseTimeSpentHeaderDefinitionsIcon;

    @FindBy(css = ".subjectwise-time-spent-wrapper .toolTip.bottomRight")
    private WebElement idealTimeDefinitonText;

    @FindBy(css = ".subjectwise-time-spent-wrapper .toolTip.bottom")
    private List<WebElement> attemptedTimeAndUnAttemptedTimeDefinitionText;

    @FindBy(css = ".subjectwise-time-spent-chart .highcharts-xaxis-labels text")
    private List<WebElement> xAxisLabelOfSubjectWiseTimeSpent;

    @FindBy(css = ".subject-wise-accuracy-chart .highcharts-xaxis-labels text")
    private List<WebElement> xAxisLabelOfSubjectWiseAccuracy;

    @FindBy(css = ".subjectwise-time-spent-chart .highcharts-axis-title")
    private WebElement yAxisLabelOfSubjectWiseTimeSpent;

    @FindBy(css = ".subject-wise-accuracy-chart .highcharts-axis-title")
    private WebElement yAxisLabelofSubjectWiseAccuracy;

    @FindBy(css = ".subjectwise-time-spent-wrapper .widget-sub-heading")
    private WebElement subjectWiseTimeSpentWidgetSubHeading;

    @FindBy(css = ".subject-wise-accuracy-wrapper .widget-sub-heading")
    private WebElement subjectWiseAccuracyWidgetSubHeading;

    public SubjectWiseAnalysisSection() {
        softAssert = new SoftAssert();
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        questionsHomePage = new QuestionsHomePage();

    }
    public void verifyThatSubjectWiseAnalysisSectionIsDisplayed() {

        softAssert.assertTrue(subjectWiseAnalysisHeader.isDisplayed(),"Subject Wise Analysis heading is not displayed");
        softAssert.assertTrue(subjectWiseTimeSpent.isDisplayed(),"Under subject wise analysis subject wise time spent is not shown");
        softAssert.assertTrue(subjectWiseAccuracy.isDisplayed(),"Under subject wise analysis subject wise accuracy is not shown");
        softAssert.assertAll();
    }

    public void hoverOnSubjectWiseTimeSpentBarGraph(WebDriver driver) {

        waitForPageToLoad();
        wait(2000);
        System.out.println("Size:"+subjectWiseTimeSpentGraphs.size());
        System.out.println();
        for (WebElement subjectWiseBar:
             subjectWiseTimeSpentGraphs) {
            mouseHoverJScript(driver, subjectWiseBar);
            wait(2000);
            String tooltipText = driver.findElement(By.cssSelector(".highcharts-tooltip.highcharts-color-undefined > span")).getText();
            System.out.println("ToolTip" + tooltipText);
            wait(5000);
        }

    }
    public void hoverOnSubjectWiseAccuracyGraph(WebDriver driver) {
        waitForPageToLoad();
        wait(2000);
        System.out.println("Size:"+ subjectWiseAccuracyGraph.size());
        mouseHoverJScript(driver,subjectWiseAccuracyGraph.get(1));
//        System.out.println(driver.findElement(By.cssSelector(".highcharts-label.highcharts-tooltip.highcharts-color-0 > span")).getText());
        for (WebElement subjectWiseBar: subjectWiseAccuracyGraph) {
            mouseHoverJScript(driver, subjectWiseBar);
            String tooltipText = driver.findElement(By.cssSelector(".highcharts-label.highcharts-tooltip.highcharts-color-0 > span")).getText();
            System.out.println("ToolTip" + tooltipText);
            wait(5000);
        }
    }

    public void hoverOnSubjectWiseTimeSpentInfoIcon(WebDriver driver) {
        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver,subjectWiseTimeSpentInfoIcon);
        String tooltipText = subjectWiseTimeSpentInfoIconText.getText();
        System.out.println("ToolTip" + tooltipText);
        Assert.assertEquals(tooltipText,"Subject-wise analysis of time spent on questions","Subject Wise TimeSpent Info Icon is not matching");
        wait(5000);
    }

    public void hoverOnIdealTimeIcon(WebDriver driver) {
        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver,subjectWiseTimeSpentHeaderDefinitionsIcon.get(0));
        String tooltipText = idealTimeDefinitonText.getText();
        System.out.println("Ideal Time Definition Text: " + tooltipText);
        Assert.assertEquals(tooltipText,"Time defined by experts to answer all questions","Ideal Time Definition is not shown correct in subject-wise time spent graph");
        wait(5000);
    }

    public void hoverOnAttemptedTimeIcon(WebDriver driver){

        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver,subjectWiseTimeSpentHeaderDefinitionsIcon.get(1));
        String tooltipText = attemptedTimeAndUnAttemptedTimeDefinitionText.get(0).getText();
        System.out.println("Attempted Time Definition Text: " + tooltipText);
        Assert.assertEquals(tooltipText,"Actual time you spent on answering questions","Attempted Time Definition is not shown correct in subject-wise time spent graph");
        wait(5000);
    }

    public void hoverOnUnAttemptedTimeIcon(WebDriver driver) {

        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver,subjectWiseTimeSpentHeaderDefinitionsIcon.get(2));
        String tooltipText = attemptedTimeAndUnAttemptedTimeDefinitionText.get(1).getText();
        System.out.println("UnAttempted Time Definition Text: " + tooltipText);
        Assert.assertEquals(tooltipText,"Time spent on questions which you did not attempt","UnAttempted Time Definition is not shown correct in subject-wise time spent graph");
        wait(5000);

    }

    public void hoverOnSubjectAccuracyInfoIcon(WebDriver driver) {
        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver,subjectWiseAccuracyInfoIcon);
        String tooltipText = subjectWiseAccuracyInfoIconText.getText();
        System.out.println("ToolTip" + tooltipText);
        Assert.assertEquals(tooltipText,"Subject-wise analysis of correct attempts vs all attempts","Subject Wise Accuracy Info Icon Text is not matching");
        wait(5000);
    }

//    public void verifyXAxisLabelOfSubjectWiseTimeSpent(List<String> subjectNames) {
//
//        for (int i = 0; i < subjectNames.size(); i++) {
//            System.out.println(xAxisLabelOfSubjectWiseTimeSpent.get(i).getText());
//            System.out.println("Subject Name:"+ subjectNames.get(i));
//            Assert.assertEquals(xAxisLabelOfSubjectWiseTimeSpent.get(i).getText().toLowerCase(), subjectNames.get(i).toLowerCase(), "x-axis labels are not matching with the section names");
////            Assert.assertTrue(xAxisLabelOfSubjectWiseTimeSpent.get(i).getText().toLowerCase().contains(subjectNames.get(i).toLowerCase()));
//
//        }
//
//    }

    public void verifyYAxisLabelOfSubjectWiseTimeSpent() {
        Assert.assertEquals(yAxisLabelOfSubjectWiseTimeSpent.getText(),"Time (in minutes)","The Y-axis label of subject-wise time spent graph is incorrect");
    }

//    public void verifyXAxisLabelOfSubjectWiseAccuracy(List<String> subjectNames) {
//
//        for (int i = 0; i < subjectNames.size(); i++) {
//
////            Assert.assertEquals(xAxisLabelOfSubjectWiseAccuracy.get(i).getText().toLowerCase(), subjectNames.get(i).toLowerCase(), "x-axis labels are not matching with the section names in subject wise accuracy");
////            Assert.assertTrue(xAxisLabelOfSubjectWiseTimeSpent.get(i).getText().contains(subjectNames.get(i)));
//
//
//        }
//
//    }

    public void verifyYAxisLabelOfSubjectWiseAccuracy() {
        Assert.assertEquals(yAxisLabelofSubjectWiseAccuracy.getText(),"Accuracy (in percent)","The Y-axis label of subject-wise time Accuracy graph is incorrect");
    }


    public void verifyTheHeaderInSubjectWiseTimeSpent() {

        List<String> headers = new ArrayList<>();
        headers.add("Ideal time");
        headers.add("Attempted time");
        headers.add("Unattempted time");
        for (int i = 0; i < subjectWiseTimeSpentHeaderDefinitionsIcon.size(); i++) {
            System.out.println(subjectWiseTimeSpentHeaderDefinitionsIcon.get(i).getText());
            Assert.assertEquals(subjectWiseTimeSpentHeaderDefinitionsIcon.get(i).getText(),headers.get(i), String.format("The headers should be shown as %s but found to be %s",headers.get(i),subjectWiseTimeSpentHeaderDefinitionsIcon.get(i).getText()));
        }
    }

    public void verifyTheSubHeadingOfTheSubjectWiseTimeSpentWidget() {
        Assert.assertEquals(subjectWiseTimeSpentWidgetSubHeading.getText(),"SUBJECT-WISE TIME SPENT", String.format("subject-wise time spent sub heading has found to be %s", subjectWiseTimeSpentWidgetSubHeading));
    }

    public void verifyTheSubHeadingOfTheSubjectWiseAccuracyWidget() {
        Assert.assertEquals(subjectWiseAccuracyWidgetSubHeading.getText(),"SUBJECT-WISE ACCURACY", String.format("subject-wise Accuracy sub heading has found to be %s", subjectWiseAccuracyWidgetSubHeading));
    }
}
