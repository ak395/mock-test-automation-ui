package page.examSummary.overallPerformanceSection;

import driver.DriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.List;

public class OverallTimeAndAccuracyAnalysisSection extends BasePage {

    @FindBy(css = ".overall-performance-widgets-chart-wrapper .widget-name")
    private List<WebElement> heading;

    @FindBy(css = ".attempted-time-spent-wrapper")
    private WebElement attemptedTimeSpentWidget;

    @FindBy(css = ".accuracy-with-difficulty-chart-wrapper")
    private WebElement accuracyWithDifficultyWidget;

    @FindBy(css = ".attempted-time-spent-wrapper .widget-sub-heading")
    private WebElement attemptedTimeSpentWidgetHeading;

    @FindBy(css = ".accuracy-with-difficulty-chart-wrapper .widget-sub-heading")
    private WebElement accuracyWithDifficultyWidgetHeading;

    @FindBy(css = ".accuracy-with-difficulty-chart-wrapper .highcharts-xaxis")
    private WebElement xAxisLabelOfAccuracyWithDifficulty;

    @FindBy(css = ".attempted-time-spent-wrapper .highcharts-xaxis tspan")
    private WebElement xAxisLabelOfAttemptedTimeSpent;

    @FindBy(css = ".attempted-time-spent-wrapper .highcharts-yaxis tspan")
    private WebElement yAxisLabelOfAttemptedTimeSpent;

    @FindBy(css = ".accuracy-with-difficulty-chart .highcharts-yaxis tspan")
    private WebElement yAxisLabelOfAccuracyWithDifficulty;

    @FindBy(css = ".attempted-time-spent-wrapper .toolTip-wrapper .chart-info-icon")
    private WebElement attemptedTimeSpentInfoIcon;

    @FindBy(css = ".attempted-time-spent-wrapper .bottomLeft .toolTip-text")
    private WebElement attemptedTimeSpentInfoIconText;

    @FindBy(css = ".accuracy-with-difficulty-chart-wrapper .toolTip-wrapper .chart-info-icon")
    private WebElement accuracyWithDifficultyInfoIcon;

    @FindBy(css = ".accuracy-with-difficulty-chart-wrapper .bottomLeft .toolTip-text")
    private WebElement accuracyWithDifficultyInfoIconText;

    @FindBy(css = ".attempted-time-spent-wrapper   .chart-legend-wrapper .chart-legend")
    private List<WebElement> attemptedTimeSpentSubHeading;

    @FindBy(css = ".attempted-time-spent-wrapper  .toolTip.bottom")
    private List<WebElement> attemptedTimeSpentSubHeadingTooltipText;

    @FindBy(css = ".accuracy-with-difficulty-chart-wrapper  .toolTip.bottom")
    private List<WebElement> accuracyWithDifficultySubHeadingToolTipText;

    @FindBy(css = ".accuracy-with-difficulty-chart-wrapper  .chart-legend-wrapper .chart-legend")
    private List<WebElement> accuracyWithDifficultySubHeading;

    @FindBy(css = ".attempted-time-spent-wrapper .highcharts-series-0 .highcharts-point:nth-child(1)")
    private WebElement toolTipDataForEasyQuestions;

    @FindBy(css = ".attempted-time-spent-wrapper .highcharts-series-0 .highcharts-point:nth-child(2)")
    private WebElement toolTipDataForMediumQuestions;

    @FindBy(css = ".attempted-time-spent-wrapper .highcharts-series-0 .highcharts-point:nth-child(3)")
    private WebElement toolTipDataForHardQuestions;

    @FindBy(css = ".attempted-time-spent-wrapper .highcharts-label.highcharts-tooltip.highcharts-color-undefined > span")
    private WebElement toolTipText;


    public OverallTimeAndAccuracyAnalysisSection() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver,this);
    }

    protected void verifyTheHeading(){
        Assert.assertEquals(heading.get(2).getText(),"Overall Time And Accuracy Analysis","The Overall Time and Accuracy Section Heading is not Correct");

    }

    protected void verifyTheSubWidgets() {
        Assert.assertTrue(attemptedTimeSpentWidget.isDisplayed(),"Attempted Time Spent Widget is not displayed in Overall Time and Accuracy Section");
        Assert.assertTrue(accuracyWithDifficultyWidget.isDisplayed(),"Attempted Time Spent Widget is not displayed in Overall Time and Accuracy Section");
    }

    protected void verifyTheHeadingOfSubWidgets() {
        Assert.assertEquals(attemptedTimeSpentWidgetHeading.getText(),"ATTEMPTED TIME SPENT", String.format("The Heading displayed is not correct: %s",attemptedTimeSpentWidgetHeading.getText()));
        Assert.assertEquals(accuracyWithDifficultyWidgetHeading.getText(),"ACCURACY WITH DIFFICULTY",String.format("The Heading displayed is not correct: %s", accuracyWithDifficultyWidgetHeading.getText()));
    }

    protected void verifyXAxisDataOfAttemptedTimeSpent() {
        Assert.assertEquals(xAxisLabelOfAttemptedTimeSpent.getText(),"Question Difficulty Level","X-Axis Label of Attempted Time Spent is not correct");
    }

    protected void verifyXAxisDataOfAccuracyWithDifficulty() {
        Assert.assertEquals(xAxisLabelOfAccuracyWithDifficulty.getText(),"Question Difficulty Level","X-Axis Label of Accuracy with Difficulty is not Correct");

    }

    protected void verifyYAxisDataOfAttemptedTimeSpent() {
        Assert.assertEquals(yAxisLabelOfAttemptedTimeSpent.getText(),"Time (in minutes)","Y-Axis Label of Attempted Time Spent is not correct");
    }

    protected void verifyYAxisDataOfAccuracyWithDifficulty() {
        Assert.assertEquals(yAxisLabelOfAccuracyWithDifficulty.getText(),"Accuracy (in percent)","Y-Axis Label of Accuracy with Difficulty is not Correct");
    }

    protected void verifyIdealTimeToolTip(WebDriver driver) {
        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver,attemptedTimeSpentSubHeading.get(0));
        String tooltipText = attemptedTimeSpentSubHeadingTooltipText.get(0).getText();
        System.out.println("Ideal Time Definition Text: " + tooltipText);
        Assert.assertEquals(tooltipText,"Time defined by experts to attempt","Ideal Time Definition is not shown correct in subject-wise time spent graph");
        wait(5000);

    }

    protected void verifyAttemptedTimeTooltip(WebDriver driver) {
        {
            waitForPageToLoad();
            wait(2000);
            mouseHoverJScript(driver,attemptedTimeSpentSubHeading.get(1));
            String tooltipText = attemptedTimeSpentSubHeadingTooltipText.get(1).getText();
            System.out.println("Attempted Time Definition Text: " + tooltipText);
            Assert.assertEquals(tooltipText,"Time spent on attempting questions","Attempted Time Definition is not shown correct in subject-wise time spent graph");
            wait(5000);
        }

    }

    protected void verifyAttemptedTimeSpentInfoIconToolTip(WebDriver driver) {

        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver,attemptedTimeSpentInfoIcon);
        String tooltipText = attemptedTimeSpentInfoIconText.getText();
        System.out.println("ToolTip" + tooltipText);
        Assert.assertEquals(tooltipText,"Time spent on attempting questions by difficulty","Attempted Time Spent Info Icon Text is not matching");
        wait(5000);

    }
    protected void verifyAccuracyWithDifficultyInfoIconToolTip(WebDriver driver) {

        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver,accuracyWithDifficultyInfoIcon);
        String tooltipText = accuracyWithDifficultyInfoIconText.getText();
        System.out.println("ToolTip" + tooltipText);
        Assert.assertEquals(tooltipText,"Difficulty-wise analysis of your correct attempts in comparison to other test-takers","Accuracy with Difficulty Info Icon Text is not matching");
        wait(5000);

    }

    public String hoverOnEasyBarGraph(WebDriver driver) {
        String easyAttemptedTimeSpentData;
        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver, toolTipDataForEasyQuestions);
        if (driver.findElements(By.cssSelector(".attempted-time-spent-wrapper .highcharts-label.highcharts-tooltip.highcharts-color-undefined > span")).size() !=0){
            easyAttemptedTimeSpentData = driver.findElement(By.cssSelector(".attempted-time-spent-wrapper .highcharts-label.highcharts-tooltip.highcharts-color-undefined > span")).getText();
            System.out.println("The Attempted Time Spent Tool Tip Data: " + easyAttemptedTimeSpentData);
            return easyAttemptedTimeSpentData;
        }

        else{
            System.out.println(" No Element Found");
            return null;
        }

    }




    public String hoverOnMediumBarGraph(WebDriver driver) {
        String mediumAttemptedTimeSpentData;
        waitForPageToLoad();
        wait(2000);
        mouseHoverJScript(driver, toolTipDataForMediumQuestions);
        if (driver.findElements(By.cssSelector(".attempted-time-spent-wrapper .highcharts-label.highcharts-tooltip.highcharts-color-undefined > span")).size() !=0){
            mediumAttemptedTimeSpentData = driver.findElement(By.cssSelector(".attempted-time-spent-wrapper .highcharts-label.highcharts-tooltip.highcharts-color-undefined > span")).getText();
            System.out.println("The Attempted Time Spent Tool Tip Data: " + mediumAttemptedTimeSpentData);
            return mediumAttemptedTimeSpentData;
        }

        else{
            System.out.println(" No Element Found");
            return null;
        }

    }

    public String hoverOnHardBarGraph(WebDriver driver) {
        String hardAttemptedTimeSpentData;
        waitForPageToLoad();
        wait(2000);

        mouseHoverJScript(driver, toolTipDataForHardQuestions);
        if (driver.findElements(By.cssSelector(".attempted-time-spent-wrapper .highcharts-label.highcharts-tooltip.highcharts-color-undefined > span")).size() !=0){
            hardAttemptedTimeSpentData = driver.findElement(By.cssSelector(".attempted-time-spent-wrapper .highcharts-label.highcharts-tooltip.highcharts-color-undefined > span")).getText();
            System.out.println("The Attempted Time Spent Tool Tip Data: " + hardAttemptedTimeSpentData);
            return hardAttemptedTimeSpentData;
        }

        else{
            System.out.println(" No Element Found");
            return null;
        }
    }


}
