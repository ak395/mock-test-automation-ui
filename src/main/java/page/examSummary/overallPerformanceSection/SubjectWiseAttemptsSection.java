package page.examSummary.overallPerformanceSection;

import driver.DriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import page.examSummary.ExamSummaryPage;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubjectWiseAttemptsSection extends BasePage {


    @FindBy(css = ".subject-wise-attempts .widget-name")
    private WebElement subjectWiseAttemptsHeading;

    @FindBy(css = ".subjectwise-attempt-wrapper .widget-sub-heading")
    private List<WebElement> subjectWiseAttemptsWidget;

    @FindBy(css = ".subjectwise-attempt-wrapper .highcharts-axis-title")
    private List<WebElement> yAxisLabel;

    @FindBy(css = ".subjectwise-attempt-wrapper .highcharts-xaxis-labels ")
    private List<WebElement> xAxisLabel;

    @FindBy(css = ".swiper-slide:nth-child(3) .subjectwise-attempt-chart .highcharts-series-0 > path")
    private List<WebElement> tooltipData;

    public SubjectWiseAttemptsSection() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver,this);
    }

    public void verifyTheSubjectWiseAttemptsHeading(){
        Assert.assertTrue(subjectWiseAttemptsHeading.isDisplayed(),"Subject Wise Attempts heading is not displayed");
        Assert.assertEquals(subjectWiseAttemptsHeading.getText(),"Subject-Wise Attempts",String.format("Subject- Wise Heading should be:Subject-Wise Attempts but shown as %s",subjectWiseAttemptsHeading.getText()));
    }

    public void verifyNumberAndNameOfSubjectWiseAttemptsWidget(int numberOfSections,List<String> subjectNames) {

        Assert.assertEquals(subjectWiseAttemptsWidget.size(),numberOfSections,String.format("The no. of subject-wise attempts widgets should be: %d  but found to be: %d",numberOfSections,subjectWiseAttemptsWidget.size()));

        for (int i = 0; i < subjectNames.size(); i++) {
                Assert.assertEquals(subjectWiseAttemptsWidget.get(i).getText(), subjectNames.get(i), String.format("Subject name should be: %s but found to be: %s",subjectNames.get(i),subjectWiseAttemptsWidget.get(i)));
            }

    }

    public void verifyYAxisLabel() {
        for(int i=0; i<subjectWiseAttemptsWidget.size(); i++) {
            Assert.assertEquals(yAxisLabel.get(i).getText(),"Questions Attempted", "The Data on Y-axis in Subject Wise Attempts is not Correct");
        }
    }

    public void verifyXAxisLabel() {
        List<String> xAxisLabels = new ArrayList<>();
        xAxisLabels.add("Correct");
        xAxisLabels.add("Incorrect");
        xAxisLabels.add("Unattempted");
        String labels[] = xAxisLabel.get(0).getText().split("\\r?\\n");
        for(int i = 0; i<xAxisLabels.size();i++) {

        Assert.assertEquals(labels[i],xAxisLabels.get(i),"X-Axis Labels in Subject Wise Attempts in overall Performace Section is not appearing correct");
        }

    }

    public void verifyTheDataForCorrectQuestions(WebDriver driver, List<Integer> correctQuestions, List<Integer> incorrectQuestions) {
        waitForPageToLoad();
        wait(2000);
        System.out.println("Size:" + tooltipData.size());
        for (int i = 1; i<=3; i++) {

            refreshPage();
            new ExamSummaryPage().clickOnOverallPerformanceSection();
            wait(3000);
            List<WebElement> elements = driver.findElements(By.cssSelector(".swiper-slide:nth-child("+i+") .subjectwise-attempt-chart .highcharts-series-0 > path"));
            System.out.println("Elements Size" + elements.size());

                String tooltipTxt = "";
                new Actions(driver).moveToElement(elements.get(0)).clickAndHold().build().perform();
                wait(3000);
                if (driver.findElements(By.cssSelector(".highcharts-label.highcharts-tooltip.highcharts-color-0 > span")).size() !=0) {
                    tooltipTxt = driver.findElement(By.cssSelector(".highcharts-label.highcharts-tooltip.highcharts-color-0 > span")).getText();
                    System.out.println("ToolTip" + tooltipTxt);
                    Pattern patternNoOfQuestions = Pattern.compile("Questions:(.*?)$");
                    Matcher noOfQuestions = patternNoOfQuestions.matcher(tooltipTxt);
                    System.out.println("Expected Number of Correct Questions: " + correctQuestions.get(i - 1));
                    if (noOfQuestions.find()) {
                        System.out.println("Actual Number of Correct Questions: " + noOfQuestions.group(1));
                        Assert.assertEquals(String.valueOf(correctQuestions.get(i - 1)), noOfQuestions.group(1).trim(), "The number of correct questions is not working");
                    }

                }

                else {
                    System.out.println("No Bar Graph is Visible");
                }
        }
    }

    public void verifyTheDataForIncorrectQuestions(WebDriver driver, List<Integer> inCorrectQuestions) {

        waitForPageToLoad();
        wait(2000);
        System.out.println("Size:" + tooltipData.size());
        for (int i = 1; i<=3; i++) {

            refreshPage();
            new ExamSummaryPage().clickOnOverallPerformanceSection();
            wait(3000);
            List<WebElement> elements = driver.findElements(By.cssSelector(".swiper-slide:nth-child("+i+") .subjectwise-attempt-chart .highcharts-series-0 > path"));
            System.out.println("Elements Size" + elements.size());

            String tooltipTxt = "";
            new Actions(driver).moveToElement(elements.get(1)).clickAndHold().build().perform();
            wait(3000);
            if (driver.findElements(By.cssSelector(".highcharts-label.highcharts-tooltip.highcharts-color-0 > span")).size() !=0) {
                tooltipTxt = driver.findElement(By.cssSelector(".highcharts-label.highcharts-tooltip.highcharts-color-0 > span")).getText();
                System.out.println("ToolTip" + tooltipTxt);
                Pattern patternNoOfQuestions = Pattern.compile("Questions:(.*?)$");
                Matcher noOfQuestions = patternNoOfQuestions.matcher(tooltipTxt);
                System.out.println("Expected Number of InCorrect Questions: " + inCorrectQuestions.get(i - 1));
                if (noOfQuestions.find()) {
                    System.out.println("Actual Number of InCorrect Questions: " + noOfQuestions.group(1));
                    Assert.assertEquals(String.valueOf(inCorrectQuestions.get(i - 1)), noOfQuestions.group(1).trim(), "The number of correct questions is not working");
                }
            }
            else {
                System.out.println("No Bar Graph is Visible");
            }
        }

    }

    public void verifyTheDataForUnAttemptedQuestions(WebDriver driver, List<Integer> unAttemptedQuestions) {
        waitForPageToLoad();
        wait(2000);
        System.out.println("Size:" + tooltipData.size());
        for (int i = 1; i<=3; i++) {

            refreshPage();
            new ExamSummaryPage().clickOnOverallPerformanceSection();
            wait(3000);
            List<WebElement> elements = driver.findElements(By.cssSelector(".swiper-slide:nth-child("+i+") .subjectwise-attempt-chart .highcharts-series-0 > path"));
            System.out.println("Elements Size" + elements.size());

            String tooltipTxt = "";
            new Actions(driver).moveToElement(elements.get(2)).clickAndHold().build().perform();
            wait(3000);
            if (driver.findElements(By.cssSelector(".highcharts-label.highcharts-tooltip.highcharts-color-0 > span")).size() !=0) {
                tooltipTxt = driver.findElement(By.cssSelector(".highcharts-label.highcharts-tooltip.highcharts-color-0 > span")).getText();
                System.out.println("ToolTip" + tooltipTxt);
                Pattern patternNoOfQuestions = Pattern.compile("Questions:(.*?)$");
                Matcher noOfQuestions = patternNoOfQuestions.matcher(tooltipTxt);
                System.out.println("Expected Number of UnAttempted Questions: " + unAttemptedQuestions.get(i - 1));
                if (noOfQuestions.find()) {
                    System.out.println("Actual Number of UnAttempted Questions: " + noOfQuestions.group(1));
                    Assert.assertEquals(String.valueOf(unAttemptedQuestions.get(i - 1)), noOfQuestions.group(1).trim(), "The number of correct questions is not working");
                }
            }

            else {
                System.out.println("No Bar Graph is Visible");
            }
        }
    }


}



