package page.rankUpPage;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.rankUpPage.rankUpHeader.RankUpHeaderSection;
import page.rankUpPage.rankUpTabs.RankUpTabSection;
import page.rankUpPage.rankUpVideoSection.RankUpVideoSection;


@Getter
public class RankUpHomePage extends BasePage {

        private RankUpHeaderSection rankUpHeaderSection;
        private RankUpTabSection rankUpTabSection;
        private RankUpVideoSection rankUpVideoSection;
        public RankUpHomePage() {

            driver = DriverProvider.getDriver();
            PageFactory.initElements(driver, this);

            rankUpHeaderSection = new RankUpHeaderSection(driver);
            rankUpTabSection = new RankUpTabSection(driver);
            rankUpVideoSection = new RankUpVideoSection(driver);


        }

}
