package page.rankUpPage.rankUpHeader;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import utils.Properties;
import verification.Verify;
import java.util.Optional;

public class RankUpHeaderSection extends BasePage {

    private RankUpHeaderLocators locators;

    @FindBy(className = "rank-up-header")
    private WebElement rootElement;

    public RankUpHeaderSection(WebDriver driver) {
        this.driver = driver;
        locators = new RankUpHeaderLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getApplyToRankUpElement() {
       return getElement(rootElement,locators.applyToRankUp);
    }

    public WebElement getLoginElement() {
        return getElement(rootElement,locators.login);
    }



    @Step("User is clicking on Apply to Rank Up Button")
    public void applyToRankUp() {
        waitForPageToLoad();
        getApplyToRankUpElement().click();
        switchToActiveTab();

    }

    @Step("User is clicking on Login button")
    public void clickOnLogin() {
        waitForPageToLoad();
        getLoginElement().click();
        waitForPageToLoad();
        assertUrlHasSetTo(Properties.rankUpLoginUrl);
    }

    @Step("User sees URL to be : {url}")
    public void assertUrlHasSetTo(String url) {
        wait(5000);
        Verify.assertEquals(driver.getCurrentUrl(),url,"url not matching", Optional.of(true));
    }


}
