package page.rankUpPage.rankUpHeader;

import org.openqa.selenium.By;

public class RankUpHeaderLocators {

    By applyToRankUp = By.className("rank-up-header--text-btn");
    By login = By.xpath("//p[@class= 'rank-up-header--text-already-applied-label']//span[contains(text(),'LOGIN')]");

}
