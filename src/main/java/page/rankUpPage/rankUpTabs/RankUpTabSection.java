package page.rankUpPage.rankUpTabs;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;
import java.util.Optional;

public class RankUpTabSection extends BasePage {

    private RankUpTabLocators locators;

    @FindBy(className = "rank-up")
    private WebElement rootElement;

    public RankUpTabSection(WebDriver driver){
        this.driver =  driver;
        locators = new RankUpTabLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getTab1Element() {
        return getElement(rootElement, locators.tab1);
    }

    public WebElement getTab2Element() {
        return getElement(rootElement, locators.tab2);
    }
    public WebElement getTab3Element() {
        return getElement(rootElement, locators.tab3);
    }

    public WebElement getTab4Element() {
        return getElement(rootElement, locators.tab4);
    }

    public WebElement getTab3AssertionElement() {
        return getElement(rootElement,locators.tab3Assertion);
    }

    public WebElement getTab1AssertionElement() {
        return getElement(rootElement,locators.tab1Assertion);
    }

    public WebElement getTab2AssertionElement() {
        return getElement(rootElement,locators.tab2Assertion);
    }

    public WebElement getTab4AssertionElement() {
        return getElement(rootElement,locators.tab4Assertion);
    }

    @Step
    public void clickOnTab1() {
        waitForPageToLoad();
        getTab1Element().click();
        Verify.assertTrue(getTab1AssertionElement().isDisplayed(),"Not Displayed", Optional.of(true));
    }

    public void clickOnTab2() {
        waitForPageToLoad();
        getTab2Element().click();
        Verify.assertTrue(getTab2AssertionElement().isDisplayed(),"Not Displayed", Optional.of(true));
    }

    public void clickOnTab3() {
        waitForPageToLoad();
        getTab3Element().click();
        Verify.assertTrue(getTab3AssertionElement().isDisplayed(),"Not Displayed", Optional.of(true));
    }

    public void clickOnTab4() {
        waitForPageToLoad();
        getTab4Element().click();
        Verify.assertTrue(getTab4AssertionElement().isDisplayed(),"Not Displayed", Optional.of(true));
    }

}
