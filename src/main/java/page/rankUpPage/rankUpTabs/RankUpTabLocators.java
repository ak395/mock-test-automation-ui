package page.rankUpPage.rankUpTabs;

import org.openqa.selenium.By;

public class RankUpTabLocators {

    By tab1 = By.cssSelector("[id='tabTitle0']");
    By tab2 = By.cssSelector("[id='tabTitle1']");
    By tab3 = By.cssSelector("[id='tabTitle2']");
    By tab4 = By.cssSelector("[id='tabTitle3']");
    By tab1Assertion = By.xpath("//h3[contains(text(),'61% of your score depends on knowledge')]");
    By tab2Assertion = By.xpath("//h3[contains(text(),'39% of your score depends on test-taking strategy')]");
    By tab3Assertion = By.xpath("//h3[contains(text(),'College prediction linked to improvement plan')]");
    By tab4Assertion = By.xpath("//h3[contains(text(),'Track your effort rating for higher score')]");

}
