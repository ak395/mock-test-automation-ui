package page.rankUpPage.rankUpVideoSection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.JumpHomePage;
import verification.Verify;

import java.util.Optional;

public class RankUpVideoSection extends BasePage {

    private RankUpVideoSectionLocators locators;

    @FindBy(className = "second-section")
    private WebElement rootElement;

    public RankUpVideoSection(WebDriver driver){
        this.driver =  driver;
        locators = new RankUpVideoSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getGoToJumpButtonElement() {
        return getElement(rootElement,locators.goToJumpButton);
    }

    public void navigateToJumPage() {
        getGoToJumpButtonElement().click();
        waitForPageToLoad();
        JumpHomePage jumpHomePage = new JumpHomePage();
        Verify.assertTrue(jumpHomePage.getJumpPageSection().getjumpbuttonElement().isDisplayed(),"Not Displayed", Optional.of(true));

    }
}
