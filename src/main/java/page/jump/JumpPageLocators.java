package page.jump;

import org.openqa.selenium.By;
import page.BasePage;

public class JumpPageLocators extends BasePage
{

 By jumpbutton= By.cssSelector("button[class='jump-up-header--text-btn']");
 By header=By.cssSelector("div[class='jump-up--buy-packs__title ']");
 By tab0=By.cssSelector("#tabTitle0");
 By tab1=By.cssSelector("#tabTitle1");
 By tab2=By.cssSelector("#tabTitle2");
 By tab3=By.cssSelector("#tabTitle3");
 By readtheirstory=By.partialLinkText("READ THEIR STORY ");
 By learnmore=By.cssSelector("p[class*='right-col-part']");
 By explorerankup=By.cssSelector("div[class='try-rank-up-button-div']");
 By engineering = By.cssSelector("#packageChoosedImg1>img");
 By medical = By.className("#packageChoosedImg4>img");
 By class11 = By.className("#packageChoosedImg5>img");
 By class12 = By.className("#packageChoosedImg8>img");
 By class12plus = By.className("#packageChoosedImg10>img");
 By may2019 = By.className("#packageChoosedImg11>img");
 By may2020 = By.className("#packageChoosedImg14>img");

}
