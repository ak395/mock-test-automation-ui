package page.jump;

import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static org.testng.Assert.assertEquals;

@Getter
public class GetJumpPacksTitleSection extends BasePage {
    private JumpPageLocators locators;

    @FindBy(className = "jump-up--buy-packs")
    private WebElement rootElement;

    public  GetJumpPacksTitleSection(WebDriver driver)
    {
        this.driver = driver;
        locators = new JumpPageLocators();
        PageFactory.initElements(driver, this);
    }

    private WebElement getHeaderElement() {
        return getElement(rootElement, locators.header);
    }

    @Step("Verify user should be navigated to the Packs section when clicked on the Get Jump Button")
    public void assertHeaderToBe(String expectedHeader) {
        assertEquals(getHeaderElement().getText().trim(), expectedHeader);
    }
}
