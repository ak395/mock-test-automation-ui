package page.jump;

import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

@Getter
public class GetJumpSection extends BasePage {
    private JumpPageLocators locators;

    @FindBy(className = "jump-up")
    private WebElement rootElement;

    public GetJumpSection(WebDriver driver)
    {
        this.driver = driver;
        locators = new JumpPageLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getjumpbuttonElement() {
        return getElement(rootElement, locators.jumpbutton);

    }

    @Step("Clicking on Get Jump button")
    public void clickOnGetJumpButton() {
        getjumpbuttonElement().click();
        waitForPageToLoad();
    }

/*
    @Step("Verify user is able to click on the jump button")
    public void assertThatJumpButtonIsClicked(AJumpPage page)
    {
        WebElement jumpPageTextWebElement=getjumpPageTextElement();
        String message=jumpPageTextWebElement.getText();
        String successMsg="Get your unfair advantage. JUMP NOW.";
      //  Assert.assertEquals(message,successMsg);
        Verify.assertEquals(message,successMsg,"True");

    }
    public void assertHeaderToBe(String expectedHeader) {
        assertEquals(getHeaderElement().getText().trim(), expectedHeader);
    }
    */


}
