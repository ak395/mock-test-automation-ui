package page.Ask;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;
public class AsksTrendingConceptsPaneSection extends BasePage
{

    private AskPageLocator locators;
    @FindBy(className = "ask-trending-concepts-pane")
    private WebElement rootElement;

    public AsksTrendingConceptsPaneSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new AskPageLocator();
        PageFactory.initElements(driver,this);
    }

    @Step("User finds all the Hyperlinks present in the concepts section")
    public void findAllLinks()
    {
        driver.get("https://www.embibe.com/ask");
        driver.manage().window().maximize();
        driver.navigate().refresh();
        waitForPageToLoad();
        List<WebElement> allLinks=driver.findElements(By.xpath("//div[@class='ask-trending-concepts-pane']"));
        int linkSize=allLinks.size();
        System.out.print("Size of Links :"+linkSize);
        for(int i=0;i<allLinks.size();i++)
        {
            System.out.println(allLinks.get(i).getText());
            String links=allLinks.get(i).getText();

            if(links !=" ")
            {
                allLinks.get(i).click();
                waitForPageToLoad();
            }

        }
        for(int i=0;i<allLinks.size();i++){
            allLinks.get(i).click();
            driver.navigate().back();
        }

    }



}
