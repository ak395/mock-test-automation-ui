package page.Ask;

import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

@Getter
public class TakeATourWrapperSection extends BasePage
{
    private AskPageLocator locators;
    @FindBy(className = "takeTourWrapper")
    private WebElement rootElement;

    public TakeATourWrapperSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new AskPageLocator();
        PageFactory.initElements(driver,this);
    }

    public WebElement getExploreStudyButtonElement()
    {
        return getElement(rootElement,locators.exploreStudyButton);
    }

    @Step("User clicks on the Explore Study Button")
    public void clickOnExploreStudyButton()
    {
        waitForPageToLoad();
        getExploreStudyButtonElement().click();
        waitForPageToLoad();
    }

    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }

}
