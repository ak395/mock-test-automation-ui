package page.Ask;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

import static org.testng.Assert.assertEquals;

public class AskPAgeSearchContainerSection extends BasePage
{
    private AskPageLocator locators;
    @FindBy(className = "ask-search-container")
    private WebElement rootElement;

    public AskPAgeSearchContainerSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new AskPageLocator();
        PageFactory.initElements(driver,this);
    }

    public WebElement getAskPageTitleElement()
    {
        return getElement(rootElement,locators.askPageTitle);
    }

    public WebElement getAskPageSearchTextFieldElement()
    {
        return getElement(rootElement,locators.searchTextField);
    }

    public WebElement getSearchButtonElement()
    {
        return  getElement(rootElement,locators.searchButton);
    }
    @Step("User Verifies the Title of the ASK Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        assertEquals(getAskPageTitleElement().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }

    @Step("User clicks on the  Search Field")
    public void clickOnSearchField()
    {

        waitForPageToLoad();
        getAskPageSearchTextFieldElement().click();
        waitForPageToLoad();
    }

    @Step("User verifies whether search field is displayed or not")
    public void searchFieldDisplayed()
    {
        waitForPageToLoad();
        Assert.assertTrue(getAskPageSearchTextFieldElement().isDisplayed(),"Search field should be displayed");
    }

   @Step("Search Button should be Clickable")
   public void clickOnSearchButton()
   {
       waitForPageToLoad();
       getSearchButtonElement().click();
       waitForPageToLoad();
   }

    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }




}
