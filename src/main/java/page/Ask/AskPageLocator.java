package page.Ask;

import org.openqa.selenium.By;
import page.BasePage;

public class AskPageLocator extends BasePage
{
    By askPageTitle = By.cssSelector("div[class='ask-heading']");
    By searchTextField = By.xpath("//input[@placeholder='Ask an academic question']");
    By searchButton = By.xpath("//span[@class='searchicon']");
    By exploreStudyButton = By.linkText("Explore Study");
    By facebookLink = By.cssSelector("a[class='sidebar-social-facebook']");
    By twitterLink = By.xpath("//a[@class='sidebar-social-twitter']");
    By socialMediumLink=By.xpath("//a[@class='sidebar-social-medium']");

}