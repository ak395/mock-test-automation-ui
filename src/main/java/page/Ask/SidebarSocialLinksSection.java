package page.Ask;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

public class SidebarSocialLinksSection extends BasePage {
    private AskPageLocator locators;
    @FindBy(className = "sidebar-social-links")
    private WebElement rootElement;

    public SidebarSocialLinksSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new AskPageLocator();
        PageFactory.initElements(driver,this);
    }

    public WebElement getFacebookLinkElement()
    {
        return getElement(rootElement,locators.facebookLink);
    }

    public WebElement getTwitterLinkElement()
    {
        return getElement(rootElement,locators.twitterLink);
    }

    public WebElement getSocialMediumLink()
    {
        return getElement(rootElement,locators.socialMediumLink);
    }

    @Step("User clicks on the Facebook Social Link")
    public void clickOnFacebookIcon()
    {
        waitForPageToLoad();
        getFacebookLinkElement().click();
        waitForPageToLoad();
    }
    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }

    @Step("User clicks on the Twitter Social Link")
    public void clickOnTwitterLink()
    {
        waitForPageToLoad();
        getTwitterLinkElement().click();
        waitForPageToLoad();
    }

    @Step("User clicks on the Social Medium Link")
    public void clickOnSocialMediumLink()
    {
        waitForPageToLoad();
        getSocialMediumLink().click();
        waitForPageToLoad();

    }
}

