package page.rankUpSignUpPage.rankUpSignUpHeader;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

public class RankUpSignUpHeaderSection extends BasePage {

    private RankUpSignUpHeaderSectionLocators locators;

    @FindBy(className = "sign-up-txt")
    private WebElement rootElement;

    public RankUpSignUpHeaderSection(WebDriver driver) {
        this.driver = driver;
        locators = new RankUpSignUpHeaderSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getHeaderElement() {

        return getElement(rootElement,locators.headerText);
    }

    public void assertHeaderToBe(String headerText) {

        WebElement headerElement = getHeaderElement();
        String text = headerElement.getText();

        Verify.assertEquals(text,headerText,"String Matched", Optional.of(true));
    }
}
