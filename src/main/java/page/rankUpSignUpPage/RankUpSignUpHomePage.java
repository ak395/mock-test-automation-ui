package page.rankUpSignUpPage;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.rankUpSignUpPage.academicDetails.AcademicsDetailsSection;
import page.rankUpSignUpPage.rankUpSignUpHeader.RankUpSignUpHeaderSection;

@Getter
public class RankUpSignUpHomePage extends BasePage {

    private RankUpSignUpHeaderSection rankUpSignUpHeaderSection;
    private AcademicsDetailsSection academicsDetailsSection;


    public RankUpSignUpHomePage(){
        driver = DriverProvider.getDriver();

        PageFactory.initElements(driver, this);

        rankUpSignUpHeaderSection = new RankUpSignUpHeaderSection(driver);
        academicsDetailsSection = new AcademicsDetailsSection(driver);

    }
}
