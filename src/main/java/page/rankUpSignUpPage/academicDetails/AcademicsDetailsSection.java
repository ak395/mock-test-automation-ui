package page.rankUpSignUpPage.academicDetails;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import utils.Properties;
import verification.Verify;

import java.util.List;
import java.util.Optional;

public class AcademicsDetailsSection extends BasePage {

    private AcademicDetailsSectionLocators locators;

    @FindBy(className = "signup-box")
    private WebElement rootElement;

    public AcademicsDetailsSection(WebDriver driver) {
        this.driver = driver;
        locators = new AcademicDetailsSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getSelectStreamElement() {

        return getElement(rootElement,locators.selectStream);
    }

    public List <WebElement> getErrorClassElements() {

        return getElements(rootElement,locators.errorClass);
    }

    public WebElement getSelectExamElement() {

        return getElement(rootElement,locators.selectExam);
    }

    public WebElement getContinueButtonElement() {
        return getElement(rootElement,locators.continueButton);
    }

    public void checkValidation() {
        waitForPageToLoad();
        getContinueButtonElement().click();
        List<WebElement> elements = getErrorClassElements();
        Verify.assertEquals(elements.size(), Properties.rankUpValidationCount, "Count should not match", Optional.of(true));

    }
}
