package page.rankUpSignUpPage.academicDetails;

import org.openqa.selenium.By;

public class AcademicDetailsSectionLocators {

   By selectStream =  By.className("stream-btn");
   By selectExam = By.className("");
   By selectDreamCollege = By.className("dream-clg-btn");
   By selectInterestedBranch = By.className("");
   By errorClass = By.className("error-msg-form");
   By continueButton = By.className("start-btn");


}
