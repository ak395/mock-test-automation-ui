package page.Search;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

import static org.testng.Assert.assertEquals;

public class StartPracticeSection extends BasePage {
    private SearchPageLocators locators;

    @FindBy(className = "search-wrapper")
    private WebElement rootElement;

    @FindBy(css = ".practice-btn.btn")
    private WebElement startPracticeButton;

    public StartPracticeSection(WebDriver driver) {
        this.driver = driver;
        locators = new SearchPageLocators();
        PageFactory.initElements(driver, this);
    }

    public boolean isPracticeButtonVisible(){
        waitForElementToBeVisible(startPracticeButton);
        return startPracticeButton.isDisplayed();
    }

    public WebElement getSearchPracticeButtonElement() {
        return getElement(rootElement, locators.startPracticeButton);
    }

    public WebElement getStartPracticeHeader() {
        return getElement(rootElement, locators.startPracticeHeader);
    }

    @Step("User clicks on the Practice Button ")
    public void clickOnStartPracticeButton() {
        waitForPageToLoad();
        getSearchPracticeButtonElement().click();
        waitForPageToLoad();
    }

    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
        waitForPageToLoad();
    }

    @Step("User verifies the title of the Search Page")
    public void assertHeaderToBe(String expectedHeader) {
        waitForPageToLoad();
        assertEquals(getStartPracticeHeader().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }
}
