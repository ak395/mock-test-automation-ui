package page.Search;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

import static org.testng.Assert.assertEquals;

public class SearchPageTitleSection extends BasePage
{
    private SearchPageLocators locators;
    @FindBy(className = "search-wrapper")
    private WebElement rootElement;


    public SearchPageTitleSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new SearchPageLocators();
        PageFactory.initElements(driver,this);
    }
    public WebElement getSearchTitleElement()
    {
        WebElement element = getElement(rootElement, locators.searchTitle);
        return element;
    }
    @Step("User verifies the title of the Search Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        assertEquals(getSearchTitleElement().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }

    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }

}
