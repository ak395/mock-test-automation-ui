package page.Search;

import driver.DriverProvider;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

import static org.testng.Assert.assertEquals;

public class TakeATestSection extends BasePage {
    private SearchPageLocators locators;
    @FindBy(className = "search-wrapper")
    private WebElement rootElement;

    public TakeATestSection() {
        driver = DriverProvider.getDriver();
        locators = new SearchPageLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getTakeATestButton() {
        return getElement(rootElement, locators.takeATestButton);
    }

    public WebElement getTakeATestHeader() {
        return getElement(rootElement, locators.takeATestHeader);
    }

    @Step("User clicks on Take A Test Button")
    public void clickOnTakeATestButton() {
        waitForPageToLoad();
        getTakeATestButton().click();
        waitForPageToLoad();
    }

    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
        waitForPageToLoad();
    }

    @Step("User verifies the title of the Search Page")
    public void assertHeaderToBe(String expectedHeader) {
        waitForPageToLoad();
        assertEquals(getTakeATestHeader().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }
}
