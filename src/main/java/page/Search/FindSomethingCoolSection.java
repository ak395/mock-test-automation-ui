package page.Search;

import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;
@Getter
public class FindSomethingCoolSection extends BasePage
{
    private SearchPageLocators locators;
    @FindBy(className = "showlabel")
    private WebElement rootElement;

    public FindSomethingCoolSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new SearchPageLocators();
        PageFactory.initElements(driver,this);
    }
    public WebElement getFindSomethingCoolElement()
    {
        return getElement(rootElement,locators.findSomethingCoolLink);
    }
    @Step("User Scrolls to Knowledge Tree")
    public void scrollToFindSomethingCoolLink()
    {
        try
        {
            wait(5000);
        }
        catch (Exception e)
        {

            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", locators.findSomethingCoolLink);

        }
    }
    @Step("User clicks on Find Something Cool Link")
    public void clickOnFindSomethingCoolLink()
    {
        waitForPageToLoad();
        int x= getFindSomethingCoolElement().getLocation().getX();
        int y= getFindSomethingCoolElement().getLocation().getY();
        Actions actionOnSearchNowButton=new Actions(driver);
        actionOnSearchNowButton.moveToElement(getFindSomethingCoolElement(),x,y).build().perform();
       getFindSomethingCoolElement().click();
        waitForPageToLoad();

    }
    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url)
    {
        waitForPageToLoad();
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
        waitForPageToLoad();
    }
}
