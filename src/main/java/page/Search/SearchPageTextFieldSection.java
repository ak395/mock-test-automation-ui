package page.Search;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.UtilityClassess.LinkUtility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class SearchPageTextFieldSection extends BasePage {
    private SearchPageLocators locators;
    @FindBy(className = "search-wrapper")
    private WebElement rootElement;

    @FindBy(css = ".react-typeahead-usertext")
    private WebElement searchTextBox;

    public SearchPageTextFieldSection(WebDriver driver) {
        this.driver = driver;
        locators = new SearchPageLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchtTextFieldElement() {
        return getElement(rootElement, locators.searchTextField);
    }

    @Step("User clicks on the Search  Text Field")
    public void clickOnSearchTextField() {
        waitForPageToLoad();
        getSearchtTextFieldElement().click();
        waitForPageToLoad();
    }

    @Step("User enters value in the text field")
    public void enterAmbiguityKeyWord() throws IOException {
        waitForPageToLoad();
        Properties properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\DELL\\git\\master\\src\\main\\java\\Config.Properties");
        properties.load(fileInputStream);
        waitForPageToLoad();
        getSearchtTextFieldElement().sendKeys(properties.getProperty("Ambiguity"));
        waitForPageToLoad();
    }

    @Step("User enters a disambiguity Keyword in the text field")
    public void enterDisAmbiguityKeyWord() throws IOException {
        waitForPageToLoad();
        Properties properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\DELL\\git\\master\\src\\main\\java\\Config.Properties");
        properties.load(fileInputStream);
        waitForPageToLoad();
        getSearchtTextFieldElement().sendKeys(properties.getProperty("Disambiguity"));
        waitForPageToLoad();

    }

    @Step("User enters random ambiguity words")
    public void enterRandomKeywords() {
        waitForPageToLoad();
        String[] arr = {"A", "B", "C", "D"};
        Random random = new Random();

        //Randomly select an integer from the arr
        int select = random.nextInt(arr.length);

        System.out.println(arr[select]);
        getSearchtTextFieldElement().sendKeys(arr[select]);
        waitForPageToLoad();
        // find all elements which has href attribute & process one by one
        // get the value of href
        // trim the text
        // there could be duplicate links , so find unique
        // group the links based on the response code
        Map<Integer, List<String>> map = driver.findElements(By.xpath("//div[@class='search_results']//a[@href]"))
                .stream()                             // find all elements which has href attribute & process one by one
                .map(ele -> ele.getAttribute("href")) // get the value of href
                .map(String::trim)                    // trim the text
                .distinct()                           // there could be duplicate links , so find unique
                .collect(Collectors.groupingBy(LinkUtility::getResponseCode)); // group the links based on the response code

        map.get(200)
                .stream()
                .forEach(System.out::println);

        waitForPageToLoad();

        for (Map.Entry<Integer, List<String>> mp : map.entrySet()) {
            System.out.print(mp.getKey() + " | ");
            for (String url : mp.getValue()) {
                driver.get(url);
                // waitForPageToLoad();
            }
            System.out.println();
        }
    }

    @Step("User enters random disambiguity keywords")
    public void enterRandomDisambiguityKeywords() throws IOException {

        //local Variables:
        String homePage = "http://www.embibe.com";
        String url = "";
        HttpURLConnection huc = null;
        int respCode = 400;


        waitForPageToLoad();
        String[] arr = {"Heat", "Water", "Matrix", "Physics"};
        Random random = new Random();
        //Randomly select an integer from the arr
        int select = random.nextInt(arr.length);
        System.out.println(arr[select]);
        getSearchtTextFieldElement().sendKeys(arr[select]);
        waitForPageToLoad();


        //tagName = a
        List<WebElement> allLinks = driver.findElements(By.tagName("a"));
        for (WebElement link : allLinks) {
            System.out.println(link.getText() + " - " + link.getAttribute("href"));

            //Obtain Iterator to traverse to the List
            Iterator<WebElement> iterator = allLinks.iterator();

            //Identifying and validating URL:
            url = iterator.next().getAttribute("href");
            huc = (HttpURLConnection) (new URL(url).openConnection());
            huc.setRequestMethod("HEAD");
            huc.connect();
            respCode = huc.getResponseCode();
            if (respCode >= 400) {
                System.out.println(url + " is a broken link");
            } else {
                System.out.println(url + " is a valid link");
            }


        }


    }

    @Step("User clicks on the Learn More Link in the Study Resultant Page")
    public void clickOnLearnMoreLinkDisplayedInStudyResultantPage() throws IOException {

        //local Variables:
        String homePage = "http://www.embibe.com";
        String url = "";
        HttpURLConnection huc = null;
        int respCode = 400;

        waitForPageToLoad();
        String[] arr = {"Heat", "Water", "Heart", "Figure Matrix"};
        Random random = new Random();
        //Randomly select an integer from the arr:
        int select = random.nextInt(arr.length);
        System.out.println(arr[select]);
        getSearchtTextFieldElement().sendKeys(arr[select]);

        wait(2000);
        waitForPageToLoad();
        driver.findElement(By.xpath("//a[@class='linkMore']")).click();
        waitForPageToLoad();
        List<WebElement> allLinks = driver.findElements(By.tagName("a"));
        for (WebElement link : allLinks) {
            System.out.println(link + " - " + link.getAttribute("href"));
            System.out.println(link + "-" + link.getText());
            //Obtain Iterator to traverse to the List
            Iterator<WebElement> iterator = allLinks.iterator();

            //Identifying and validating URL:
            url = iterator.next().getAttribute("href");
            huc = (HttpURLConnection) (new URL(url).openConnection());
            huc.setRequestMethod("HEAD");
            huc.connect();
            respCode = huc.getResponseCode();
            if (respCode >= 400) {
                System.out.println(url + " is a broken link");
            } else {
                System.out.println(url + " is a valid link");
            }


        }


    }

    public void searchByKeyword(String keyWord){
        waitForElementTobeClickable(searchTextBox);
        searchTextBox.sendKeys(keyWord);
    }



  /*  @Step("User selects keywords from data drivem framework")
    public void userClicksOnTheLearnMoreLink() throws IOException {
        //local Variables:
        String homePage = "http://www.embibe.com";
        String url = "";
        HttpURLConnection huc = null;
        int respCode = 200;

        waitForPageToLoad();
        driver.findElement(By.xpath("//a[@class='linkMore']")).click();
        waitForPageToLoad();
        List<WebElement> allLinks=driver.findElements(By.tagName("a"));
        for(WebElement link:allLinks){
            System.out.println(link + " - " + link.getAttribute("href"));

            //Obtain Iterator to traverse to the List
            Iterator<WebElement> iterator=allLinks.iterator();

            //Identifying and validating URL:
            url=iterator.next().getAttribute("href");
            huc = (HttpURLConnection)(new URL(url).openConnection());
            huc.setRequestMethod("HEAD");
            huc.connect();
            respCode = huc.getResponseCode();
            if(respCode >= 400){
                System.out.println(url+" is a broken link");
            }
            else{
                System.out.println(url+" is a valid link");
            }


        }
    }
    */

}
