package page.Search;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static org.testng.Assert.assertEquals;

public class FindSomethingCoolHeaderSection extends BasePage
{
    private SearchPageLocators locators;
    @FindBy(className = "divam-wrapper")
    private WebElement rootElement;

    public FindSomethingCoolHeaderSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new SearchPageLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getFindSomethingCoolHeaderAssertion()
    {
        WebElement element=getElement(rootElement,locators.findSomethingCoolHeaderAssertion);
        return element;
    }

    @Step("User verifies the title of the Search Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        System.out.println(getFindSomethingCoolHeaderAssertion().getText());
        assertEquals(getFindSomethingCoolHeaderAssertion().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }

}
