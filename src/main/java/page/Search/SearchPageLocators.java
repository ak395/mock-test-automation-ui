package page.Search;

import org.openqa.selenium.By;
import page.BasePage;

public class SearchPageLocators extends BasePage
{
    By searchTitle=By.cssSelector("div[class='search-title']");
    By startPracticeButton=By.cssSelector("a[class='practice-btn btn']");
    By startPracticeHeader=By.xpath("//span[contains(text(),'Start Practice')]");
    By takeATestHeader=By.xpath("//span[text()='Take a mock test, review performance of past tests']");
    By takeATestButton=By.cssSelector("a[class='test-btn btn']");
    By findSomethingCoolLink=By.cssSelector("a[class='find-cool']");
    By searchTextField=By.cssSelector("[class='react-typeahead-input react-typeahead-usertext']");
    By startPracticeHeaderAssertion=By.xpath("//span[@class='phoenix-candy__recommendation-large practice_phoenix_candy_recommendation']");
    By searchAmbiguityResult=By.cssSelector("[class*='ta3']");
    By takeATestHeaderAssertion=By.xpath("//span[@class='phoenix-candy__recommendation-large']");
    By findSomethingCoolHeaderAssertion=By.xpath("//p[@class='AI-banner__content--text']");
}
