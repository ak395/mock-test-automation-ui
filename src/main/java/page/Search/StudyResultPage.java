package page.Search;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static org.testng.Assert.assertEquals;

public class StudyResultPage extends BasePage
{

    private SearchPageLocators locators;
    @FindBy(className = "phoenix-candy__container")
    private WebElement rootElement;

    public StudyResultPage(WebDriver driver)
    {
        this.driver=driver;
        locators=new SearchPageLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getStartPracticeHeader()
    {
        return getElement(rootElement,locators.takeATestHeader);
    }

    @Step("User verifies the Header Section of the Take A Test Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        assertEquals(getStartPracticeHeader().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }
}
