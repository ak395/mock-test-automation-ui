package page.Search;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class LeftTreeSection extends BasePage
{
    private SearchPageLocators locators;
    @FindBy(className = "left-tree no-padding")
    private WebElement rootElement;

    public LeftTreeSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new SearchPageLocators();
        PageFactory.initElements(driver,this);
    }


}
