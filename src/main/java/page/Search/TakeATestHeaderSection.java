package page.Search;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static org.testng.Assert.assertEquals;

public class TakeATestHeaderSection extends BasePage {
    private SearchPageLocators locators;
    @FindBy(id = "blur_overlay_test")
    private WebElement rootElement;

    public TakeATestHeaderSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new SearchPageLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getTakeATestHeaderSectionAssertion()
    {
        WebElement element=getElement(rootElement,locators.takeATestHeaderAssertion);
        return element;
    }

    @Step("User verifies the title of the Search Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        System.out.println(getTakeATestHeaderSectionAssertion().getText());
        assertEquals(getTakeATestHeaderSectionAssertion().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }
}
