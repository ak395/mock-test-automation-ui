package page.mockTestQuestionsPage;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Getter
public class TestGridNavSection extends BasePage {

    @FindBy(css = ".sumbit-button")
    private WebElement finishButton;

    @FindBy(xpath = "//div[text()='Submit Test']")
    private WebElement submitTestButton;

    @FindBy(css = ".inverse-btn")
    private WebElement resumeTestButton;

    @FindBy(css = ".align-center > span > span")
    private WebElement examTimer;

    @FindBy(css = ".back-btn")
    private List<WebElement> backToTestButton;

    @FindBy(css = ".practice-legends-items-div  .test-legend-items--image-text")
    private List<WebElement>  questionGridWrapper;

    @FindBy(css=".prev-btn")
    private WebElement  previousButton;

    @FindBy(css=".next-btn")
    private WebElement nextButton;

    @FindBy(css = ".inverse-btn")
    private WebElement continueButton;

    @FindBy(css = ".button-clear")
    private WebElement clearSelection;

    public void submitTheTest(){
        waitForElementToBeVisible(submitTestButton);
        submitTestButton.click();
    }

    public TestGridNavSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void finishTheTest() {
        waitForElementToBeVisible(finishButton);
        finishButton.click();
    }

    public void continueTheTest() {
        waitForElementToBeVisible(continueButton);
        continueButton.click();
    }

    public String getCurrentTimer() {
        String currentTime = examTimer.getText();
        return currentTime;
    }

    public void backToTest() {
        backToTestButton.get(0).click();
    }

    public void goToPreviousQuestion() {
        waitForElementToBeVisible(previousButton);
        previousButton.click();
    }

    public void goToNextQuestion() {
        waitForElementToBeVisible(nextButton);
        nextButton.click();
    }


    public void clickQuestionInGrid(int questionNumber){
            jsClick(questionGridWrapper.get(questionNumber - 1));

    }

    public void clearSelection() {
        clearSelection.click();
    }
    public String getSystemTime() {
        //getting current date and time using Date class
        DateFormat dateFormat = new SimpleDateFormat("mm:ss");
         Date dateobj = new Date();
         return dateFormat.format(dateobj);
    }

    public long getTimeDifferenceInSecond(String time1,String time2) throws ParseException {
        DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        Date date = sdf.parse(time1);
        Date date2 = sdf.parse(time2);
        long difference = Math.abs(date2.getTime() - date.getTime());
            return difference / 1000 % 60;

    }
}
