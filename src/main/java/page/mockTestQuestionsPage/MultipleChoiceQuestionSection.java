package page.mockTestQuestionsPage;

import constants.Badge;
import entities.MockTestData;
import entities.MockTestPlanner;
import entities.questionTypes.mockTest.MultipleChoiceQuestion;
import io.restassured.response.ResponseBody;
import lombok.Getter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;
import mockTestService.MockTestClient;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
class MultipleChoiceQuestionSection extends BasePage {

    private MockTestClient client;
    private ResponseBody response;
    @FindAll({
            @FindBy(xpath = "//test-question[@class='ng-scope ng-isolate-scope']"),
            @FindBy(css = ".multiple_choice")
    })
    private List<WebElement> optionFinder;

    @FindBy(css = ".option-selected")
    private List<WebElement> optionSelected;

    MultipleChoiceQuestionSection(WebDriver driver) {
        client = new MockTestClient();
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    void selectAnswersBasedOnBadge(MockTestData mockTestData, List<String> correctAnswers, List<String> incorrectAnswers, MockTestPlanner mockTestPlanner, String activeSubject, String questionCode, int idealTime) {
        int marks = 0;
        AttemptType attemptType = AttemptType.ATTEMPT_TYPE_NOT_VISITED;
        MyChoice myChoice = mockTestData.getMyChoice();
        Badge badge = mockTestData.getBadge();
        int waitTime = waitForThinkTime(badge, idealTime);
        response = mockTestData.getResponseBody();

        String questionType = "Multiple Choice";

        switch (myChoice) {

            case CHOICE_CORRECT:
                selectAnswers(correctAnswers);
                if (optionSelected.size() < 1) {
                    selectAnswers(correctAnswers);
                }
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getPositiveMarksForQuestion(response, questionCode);
                System.out.println("marks for question " + marks);
                break;
            case CHOICE_INCORRECT:
                selectAnswers(incorrectAnswers);
                if (optionSelected.size() < 1) {
                    selectAnswers(correctAnswers);
                }
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getNegativeMarksForForQuestion(response, questionCode);
                System.out.println("Negative marks for question " + marks);
                break;

            case CHOICE_REVIEW_LATER:
                clickOnReviewButton();
                attemptType = AttemptType.ATTEMPT_TYPE_REVIEW_LATER;
                break;

            case CHOICE_ANSWERED_REVIEW_LATER:
                selectAnswers(correctAnswers);
                clickOnReviewButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED_REVIEW_LATER;
                break;

            case CHOICE_PARTIAL_CORRECT:
                selectAnswers(Collections.singletonList(correctAnswers.get(0)));
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = 1;
                break;

            case CHOICE_PARTIAL_CORRECT_AND_INCORRECT:
                selectAnswers(Arrays.asList(correctAnswers.get(0), incorrectAnswers.get(0)));
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getNegativeMarksForForQuestion(response, questionCode);
                System.out.println("Partial correctness marks for question " + marks);

                break;

            case CHOICE_PARTIAL_INCORRECT:
                selectAnswers(Collections.singletonList(incorrectAnswers.get(0)));
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                break;

            case CHOICE_UNATTEMPTED:
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_NOT_ANSWERED;
                break;

            case CHOICE_NOT_VISITED:
                attemptType = AttemptType.ATTEMPT_TYPE_NOT_VISITED;
                break;

        }

        MultipleChoiceQuestion multipleChoiceQuestion = new MultipleChoiceQuestion(questionType, myChoice, attemptType, badge, correctAnswers, incorrectAnswers, activeSubject, marks, questionCode, waitTime);
        mockTestPlanner.addQuestion(multipleChoiceQuestion);
    }

    private void clickOnReviewButton() {
        new TestGridActionSection(driver).reviewAnswer();
    }

    private void clickOnSaveButton() {
        new TestGridActionSection(driver).saveAnswer();
    }

    private void selectAnswers(List<String> answers) {
        waitForPageToLoad();
        answers.forEach(this::selectAnswer);
    }

    private void selectAnswer(String answer) {
        try {
            waitForElementTobeClickable(driver.findElement(By.id(answer)));
            driver.findElement(By.id(answer)).click();

        } catch (Exception ignore) {
            System.out.println("user in catch block");
            String questionCode = answer.trim().split("-")[0];
            switch (answer.trim().split("-")[1]) {
                case "1":
                    driver.findElement(By.id(questionCode + "-a")).click();
                    break;
                case "2":
                    driver.findElement(By.id(questionCode + "-b")).click();
                    break;

                case "3":
                    driver.findElement(By.id(questionCode + "-c")).click();
                    break;

                case "4":
                    driver.findElement(By.id(questionCode + "-d")).click();
                    break;
                case "a":
                    driver.findElement(By.id(questionCode + "-1")).click();
                    break;
                case "b":
                    driver.findElement(By.id(questionCode + "-2")).click();
                    break;
                case "c":
                    driver.findElement(By.id(questionCode + "-3")).click();
                    break;
                case "d":
                    driver.findElement(By.id(questionCode + "-4")).click();
                    break;
            }
        }

    }
}
