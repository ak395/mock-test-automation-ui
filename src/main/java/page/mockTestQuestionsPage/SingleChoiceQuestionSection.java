package page.mockTestQuestionsPage;

import constants.Badge;
import entities.MockTestData;
import entities.MockTestPlanner;
import entities.questionTypes.mockTest.SingleChoiceQuestion;
import io.restassured.response.ResponseBody;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;
import mockTestService.MockTestClient;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;

public class SingleChoiceQuestionSection extends BasePage {

    private String optionFinder = "//*[@question-num=%s]//span[contains(@class,'__choice')]";
    private MockTestClient client;
    private ResponseBody response;

    @FindBy(css = ".option-selected")
    private List<WebElement> optionSelected;


    SingleChoiceQuestionSection(WebDriver driver) {
        client = new MockTestClient();
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void selectAnswersBasedOnBadge(MockTestData mockTestData, List<String> correctAnswers, List<String> incorrectAnswers, MockTestPlanner mockTestPlanner, String activeSubject, String questionCode, int idealTime, int difficultyLevel) {
        int marks = 0;
        response = mockTestData.getResponseBody();
        AttemptType attemptType = AttemptType.ATTEMPT_TYPE_NOT_VISITED;
        Badge badge = mockTestData.getBadge();
        MyChoice myChoice = mockTestData.getMyChoice();

        int waitTime = waitForThinkTime(badge, idealTime);
        String questionType = "Single Choice";
        System.out.println(waitTime + " " + questionCode + " " + activeSubject);

        switch (myChoice) {

            case CHOICE_CORRECT:
                selectAnswer(correctAnswers.get(0), questionCode);
                System.out.println("correct answer is" + correctAnswers.get(0));
                if (optionSelected.size() < 1) {
                    selectAnswer(correctAnswers.get(0), questionCode);
                }
                clickOnNextButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getPositiveMarksForQuestion(response, questionCode);
                System.out.println("marks for question " + marks);
                break;

            case CHOICE_INCORRECT:
                selectAnswer(incorrectAnswers.get(0), questionCode);
                System.out.println("incorrect answer is" + incorrectAnswers.get(0));
                if (optionSelected.size() < 1) {
                    selectAnswer(incorrectAnswers.get(0), questionCode);
                }
                clickOnNextButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getNegativeMarksForForQuestion(response, questionCode);
                System.out.println("Negative marks for question " + marks);

                break;

            case CHOICE_REVIEW_LATER:
                clickOnReviewButton();
                clickOnNextButton();
                attemptType = AttemptType.ATTEMPT_TYPE_REVIEW_LATER;
                break;

            case CHOICE_ANSWERED_REVIEW_LATER:
                selectAnswer(correctAnswers.get(0), questionCode);
                clickOnReviewButton();
                clickOnNextButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED_REVIEW_LATER;
                break;

            case CHOICE_UNATTEMPTED:
                clickOnNextButton();
                attemptType = AttemptType.ATTEMPT_TYPE_NOT_ANSWERED;
                break;

            case CHOICE_NOT_VISITED:
                attemptType = AttemptType.ATTEMPT_TYPE_NOT_VISITED;
                break;

        }


        SingleChoiceQuestion singleChoiceQuestion = new SingleChoiceQuestion(questionType, myChoice, attemptType, badge, correctAnswers.get(0), incorrectAnswers, activeSubject, marks,questionCode,idealTime,difficultyLevel);
        mockTestPlanner.addQuestion(singleChoiceQuestion);
    }

    private void selectAnswer(String answer, String questionCode) {
        waitForPageToLoad();
        try {
            scrollToView(driver.findElement(By.id(answer)));
            jsClick(driver.findElement(By.id(answer)));

        } catch (Exception ignore) {
            System.out.println("user in catch block");
            switch (answer.trim().split("-")[1]) {
                case "1":
                    scrollToView(driver.findElement(By.id(questionCode + "-a")));
                    driver.findElement(By.id(questionCode + "-a")).click();
                    break;
                case "2":
                    scrollToView(driver.findElement(By.id(questionCode + "-b")));
                    driver.findElement(By.id(questionCode + "-b")).click();
                    break;

                case "3":
                    scrollToView(driver.findElement(By.id(questionCode + "-c")));
                    driver.findElement(By.id(questionCode + "-c")).click();
                    break;

                case "4":
                    scrollToView(driver.findElement(By.id(questionCode + "-d")));
                    driver.findElement(By.id(questionCode + "-d")).click();
                    break;
            }
        }
    }

    private void selectOption(String questionNum, int index) {
        waitForPageToLoad();
        List<WebElement> optionsForEachQtn = driver.findElements(By.xpath(String.format(optionFinder, questionNum)));
        try {
            scrollToView(optionsForEachQtn.get(index));
            optionsForEachQtn.get(index).click();
        } catch (WebDriverException e) {
            waitForElementToBeVisible(optionsForEachQtn.get(index));
            jsClick(optionsForEachQtn.get(index));
        }
    }

    private void clickOnReviewButton() {
        new TestGridActionSection(driver).reviewAnswer();
    }

    private void clickOnNextButton() {
        new TestGridActionSection(driver).saveAnswer();
    }

}
