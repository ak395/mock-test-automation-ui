package page.mockTestQuestionsPage;

import constants.Badge;
import entities.MockTestData;
import entities.MockTestPlanner;
import entities.questionTypes.mockTest.MatrixChoiceQuestion;
import io.restassured.response.ResponseBody;
import lombok.Getter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;
import mockTestService.MockTestClient;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
public class MatrixChoiceQuestionSection extends BasePage {

    private MockTestClient client;
    private ResponseBody mockTestResponse;

    MatrixChoiceQuestionSection(WebDriver driver) {
        client = new MockTestClient();
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    void selectAnswersBasedOnBadge(MockTestData mockTestData, List<String> correctAnswers, List<String> incorrectAnswers, MockTestPlanner mockTestPlanner, String activeSubject, String questionCode, int idealTime) {
        AttemptType attemptType = AttemptType.ATTEMPT_TYPE_NOT_VISITED;
        MyChoice myChoice = mockTestData.getMyChoice();
        Badge badge = mockTestData.getBadge();
        mockTestResponse = mockTestData.getResponseBody();
        int marks = 0;
        int waitTime = waitForThinkTime(badge, idealTime);

        String questionType = "Matrix Choice";

        switch (myChoice) {

            case CHOICE_CORRECT:
                selectAnswers(correctAnswers);
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getPositiveMarksForQuestion(mockTestResponse, questionCode);
                System.out.println("marks for question " + marks);
                break;
            case CHOICE_INCORRECT:
                selectAnswers(incorrectAnswers);
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getNegativeMarksForForQuestion(mockTestResponse, questionCode);
                System.out.println("marks for question " + marks);
                break;

            case CHOICE_REVIEW_LATER:
                clickOnReviewButton();
                attemptType = AttemptType.ATTEMPT_TYPE_REVIEW_LATER;
                break;

            case CHOICE_ANSWERED_REVIEW_LATER:
                selectAnswers(correctAnswers);
                clickOnReviewButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED_REVIEW_LATER;
                break;

            case CHOICE_PARTIAL_CORRECT:
                selectAnswers(Collections.singletonList(correctAnswers.get(0)));
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                break;

            case CHOICE_PARTIAL_CORRECT_AND_INCORRECT:
                selectAnswers(Arrays.asList(correctAnswers.get(0), incorrectAnswers.get(0)));
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                break;

            case CHOICE_PARTIAL_INCORRECT:
                selectAnswers(Collections.singletonList(incorrectAnswers.get(0)));
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                break;

            case CHOICE_UNATTEMPTED:
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_NOT_ANSWERED;
                break;

            case CHOICE_NOT_VISITED:
                attemptType = AttemptType.ATTEMPT_TYPE_NOT_VISITED;
                break;

        }

        MatrixChoiceQuestion matrixChoiceQuestion = new MatrixChoiceQuestion(questionType, myChoice, attemptType, badge, correctAnswers, incorrectAnswers, activeSubject, marks, questionCode, waitTime);
        mockTestPlanner.addQuestion(matrixChoiceQuestion);
    }

    private void clickOnReviewButton() {
        new TestGridActionSection(driver).reviewAnswer();
    }

    private void clickOnSaveButton() {
        new TestGridActionSection(driver).saveAnswer();
    }

    private void selectAnswers(List<String> answers) {
        answers.forEach(t -> {
            String[] sp = t.split("-");

            String a = sp[0].toUpperCase();
            String b = sp[1].toUpperCase();

            selectAnswer(a, b);
        });


    }

    private void selectAnswer(String row, String column) {
        String[] ans1 = column.split(",");
        if (ans1.length > 1)
            for (String s : ans1)
                driver.findElement(By.xpath(String.format("//th[text()='%s']/following::span[text()='%s']", row, s))).click();
        else
            driver.findElement(By.xpath(String.format("//th[text()='%s']/following::span[text()='%s']", row, column))).click();


    }
}




