package page.mockTestQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;

public class QuestionMapSection extends BasePage{

    @FindBy(css = ".question-map__attempts .question-map__attempt-icon")
    private List<WebElement> numberOfQuestions;

    @FindBy(css = ".question-map__attempts .question-map__attempt-icon")
    private WebElement numberOfQuestion;

    @FindBy(css = ".question-heading__number")
    private WebElement currentQuestionNumber;
    QuestionMapSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public int getTotalNumberOfQuestions(){
        waitForElementToBeVisible(numberOfQuestion);
        return numberOfQuestions.size();
    }

    public String  getCurrentQuestionNumber(){

        String questionNumberText = currentQuestionNumber.getText();
        String questionNumberClicked = questionNumberText.substring(2,4).trim();
        return  questionNumberClicked;

    }

}
