package page.mockTestQuestionsPage;

import constants.Badge;
import constants.QuestionType;
import driver.DriverProvider;
import entities.MockTestData;
import entities.MockTestPlanner;
import entities.questionTypes.mockTest.MockTestQuestion;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import lombok.Getter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;
import mockTestService.MockTestClient;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import page.BasePage;
import questionService.QuestionClient;
import services.mocktestDataService.MocktestDataService;
import verification.Verify;

import java.text.ParseException;
import java.util.*;

import static org.testng.Assert.assertEquals;

@Getter
public class QuestionsHomePage extends BasePage {

    SoftAssert softAssert;

    private SubjectLabelSection subjectLabelSection;
    private QuestionTypeSection questionTypeSection;
    private MultipleChoiceQuestionSection multipleChoiceQuestionSection;
    private TestGridNavSection testGridNavSection;
    private IntegerChoiceQuestionSection integerChoiceQuestionSection;
    private TestGridActionSection testGridActionSection;
    private QuestionMapSection questionMapSection;
    private BadgeSummarySection badgeSummarySection;
    private SingleChoiceQuestionSection singleChoiceQuestionSection;
    private InstructionPopupSection instructionPopupSection;
    private QuestionStatusSection questionStatusSection;
    private MatrixChoiceQuestionSection matrixChoiceQuestionSection;

    @FindBy(css = ".button-skip")
    private WebElement skipButton;

    @FindBy(css = ".question-options-container")
    private WebElement questionsOptionContainerElement;

    @FindBy(className = "question-heading__type")
    private WebElement questionHeading;

    @FindBy(className = "question-heading__number")
    private WebElement questionNumber;

    @FindBy(css = ".message-text")
    private WebElement multipleTabPopupText;

    @FindBy(css = ".btn-container.round.btn.positive-btn ")
    private WebElement clickResumeTest;

    @FindBy(css = ".inner-footer-wrapper")
    private WebElement bottomHeader;

    @FindBy(css = ".button-clear")
    private WebElement clearButton;

    @FindBy(css = ".question-options-container:nth-child(2) .mcq-container> .answer-mcq-wrapper")
    private List<WebElement> clickOnAnyOption;

    @FindBy(css = ".options-container")
    private List<WebElement> optionContainer;

    @FindBy(css = ".options-container")
    private WebElement optionContain;

    @FindBy(css = ".test-header-title.toolTip-parent")
    private WebElement testName;

    @FindBy(css = ".btn-box")
    private WebElement clickResumeOnTestListing;

    @FindBy(css = ".note-for-keypad")
    private WebElement noteForIntegerQuestion;

    @FindBy(css = ".questions-papers-right-component >.inactive-btn")
    private WebElement questionWithoutOption;

    @FindBy(css = ".keypad-wrapper:nth-of-type(3) .keypad-button")
    private WebElement numericalKeypad;

    @FindBy(css = ".next-btn ")
    private WebElement clickNext;

    @FindBy(css = ".prev-btn")
    private WebElement clickPrevious;

    @FindBy(className = "keypad-button")
    private List<WebElement> clickAnyKeypadNumber;

    @FindBy(css = ".keypad-button.clear-btn")
    private WebElement clearOnNumericKeyPad;

    @FindBy(css = ".error-modal-wrapper .title")
    private WebElement multipleSession;

    public QuestionsHomePage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        subjectLabelSection = new SubjectLabelSection(driver);
        questionTypeSection = new QuestionTypeSection(driver);
        multipleChoiceQuestionSection = new MultipleChoiceQuestionSection(driver);
        testGridNavSection = new TestGridNavSection(driver);
        integerChoiceQuestionSection = new IntegerChoiceQuestionSection(driver);
        testGridActionSection = new TestGridActionSection(driver);
        questionMapSection = new QuestionMapSection(driver);
        badgeSummarySection = new BadgeSummarySection(driver);
        singleChoiceQuestionSection = new SingleChoiceQuestionSection(driver);
        instructionPopupSection = new InstructionPopupSection(driver);
        matrixChoiceQuestionSection = new MatrixChoiceQuestionSection(driver);
        questionStatusSection = new QuestionStatusSection();
        softAssert = new SoftAssert();

    }


    public void selectAnswers(MockTestPlanner mockTestPlanner) {
        mockTestPlanner.getMockTestDataList().forEach(x -> {
            try {
                if (x.getSection() > 0)
                    subjectLabelSection.clickOnSubjectWithIndex(x.getSection());
                selectAnswersForQuestionTypes(x, mockTestPlanner);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void selectAnswersForQuestionTypes(MockTestData mockTestData, MockTestPlanner mockTestPlanner) throws Exception {

        for (int questionNum = mockTestData.getStartIndex(); questionNum <= mockTestData.getEndIndex(); questionNum++) {
            if (mockTestData.getSection() > 0)
                jumpToQuestionUsingQuestionPad(questionNum);

            String questionCode = getActiveQuestionCodeFromDom();
            ResponseBody responseBody = new MockTestClient().getMockTestData(getCookies(), getMockTestBundlePath());
            String version = new MockTestClient().getVersionForQuestion(responseBody, questionCode);
            Response response = new QuestionClient().getQuestionDetailsWithAnswersByQuestionCode(questionCode, version);
            String questionCategory = new QuestionClient().getQuestionCategoryFor(response, questionCode);
            String subject = new QuestionClient().getSubjectByQuestionCode(response, questionCode);
            int idealTime = new QuestionClient().getIdealTimeFor(response, questionCode);
            int difficultyLevel = new QuestionClient().getDifficultyLevelByQuestionCode(response,questionCode);
            System.out.println("Difficulty Level Of Question: "+ difficultyLevel);

            System.out.println("Subject: "+ subject + "Ideal Time: "+ idealTime);
            List<String> correctAnswers = new QuestionClient().getCorrectAnsFor(response, questionCode);
            List<String> incorrectAnswers = new QuestionClient().getIncorrectAnswer(response, questionCode);

            switch (questionCategory) {
                case QuestionType.MULTIPLE_CHOICE:
                    getMultipleChoiceQuestionSection().selectAnswersBasedOnBadge(mockTestData, correctAnswers, incorrectAnswers, mockTestPlanner, subject, questionCode, idealTime);
                    break;

                case QuestionType.INTEGER:
                    verifyNoteForIntegerTypeQuestion();
                    getIntegerChoiceQuestionSection().selectAnswerBasedOnBadge(mockTestData, correctAnswers, incorrectAnswers, mockTestPlanner, subject, questionCode, idealTime);
                    break;

                case QuestionType.SINGLE_CHOICE:

                case QuestionType.LINKEDCOMPREHENSION:
                    getSingleChoiceQuestionSection().selectAnswersBasedOnBadge(mockTestData, correctAnswers, incorrectAnswers, mockTestPlanner, subject, questionCode, idealTime,difficultyLevel);
                    break;

                case QuestionType.MATRIX:
                    getMatrixChoiceQuestionSection().selectAnswersBasedOnBadge(mockTestData, correctAnswers, incorrectAnswers, mockTestPlanner, subject, questionCode, idealTime);
                    break;
            }

        }
    }


    private String getActiveQuestionCodeFromDom() {
        waitForElementToBeVisible(questionsOptionContainerElement);
        return questionsOptionContainerElement.getAttribute("id");
    }

    public void finishTheTest() {
        getTestGridNavSection().finishTheTest();
    }

    public void submitTheTest() {
        getTestGridNavSection().submitTheTest();
    }

    public void continueTheTest() {
        getTestGridNavSection().continueTheTest();
    }

    public void goToNextQuestion() {
        getTestGridNavSection().goToNextQuestion();
    }

    public void previousQuestion() {
        getTestGridNavSection().goToPreviousQuestion();
    }

    public void verifyTheQuestionNumberCount(MockTestPlanner mockTestPlanner) {
        MocktestDataService dataService = new MocktestDataService(Integer.parseInt(mockTestPlanner.getTestId()));
        assertEquals(getQuestionMapSection().getTotalNumberOfQuestions(), dataService.getQuestions().size());
    }

    public void clearSelectedOption() {
        getTestGridNavSection().clearSelection();

    }

    @Step("If user wants to change the selected option with some different option")
    public void selectDifferentOption(MockTestPlanner mockTestPlanner, Integer questionNumber) throws Exception {
        if (mockTestPlanner.getMockTestDataList().get(questionNumber).getMyChoice() == MyChoice.CHOICE_CORRECT) {
            selectAnswersForQuestionTypes(new MockTestData(1, 1, MyChoice.CHOICE_INCORRECT, Badge.BADGE_TOO_FAST_CORRECT), mockTestPlanner);
        } else {
            selectAnswersForQuestionTypes(new MockTestData(1, 1, MyChoice.CHOICE_CORRECT, Badge.BADGE_TOO_FAST_CORRECT), mockTestPlanner);
        }

    }

    public void clickingOnInstructionPopupAppears() {
        getSubjectLabelSection().clickOnOverallInstructions();
        getInstructionPopupSection().verifyOverallInstructionPopupAppears();
        getInstructionPopupSection().closeInstructionPopup();
    }

    public void onReloadingPageInstructionShouldDisappear() {
        getSubjectLabelSection().clickOnOverallInstructions();
        getInstructionPopupSection().verifyOverallInstructionPopupAppears();
        wait(5000);
        refreshPage();
        wait(5000);
    }

    public void verifyTimerIsRunningWhileReadingInstructions() {
        String timeBeforeReading = getTestGridNavSection().getCurrentTimer();
        getSubjectLabelSection().clickOnOverallInstructions();
        getInstructionPopupSection().verifyOverallInstructionPopupAppears();
        wait(2000);
        getInstructionPopupSection().closeInstructionPopup();
        String timeAfterReading = getTestGridNavSection().getCurrentTimer();
        Boolean result = timeAfterReading.equals(timeBeforeReading);
        Verify.assertFalse(result, "Timer before and after reading instruction are same.", Optional.of(false));

    }

    public void verifyTimerIsVisibleWhileReadingQuestionPaper() {
        getSubjectLabelSection().clickOnQuestionPaperLink();
        Verify.assertTrue(getTestGridNavSection().getExamTimer().isDisplayed(), "Exam Timer is not displayed while reading question paper", Optional.of(true));
        getTestGridNavSection().backToTest();
    }

    public void verifyTimerIsDisplay() {
        Verify.assertTrue(getTestGridNavSection().getExamTimer().isDisplayed(), "Exam Timer is not displayed", Optional.of(true));

    }

    public void verifyTimerIsDisplayInDifferentScreenSize() {
        verifyTimerIsDisplay();
        resizeBrowser(800, 480);
        verifyTimerIsDisplay();
        resizeBrowser(600, 300);
        verifyTimerIsDisplay();
        resizeBrowser(720, 480);
        verifyTimerIsDisplay();
        resizeBrowser(820, 380);
        verifyTimerIsDisplay();
        resizeBrowser(1024, 768);
        verifyTimerIsDisplay();
        resizeBrowser(800, 600);
        verifyTimerIsDisplay();
        resizeBrowser(1280, 1024);
        verifyTimerIsDisplay();
        resizeBrowser(1600, 1200);
        verifyTimerIsDisplay();
        resizeBrowser(1920, 1200);
        verifyTimerIsDisplay();
        resizeBrowser(2560, 1440);
        verifyTimerIsDisplay();

    }

    @Step("User is able to navigate to different Sections")
    public void userIsAbleToNavigateBetweenDifferentSections() {
        int numberOfSubjects = getSubjectLabelSection().getNumberOfSubjects();
        for (int i = 1; i < numberOfSubjects; i++) {
            getSubjectLabelSection().clickOnSubjectWithIndex(i);
            String questionCode = getActiveQuestionCodeFromDom();
            Response response = new QuestionClient().getQuestionDetailsWithAnswersByQuestionCode(questionCode);
            String expectedSubject = new QuestionClient().getSubjectByQuestionCode(response, questionCode);
            Assert.assertEquals(getSubjectLabelSection().getNameOfActiveSubject(), expectedSubject.toUpperCase(), String.format("User should navigate to section: %s but still on section:%s", expectedSubject, getSubjectLabelSection().getNameOfActiveSubject()));
        }
    }

    public void jumpToQuestionUsingQuestionPad(int questionNumber) {

        getTestGridNavSection().clickQuestionInGrid(questionNumber);
        String currentQuestionNumber = getQuestionMapSection().getCurrentQuestionNumber();
        Assert.assertEquals(Integer.parseInt(currentQuestionNumber), questionNumber, String.format("User did not jump to the question number %d:", questionNumber));

    }

    public void jumpToQuestionUsingQuestionInSection(int section, int questionNumber) {
        getSubjectLabelSection().goToSection(section);
        getTestGridNavSection().clickQuestionInGrid(questionNumber);
        String currentQuestionNumber = getQuestionMapSection().getCurrentQuestionNumber();
        Assert.assertEquals(Integer.parseInt(currentQuestionNumber), questionNumber, String.format("User did not jump to the question number %d:", questionNumber));

    }

    public void verifyTimerIsWorkingProperlyDuringTest(int waitForSecond) throws ParseException {
        Verify.assertTrue(getTestGridNavSection().getExamTimer().isDisplayed(), "Exam Timer is not displayed", Optional.of(true));
        String timeBeforeReading = getTestGridNavSection().getCurrentTimer();
        System.out.println(timeBeforeReading);
        wait(waitForSecond * 1000);
        String timeAfterReading = getTestGridNavSection().getCurrentTimer();
        System.out.println(timeAfterReading);
        long durationOfTest = getTestGridNavSection().getTimeDifferenceInSecond(timeBeforeReading, timeAfterReading);
        System.out.println(durationOfTest);
        Boolean difference = durationOfTest - waitForSecond <= 2;
        Assert.assertTrue(difference, "timer is not working properly during test its expacted " + waitForSecond + " second" + " but found " + durationOfTest + " second");
    }

    public String getExamCurrentTime() {
        Verify.assertTrue(getTestGridNavSection().getExamTimer().isDisplayed(), "Exam Timer is not displayed", Optional.of(true));
        return getTestGridNavSection().getCurrentTimer();
    }

    public void verifyTimerIsWorkingProperlyAfterResumingTest(String timeAfterStartTime, String timeAfterResumingTest) throws ParseException {
        Assert.assertNotEquals(timeAfterStartTime, timeAfterResumingTest);

    }

    public void verifyTimerIsDisplaySubmitTestPopUp() {
        finishTheTest();
        Assert.assertTrue(getTestGridNavSection().getExamTimer().isDisplayed(), "Exam Timer is not displayed on SubmitTestPopup");

    }


    @Step("Verify Previous Button functionality when user is on first Question in first Section")
    public void verifyPreviousButtonFunctionality() {
        getTestGridNavSection().goToPreviousQuestion();
        int numberOfSections = getSubjectLabelSection().getNumberOfSubjects();
        String currentSection = subjectLabelSection.getNameOfActiveSubject();
        String expectedSection = subjectLabelSection.getSubjectNameInIndex(numberOfSections - 1);
        softAssert.assertEquals(currentSection, expectedSection, String.format("Expected Section should be %s but found %s", expectedSection, currentSection));
        softAssert.assertEquals(getQuestionMapSection().getCurrentQuestionNumber(), "30", "User did not landed on correct question");
        softAssert.assertAll();
    }

    @Step("Verify Next Button functionality When user is on the last Question in last Section")

    public void verifyNextButtonFunctionality() {
        getTestGridNavSection().goToNextQuestion();
        String currentSection = subjectLabelSection.getNameOfActiveSubject();
        String expectedSection = subjectLabelSection.getSubjectNameInIndex(0);
        softAssert.assertEquals(currentSection, expectedSection, String.format("Expected Section should be %s but found %s", expectedSection, currentSection));
        softAssert.assertEquals(getQuestionMapSection().getCurrentQuestionNumber(), "1", "User did not landed on correct question");
        softAssert.assertAll();

    }

    public void verifyNumberingInEachSection() {
        int numberOfSubjects = getSubjectLabelSection().getNumberOfSubjects();
        for (int i = 1; i < numberOfSubjects; i++) {
            getSubjectLabelSection().clickOnSubjectWithIndex(i);
            String questionCode = getActiveQuestionCodeFromDom();
            Response response = new QuestionClient().getQuestionDetailsWithAnswersByQuestionCode(questionCode);
            String expectedSubject = new QuestionClient().getSubjectByQuestionCode(response, questionCode);
            softAssert.assertEquals(getSubjectLabelSection().getNameOfActiveSubject(), expectedSubject.toUpperCase(), String.format("Active Section should be: %s but found %s", expectedSubject, getSubjectLabelSection().getNameOfActiveSubject()));
            softAssert.assertEquals(getQuestionMapSection().getCurrentQuestionNumber(), "1", String.format("Expected question number should be 1 but found : %s", getQuestionMapSection().getCurrentQuestionNumber()));
            softAssert.assertAll();
        }

    }

    @Step("Verify for User should clearly different UI view of Answer, Not answered, Review Later, Marked for Review or Not Visited")
    public void verifyQuestionStatus(MockTestPlanner mockTestPlanner) {

        for (int i = 0; i < mockTestPlanner.getMockTestQuestions().size(); i++) {

            MockTestQuestion mockTestQuestion = mockTestPlanner.getMockTestQuestions().get(i);
            questionStatusSection.verifyQuestionNumberWithChoiceIsDisplayed(mockTestQuestion, i);
        }
    }

    @Step("Verify after attempting a question,If user clears the selection the colors changes to RED from GREEN")
    public void verifyQuestionNumberColourChangesToRed() {
        Assert.assertTrue(questionStatusSection.verifyCurrentQuestionStatusChangesToRed(), "After clearing the selected answer the question number in question pad did not change to red");

    }


    @Step("User clicks on Mark For Review")
    public void markForReview() {
        testGridActionSection.reviewAnswer();

    }

    public void verifyQuestionIsMarkedForReview() {
        Assert.assertTrue(questionStatusSection.verifyCurrentQuestionIsMarkedForReview(), "After marking for review also the question status did not changed to mark for review");
    }

    @Step("Verify after attempting a question,If user mark for review the selection the colors changes to BLUE from GREEN")
    public void verifyQuestionNumberColourChangesToBlue() {
        Assert.assertTrue(questionStatusSection.verifyCurrentQuestionStatusChangesToBlue());
    }


    /*TODO::
     * Methods for verification also available in questions Home page. Duplicated Code
     * */

    private void verifyBadgeSummaryForAnsweredQuestions(MockTestPlanner mockTestPlanner) {
        Set< AttemptType > answered = new HashSet<>();
        answered.add(AttemptType.ATTEMPT_TYPE_ANSWERED);
        assertEquals(getBadgeSummarySection().getNumberOfAnsweredQuestions(), mockTestPlanner.getNumberOfQuestionForAttemptTypes(answered));
    }

    private void verifyBadgeSummaryForNotAnsweredQuestions(MockTestPlanner mockTestPlanner) {
        Set< AttemptType > notAnswered = new HashSet<>();
        notAnswered.add(AttemptType.ATTEMPT_TYPE_NOT_ANSWERED);
        int actualNumberOfQuestions = mockTestPlanner.getNumberOfQuestionForAttemptTypes(notAnswered);
        assertEquals(getBadgeSummarySection().getNumberOfNotAnsweredQuestions(), actualNumberOfQuestions);
    }

    private void verifyBadgeSummaryForReviewLater(MockTestPlanner mockTestPlanner) {
        Set< AttemptType > reviewLater = new HashSet<>();
        reviewLater.add(AttemptType.ATTEMPT_TYPE_REVIEW_LATER);
        reviewLater.add(AttemptType.ATTEMPT_TYPE_ANSWERED_REVIEW_LATER);
        assertEquals(getBadgeSummarySection().getNumberOfAnsweredReviewLaterQuestions(), mockTestPlanner.getNumberOfQuestionForAttemptTypes(reviewLater));
    }

    private void verifyBadgeSummaryForNotVisited(MockTestPlanner mockTestPlanner) {
        Set< AttemptType > notVisited = new HashSet<>();
        notVisited.add(AttemptType.ATTEMPT_TYPE_NOT_VISITED);
        int actualNumberOfQuestions = mockTestPlanner.getNumberOfQuestionForAttemptTypes(notVisited);
        if (actualNumberOfQuestions > 0)
            assertEquals(getBadgeSummarySection().getNumberOfNotVisitedQuestions(), actualNumberOfQuestions - 1);
        else assertEquals(getBadgeSummarySection().getNumberOfNotVisitedQuestions(), 0);
    }


    public void verifyBadgeSummary(MockTestPlanner mockTestPlanner) {
        wait(2000);
        verifyBadgeSummaryForAnsweredQuestions(mockTestPlanner);
        verifyBadgeSummaryForReviewLater(mockTestPlanner);
        verifyBadgeSummaryForNotAnsweredQuestions(mockTestPlanner);
        verifyBadgeSummaryForNotVisited(mockTestPlanner);
    }

    public void verifyUserLandedOnTestPage() {
        Assert.assertTrue(testName.isDisplayed(), "Test name is not displaying");
    }

    public void verifyInstructionPopUpDisappearOnRefreshingPage() {
        getSubjectLabelSection().clickOnOverallInstructions();
        getInstructionPopupSection().verifyOverallInstructionPopupAppears();
        driver.navigate().refresh();
        acceptAlertPopUp();
        getInstructionPopupSection().verifyOverallInstructionPopupDisAppears();
        Assert.assertTrue(questionNumber.getText().contains("Q 1 -"));
    }

    public void verifyBrowserPopUpWhileRefreshingTheTab() {
        getSubjectLabelSection().clickOnOverallInstructions();
        getInstructionPopupSection().verifyOverallInstructionPopupAppears();
        driver.navigate().refresh();
        Assert.assertTrue(acceptAlertPopUp());
    }

    public void openTestInDifferentTab(String url) {
        openUrlInNewTab(url);
    }

    public void verifyTestInDifferentBrowserOrTab() {
        switchToParentTab();
        try {
            //Assert.assertFalse( == 0, "Question with option");
            multipleSession.isDisplayed();
            Assert.assertEquals(multipleSession.getText(), "Looks like you’re taking the test in multiple tabs/devices");
            Assert.assertEquals(multipleTabPopupText.getText(), "There is a session running for the current test, do you want to resume here?");
        } catch (NoSuchElementException exception) {
            System.out.println("ggrgt5g");
        }
    }

    public void verifyBackToTest() {
        getSubjectLabelSection().clickOnQuestionPaperLink();
        getTestGridNavSection().backToTest();
        Assert.assertTrue(bottomHeader.isDisplayed(), "User is on Question Paper Section");
    }


    public void verifyQuestionWithOption() {
        getSubjectLabelSection().clickOnQuestionPaperLink();
        Assert.assertTrue(optionContainer.size() > 0, "Question without option");
    }

    public void verifyQuestionWithOutOption() {
        getSubjectLabelSection().clickOnQuestionPaperLink();
        questionWithoutOption.click();
        try {
            Assert.assertFalse(optionContain.isDisplayed());
        } catch (NoSuchElementException exception) {
            exception.printStackTrace();
            System.out.println("Question without options");
        }
        //Assert.assertTrue(optionContainer.size() == 0, "Question contains option even though user select question without options");

    }

    public void verifyTestNameAfterResumingTest(String testBeforeResume, String testAfterResume) {
        Assert.assertEquals(testBeforeResume, testAfterResume);
    }

    public String getTestName() {
        return testName.getText();
    }

    public void clickOnClearButton() {
        waitForElementToBeVisible(clearButton);
        jsClick(clearButton);
    }


    public void clickOnAnyOption() {
        Random random = new Random();
        int option = random.nextInt(4);
        jsClick(clickOnAnyOption.get(option));
    }

    public void clickAnyOptionOnKeyPad() {
        waitForListOfToBeVisible(clickAnyKeypadNumber);
        Random random = new Random();
        int option = random.nextInt(clickAnyKeypadNumber.size());
        jsClick(clickAnyKeypadNumber.get(option));
    }

    public void verifyUserAbleToNavigateNextQuestionAfterClickOnClearButtonFollowedByNextButton() {
        clickOnAnyOption();
        clickOnClearButton();
        new TestGridActionSection(driver).saveAnswer();
        Assert.assertTrue(questionNumber.getText().contains("2"));
    }

    public void verifyUserAbleToNavigateNextQuestionAfterClickOnClearButtonFollowedByReviewLaterAndNextButton() {
        clickOnAnyOption();
        clickOnClearButton();
        new TestGridActionSection(driver).reviewAnswer();
        new TestGridActionSection(driver).saveAnswer();
        Assert.assertTrue(questionNumber.getText().contains("2"));
    }

    public void verifyNoteForIntegerTypeQuestion() {
        Assert.assertEquals(noteForIntegerQuestion.getText(), "Note : Please use the numerical keypad provided");
    }

    public void clickOnNumericalKeyPad() {
        waitForElementToBeVisible(numericalKeypad);
        jsClick(numericalKeypad);
    }

    public void clickOnNextButton() {
        testGridNavSection.goToNextQuestion();
    }

    public void clickOnPreviousButton() {
        testGridNavSection.goToPreviousQuestion();
    }

    public void clickMarkForReviewQuestion() {
        questionStatusSection.clickMarkForReview();
    }

    public void verifyQuestionNumberColourChangesToGreen() {
        Assert.assertTrue(questionStatusSection.verifyCurrentQuestionStatusChangesToGreen());
    }

    public void clearOnNumericKeyPad() {
        clearOnNumericKeyPad.click();
    }

    public List<String> getSubjectName() {
        return subjectLabelSection.getSubjectName();
    }

    public void verifySwitchToActiveTab() {
        switchToActiveTab();
    }
}

