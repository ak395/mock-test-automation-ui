package page.mockTestQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

public class InstructionPopupSection extends BasePage {

    @FindBy(css=".instruction-model-container")
    private WebElement overallInstructionsPopup;

    @FindBy(css=".instruction-close-button")
    private WebElement insctructionCloseButton;

    InstructionPopupSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void verifyOverallInstructionPopupAppears() {
        Verify.assertTrue(overallInstructionsPopup.isDisplayed(),"Popup is not displayed", Optional.of(true));
    }

    public void verifyOverallInstructionPopupDisAppears(){
        boolean isDisplaying;
        try{
            isDisplaying = overallInstructionsPopup.isDisplayed();
        }
        catch (Exception NoSuchElementException)
        {
            isDisplaying = false;
        }
        Assert.assertFalse(isDisplaying,"Instruction page popup is displaying");

    }

    public void closeInstructionPopup() {
        insctructionCloseButton.click();
    }

}
