package page.mockTestQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class TestGridActionSection extends BasePage {

    @FindBy(css = ".next-btn")
    private WebElement saveAnswerBtn;

    @FindBy(css = "span.review-now-text")
    private WebElement reviewAnswerBtn;


    public TestGridActionSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void saveAnswer() {
        waitForElementToBeVisible(saveAnswerBtn);
        saveAnswerBtn.click();
    }

    public void reviewAnswer() {
        waitForElementToBeVisible(reviewAnswerBtn);
        reviewAnswerBtn.click();
    }
}
