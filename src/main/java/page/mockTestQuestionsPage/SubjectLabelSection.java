package page.mockTestQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.ArrayList;
import java.util.List;

public class SubjectLabelSection extends BasePage {

    @FindBy(css = ".section-name")
    private List<WebElement> subjectLabelList;

    @FindBy(css = ".active-section > .section-name")
    private WebElement activeSubjectElement;

    @FindBy(css = ".btn-txt")
    private List<WebElement> instructionAndQuestionPaper;

    public SubjectLabelSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnSubjectWithIndex(int index) {
        if (subjectLabelList.size() > 1)
            jsClick(subjectLabelList.get(index-1));
        else
            jsClick(subjectLabelList.get(0));
    }

    public void clickOnOverallInstructions() {
        instructionAndQuestionPaper.get(0).click();
    }

    public void clickOnQuestionPaperLink() {
        instructionAndQuestionPaper.get(1).click();
    }

    public int getNumberOfSubjects() {
        return subjectLabelList.size();
    }

    public String getSubjectNameInIndex(int i) {
        return subjectLabelList.get(i).getText();
    }

    public String getNameOfActiveSubject() {
        System.out.println(activeSubjectElement.getText());
        return activeSubjectElement.getText();
    }

    public List<String> getSubjectName() {
        List<String> subjectName = new ArrayList();
        for (WebElement webElement : subjectLabelList) {
            subjectName.add(webElement.getText());
        }
        return subjectName;
    }

    public void goToSection(int sectionNumber) {
        subjectLabelList.get(sectionNumber-1).click();
    }
}
