package page.mockTestQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class QuestionTypeSection extends BasePage{

    @FindBy(css = ".question-info .pull-left")
    private WebElement questionNumberElement;

    public QuestionTypeSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getQuestionNumber(){
        waitForElementToBeVisible(questionNumberElement);
        return questionNumberElement.getText().split(":")[1];
    }
}
