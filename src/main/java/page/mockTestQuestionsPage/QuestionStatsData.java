package page.mockTestQuestionsPage;

public class QuestionStatsData {

    private int answered;
    private int notAnswered;
    private int notVisited;
    private int reviewNow;
    private int answeredAndMarkedForReview;

    public QuestionStatsData(int answered, int notAnswered, int notVisited, int reviewNow, int answeredAndMarkedForReview){

        this.answered = answered;
        this.notAnswered = notAnswered;
        this.notVisited = notVisited;
        this.reviewNow= reviewNow;
        this.answeredAndMarkedForReview = answeredAndMarkedForReview;

    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }

        if (!(object instanceof QuestionStatsData)) {
            return false;
        }

        QuestionStatsData questionStatsData = (QuestionStatsData) object;

        return  answered == questionStatsData.answered
                && notAnswered == questionStatsData.notAnswered
                && notVisited == questionStatsData.notVisited
                && reviewNow == questionStatsData.reviewNow
                && answeredAndMarkedForReview == questionStatsData.answeredAndMarkedForReview;
    }


}
