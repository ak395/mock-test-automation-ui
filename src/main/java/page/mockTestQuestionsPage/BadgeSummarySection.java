package page.mockTestQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;

public class BadgeSummarySection extends BasePage{

    @FindBy(css = ".question-map__answered .ng-binding")
    private WebElement badgeAnsweredNumber;

    @FindBy(css = ".question-map__not-answered .ng-binding")
    private WebElement badgeNotAnsweredNumber;

    @FindBy(css = ".question-map__not-visited .ng-binding")
    private WebElement badgeNotVisitedNumber;

    @FindBy(css = ".question-map__review .ng-binding")
    private WebElement badgeReviewLaterNumber;

    @FindBy(css = ".test-summary-section .test-legend-items--image-text")
    private List<WebElement> badgesCount;



    public BadgeSummarySection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public int getNumberOfAnsweredQuestions(){
        waitForElementToBeVisible(badgesCount.get(0));
        return Integer.parseInt(badgesCount.get(0).getText());
    }

    public int getNumberOfNotAnsweredQuestions(){
        waitForElementToBeVisible(badgesCount.get(1));
        return Integer.parseInt(badgesCount.get(1).getText());
    }

    public int getNumberOfNotVisitedQuestions(){
        waitForElementToBeVisible(badgesCount.get(2));
        return Integer.parseInt(badgesCount.get(2).getText());
    }

    public int getNumberOfAnsweredReviewLaterQuestions(){
        waitForElementToBeVisible(badgesCount.get(4));
        return Integer.parseInt(badgesCount.get(4).getText());
    }
}
