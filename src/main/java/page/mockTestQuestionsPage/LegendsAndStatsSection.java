package page.mockTestQuestionsPage;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

public class LegendsAndStatsSection extends BasePage {

    private QuestionStatsData questionStatsData;
    @FindBy(css = ".test-legend-item-container--main-spacing .answered > .test-legend-items--image-text")
    private WebElement answeredQuestions;

    @FindBy(css=".test-legend-item-container--main-spacing .not-answered > .test-legend-items--image-text")
    private WebElement notAnsweredQuestions;

    @FindBy(css =".test-legend-item-container--main-spacing .not-visited > .test-legend-items--image-text")
    private WebElement notVisitedQuestions;

    @FindBy(css=".test-legend-item-container--main-spacing .answered-and-marked-for-review > .test-legend-items--image-text")
    private WebElement answeredAndMarkForReviewQuestions;

    @FindBy(css=".test-legend-item-container--main-spacing .marked-for-review > .test-legend-items--image-text")
    private WebElement reviewNowQuestions;

    public LegendsAndStatsSection() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    public int getNumberOfAnsweredQuestions() {
        waitForElementToBeVisible(answeredQuestions);
        int numberOfAnsweredQuestions = Integer.parseInt(answeredQuestions.getText());
        return numberOfAnsweredQuestions;
    }

    public int getNumberNotAnsweredQuestions(){
        waitForElementToBeVisible(notAnsweredQuestions);
       int numberOfNotAnsweredQuestions = Integer.parseInt(notAnsweredQuestions.getText());
       return numberOfNotAnsweredQuestions;
    }

    public int  getNumberOfAnsweredAndMarkForReviewQuestions() {
        waitForElementToBeVisible(answeredAndMarkForReviewQuestions);
        int numberOfAnsweredAndMarkForReviewQuestions = Integer.parseInt(notVisitedQuestions.getText());
        return numberOfAnsweredAndMarkForReviewQuestions;
    }

    public int  getNumberOfNotVisitedQuestions() {
        waitForElementToBeVisible(notVisitedQuestions);
        int numberOfNotVisitedQuestions = Integer.parseInt(notVisitedQuestions.getText());
        return numberOfNotVisitedQuestions;
    }

    public int  getNumberOfReviewNowQuestions() {
        waitForElementToBeVisible(reviewNowQuestions);
        int numberOfReviewNowQuestions = Integer.parseInt(reviewNowQuestions.getText());
        return  numberOfReviewNowQuestions;
    }

    public QuestionStatsData addDataToQuestionStats() {

        return new QuestionStatsData(getNumberOfAnsweredQuestions(),getNumberNotAnsweredQuestions(),getNumberOfNotVisitedQuestions(),getNumberOfReviewNowQuestions(),getNumberOfAnsweredAndMarkForReviewQuestions());
    }

    public void compareTheData(QuestionStatsData questionStatsData, QuestionStatsData questionStatsDataNew){

        Assert.assertTrue(questionStatsData.equals(questionStatsDataNew),"Question stats data is not matching in the different browsers");

    }

}
