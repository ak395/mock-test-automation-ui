package page.mockTestQuestionsPage;

import constants.Badge;
import entities.MockTestData;
import entities.MockTestPlanner;
import entities.questionTypes.mockTest.IntegerChoiceQuestion;
import io.restassured.response.ResponseBody;
import lombok.Getter;
import mockTestFactory.AttemptType;
import mockTestFactory.MyChoice;
import mockTestService.MockTestClient;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.ArrayList;
import java.util.List;

@Getter
public class IntegerChoiceQuestionSection extends BasePage {
    private List<IntegerChoiceQuestion> integerChoiceQuestions;
    private MockTestClient client;
    private ResponseBody response;

    public IntegerChoiceQuestionSection(WebDriver driver) {
        client = new MockTestClient();
        this.driver = driver;
        PageFactory.initElements(driver, this);
        integerChoiceQuestions = new ArrayList<>();
    }

    public void selectAnswerBasedOnBadge(MockTestData mockTestData, List<String> correctAnswers, List<String> incorrectAnswers, MockTestPlanner mockTestPlanner, String activeSubject, String questionCode, int idealTime) {
        AttemptType attemptType = AttemptType.ATTEMPT_TYPE_NOT_VISITED;
        MyChoice myChoice = mockTestData.getMyChoice();
        Badge badge = mockTestData.getBadge();
        response = mockTestData.getResponseBody();
        int marks = 0;
        int waitTime = waitForThinkTime(badge, idealTime);

        String questionType = "Integer";

        switch (myChoice) {

            case CHOICE_CORRECT:
                selectCorrectAnswer(questionCode, correctAnswers.get(0));
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getPositiveMarksForQuestion(response, questionCode);
                System.out.println("marks for question " + marks);
                break;

            case CHOICE_INCORRECT:
                selectIncorrectAnswer(questionCode, correctAnswers.get(0));
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED;
                marks = client.getNegativeMarksForForQuestion(response, questionCode);
                System.out.println("negative marks for question " + marks);
                break;

            case CHOICE_REVIEW_LATER:
                clickOnReviewAnswer();
                attemptType = AttemptType.ATTEMPT_TYPE_REVIEW_LATER;
                break;

            case CHOICE_ANSWERED_REVIEW_LATER:
                clickOnReviewAnswer();
                attemptType = AttemptType.ATTEMPT_TYPE_ANSWERED_REVIEW_LATER;
                break;

            case CHOICE_UNATTEMPTED:
                clickOnSaveButton();
                attemptType = AttemptType.ATTEMPT_TYPE_NOT_ANSWERED;
                break;

            case CHOICE_NOT_VISITED:
                attemptType = AttemptType.ATTEMPT_TYPE_NOT_VISITED;
                break;

        }


        IntegerChoiceQuestion integerChoiceQuestion = new IntegerChoiceQuestion(questionType, myChoice, attemptType, badge, correctAnswers.get(0), activeSubject, marks, questionCode, waitTime);
        mockTestPlanner.addQuestion(integerChoiceQuestion);

    }

    private void clickOnReviewAnswer() {
        new TestGridActionSection(driver).reviewAnswer();
    }

    private void clickOnSaveButton() {
        new TestGridActionSection(driver).saveAnswer();
    }

    private void selectCorrectAnswer(String questionNum, String ans) {
        waitForPageToLoad();
        selectAnswer(questionNum, ans);
    }


    private void selectIncorrectAnswer(String questionNum, String ans) {
        waitForPageToLoad();
        int body = Integer.parseInt(ans) - 1;
        selectAnswer(questionNum, String.valueOf(body));
    }

    private void selectAnswer(String questionNum, String answer) {
        WebElement option = driver.findElement(By.xpath(String.format("//div[text()='%s' and @class='keypad-button']", answer.trim())));
        try {
            option.click();
        } catch (WebDriverException ignore) {
            waitForElementToBeVisible(option);
            jsClick(option);
        }
    }
}
