package page.mockTestQuestionsPage;

import driver.DriverProvider;
import entities.questionTypes.mockTest.MockTestQuestion;
import mockTestFactory.AttemptType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;

import java.util.List;

public class QuestionStatusSection extends BasePage {

    @FindBy(css = ".test-question-legend-wrapper--items  .test-legend-items--image.marked-for-review > .test-legend-items--image-text")
    private WebElement markedForReviewStatus;

    @FindBy(css=".test-question-legend-wrapper--items .test-legend-items--image.answered > .test-legend-items--image-text")
    private WebElement answeredStatus;

    @FindBy(css=".test-question-legend-wrapper--items .test-legend-items--image.not-answered > .test-legend-items--image-text")
    private List<WebElement> notAnsweredStatus;

    @FindBy(css=".test-question-legend-wrapper .test-legend-items--image.answered-and-marked-for-review >.test-legend-items--image-text")
    private WebElement answeredAndMarkedForReviewStatus;

    @FindBy(css=".not-visited")
    private List<WebElement> notVisitedStatus;

    @FindBy(css = ".current-question.not-answered")
    private WebElement currentQuestionNotAnswered;

    @FindBy(css = ".current-question.marked-for-review")
    private WebElement currentQuestionMarkedForReview;

    @FindBy(css = ".review-now-text")
    private WebElement markForReview;




    public QuestionStatusSection() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

    void verifyQuestionNumberWithChoiceIsDisplayed(MockTestQuestion mockTestQuestion, int i) {

        AttemptType attemptType = mockTestQuestion.getAttemptType();

        switch(attemptType) {

            case ATTEMPT_TYPE_ANSWERED:
                Assert.assertEquals(Integer.parseInt(answeredStatus.getText()),i+1,String.format("The Question Number: %d is answered as %s but found Question Number: %s",i+1,attemptType,answeredStatus.getText()));
                break;

            case ATTEMPT_TYPE_ANSWERED_REVIEW_LATER:
                Assert.assertEquals(Integer.parseInt(answeredAndMarkedForReviewStatus.getText()),i+1,String.format("The Question Number: %d is answered as %s but found Question Number: %s",i+1,attemptType,answeredAndMarkedForReviewStatus.getText()));
                break;

            case ATTEMPT_TYPE_NOT_ANSWERED:
                Assert.assertEquals(Integer.parseInt(notAnsweredStatus.get(0).getText()),i+1,String.format("The Question Number: %d is answered as %s but found Question Number: %s",i+1,attemptType,notAnsweredStatus.get(0).getText()));
                break;

            case ATTEMPT_TYPE_REVIEW_LATER:
                Assert.assertEquals(Integer.parseInt(markedForReviewStatus.getText()),i+1,String.format("The Question Number: %d is answered as %s but found Question Number: %s",i+1,attemptType, markedForReviewStatus.getText()));
                break;
        }


    }

    public boolean verifyCurrentQuestionStatusChangesToRed() {
        waitForElementToBeVisible(currentQuestionNotAnswered);
        return  currentQuestionNotAnswered.isDisplayed();
    }


    public boolean verifyCurrentQuestionIsMarkedForReview() {
        waitForElementToBeVisible(currentQuestionMarkedForReview);
        return  currentQuestionMarkedForReview.isDisplayed();
    }

    public void verifyQuestionIsAnsweredAndMarkForReview(int questionNumber) {
        Assert.assertEquals(Integer.parseInt(answeredAndMarkedForReviewStatus.getText()), questionNumber, String.format("The Question answered and marked for review is %d but shown as %s",questionNumber,answeredAndMarkedForReviewStatus.getText()));
    }
    public boolean verifyCurrentQuestionStatusChangesToBlue(){
        waitForElementToBeVisible(answeredAndMarkedForReviewStatus);
        return  answeredAndMarkedForReviewStatus.isDisplayed();
    }

    public boolean verifyCurrentQuestionStatusChangesToGreen(){
        waitForElementToBeVisible(answeredStatus);
        return  answeredStatus.isDisplayed();
    }

    public void clickMarkForReview() {
        markForReview.click();
    }
}

