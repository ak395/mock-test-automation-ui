package page;

import driver.DriverProvider;
import entities.practice.PracticePlanner;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.Search.*;
import utils.Properties;
import verification.Verify;

import java.util.Optional;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Getter
public class SearchHomePage extends BasePage {
    private SearchPageTitleSection searchPageTitleSection;
    private StartPracticeSection searchPracticeSection;
    private TakeATestSection takeATestSection;
    private FindSomethingCoolSection findSomethingCoolSection;
    private SearchPageTextFieldSection searchPageTextFieldSection;
    private StudyResultPage studyResultPage;
    private StartPracticeSectionHeaderSection startPracticeSectionHeaderSection;
    private TakeATestHeaderSection takeATestHeaderSection;
    private FindSomethingCoolHeaderSection findSomethingCoolHeaderSection;
    private BookMarksTabSection bookMarksTabSection;
    private RegistrationSection registrationSection;

    @FindBy(css = ".glyphicon-bookmark.icon-bookmark ")
    private WebElement bookmarksTabElement;

    @FindBy(className = "test-btn")
    private WebElement clickTakeATestButton;

    public SearchHomePage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        searchPageTitleSection = new SearchPageTitleSection(driver);
        searchPracticeSection = new StartPracticeSection(driver);
        takeATestSection = new TakeATestSection();
        findSomethingCoolSection = new FindSomethingCoolSection(driver);
        searchPageTextFieldSection = new SearchPageTextFieldSection(driver);
        studyResultPage = new StudyResultPage(driver);
        startPracticeSectionHeaderSection = new StartPracticeSectionHeaderSection(driver);
        takeATestHeaderSection = new TakeATestHeaderSection(driver);
        findSomethingCoolHeaderSection = new FindSomethingCoolHeaderSection(driver);
        bookMarksTabSection = new BookMarksTabSection(driver);
        registrationSection = new RegistrationSection(driver);
    }

    public void assertHeaderToBe(String expectedHeader)
    {
        searchPageTitleSection.assertHeaderToBe(getSearchPageTitleSection().getSearchTitleElement().getText());
    }

    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }

    public void verifyTitleOfSearchPage() {
        searchPageTitleSection.assertHeaderToBe("What would you like to STUDY today?");
    }

    public void clickOnStartPracticeButton(PracticePlanner practicePlanner) {
        practicePlanner.getPracticeQuestionList().clear();
        searchPracticeSection.clickOnStartPracticeButton();
        waitForPageToLoad();
        searchPracticeSection.assertUrlHasBeenResetTo(Properties.practicePageUrl);
    }

    public void verifyStartPracticeButtonIsVisible() {
        assertTrue(searchPracticeSection.isPracticeButtonVisible(), "User is not navigated to Home Page");
    }



    public void clickOnFindSomethingCoolLink() {
        waitForPageToLoad();
        findSomethingCoolSection.clickOnFindSomethingCoolLink();
        waitForPageToLoad();
        findSomethingCoolSection.assertUrlHasBeenResetTo("https://www.embibe.com/ai");
        waitForPageToLoad();
    }

    public void clickOnSearchFieldTextSection() {
        waitForPageToLoad();
        searchPageTextFieldSection.getSearchtTextFieldElement();
        searchPageTextFieldSection.clickOnSearchTextField();
        waitForPageToLoad();
    }


    public void searchByUnAmbiguousKeyWord(String keyWord) {
        searchPageTextFieldSection.searchByKeyword(keyWord);
    }


    public void clickOnBookMarksTab() {
        waitForElementToBeVisible(bookmarksTabElement);
        bookmarksTabElement.click();
    }

    public void verifyTheQuestionIsBookMarked(){
        bookMarksTabSection.clickOnQuestionsFilter();
        boolean actualValue = bookMarksTabSection.isQuestionPresentInBookmarks();
        assertTrue(actualValue, "Bookmarked question is not present in bookmarks tab");
    }

    public void clickOnTakeATestButton(){
       waitForElementTobeClickable(clickTakeATestButton);
      jsClick(clickTakeATestButton);
    }

    public void clickTakeTestWithNewDriverSession(WebDriver driver){
        driver.findElement(By.cssSelector(".test-btn")).click();
    }
}
