package page;

import constants.Badge;
import driver.DriverProvider;
import entities.MockTestPlanner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.examSummary.overallPerformanceSection.SubjectWiseAnalysisSection;

import java.util.List;

import static constants.Badge.*;
import static org.testng.Assert.assertEquals;


public class FeedbackHomePage extends BasePage {

    private SubjectWiseAnalysisSection subjectWiseAnalysisSection;
    private String subjectSummaryGraph = "//phoenix-subject-summary[@icon='%s-grey']";

    private By statValue = By.cssSelector(".pss__stat-val");

    @FindBy(css = ".pss__stat-val")
    private WebElement statisticValue;

    @FindBy(css = ".test-feedback-wrapper")
    private WebElement testFeedBackHeader;

    public FeedbackHomePage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        subjectWiseAnalysisSection = new SubjectWiseAnalysisSection();
    }

    public List<WebElement> getStatCountForSubject(String subject) {
        waitForElementToBeVisible(statisticValue);
        return driver.findElement(By.xpath(String.format(subjectSummaryGraph, subject)))
                .findElements(statValue);
    }

    public void verifyAnswerCountFor(MockTestPlanner mockTestPlanner, Badge badge) {
        List<String> subjects = mockTestPlanner.getUniqueSubjects();

        for (String subject : subjects) {
            List<WebElement> actualStatValueForSubject = getStatCountForSubject(subject.toLowerCase());
            int expectedNumberOfQuestionsCount = mockTestPlanner.getNumberOfQuestionsFor(badge, subject);

            if (badge.equals(BADGE_TOO_FAST_CORRECT))
                assertAnswerTypeCount(expectedNumberOfQuestionsCount, actualStatValueForSubject.get(0));
            else if (badge.equals(BADGE_WASTED_ATTEMPT))
                assertAnswerTypeCount(expectedNumberOfQuestionsCount, actualStatValueForSubject.get(1));
            else if (badge.equals(BADGE_UNATTEMPTED))
                assertAnswerTypeCount(expectedNumberOfQuestionsCount, actualStatValueForSubject.get(2));
        }

    }

    public void getMarksForQuestion(MockTestPlanner mockTestPlanner) {
        mockTestPlanner.getTotalMarksForTest();
        System.out.println(mockTestPlanner.getTotalMarksForTest());

    }

    private void assertAnswerTypeCount(int ans, WebElement element) {
        assertEquals(ans, Long.parseLong(element.getText()));
    }

    public void assertFeedbackPageIsLoaded() {
        waitForElementToBeVisible(testFeedBackHeader);
        Assert.assertTrue(testFeedBackHeader.isDisplayed(), "Test Feedback Heading is not displayed");

    }

    public void verifyTheTooltipForSubjectWiseAccuracy(WebDriver driver) {
        subjectWiseAnalysisSection.hoverOnSubjectWiseAccuracyGraph(driver);
    }



}
