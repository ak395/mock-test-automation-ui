package page.rankUpLoginPage;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

@Getter
public class RankUpLoginHomePage extends BasePage {

    private LoginFormSection loginFormSection;

    public RankUpLoginHomePage() {

        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);

        loginFormSection = new LoginFormSection(driver);

    }

}
