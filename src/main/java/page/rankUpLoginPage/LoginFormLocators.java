package page.rankUpLoginPage;

import org.openqa.selenium.By;

public class LoginFormLocators {

    By email = By.id("md-input-0-input");
    By password = By.id("md-input-1-input");
    By login = By.xpath("//a[contains(text(),'LOG IN')]");
    By invalidLogin = By.xpath("//p[contains(text(), 'Invalid Login')]");
}
