package page.rankUpLoginPage;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import utils.Properties;
import verification.Verify;

import java.util.List;
import java.util.Optional;

public class LoginFormSection extends BasePage {

    private LoginFormLocators locators;

    @FindBy(className = "login-form")
    private WebElement rootElement;

    @FindBy(css = "button.loginBtn")
    private List<WebElement> loginButtons;

    @FindBy(css = ".register-user > a")
    private List<WebElement> registerHereLink;

    @FindBy(css = ".error")
    private List<WebElement> errorFields;

    @FindBy(css = ".forget-pass > a")
    private List<WebElement> forgotPassword;

    @FindBy(className = "get-rankup-btn")
    private WebElement getRankUpButton;

    @FindBy(css = "span.checkmark")
    private WebElement termsAndConditions;

    @FindBy(css = ".form-control.emailField")
    private List<WebElement> emailAndPasswordField;



    public LoginFormSection(WebDriver driver) {
        this.driver = driver;
        locators = new LoginFormLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getEmailElement() {
        return getElement(rootElement, locators.email);
    }

    public WebElement getPasswordElement() {

        return getElement(rootElement, locators.password);
    }

    public WebElement getLoginElement() {

        return getElement(rootElement, locators.login);
    }

    public WebElement getInvalidLoginElement() {
        return getElement(rootElement, locators.invalidLogin);
    }

    public void enterUserName(String userName) {
        getEmailElement().sendKeys(userName);
    }

    public void enterPassword(String password) {
        getPasswordElement().sendKeys(password);
    }


    @Step("Click on Login Button in the Header Section")
    public void clickOnLogin() {
        getLoginElement().click();
        waitForElementToBeVisible(emailAndPasswordField.get(1));
    }

    public void displayInvalidLoginMessage() {
        WebElement element = getInvalidLoginElement();
        boolean result = isElementPresent(rootElement, locators.invalidLogin);
        Verify.assertTrue(result, "result does not match", Optional.of(true));
    }

    @Step("Login with the given credentials")
    public void loginAs(String username, String password) {
        emailAndPasswordField.get(2).sendKeys(username);
        emailAndPasswordField.get(3).sendKeys(password);
        loginButtons.get(1).click();
        System.out.println("user successfull login");
    }

    @Step("clicks on register here link")
    public void clickOnRegisterHereLink() {
        waitForElementToBeVisible(registerHereLink.get(1));
        jsClick(registerHereLink.get(1));
    }

    @Step("Login to RankUp with valid credentials")
    public void loginAsRankUpUser(String username, String password) {
        getEmailElement().sendKeys(username);
        getPasswordElement().sendKeys(password);
        getLoginElement().click();
        waitForPageToLoad();
        Verify.assertTrue(getRankUpButton.isDisplayed(),"Button Not Displayed",Optional.of(true));

    }

    @Step("Enter Invalid Email and verify an error message is thrown")
    public void verifyInvalidEmail() {

        emailAndPasswordField.get(2).sendKeys(Properties.invalidEmailId);
        waitForElementTobeClickable(loginButtons.get(1));
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(2).getText(), "Invalid Credentials!", "Text Not Matching", Optional.of(true));

    }

    @Step("Enter an Invalid Mobile Number: More than 10 digits and verify an error message is thrown")
    public void verifyMobileNumberMoreThanTenDigits() {
        emailAndPasswordField.get(2).clear();
        emailAndPasswordField.get(2).sendKeys(Properties.phoneNoMoreThanTenDigits);
        waitForElementTobeClickable(loginButtons.get(1));
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(2).getText(), "Invalid Credentials!", "Text Not Matching", Optional.of(true));
    }
    @Step("Enter an Invalid Mobile Number: Less than 10 digits and verify an error message is thrown")
    public void verifyMobileNumberLessThanTenDigits() {
        emailAndPasswordField.get(2).clear();
        emailAndPasswordField.get(2).sendKeys(Properties.phoneNoLessThanTenDigits);
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(2).getText(), "Invalid Credentials!", "Text Not Matching", Optional.of(true));

    }
    @Step("Without Entering data in any field click on signup button, verify error messages are thrown")
    public void verifyAllFieldsAreBlank() {
        termsAndConditions.click();
        waitForElementTobeClickable(loginButtons.get(1));
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(2).getText(), "Please enter your Email!", "Text not Matching", Optional.of(true));
        Verify.assertEquals(errorFields.get(3).getText(), "Please enter your Password!", "Text not Matching", Optional.of(true));
        Verify.assertEquals(errorFields.get(4).getText(), "Please enter your Passwordconfirm!", "Text no Matching", Optional.of(true));
    }

    @Step("Verify Error message is thrown when entering different passwords during Sign Up")
    public void verifyPasswordMismatch() {
        emailAndPasswordField.get(2).clear();
        emailAndPasswordField.get(2).sendKeys(Properties.invalidUserName);
        emailAndPasswordField.get(3).sendKeys(Properties.invalidPassword);
        emailAndPasswordField.get(4).sendKeys(Properties.invalidConfirmPassword);
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(3).getText(), "Password did not match!", "Text not matching", Optional.of(true));

    }
    @Step("Verify an error message is thrown while trying to login with unregistered Email Id")
    public void verifyUnregisteredEmailId() {
        emailAndPasswordField.clear();
        emailAndPasswordField.get(2).sendKeys(Properties.unregisteredEmailId);
        emailAndPasswordField.get(3).sendKeys(Properties.invalidPassword);
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(2).getText(), "Username is not registered", "Text not Matching", Optional.of(true));
    }

    @Step("Verify an error message is thrown while trying to login with unregistered Mobile Number")
    public void verifyUnregisteredPhoneNo() {
        emailAndPasswordField.clear();
        emailAndPasswordField.get(2).sendKeys(Properties.unregisteredMobileNo);
        emailAndPasswordField.get(3).sendKeys(Properties.invalidPassword);
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(2).getText(), "Username is not registered", "Text not Matching", Optional.of(true));
    }

    @Step("Verify an Error message is thrown username and password is not matching to the database while log in")
    public void verfiyUnamePaswordNotMatching() {
        emailAndPasswordField.get(2).clear();
        emailAndPasswordField.get(2).sendKeys(Properties.invalidUserName);
        emailAndPasswordField.get(3).sendKeys(Properties.invalidPassword);
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(3).getText(),"Password is invalid","Text not matching",Optional.of(true));
    }

    @Step("Click on Forgot Password Link")
    public void clickOnForgotPasswordLink() {
        forgotPassword.get(1).click();
    }

    @Step("Verify error message is thrown when trying to Login without entering any details")
    public void verifyAllLoginFieldsAreBlank() {
        emailAndPasswordField.get(2).clear();
        emailAndPasswordField.get(3).clear();
        loginButtons.get(1).click();
        Verify.assertEquals(errorFields.get(2).getText(), "Please enter your Email!", "Text not Matching", Optional.of(true));
        Verify.assertEquals(errorFields.get(3).getText(), "Please enter your Password!", "Text not Matching", Optional.of(true));
    }

}
