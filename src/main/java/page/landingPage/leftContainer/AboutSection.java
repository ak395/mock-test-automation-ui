package page.landingPage.leftContainer;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

public class AboutSection extends BasePage {

    private AboutSectionLocators locators;

    @FindBy(id = "about")
    private WebElement rootElement;

    public AboutSection(WebDriver driver) {
        this.driver = driver;
        locators = new AboutSectionLocators();
        PageFactory.initElements(driver, this);
    }

    private WebElement getStartNowElement() {

        return getElement(rootElement,locators.startNow);
    }

    @Step("User is clicking on Start Now Button")
    public void clickStartNowButton() {
        waitForPageToLoad();
        getStartNowElement().click();
        waitForPageToLoad();
    }

    @Step("User sees URL to be : {url}")
    public void assertUrlHasSetTo(String url) {
        wait(5000);
        Verify.assertEquals(driver.getCurrentUrl(),url,"url not matching", Optional.of(true));
    }

}
