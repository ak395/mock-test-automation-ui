package page.FooterPage;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;

public class Footer  extends BasePage
{

    @FindBy(className = "footer-div")
    private WebElement rootElement;

    public Footer(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @Step("User clicks on all the links displayed in the footer section")
    public void fetchAllLinks()
    {
        String[] links = null;
        waitForPageToLoad();
       List<WebElement> linksize = driver.findElements(By.tagName("a"));
      //  List<WebElement> linksize=driver.findElements(By.xpath("//div[@class='row']"));
        int linksCount = linksize.size();
        System.out.println("Total no of links Available: "+linksCount);
          links= new String[linksCount];
          System.out.println("List of links Available: ");
            // print all the links from webpage
        for(int i=0;i<linksCount;i++)
        {
            links[i] = linksize.get(i).getAttribute("href");
            System.out.println(linksize.get(i).getAttribute("href"));
        }
      // navigate to each Link on the webpage
        for(int i=0;i<linksCount;i++)
        {
            driver.navigate().to(links[i]);
            //driver.navigate().back();
            waitForPageToLoad();
        }
    }

}
