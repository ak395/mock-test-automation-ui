package page;

import driver.DriverProvider;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.support.PageFactory;
import page.jump.GetJumpPacksTitleSection;
import page.jump.GetJumpSection;

@Getter
@Setter
public class JumpHomePage extends BasePage {

    private GetJumpSection jumpPageSection;
    private GetJumpPacksTitleSection jumpPacksTitleSection;


    public JumpHomePage() {
        driver = DriverProvider.getDriver();

        PageFactory.initElements(driver, this);

        jumpPageSection = new GetJumpSection(driver);
        jumpPacksTitleSection = new GetJumpPacksTitleSection(driver);

    }

    public void clickonjump()
    {
        jumpPageSection.getjumpbuttonElement();
        jumpPacksTitleSection.assertHeaderToBe("Get your unfair advantage. JUMP NOW.");
        jumpPageSection.clickOnGetJumpButton();


    }
}
