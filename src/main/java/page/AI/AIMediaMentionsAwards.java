package page.AI;
import io.qameta.allure.Step;
import org.apache.http.HttpResponse;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import page.BasePage;
import page.UtilityClassess.LinkUtility;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static org.testng.AssertJUnit.assertEquals;

public class AIMediaMentionsAwards extends BasePage
{
    private AIPageLocators locators;
    @FindBy(className = "AI__features")
    public WebElement rootElement;

    public AIMediaMentionsAwards(WebDriver driver)
    {
        this.driver = driver;
        locators = new AIPageLocators();
        PageFactory.initElements(driver, this);
    }

    @Step("User finds the Media Section Element")
    public WebElement getMediaSection() {
        return getElement(rootElement, locators.mediaSection);
    }

    @Step("User finds the Title of the Media Section")
    public WebElement getMediaSectionTitle() {
        return getElement(rootElement, locators.mediaSectionTitle);
    }

    @Step("Scroll down to the Media and Awards Section")
    public void scrollToMediaAndAwardsSection()
    {
        waitForPageToLoad();
        Actions actions = new Actions(driver);
        int x = getMediaSection().getLocation().getX();
        int y = getMediaSection().getLocation().getY();
        actions.moveToElement(getMediaSection(), x, y).build().perform();
        waitForPageToLoad();
    }

    @Step("User verifies the Title of the Media and Awards Section")
    public void assertMediaSectionHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        System.out.println(getMediaSectionTitle().getText());
        assertEquals(getMediaSectionTitle().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }

    @Step("User fetch all links from the media section and clicks all the links")
    public void clickAllLinks()
    {
        //driver.findElement(By.xpath("//div[@id='features']//a[@href]"));
        Map<Integer, List<String>> map = driver.findElements(By.xpath("//div[@id='features']//a[@href]")).stream()                             // find all elements which has href attribute & process one by one
                .map(ele -> ele.getAttribute("href")) // get the value of href
                .map(String::trim)                    // trim the text
                .distinct()                           // there could be duplicate links , so find unique
                .collect(Collectors.groupingBy(LinkUtility::getResponseCode)); // group the links based on the response code

        map.get(200).stream().forEach(System.out::println);

        waitForPageToLoad();
        for (Map.Entry<Integer, List<String>> mp : map.entrySet()) {
            System.out.print(mp.getKey() + " | ");
            for (String url : mp.getValue())
            {
                driver.get(url);
                // waitForPageToLoad();
            }
            System.out.println();
        }


    }

    public void getLinks() {
        try {
            List<WebElement> links = driver.findElements(By.xpath("//div[@id='features']//a[@href]"));
            int linkcount = links.size();
            System.out.println(links.size());
          /*  for (WebElement myElement : links)
            {
                String link = myElement.getText();
                if (link !="")
                {
                    myElement.click();
                }
            }*/
            for (WebElement element : driver.findElements(By.xpath("//div[@id='features']//a[@href]")))
            {
                String link = element.getText();
                String value=element.getAttribute("href");

                if (link !="")
                {
                    element.click();
                    URL myUrl=new URL(value);
                    HttpURLConnection connection = (HttpURLConnection) myUrl.openConnection();
                    connection.setRequestMethod("HEAD");
                    int code = connection.getResponseCode();
                    Assert.assertEquals(code, 200);
                }

            }
        }
        catch (Exception e)
        {
            System.out.println("error "+e);
        }
    }
}









