package page.AI;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class AIVideoSection extends BasePage
{
    private AIPageLocators locators;
    @FindBy(className = "AI__video")
    private WebElement rootElement;

    public AIVideoSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new AIPageLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getVideoPlayButtonElement()
    {
        return getElement(rootElement,locators.videoPlayButton);

    }

    public void clickOnVideoPlayButton()
    {
        waitForPageToLoad();
        Actions actions=new Actions(driver);
        int x=getVideoPlayButtonElement().getLocation().getX();
        int y=getVideoPlayButtonElement().getLocation().getY();
        actions.moveToElement(getVideoPlayButtonElement(),x,y).build().perform();
        getVideoPlayButtonElement().click();
        wait(10000);
        waitForPageToLoad();


    }
}
