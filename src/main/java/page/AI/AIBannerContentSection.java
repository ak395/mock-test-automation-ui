package page.AI;
import io.qameta.allure.Step;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
@Getter
@Setter
public class AIBannerContentSection extends BasePage
{
    private AIPageLocators locators;
    @FindBy(className = "AI-banner__content")
    private WebElement rootElement;

    public AIBannerContentSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new AIPageLocators();
        PageFactory.initElements(driver,this);
    }

    @Step("User finds the Research Button Element present in the left section of the AI Page")
    public WebElement getResearchButtonElement()
    {
        return getElement(rootElement,locators.researchButton);
    }

    @Step("User finds the OpenProblems Button Element present in the left section of the AI Page")
    public WebElement getOpenProblemsButtonElement()
    {
        return getElement(rootElement,locators.openProblemsButton);
    }

    @Step("User clicks on the Research Button")
    public void clickOnResearchButton()
    {
        waitForPageToLoad();
        getResearchButtonElement().click();
        waitForPageToLoad();
    }

    @Step("User clicks on the Open Problems Button")
    public void clickOnOpenProblemsButton()
    {
        waitForPageToLoad();
        getOpenProblemsButtonElement().click();
        waitForPageToLoad();
    }

}
