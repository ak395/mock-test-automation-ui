package page.AI;
import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;
import java.util.Optional;
import static org.testng.Assert.assertEquals;
public class AIBannerCarouselCardSection extends BasePage
{
    private AIPageLocators locators;
    @FindBy(className = "divam-wrapper")
    public WebElement rootElement;

    public AIBannerCarouselCardSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new AIPageLocators();
        PageFactory.initElements(driver,this);
    }

    @Step("User finds the AI Banner Card Section element")
    public WebElement getAIBanner_Carousel_cardSectionElement()
    {
        return getElement(rootElement,locators.aiBanner_Carousel_cardSection);
    }

    @Step("User finds the AI Banner Card Section Title Element")
    public WebElement getAIBanner_Carousel_cardSection_TitleElement()
    {
        return getElement(rootElement,locators.getAiBanner_Carousel_cardSection_Title);
    }

    @Step("User finds the Get Read More Link Element")
    public WebElement getReadMoreLinkElement()
    {
        return getElement(rootElement,locators.readMoreLink);
    }

    @Step("User clicks on the AI_Banner Carousel Card Section")
    public void clickOnAIBannerCarouselCardSection()
    {
        waitForPageToLoad();
        getAIBanner_Carousel_cardSectionElement().click();
        waitForPageToLoad();
    }

    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url)
    {
        waitForPageToLoad();
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
        waitForPageToLoad();
    }

    @Step("User scrolls down to the AI Banner Carousel Card Content Section")
    public void scrollToAIBannerCarouselCardContentSection()
    {
        waitForPageToLoad();
        try
        {
            wait(5000);
        }
        catch (Exception e)
        {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollInToView();",locators.getAiBanner_Carousel_cardSection_Title);
        }
    }
    @Step("User verifies the title of the content of the - AI Banner Carousel Card Section- Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        Actions actions=new Actions(driver);
        int x=getAIBanner_Carousel_cardSection_TitleElement().getLocation().getX();
        int y=getAIBanner_Carousel_cardSection_TitleElement().getLocation().getY();
        actions.moveToElement(getAIBanner_Carousel_cardSection_TitleElement(),x,y).build().perform();
        assertEquals(getAIBanner_Carousel_cardSection_TitleElement().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }

    @Step("User scrolls down to the Read More Link Of - AI Banner Carousel Card - Section")
    public void scrollToReadMoreLink()
    {
        waitForPageToLoad();
        try
        {
            wait(5000);
        }
        catch (Exception e)
        {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollInToView();",locators.readMoreLink);
        }
    }

    @Step("User clicks on the Read More Link of the -AI Banner Carousel Card - section")
    public void clickOnReadMoreLink()
    {
        waitForPageToLoad();
        getReadMoreLinkElement();
        waitForPageToLoad();
        int x=getReadMoreLinkElement().getLocation().getX();
        int y=getReadMoreLinkElement().getLocation().getY();
        Actions actions=new Actions(driver) ;
        actions.moveToElement(getReadMoreLinkElement(),x,y).build().perform();
        getReadMoreLinkElement().click();
        waitForPageToLoad();
    }
}
