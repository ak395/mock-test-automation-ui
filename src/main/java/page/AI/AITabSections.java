package page.AI;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import static org.testng.AssertJUnit.assertEquals;
@Getter
public class AITabSections extends BasePage
{
    private AIPageLocators locators;
    @FindBy(className = "AI-tabs")
    private WebElement rootElement;

    public AITabSections(WebDriver driver)
    {
        this.driver=driver;
        locators=new AIPageLocators();
        PageFactory.initElements(driver,this);

    }

    public WebElement getTabSectionsTitleElement()
    {
        return getElement(rootElement,locators.tabSectionsTitle);
    }
     public WebElement getMentorIntelligenceTab()
     {
         return getElement(rootElement,locators.mentorIntelligenceTab);
     }
     public WebElement getOpenProblemsTab()
     {
         return getElement(rootElement,locators.openProblemsTab);
     }

     public WebElement getContentIntelligenceTab()
     {
         return getElement(rootElement,locators.contentIntelligenceTab);
     }

     public WebElement getStudentIntelligenceTab()
     {
         return getElement(rootElement,locators.studentIntelligenceTab);
     }

     public WebElement getResearchBlogTab()
     {
         return getElement(rootElement,locators.researchBlogTab);
     }

     public WebElement getMentorIntelligenceTabContentTitle()
     {
         return getElement(rootElement, locators.mentorIntelligenceTabContentTitle);
     }
     public WebElement getContentIntelligenceTabContentTitle()
     {
         return getElement(rootElement,locators.contentIntelligenceTabContentTitle);
     }
     public WebElement getStudentIntelligenceTabContentTitle()
     {
         return getElement(rootElement,locators.studentIntelligenceTabContentTitle);
     }
     public WebElement getOpenProblemsTabLeftContentTitle()
        {
            return getElement(rootElement,locators.openProblemsTabLeftContentTitle);
        }
        public WebElement getOpenProblemsTabRightContentTitle()
        {
            return getElement(rootElement,locators.openProblemsTabRightContentTitle);
        }
        public WebElement getResearchBlogContentTitle()
        {
            return getElement(rootElement,locators.researchBlogTabContentTitle);

        }
    @Step("User verifies the title of the AI Page")
    public void assertHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        assertEquals(getTabSectionsTitleElement().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }

    @Step("User Verifies the Content-Title of the Mentor Intelligence Tab ")
    public void assertMentorIntelligenceTabContentHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        assertEquals(getMentorIntelligenceTabContentTitle().getText().trim(),expectedHeader);
        waitForPageToLoad();
    }

    @Step("User scrolls down to the Tab sections")
    public void scrollToTabSections()
    {
        waitForPageToLoad();
        try
        {
            wait(5000);
        }
        catch (Exception e)
        {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollInToView();",locators.tabSections);
        }
    }

    @Step("User clicks on the Mentor Intelligence Tab")
    public void clickOnMentorIntelligenceTab()
    {
        waitForPageToLoad();
        getMentorIntelligenceTab().click();
        waitForPageToLoad();
    }

    @Step("User clicks on the Content Intelligence Tab")
    public void clickOnContentIntelligenceTab()
    {
        waitForPageToLoad();
        getContentIntelligenceTab().click();
        waitForPageToLoad();
    }

    @Step("User verifies the Content Title of the Content Intelligence Tab")
    public void assertContentIntelligenceTabContentHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        System.out.println(getContentIntelligenceTabContentTitle().getText());
        assertEquals(getContentIntelligenceTabContentTitle().getText().trim(),expectedHeader);
        waitForPageToLoad();
    }

    @Step("User clicks on the Student intelligence Tab ")
    public void clickOnStudentIntelligenceTab()
    {
        waitForPageToLoad();
        getStudentIntelligenceTab().click();
        waitForPageToLoad();

    }

    @Step("User verifies the Content Title of the Student Intelligence Tab")
    public void assertStudentIntelligenceTabContentHeaderToBe(String expectedHeader)
    {
        waitForPageToLoad();
        assertEquals(getStudentIntelligenceTabContentTitle().getText().trim(),expectedHeader);
    }

    @Step("User scrolls down to the Open Problem Tab sections")
    public void scrollToOpenProblemsTabSections()
    {
        waitForPageToLoad();
        try
        {
            wait(5000);
        }
        catch (Exception e)
        {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollInToView();",locators.openProblemsTab);
        }
    }
    @Step("User clicks on the Open Problems Tab")
    public void clickOnOpenProblemsTab()
    {
        waitForPageToLoad();
        getOpenProblemsTab().click();
        waitForPageToLoad();

    }

    @Step("User verifies the Title of the Left Content Section ")
    public void assertOpenProblemsTableftContentTitle(String expectedHeader)
    {
        waitForPageToLoad();
        System.out.println(getOpenProblemsTabLeftContentTitle().getText());
        assertEquals(getOpenProblemsTabLeftContentTitle().getText().trim(),expectedHeader);
        waitForPageToLoad();
    }

    @Step("User verifies the Title of the Right Content section")
    public void assertOpenProblemsRightContentTitle(String expectedHeader)
    {
        waitForPageToLoad();
        System.out.println(getOpenProblemsTabRightContentTitle().getText());
        assertEquals(getOpenProblemsTabRightContentTitle().getText().trim(),expectedHeader);
        waitForPageToLoad();
    }
    @Step("User scrolls down to the Research Blog Tab sections")
    public void scrollToResearchBlogTabSections()
    {
        waitForPageToLoad();
        try
        {
            wait(5000);
        }
        catch (Exception e)
        {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollInToView();",locators.researchBlogTab);
        }
    }
    @Step("User clicks on the Research Blog Tab")
    public void clickOnResearchBlogTab()
    {
        waitForPageToLoad();
        getResearchBlogTab().click();
        waitForPageToLoad();
    }

    @Step("User verifies the Title of the Content section for ResearchBlog")
    public void assertResearchBlogContentTitle(String expectedHeader)
    {
        waitForPageToLoad();
        System.out.println(getResearchBlogContentTitle().getText());
        assertEquals(getResearchBlogContentTitle().getText().trim(),expectedHeader);
        waitForPageToLoad();
}}
