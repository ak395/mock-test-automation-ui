package page.AI;
import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class ContactUsContainerSection extends BasePage
{
    private AIPageLocators locators;
    @FindBy(className = "contact-us-container")
    private WebElement rootElement;

    public ContactUsContainerSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new AIPageLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getContactUsContainerElement()
    {
        return getElement(rootElement,locators.contactUsContainer);
    }

    @Step("User scrolls down to the Contact Us section")
    public void ScrollToContactUsSection()
    {
        waitForPageToLoad();
        try
        {
            wait(5000);
        }
        catch(Exception e)
        {
            ((JavascriptExecutor)driver).executeScript("arguments[0].scrollInToView();",locators.contactUsContainer);
        }
    }

    @Step("User clicks on the Contact Us section")
    public void clickOnContactUsSection()
    {
        waitForPageToLoad();
        Actions actions=new Actions(driver);
        int x=getContactUsContainerElement().getLocation().getX();
        int y=getContactUsContainerElement().getLocation().getY();
        waitForPageToLoad();
        actions.moveToElement(getContactUsContainerElement(),x,y).build().perform();
        getContactUsContainerElement().click();
        waitForPageToLoad();

    }


}
