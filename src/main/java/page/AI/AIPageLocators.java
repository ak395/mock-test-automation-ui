package page.AI;
import org.openqa.selenium.By;
import page.BasePage;
public class AIPageLocators extends BasePage
{
    By researchButton=By.cssSelector("div[class='u-display-flex u-cursor-pointer']");
    By openProblemsButton=By.cssSelector("div[class='u-cursor-pointer u-display-flex']");
    By tabSectionsTitle =By.cssSelector("p[class='AI-tabs__heading']");
    By mentorIntelligenceTab=By.cssSelector("#tab2");
    By mentorIntelligenceTabContentTitle=By.xpath("//p[@class='AI-tabs-content__content--header'][contains(text(),'Behavioural Nudges That Work for Learning Outcomes')]");
    By openProblemsTab=By.cssSelector("div[id='tab3']");
    By openProblemsTabLeftContentTitle =By.xpath("//p[contains(text(),'Automated Discovery of Knowledge Graph')]");
    By openProblemsTabRightContentTitle=By.xpath("//p[contains(text(),'Auto-classification of Relationships between Knowl')]");
    By contentIntelligenceTab=By.cssSelector("div[id='tab1']");
    By contentIntelligenceTabContentTitle=By.xpath("//p[@class='AI-tabs-content__content--header'][contains(text(),'Intelligent Content Ingestion')]");
    By studentIntelligenceTab=By.cssSelector("div[id='tab0']");
    By studentIntelligenceTabContentTitle=By.xpath("//p[@class='AI-tabs-content__content--header'][contains(text(),'Attributes That Create Learning Outcomes')]");
    By researchBlogTab=By.cssSelector("div[id='tab4']");
    By researchBlogTabContentTitle=By.xpath("//p[@class='AI-tabs-content__content--header']");
    By aiBanner_Carousel_cardSection=By.xpath("//div[@class='AI-banner__carousel-block--card-image']");
    By getAiBanner_Carousel_cardSection_Title=By.cssSelector("p[class='AI-detail__content--title']");
    By readMoreLink=By.cssSelector("a[href='/ai-detail?id=9']");
    By tabSections =By.cssSelector("div[id='tabSection']");
    By contactUsContainer=By.cssSelector("div[class='contact-us-content']");
    By videoPlayButton=By.cssSelector("img[class='AI__video--block--play-button']");
    By mediaSection=By.id("features");
    By mediaSectionTitle=By.xpath("//p[@id='features-heading']");
}
