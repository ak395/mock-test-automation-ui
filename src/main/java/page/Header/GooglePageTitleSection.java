package page.Header;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static org.testng.Assert.assertEquals;

public class GooglePageTitleSection extends BasePage
{
    private HeaderLocators locators;
    @FindBy(className = "VmOpGe")
    private WebElement rootElement;

    public GooglePageTitleSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new HeaderLocators();
        PageFactory.initElements(driver,this);
    }
    public WebElement getGoogleTitle()
    {
        return getElement(rootElement,locators.googleTitle);
    }
@Step("User verifies the Title of the Google Page")
    public void assertTitleOfGooglePage(String expectedHeader)
{
    waitForPageToLoad();
    assertEquals(getGoogleTitle().getText().trim(),expectedHeader);
    waitForPageToLoad();
}

}
