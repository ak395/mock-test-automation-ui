package page.Header;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class FaceBookLogin extends BasePage
{
    private HeaderLocators locators;
    @FindBy(className = "headerWrapper")
    private WebElement rootElement;

    public FaceBookLogin(WebDriver driver)
    {
        this.driver=driver;
        locators=new HeaderLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getFaceBookLoginButton()
    {
        return getElement(rootElement,locators.faceBookLoginButton);
    }
    public WebElement getEmbibeLoginButton()
    {
        return getElement(rootElement,locators.loginButton);
    }


    public void clickOnFaceBookLoginButton()
    {
        getEmbibeLoginButton().click();
        getFaceBookLoginButton().click();
        waitForPageToLoad();
}


}
