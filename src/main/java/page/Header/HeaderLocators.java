package page.Header;

import org.openqa.selenium.By;
import page.BasePage;

public class HeaderLocators extends BasePage {

    By jumpLink= By.linkText("JUMP");
    By studyLink=By.linkText("STUDY");
    By rankupLink=By.linkText("RANKUP");
    By aiLink=By.linkText("AI");
    By instituteLink=By.linkText("INSTITUTE");
    By askLink=By.linkText("Ask");
    By embibeLogo=By.xpath("//img[@class='logoImage img-responsive']");
    By loginButton=By.xpath("//button[text()='Login']");
    By emailTextField=By.xpath("//div[@class='tooltip-modal']//input[@id='emailArea']");
    By passwordTextField=By.xpath("//div[@class='tooltip-modal']//input[@id='passwordArea']");
    By loginButton1=By.xpath("//div[@class='tooltip-modal']//button[@class='btn btn btn-custom btn-block loginBtn BtnText'][contains(text(),'Login')]");
    By dropDownButton=By.xpath("//div[@class='user-dropdown']");
    By logoutButton=By.xpath("//a[contains(text(),'Logout')]");
    By faceBookLoginButton=By.xpath("//div[@class='tooltip-modal']//button[@id='btn-facebook']");
    By faceBookTitleSection=By.xpath("//span[text()='Log in to Facebook']");
    By googleLoginButton=By.xpath("//div[@class='tooltip-modal']//button[@id='btn-google']");
    By googleTitle=By.xpath("//div[text()='Sign in with Google']");




}
