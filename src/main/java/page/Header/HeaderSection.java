package page.Header;

import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

@Getter

public class HeaderSection extends BasePage {
    private HeaderLocators locators;

    @FindBy(className = "headerWrapper")
    private WebElement rootElement;


    public HeaderSection(WebDriver driver)
    {
        this.driver = driver;
        locators = new HeaderLocators();
        PageFactory.initElements(driver, this);

    }
    public WebElement getEmbibeLogoElement()
    {
        return getElement(rootElement,locators.embibeLogo);
    }

    public WebElement getAskLinkElement()
    {
        return getElement(rootElement,locators.askLink);
    }

    public WebElement getStudyLinkElement()
    {
        return getElement(rootElement,locators.studyLink);
    }

    public WebElement getJumpLinkElement()
    {
        return getElement(rootElement,locators.jumpLink);
    }

    public WebElement getRankupLinkElement()
    {
        return getElement(rootElement,locators.rankupLink);
    }

    public WebElement getAILinkElement()
    {
        return getElement(rootElement,locators.aiLink);
    }

    public WebElement getInstituteLinkElement()
    {
        return getElement(rootElement,locators.instituteLink);
    }

    public WebElement getLoginButtonElement()
    {
        return getElement(rootElement,locators.loginButton);
    }

    @Step("User clicks on the Embibe Logo")
    public void clickOnEmbibeLogo()
    {
        waitForPageToLoad();
        getEmbibeLogoElement().click();
        waitForPageToLoad();
    }

    @Step("User clicks on the Ask Link")
    public void clickOnAskLink()
    {
        waitForPageToLoad();
        getAskLinkElement().click();
        waitForPageToLoad();
    }

    @Step("User clicks on the Study Link")
    public void clickOnStudyLink()
    {
        waitForPageToLoad();
        getStudyLinkElement().click();
        waitForPageToLoad();
    }


    @Step("User clicks on Jump Link displayed in the header section")
    public void clickOnJumpLink()
    {
        waitForPageToLoad();
        getJumpLinkElement().click();
        waitForPageToLoad();

    }

    @Step("User clicks on the Rankup Link displayed in the header section ")
    public void clickOnRankupLink()
    {
        waitForPageToLoad();
        getRankupLinkElement().click();
        waitForPageToLoad();
    }

    @Step("User clicks on the AI link displayed in the header section")
    public void clickOnAILink()
    {
        waitForPageToLoad();
        getAILinkElement().click();
        waitForPageToLoad();
    }

    @Step("User clicks on the Institute Link displayed in the header section")
    public void clickOnInstituteLink()
    {
        waitForPageToLoad();
        getInstituteLinkElement().click();
        waitForPageToLoad();
    }
    @Step("User sees the URL to be :{url}")
    public void assertUrlHasBeenResetTo(String url)
    {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));
    }

    @Step("User clicks on the Login button displayed at the header section")
    public void clickOnLoginButton()
    {
        waitForPageToLoad();
        getLoginButtonElement().click();
        waitForPageToLoad();
    }

}
