package page.Header;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class GoogleLogin extends BasePage
{
    private HeaderLocators locators;
    @FindBy(className = "headerWrapper")
    private WebElement rootElement;

    public GoogleLogin(WebDriver driver)
    {
        this.driver=driver;
        locators=new HeaderLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getLoginButton()
    {
        return getElement(rootElement,locators.loginButton);
    }
    public WebElement getGoogleLoginbutton()
    {
        return getElement(rootElement,locators.googleLoginButton);
    }

    @Step("User clicks on the Google Login button")
    public void clickOnGoogleButton()
    {
        waitForPageToLoad();
        getLoginButton().click();
        getGoogleLoginbutton().click();
        waitForPageToLoad();
    }
}
