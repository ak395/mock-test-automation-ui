package page.Header;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class EmbibeLogin extends BasePage
{
    private HeaderLocators locators;
    @FindBy(className = "headerWrapper")
    private WebElement rootElement;

    public EmbibeLogin(WebDriver driver)
    {
        this.driver=driver;
        locators=new HeaderLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getEmbibeLoginButton()
    {
        return getElement(rootElement,locators.loginButton);
    }

    public WebElement getEmailTextField()
    {
        return getElement(rootElement,locators.emailTextField);
    }

    public WebElement getPasswordTextField()
    {
        return getElement(rootElement,locators.passwordTextField);
    }

    public WebElement getLoginButton1()
    {
        return getElement(rootElement,locators.loginButton1);
    }

    public WebElement getDropdownButton()
    {
        return getElement(rootElement,locators.dropDownButton);
    }
    public WebElement getLogoutButton()
    {
        return getElement(rootElement,locators.logoutButton);
    }

    @Step("Enter the email id and password from the excel sheet")
    public void loginForEmbibeUsers() throws IOException {
        waitForPageToLoad();
        getEmbibeLoginButton().click();
        Properties properties=new Properties();
        FileInputStream fileInputStream=new FileInputStream("C:\\Users\\DELL\\git\\master\\src\\main\\java\\Config.Properties");
        properties.load(fileInputStream);
        waitForPageToLoad();
        getEmailTextField().sendKeys(properties.getProperty("user"));
        getPasswordTextField().sendKeys(properties.getProperty("password"));
        getLoginButton1().click();
        waitForPageToLoad();
        getDropdownButton().click();
        getDropdownButton().click();


    }
}
