package page.Header;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.AssertJUnit;
import page.BasePage;

public class FaceBookPageTitleSection extends BasePage
{
    private HeaderLocators locators;
    @FindBy(id = "globalContainer")
    private WebElement rootElement;

    public FaceBookPageTitleSection(WebDriver driver)
    {
        this.driver=driver;
        locators=new HeaderLocators();
        PageFactory.initElements(driver,this);
    }

    public WebElement getFaceBookTitle()
    {
        return getElement(rootElement,locators.faceBookTitleSection);
    }


    @Step("User verifies the Title of the Content section for ResearchBlog")
    public void assertFaceBookTitle(String expectedHeader) {
        waitForPageToLoad();
        System.out.println(getFaceBookTitle().getText());
        AssertJUnit.assertEquals(getFaceBookTitle().getText().trim(), expectedHeader);
        waitForPageToLoad();
    }

}
