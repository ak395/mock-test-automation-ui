package page.practiceQuestionsPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

class SingleChoiceQuestionSection extends BasePage {

    SingleChoiceQuestionSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    void answerQuestion(String answer, String questionCode) {
        try {
            waitForElementToBeDisplay(driver.findElement(By.id(answer)));
            driver.findElement(By.id(answer)).click();
        }catch (Exception ignore){

            switch(answer.trim().split("-")[1]){
                case "1":
                    driver.findElement(By.id(questionCode + "-a")).click();
                    break;
                case "2":
                    driver.findElement(By.id(questionCode + "-b")).click();
                    break;
                case "3":
                    driver.findElement(By.id(questionCode + "-c")).click();
                    break;
                case "4":
                    driver.findElement(By.id(questionCode + "-d")).click();
                    break;
                case "a":
                    driver.findElement(By.id(questionCode + "-1")).click();
                    break;
                case "b":
                    driver.findElement(By.id(questionCode + "-2")).click();
                    break;
                case "c":
                    driver.findElement(By.id(questionCode + "-3")).click();
                    break;
                case "d":
                    driver.findElement(By.id(questionCode + "-4")).click();
                    break;
            }
        }
    }

}
