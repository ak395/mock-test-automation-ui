package page.practiceQuestionsPage;

import constants.QuestionType;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;
import java.util.NoSuchElementException;

import static org.testng.Assert.assertEquals;

@Getter
class PracticeQuestionSection extends BasePage {

    private PracticeQuestionAttemptSection practiceQuestionAttemptSection;
    private SingleChoiceQuestionSection singleChoiceQuestionSection;
    private MultipleChoiceQuestionSection multipleChoiceQuestionSection;

    @FindBy(css = ".button-check.button-check-box-active")
    private WebElement checkAnswerButton;

    @FindBy(css = ".button-skip")
    private WebElement skipButton;

    @FindBy(css = ".question-options-container")
    private WebElement questionsOptionContainerElement;

    @FindBy(css = "div.solution-header-minimize-fallback")
    private WebElement minimizeSolutionButton;

    @FindBy(css = "div.maximize-ui-button")
    private WebElement maximizeSolutionButton;

    @FindBy(css = "div.solution-title")
    private WebElement solutionPopup;

    @FindBy(css = ".knowledge-tree")
    private WebElement knowledgeTreeWidget;

    @FindBy(css = "img.modal-close")
    private WebElement closeKnowledgeTreePopUp;

    PracticeQuestionSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        practiceQuestionAttemptSection = new PracticeQuestionAttemptSection(driver);
        singleChoiceQuestionSection = new SingleChoiceQuestionSection(driver);
        multipleChoiceQuestionSection = new MultipleChoiceQuestionSection(driver);
    }

    void clickOnSkipButton() {
        waitForElementToBeVisible(skipButton);
        skipButton.click();
    }

    private void clickOnCheckAnswerButton() {
        waitForElementToBeDisplay(checkAnswerButton);
        checkAnswerButton.click();
    }

    String getActiveQuestionCodeFromDom() {
        //waitForElementToBeVisible(skipButton);
        waitForElementToBeVisible(questionsOptionContainerElement);
        return questionsOptionContainerElement.getAttribute("id");
    }

    boolean isSkipButtonDisplayed() {
        waitForElementToBeVisible(skipButton);
        return skipButton.isDisplayed();
    }

    void clickOnKnowledgeTreeWidget(){
        wait(5000);
        waitForElementToBeVisible(knowledgeTreeWidget);
        knowledgeTreeWidget.click();
    }

    void closeKnowledgeTreeWidget() {
        waitForElementToBeVisible(closeKnowledgeTreePopUp);
        closeKnowledgeTreePopUp.click();
    }

    void verifyAttempt(String expectedAttempt) {
        String actualAttempt = getPracticeQuestionAttemptSection().getActualAttemptType();
        assertEquals(actualAttempt, expectedAttempt, "Expected attempt: " + expectedAttempt + " did not match actual attempt:" + actualAttempt + ";");
    }

    void clickOnContinueButton() {
        getPracticeQuestionAttemptSection().clickOnContinueButton();
    }


    void clickOnMinimizeSolutionButton() {
        waitForElementToBeVisible(minimizeSolutionButton);
        jsClick(minimizeSolutionButton);
    }

    void clickOnMaximizeSolutionButton() {
        waitForElementToBeVisible(maximizeSolutionButton);
        jsClick(maximizeSolutionButton);
    }

    boolean verifySolutionIsVisible() {
        return solutionPopupIsVisible();
    }

    private boolean solutionPopupIsVisible() {
        waitForElementToBeVisible(solutionPopup);
        return solutionPopup.isDisplayed();
    }

    void answer(List<String> answers, String questionCategory, String questionCode) {
        switch (questionCategory) {
            case QuestionType.SINGLE_CHOICE:
                try {
                    getSingleChoiceQuestionSection().answerQuestion(answers.get(0), questionCode);
                    clickOnCheckAnswerButton();
                    break;
                } catch (NoSuchElementException ex) {
                    getSingleChoiceQuestionSection().answerQuestion(answers.get(0), questionCode);
                    clickOnCheckAnswerButton();
                    break;
                }


            case QuestionType.MULTIPLE_CHOICE:
                getMultipleChoiceQuestionSection().answerQuestion(answers, questionCode);
                clickOnCheckAnswerButton();
                break;
        }
    }

}
