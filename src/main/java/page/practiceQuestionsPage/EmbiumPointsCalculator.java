package page.practiceQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static page.practiceQuestionsPage.EmbiumPointsValue.*;

import static utils.AttemptType.*;

public class EmbiumPointsCalculator extends BasePage {

    static Integer totalEmbiumCoins= FIRST_TIME_USER_EMBIUMS;

    EmbiumPointsCalculator(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Integer calculateEmbiumPoints(Integer difficultyLevel,String attempt, Boolean isFirstAttempt) {

       Double embiumCoins = 0.0;
        switch (attempt) {
            case PERFECT_ATTEMPT:
                System.out.println("Difficulty Level "+ difficultyLevel);
               embiumCoins= 100*(1+Math.floor(0.67*difficultyLevel));
               break;

            case WASTED_ATTEMPT:
                embiumCoins=-(0.5*100*(1+Math.floor(0.34*difficultyLevel)));
                break;

            case OVERTIME_CORRECT:
                embiumCoins=100*(1+Math.floor(0.34*difficultyLevel));

                break;

            case OVERTIME_INCORRECT:
                embiumCoins= -(0.25*100);
                break;

            case TOO_FAST_CORRECT:
                embiumCoins = 0.0;
                break;

            case INCORRECT_ATTEMPT:
                embiumCoins=-(0.25*100);
                break;
        }

        if (isFirstAttempt) {
            embiumCoins = embiumCoins + FIRST_ANY_ATTEMPT_EMBIUMS;
        }

        totalEmbiumCoins = totalEmbiumCoins + embiumCoins.intValue();
        return totalEmbiumCoins;

    }

    public Integer embiumCoinsForFirstHint() {
        totalEmbiumCoins=totalEmbiumCoins+ FIRST_HINT_EMBIUMS;
        return  totalEmbiumCoins;
    }

     Integer embiumCoinsForKnowledgeTreeUsedFirstTime() {
        totalEmbiumCoins=totalEmbiumCoins+FIRST_KNOWLEDGE_TREE_CLICK_EMBIUMS;
        return totalEmbiumCoins;

    }

    Integer embiumCoinsForBookMark(Boolean isFirstBookmark) {
        totalEmbiumCoins=totalEmbiumCoins+BOOKMARK_EMBIUMS;
        if(isFirstBookmark){
            totalEmbiumCoins = totalEmbiumCoins+FIRST_BOOKMARK_EMBIUMS;
        }
        return totalEmbiumCoins;
    }

    public Integer embiumCoinsForGuestUser() {
        return  totalEmbiumCoins;

    }

    Integer embiumCoinsForQuestionStreak(int i) {

        switch (i) {
            case 5:
                totalEmbiumCoins = totalEmbiumCoins+QUESTION_STREAK_OF_FIVE;
                break;
            case 10:
                totalEmbiumCoins = totalEmbiumCoins+QUESTION_STREAK_OF_TEN;
                break;
        }
        return totalEmbiumCoins;
    }

}
