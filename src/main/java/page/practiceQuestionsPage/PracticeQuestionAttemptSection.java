package page.practiceQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static org.testng.Assert.assertEquals;

class PracticeQuestionAttemptSection extends BasePage {

    @FindBy(css = ".validation-points.u-margin-bottom-0")
    private WebElement silverEmbiumPointsEarned;

    @FindBy(css = ".validation-attempt-type.u-margin-bottom-0")
    private WebElement attemptType;

    @FindBy(css = ".validation-continue-button")
    private WebElement continueBtn;

    PracticeQuestionAttemptSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    String getActualAttemptType() {
        waitForElementToBeVisible(attemptType);
        return attemptType.getText();
    }

     void clickOnContinueButton(){
        waitForElementTobeClickable(continueBtn);
        jsClick(continueBtn);
    }

    void verifyPerfectAttemptAndEmbiumCoinsEarned(String coins) {
        waitForPageToLoad();
        waitForElementToBeVisible(silverEmbiumPointsEarned);
        System.out.println("Embium Coins:" + silverEmbiumPointsEarned.getText());
        assertEquals(silverEmbiumPointsEarned.getText(), coins);
    }
}
