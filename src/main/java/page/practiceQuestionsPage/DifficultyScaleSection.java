package page.practiceQuestionsPage;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class DifficultyScaleSection extends BasePage {


    @FindBy(css = ".difficulty-slider-box")
    private WebElement difficultyScaleElement;


    @FindBy(css = ".slider")
    private WebElement selectDifficultyLevel;


    DifficultyScaleSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    boolean isDifficultyScaleDisplayed() {
        waitForElementToBeVisible(difficultyScaleElement);
        return difficultyScaleElement.isDisplayed();
    }

    void selectDifficultyLevel(int level) {
        waitForElementTobeClickable(selectDifficultyLevel);
        Actions actions = new Actions(driver);
        actions.click(selectDifficultyLevel).build().perform();

        if (level > 5) {
            for (int i = 5; i < level; i++) {
                actions.sendKeys(Keys.ARROW_RIGHT).build().perform();
            }
        } else {
            for (int i = 5; i > level; i--) {
                actions.sendKeys(Keys.ARROW_LEFT).build().perform();
            }
        }
    }

}
