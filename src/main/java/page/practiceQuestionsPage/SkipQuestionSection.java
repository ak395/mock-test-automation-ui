package page.practiceQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

class SkipQuestionSection extends BasePage {

    @FindBy(css = ".button-change-difficulty")
    private WebElement changeDifficultyButton;

    @FindBy(css = ".button-show-hint")
    private WebElement useHintAndReAttemptButton;

    @FindBy(css = ".button-check-box-active")
    private WebElement nextButton;

    @FindBy(css = ".skip-actions-wrapper .close-button")
    private WebElement closeSkipSection;

    SkipQuestionSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    void clickOnChangeDifficultyButton() {
        waitForElementToBeVisible(changeDifficultyButton);
        changeDifficultyButton.click();
    }

    boolean isHintButtonShowntoStudent() {
        return useHintAndReAttemptButton.isDisplayed();
    }

    boolean isChangeDifficultyButtonShowntoStudent() {
        return changeDifficultyButton.isDisplayed();
    }

    boolean isHintButtonEnabled() {
        waitForElementToBeVisible(useHintAndReAttemptButton);
        return useHintAndReAttemptButton.isEnabled();
    }

    void clickOnHintButton() {
            useHintAndReAttemptButton.click();
    }

    void clickOnNextButton() {
        waitForElementToBeVisible(nextButton);
        nextButton.click();
        waitForPageToLoad();
    }

    void clickOnCloseSkipSectionButton() {
        waitForElementToBeVisible(closeSkipSection);
        closeSkipSection.click();
    }
}
