package page.practiceQuestionsPage;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static utils.AttemptType.*;
@Getter
class PracticeQuestionHeaderSection extends BasePage {

    @FindBy(css = ".silver-embium-coins")
    private WebElement silverEmbiumCoinsCount;

    @FindBy(css = "div.session-snippet--heading-text--end")
    private WebElement sessionSummaryButton;

    @FindBy(css = ".practice-legend-badge__imgMile.overtimecorrect ")
    private WebElement overtimeCorrectBadge;

    @FindBy(css = ".practice-legend-badge__imgMile.perfect")
    private WebElement perfectBadge;

    @FindBy(css = ".practice-legend-badge__imgMile.wasted")
    private WebElement wastedBadge;

    @FindBy(css = ".practice-legend-badge__imgMile.overtimeincorrect")
    private WebElement overtimeIncorrectBadge;

    @FindBy(css = ".practice-legend-badge__imgMile.toofastcorrect")
    private WebElement tooFastCorrectBadge;

    PracticeQuestionHeaderSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    int getCountOfEmbiumCoins() {
        waitForElementToBeVisible(silverEmbiumCoinsCount);
        return Integer.parseInt(silverEmbiumCoinsCount.getText());
    }

    void clickOnSessionSummaryButton() {
        waitForElementToBeVisible(sessionSummaryButton);
        sessionSummaryButton.click();
    }

    boolean verifyTheBadgeInSummaryIsUpdatedAs(String attemptType) {
        switch (attemptType) {
            case PERFECT_ATTEMPT:
                waitForElementToBeVisible(perfectBadge);
                return perfectBadge.isDisplayed();

            case WASTED_ATTEMPT:
                waitForElementToBeVisible(wastedBadge);
                return wastedBadge.isDisplayed();

            case OVERTIME_CORRECT:
                waitForElementToBeVisible(overtimeCorrectBadge);
                return overtimeCorrectBadge.isDisplayed();

            case OVERTIME_INCORRECT:
                waitForElementToBeVisible(overtimeIncorrectBadge);
                return overtimeIncorrectBadge.isDisplayed();

            case TOO_FAST_CORRECT:
                waitForElementToBeVisible(tooFastCorrectBadge);
                return tooFastCorrectBadge.isDisplayed();
        }
        return false;
    }
}
