package page.practiceQuestionsPage;

public class EmbiumPointsValue {

    public static final Integer FIRST_ANY_ATTEMPT_EMBIUMS = 200;
    public static final Integer FIRST_BOOKMARK_EMBIUMS = 200;
    public static final Integer FIRST_KNOWLEDGE_TREE_CLICK_EMBIUMS = 400;
    public static final Integer FIRST_HINT_EMBIUMS = 200;
    public static final Integer FIVE_QUESTIONS_STREAK_EMBIUMS=400;
    public static final Integer FIRST_TIME_USER_EMBIUMS=500;
    public static final Integer BOOKMARK_EMBIUMS = 20;
    public static final Integer QUESTION_STREAK_OF_FIVE= 400;
    public static final Integer QUESTION_STREAK_OF_TEN= 800;
    public static final Integer EMBIUMS_FOR_GUEST_USER = 500;

}

