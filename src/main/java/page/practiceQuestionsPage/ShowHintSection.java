package page.practiceQuestionsPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

class ShowHintSection extends BasePage {

    @FindBy(css = "p.hint-text")
    private WebElement hintTextBox;

    ShowHintSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    boolean isHintShownToTheUser() {
        waitForElementToBeVisible(hintTextBox);
        return hintTextBox.isDisplayed();
    }
}
