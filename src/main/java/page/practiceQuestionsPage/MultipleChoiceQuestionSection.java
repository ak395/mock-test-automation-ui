package page.practiceQuestionsPage;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;

class MultipleChoiceQuestionSection extends BasePage {

    MultipleChoiceQuestionSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private void selectAnswer(String answer, String questionCode) {
        try {
            waitForElementTobeClickable(driver.findElement(By.id(answer)));
            driver.findElement(By.id(answer)).click();
        }catch (Exception ignore){
            switch(answer.trim().split("-")[1]){
                case "1":
                    driver.findElement(By.id(questionCode + "-a"));
                    break;
                case "2":
                    driver.findElement(By.id(questionCode + "-b"));
                    break;
                case "3":
                    driver.findElement(By.id(questionCode + "-c"));
                    break;
                case "4":
                    driver.findElement(By.id(questionCode + "-d"));
                    break;
                case "a":
                    driver.findElement(By.id(questionCode + "-1")).click();
                    break;
                case "b":
                    driver.findElement(By.id(questionCode + "-2")).click();
                    break;
                case "c":
                    driver.findElement(By.id(questionCode + "-3")).click();
                    break;
                case "d":
                    driver.findElement(By.id(questionCode + "-4")).click();
                    break;
            }
        }
    }

    void answerQuestion(List<String> answers, String questionCode) {
        answers.forEach(answer -> selectAnswer(answer, questionCode));
    }
}
