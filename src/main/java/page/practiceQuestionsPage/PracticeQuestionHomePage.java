package page.practiceQuestionsPage;

import constants.QuestionType;
import driver.DriverProvider;
import entities.practice.PracticeData;
import entities.practice.PracticePlanner;
import entities.questionTypes.practice.SingleChoiceQuestion;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.mockTestPage.InstructionPage;
import questionService.QuestionClient;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static utils.AttemptType.*;

@Getter
public class PracticeQuestionHomePage extends BasePage {

    private PracticeQuestionSection practiceQuestionSection;
    private PracticeQuestionHeaderSection practiceQuestionHeaderSection;
    private SkipQuestionSection skipQuestionSection;
    private DifficultyScaleSection difficultyScaleSection;
    private ShowHintSection showHintSection;
    private EmbiumPointsCalculator embiumPointsCalculator;

    @FindBy(css = "img.modal-close.milestone-modal-close-icon")
    private WebElement closeModalPopupButton;

    @FindBy(css = ".statistics.u-margin-bottom-0")
    private WebElement percentageGotItRightelement;

    @FindBy(css = ".bookmark-logo.inline-flex")
    private WebElement bookmarkLogo;

    @FindBy(css = ".snackbar-container--text")
    private WebElement snackBarMessage;

    @FindBy(css = "div.modal-wrap")
    private WebElement milestonePopUp;

    @FindBy(css = "span.milestone-message")
    private WebElement milestoneMessage;

    @FindBy(css = "div.title")
    private WebElement messageOfCompletion;


    public PracticeQuestionHomePage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        practiceQuestionSection = new PracticeQuestionSection(driver);
        practiceQuestionHeaderSection = new PracticeQuestionHeaderSection(driver);
        skipQuestionSection = new SkipQuestionSection(driver);
        difficultyScaleSection = new DifficultyScaleSection(driver);
        showHintSection = new ShowHintSection(driver);
        embiumPointsCalculator = new EmbiumPointsCalculator(driver);
    }

    private void clickOnCloseModalPopupIfPresent() {
        try {
            waitForElementToBeVisible(closeModalPopupButton);
            closeModalPopupButton.click();
        } catch (Exception ignore) {
        }
    }

    @Step("Verify Embium Silver Count to be Zero as soon as practice starts")
    public void assertSilverEmbiumCoinsCountToBeZero() {
        assertEquals(0, getPracticeQuestionHeaderSection().getCountOfEmbiumCoins());
    }

    @Step("Verify Milestone popup is dislpayed after reaching a milestone")
    public void verifyMilestonePopUpIsDisplayed(int i) {

        assertTrue(milestonePopUp.isDisplayed(), "Milestone PopUp is not Displayed");
        switch (i) {
            case 5:
                assertEquals(milestoneMessage.getText(), "You just hit a 5 Question Streak!", "Milestone message and Expected Text not Matching");
                break;
            case 10:
                assertEquals(milestoneMessage.getText(), "You just hit a 10 Question Streak!", "Milestone message and Expected Text not Matchin");
                break;


        }

    }

    @Step("Verify silver embium coins has updated to the expected coins")
    public void assertSilverEmbiumCoinsUpdatedTo(Integer expectedCoins) {
        System.out.println("Embium Points UI---" + getPracticeQuestionHeaderSection().getCountOfEmbiumCoins() + "," + "Embium Points Calculated----" + expectedCoins);
        assertEquals(Double.parseDouble(String.valueOf((getPracticeQuestionHeaderSection().getCountOfEmbiumCoins()))), Double.parseDouble(String.valueOf(expectedCoins)), 0);

    }

    public void clickOnSessionSummaryButton() {
        practiceQuestionHeaderSection.clickOnSessionSummaryButton();
    }

    public void skipPracticeQuestion() {
        getPracticeQuestionSection().clickOnSkipButton();
    }

    public void clickOnChangeDifficultyLevel() {
        getSkipQuestionSection().clickOnChangeDifficultyButton();
    }

    public void verifyDifficultyScaleIsDisplayed() {
        assertTrue(getDifficultyScaleSection().isDifficultyScaleDisplayed());
    }

    public void verifySkipOptionsAreShownToTheUser() {
//        assertTrue(getSkipQuestionSection().isChangeDifficultyButtonShowntoStudent());
        assertTrue(getSkipQuestionSection().isHintButtonShowntoStudent());
    }

    public void selectDifficultyLevelAs(int level) {
        getDifficultyScaleSection().selectDifficultyLevel(level);
    }

    public void clickOnNextButton(PracticePlanner practicePlanner) {
        getSkipQuestionSection().clickOnNextButton();
        practicePlanner.setNumberOfSkippedQuestions(practicePlanner.getNumberOfSkippedQuestions() + 1);
    }

    public void verifySelectedDifficultyLevelIsSetAs(int expectedLevel) {
        String questionCode = getActiveQuestionCodeFromDom();
        Response response = new QuestionClient().getQuestionDetailsWithAnswersByQuestionCode(questionCode);
        Integer actualLevel = new QuestionClient().getDifficultyLevelByQuestionCode(response, questionCode);
        assertEquals(Double.parseDouble(String.valueOf(actualLevel)), Double.parseDouble(String.valueOf(expectedLevel)), 1);
    }

    public Integer getDifficultyLevelOfActiveQuestion(String questionCode) {
        Response response = new QuestionClient().getQuestionDetailsWithAnswersByQuestionCode(questionCode);
        Integer actualLevel = new QuestionClient().getDifficultyLevelByQuestionCode(response, questionCode);
        return actualLevel;
    }


    private String getActiveQuestionCodeFromDom() {
        return practiceQuestionSection.getActiveQuestionCodeFromDom();
    }

    public void verifyHintIsVisible() {
        boolean buttonEnabled = getSkipQuestionSection().isHintButtonEnabled();

        if (buttonEnabled) {
            getSkipQuestionSection().clickOnHintButton();
            assertTrue(getShowHintSection().isHintShownToTheUser(), "Hint is not shown to the student");
        }
    }

    public void closeSkipSection() {
        getSkipQuestionSection().clickOnCloseSkipSectionButton();
    }

    public void verifySkipButtonIsVisible() {
        assertTrue(getPracticeQuestionSection().isSkipButtonDisplayed(), "Skip button is not displayed");
    }

    public void verifyTheQuestionIsRelatedToSubjectSearched(String expectedSubject) {
        String questionCode = getActiveQuestionCodeFromDom();
        Response response = new QuestionClient().getQuestionDetailsWithAnswersByQuestionCode(questionCode);
        String actualSubject = new QuestionClient().getSubjectByQuestionCode(response, questionCode);
        assertEquals(actualSubject, expectedSubject, "Expected subject: " + expectedSubject + "is different displayed subject: " + actualSubject);
    }

    private void studentAnswersQuestionAs(PracticeData practiceData, PracticePlanner practiceDataPlanner, boolean checkEmbiumCoinsForQuestionStreak) throws Exception {
        boolean flag;
        List<Integer> questionStreak = Arrays.asList(5, 10, 30);

        for (int i = practiceData.getStartIndex(); i <= practiceData.getEndIndex(); i++){
            flag = (practiceData.getEndIndex() - i) == 0;
            String questionCode = getActiveQuestionCodeFromDom();
            String attempt = practiceData.getAttemptType();

            if (questionCode.trim().equals("")) {
                throw new Exception("The Question code is not fetched from UI. Seems that Practice Test Page is not loaded");
            }

            Response response = new QuestionClient().getQuestionDetailsWithAnswersByQuestionCode(questionCode);
            String subject = new QuestionClient().getSubjectByQuestionCode(response, questionCode);
            List<String> correctAnswers = new QuestionClient().getCorrectAnsFor(response, questionCode);
            List<String> incorrectAnswers = new QuestionClient().getIncorrectAnswer(response, questionCode);
            int idealTime = new QuestionClient().getIdealTimeFor(response, questionCode);
            System.out.println("Ideal Time"+ idealTime);
            int difficultyLevel = new QuestionClient().getDifficultyLevelByQuestionCode(response, questionCode);
            System.out.println("Difficulty Level" + difficultyLevel);
            String questionCategory = new QuestionClient().getQuestionCategoryFor(response, questionCode);
            //System.out.println(correctAnswers + "  correct");
            switch (attempt) {
                case PERFECT_ATTEMPT:
                    wait(((idealTime / 2) - 7) * 1000);
                    practiceQuestionSection.answer(correctAnswers, questionCategory, questionCode);
                    break;

                case WASTED_ATTEMPT:
                    practiceQuestionSection.answer(incorrectAnswers, questionCategory, questionCode);
                    break;

                case OVERTIME_CORRECT:
                    wait((idealTime + 5) * 1000);
                    practiceQuestionSection.answer(correctAnswers, questionCategory, questionCode);
                    break;

                case OVERTIME_INCORRECT:
                    wait((idealTime + 10) * 1000);
                    practiceQuestionSection.answer(incorrectAnswers, questionCategory, questionCode);
                    break;

                case TOO_FAST_CORRECT:
                    practiceQuestionSection.answer(correctAnswers, questionCategory, questionCode);
                    break;

                case INCORRECT_ATTEMPT:
                    wait(((idealTime / 2) - 7) * 1000);
                    practiceQuestionSection.answer(incorrectAnswers, questionCategory, questionCode);
                    break;

            }
            if (checkEmbiumCoinsForQuestionStreak && questionStreak.contains(i)) {
                verifyEmbiumCoinsForQuestionStreak(i);
            }

            int no = practiceDataPlanner.getNumberOfQuestion() + 1;
            if (no % 5 == 0)
                clickOnCloseModalPopupIfPresent();

            if (!flag) {
                clickOnContinueButton();
            }

            SingleChoiceQuestion singleChoiceQuestion = new SingleChoiceQuestion(questionCode.trim(), QuestionType.SINGLE_CHOICE, attempt, correctAnswers, incorrectAnswers, subject, correctAnswers.get(0), incorrectAnswers.get(0), idealTime, difficultyLevel);
            practiceDataPlanner.addQuestion(singleChoiceQuestion);
        }


    }

    public void verifyTheAttempt(String expectedAttempt) {
        getPracticeQuestionSection().verifyAttempt(expectedAttempt);
    }

    public void clickOnContinueButton() {
        getPracticeQuestionSection().clickOnContinueButton();
    }

    public void clickOnMinimizeSolution() {
        getPracticeQuestionSection().clickOnMinimizeSolutionButton();
    }

    public void clickOnMaximizeSolution() {
        getPracticeQuestionSection().clickOnMaximizeSolutionButton();
    }

    public void verifySolutionIsVisible() {
        assertTrue(getPracticeQuestionSection().verifySolutionIsVisible(), "Solution popup is not visible after clicking on CHECK button");
    }

    public void verifyPercentageOfStudentsGotItRightIsDisplayed() {
        waitForElementToBeVisible(percentageGotItRightelement);
        assertTrue(percentageGotItRightelement.isDisplayed(), "% of students got it right is not displayed after student answering the question");
    }

    public void clickOnBookmarkLogo() {
        waitForElementToBeVisible(bookmarkLogo);
        bookmarkLogo.click();
    }

    public void verifyMessageIsDisplayedAfterBookMarkAs(String expectedMessage) {
        waitForElementToBeVisible(snackBarMessage);
        String actualMessage = snackBarMessage.getText();
        assertEquals(actualMessage, expectedMessage);
    }

    public void verifySessionSummaryStatusForTheAnswer(String attemptType) {
        boolean value = practiceQuestionHeaderSection.verifyTheBadgeInSummaryIsUpdatedAs(attemptType);
        assertTrue(value, "The badge in session summary (On the Right) is not updated accordingly for" + attemptType);
    }

    public void selectAnswersAndVerifyAttempts(PracticePlanner practiceDataPlanner) throws Exception {
        for (PracticeData practiceData : practiceDataPlanner.getPracticeDataList()){
            studentAnswersQuestionAs(practiceData, practiceDataPlanner, false);
            verifyTheAttempt(practiceData.getAttemptType());
//            verifyPercentageOfStudentsGotItRightIsDisplayed();
            verifySessionSummaryStatusForTheAnswer(practiceData.getAttemptType());
            verifySolutionIsVisible();
            clickOnMinimizeSolution();
            clickOnMaximizeSolution();
            clickOnContinueButton();
        }
    }

    public void selectAnswers(PracticePlanner practiceDataPlanner) throws Exception {
        for (PracticeData practiceData : practiceDataPlanner.getPracticeDataList()){
            studentAnswersQuestionAs(practiceData, practiceDataPlanner, false);
            try {
                clickOnContinueButton();
            } catch (Exception ignore) {
            }
        }
    }

    public void verifyEmbiumCoins(String attemptType, Integer difficultyLevel, Boolean isFirstTimeAttempt) {
        Integer embiumCoins = getEmbiumPointsCalculator().calculateEmbiumPoints(difficultyLevel, attemptType, isFirstTimeAttempt);
        refreshPage();
        assertSilverEmbiumCoinsUpdatedTo(embiumCoins);
    }

    public void selectAnswersAndVerifyEmbiumCoins(PracticePlanner practiceDataPlanner) throws Exception {

        for (PracticeData practiceData : practiceDataPlanner.getPracticeDataList()){
            String questionCode = getActiveQuestionCodeFromDom();
            Integer difficultyLevel = getDifficultyLevelOfActiveQuestion(questionCode);
            studentAnswersQuestionAs(practiceData, practiceDataPlanner, true);
//            verifyEmbiumCoins(practiceData.getAttemptType(), difficultyLevel, practiceData.isFirstTimeAttempt());
            try {
                clickOnContinueButton();
            } catch (Exception ignore) {
            }

        }

    }

    public void checkAndUseHintFirstTime() {
        skipPracticeQuestion();
        boolean buttonEnabled = getSkipQuestionSection().isHintButtonEnabled();

        if (buttonEnabled) {
            useHintForTheFirstTimeAndVerifyEmbiumCoins();
        } else {
            skipQuestionSection.clickOnNextButton();
            checkAndUseHintFirstTime();
        }
    }

    public void useHintForTheFirstTimeAndVerifyEmbiumCoins() {
        waitForPageToLoad();
        getSkipQuestionSection().clickOnHintButton();
        assertTrue(getShowHintSection().isHintShownToTheUser(), "Hint is not shown to the student");
        waitForPageToLoad();
        getSkipQuestionSection().clickOnNextButton();
        wait(2000);
        refreshPage();
        Integer embiumCoins = getEmbiumPointsCalculator().embiumCoinsForFirstHint();
        assertSilverEmbiumCoinsUpdatedTo(embiumCoins);
    }

    public void useKnowledgeTreeForFirstTimeAndVerifyEmbiumCoins() {
        getPracticeQuestionSection().clickOnKnowledgeTreeWidget();
        wait(3000);
        getPracticeQuestionSection().closeKnowledgeTreeWidget();
        Integer embiumCoins = getEmbiumPointsCalculator().embiumCoinsForKnowledgeTreeUsedFirstTime();
        refreshPage();
        assertSilverEmbiumCoinsUpdatedTo(embiumCoins);
    }

    public void bookmarkQuestionAndVerifyEmbiumCoins(Boolean firstBookmark) {
        clickOnBookmarkLogo();
        wait(5000);
        Integer embiumCoins = getEmbiumPointsCalculator().embiumCoinsForBookMark(firstBookmark);
        refreshPage();
        waitForElementToBeVisible(getPracticeQuestionHeaderSection().getSilverEmbiumCoinsCount());
        assertSilverEmbiumCoinsUpdatedTo(embiumCoins);

    }

    public void verifyTheEmbiumCoinsForGuestUser() {
        Integer embiumCoins = getEmbiumPointsCalculator().embiumCoinsForGuestUser();
        refreshPage();
        new InstructionPage().continueAsGuest();
        assertSilverEmbiumCoinsUpdatedTo(embiumCoins);
    }

    public void verifyQuestionsAreNotRepeated(PracticePlanner planner) {
        planner.areQuestionsNotRepeatedInSession();
    }

    public void verifyThatUserHasCompletedTest() {
        waitForElementToBeVisible(messageOfCompletion);
        assertEquals(messageOfCompletion.getText(), "You have completed all practice question of Dice.");   

    }

    public void verifyEmbiumCoinsForQuestionStreak(int i) {

        Integer embiumCoins = getEmbiumPointsCalculator().embiumCoinsForQuestionStreak(i);
        System.out.println("Embium Coins after adding question streak: " + embiumCoins);
        verifyMilestonePopUpIsDisplayed(i);
        clickOnCloseModalPopupIfPresent();
    }
}
