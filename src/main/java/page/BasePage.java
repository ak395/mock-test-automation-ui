package page;

import constants.Badge;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Properties;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class BasePage {

    protected WebDriver driver;

    protected int waitForThinkTime(Badge badge, int idealTime) {
        int thinkTime = 0;
        switch (badge) {
            case BADGE_PERFECT_ATTEMPT:
            case BADGE_INCORRECT_ATTEMPT:
                thinkTime = (idealTime / 4) + 2;
                break;

            case BADGE_OVERTIME_CORRECT:
            case BADGE_OVERTIME_INCORRECT:
                thinkTime = idealTime;
                break;

            case BADGE_WASTED_ATTEMPT:
            case BADGE_TOO_FAST_CORRECT:
                thinkTime = 0;
                break;
            /***Attempt in more than or equal to the Ideal Time and less than 1.5 Times Ideal Time***/
            case BADGE_OVERTIME_CORRECT_LEVEL_ONE:
            case BADGE_OVERTIME_INCORRECT_LEVEL_ONE:
            case BADGE_UNATEMPTED_LEVEL_THREE:
                thinkTime = idealTime;
                break;

            /***Attempt in more than or equal to 1.5 Times Ideal Time and less than 3 Times Ideal Time***/
            case BADGE_OVERTIME_CORRECT_LEVEL_TWO:
            case BADGE_OVERTIME_INCORRECT_LEVEL_TWO:
            case BADGE_UNATEMPTED_LEVEL_FOUR:
                thinkTime = 2*idealTime;
                break;

            /***Attempt in more than or equal to 3 Times Ideal Time and less than 5 Times Ideal Time***/
            case BADGE_OVERTIME_CORRECT_LEVEL_THREE:
            case BADGE_OVERTIME_INCORRECT_LEVEL_THREE:
            case BADGE_UNATEMPTED_LEVEL_FIVE:
                thinkTime = (3*idealTime+5*idealTime)/2;
                break;

            /***Attempt in more than or equal to 5 Times Ideal Time and less than 7 Times Ideal Time***/
            case BADGE_OVERTIME_CORRECT_LEVEL_FOUR:
            case BADGE_OVERTIME_INCORRECT_LEVEL_FOUR:
            case BADGE_UNATEMPTED_LEVEL_SIX:
                thinkTime = (5*idealTime+7*idealTime)/2;
                break;

            /***Attempt in more than or equal to 7 Times Ideal Time and less than 12 Times Ideal Time***/
            case BADGE_OVERTIME_CORRECT_LEVEL_FIVE:
            case BADGE_OVERTIME_INCORRECT_LEVEL_FIVE:
            case BADGE_UNATEMPTED_LEVEL_SEVEN:
                thinkTime = (7*idealTime+12*idealTime)/2;
                break;

            /***Unattempted in less than 25% of Ideal Time ***/
            case BADGE_UNATEMPTED_LEVEL_ONE:
                thinkTime = (idealTime/4)-5;
                break;

            /***Correct in more than or equal to 25% of Ideal Time and less than Ideal Time***/
            case BADGE_UNATEMPTED_LEVEL_TWO:
                thinkTime = ((idealTime/4)+idealTime)/2;
                break;
        }

        System.out.println(" Attempt type is " + badge.toString() + " so Waiting for seconds : " + thinkTime);
        wait(thinkTime * 1000);
        return thinkTime;

//        if (badge.equals(Badge.BADGE_OVERTIME_CORRECT) || (badge.equals(Badge.BADGE_OVERTIME_INCORRECT))) {
//            thinkTime = idealTime;
//        } else if (badge.equals(Badge.BADGE_PERFECT_ATTEMPT) || badge.equals(Badge.BADGE_INCORRECT_ATTEMPT)) {
//            thinkTime = (idealTime / 4) + 2;
//        } else if (badge.equals(Badge.BADGE_WASTED_ATTEMPT) || badge.equals(Badge.BADGE_TOO_FAST_CORRECT)) {
//            thinkTime = 0;
//        }
//        System.out.println(" Attempt type is " + badge.toString() + " so Waiting for : " + thinkTime * 1000);
//        wait(thinkTime * 1000);
    }

    protected void scrollToView(WebElement element) {
        if (element.isEnabled()) {
            ((JavascriptExecutor) driver).
                    executeScript("arguments[0].scrollIntoView(true);", element);
        }

    }

    protected void waitForElementTobeClickable(WebElement webElement) {
        new WebDriverWait(driver, 30).until(
                ExpectedConditions.elementToBeClickable(webElement));
    }


    protected void wait(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    protected void waitForElementToBeVisible(WebElement element) {

        new WebDriverWait(driver, 30).until(
                ExpectedConditions.visibilityOf(element)
        );


    }

    protected boolean waitForElementToBeDisplay(WebElement element) {
        Wait wait = new FluentWait(driver).withTimeout(Duration.ofSeconds(30)).pollingEvery(Duration.ofSeconds(2));
        wait.until((Function<WebDriver, WebElement>) driver -> element);
        return element.isDisplayed();
    }

    protected void waitForListOfElementToBeDisplay(List<WebElement> element) {
        Wait wait = new FluentWait(driver).withTimeout(Duration.ofSeconds(30)).ignoring(StaleElementReferenceException.class).pollingEvery(Duration.ofSeconds(2));
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOfAllElements(element)));
    }

    protected void waitForPageToLoad() {
        new WebDriverWait(driver, 30).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    protected void waitForElementToBeInvisible(WebElement element) {

        new WebDriverWait(driver, 20)
                .until(ExpectedConditions.invisibilityOf(element));

    }

    protected void refreshPage() {
        driver.navigate().refresh();
        waitForPageToLoad();

    }

    protected String getCookies() {
        String cookiesname = System.getProperty("env") + "-embibe-token";
        if (System.getProperty("env").equalsIgnoreCase("production"))
            cookiesname = "embibe-token";
        return driver.manage().getCookieNamed(cookiesname).getValue();
    }

    protected String getMockTestBundlePath() {
        return driver.getCurrentUrl().replace(Properties.baseUrl, "/").replace("/session", "");

    }

    protected WebElement getElement(WebElement rootElement, By by) {
        List<WebElement> elements = rootElement.findElements(by);

        if (elements.size() == 0) {
            String reason = String.format("WebElement with locator %s and locatorType %s not found");
            throw new NoSuchElementException(reason);
        }

        if (elements.size() > 1) {
            String reason = String.format("There are %s WebElements with locator %s and locatorType %s",
                    elements.size());
            try {
                throw new Exception(reason);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return elements.get(0);

    }

    protected List<WebElement> getElements(WebElement rootElement, By by) {
        List<WebElement> elements = rootElement.findElements(by);

        if (elements.size() == 0) {
            String reason = "WebElement with locator %s and locatorType %s not found";
            throw new NoSuchElementException(reason);
        }

        return elements;

    }

    protected List<WebElement> getListOfElements(WebElement rootElement, By by) {
        return rootElement.findElements(by);

    }

    public static void mouseHoverJScript(WebDriver driver, WebElement element) {
        new Actions(driver).moveToElement(element).build().perform();

    }

    protected boolean isElementPresent(WebElement rootElement, By by) {

        List<WebElement> elements = rootElement.findElements(by);

        if (elements.size() == 0) {
            return false;
        }

        if (elements.size() > 1) {
            String reason = String.format("There are %s WebElements with locator %s and locatorType %s",
                    elements.size());
            try {
                throw new Exception(reason);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;

    }

    protected boolean isAttributePresent(WebElement element, String attribute) {
        Boolean result = false;
        try {
            String value = element.getAttribute(attribute);
            if (value != null) {
                result = true;
            }
        } catch (Exception e) {

            return false;
        }

        return result;
    }

    protected void jsClick(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }

    protected void switchToActiveTab() {
        String parentWindow = driver.getWindowHandle();

        for (String childWindow : driver.getWindowHandles())
            if (!childWindow.equals(parentWindow))
                driver.switchTo().window(childWindow);
            else
                driver.close();
    }

    public boolean retryingFindClick(WebElement ele) {
        boolean result = false;
        int attempts = 0;
        while (attempts < 5) {
            try {
                driver.navigate().refresh();
                ele.isDisplayed();
                result = true;
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
        return result;
    }

    protected void switchToParentTab() {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.switchTo().window(tabs.get(0));
        driver.switchTo().window(tabs.get(1));
    }

    protected void swicthMultipleTab() {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(2));
    }

    protected void closeChildTab() {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1)).close();
    }

    protected void switchParentTab() {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
    }

    protected void waitForListOfToBeVisible(List<WebElement> element) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfAllElements(element));
    }

    protected void openUrlInNewTab(String url) {
        ((JavascriptExecutor) driver).executeScript(String.format("window.open('%s','_blank');", url));

    }

    protected boolean urlContains(String stringText) {
        return driver.getCurrentUrl().contains(stringText);
    }

    protected void resizeBrowser(int width, int height) {
        Dimension d = new Dimension(width, height);
        //Resize current window to the set dimension
        driver.manage().window().setSize(d);
    }

    protected void assertUrlHasResetTo(String url) {

        Assert.assertEquals(driver.getCurrentUrl(), url, "Url is not set to as expected");
    }

    protected boolean acceptAlertPopUp() {
        try {
            Alert alert = driver.switchTo().alert();
            alert.accept();
            return true;
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
            return false;
        }
    }


    protected String covertSecondsHoursAndMinutes(int total_seconds) {

        String timeString;
        int hours = total_seconds / 3600;
        int minutes = (total_seconds % 3600) / 60;
        int seconds = total_seconds % 60;

        if (hours > 0) {

           return String.format("%02d hr %02d min %02d sec ", hours, minutes, seconds);
        } else if (minutes > 0) {
            return String.format("%d min %d sec", minutes, seconds);

        } else {
           return String.format("%d Seconds ", seconds);
        }


    }

    protected String convertFloatToHrsMinsAndSeconds(float seconds) {

        float time = (seconds/3600);

        BigDecimal bigDecimal = new BigDecimal(String.valueOf(time));
        int hours = bigDecimal.intValue();

         float hoursAfterDecimal = Float.parseFloat(bigDecimal.subtract(new BigDecimal(hours)).toPlainString());

        float minutesInDecimal =  hoursAfterDecimal*60;

        BigDecimal bgdecimal = new BigDecimal(String.valueOf(minutesInDecimal));

        int minutes = bgdecimal.intValue();

        float minutesAfterDecimal = Float.parseFloat(bgdecimal.subtract(new BigDecimal(minutes)).toPlainString());


        int secs = Math.round(minutesAfterDecimal*60);


        if (hours > 0) {

            return String.format("%02d hr %02d min %02d sec ", hours, minutes, secs);
        } else if (minutes > 0) {
            return String.format("%d min %d sec", minutes, secs);

        }
        else if (secs>0) {
            return String.format("%d sec ", secs);
        }
        else {
            return "0";
        }


    }

    protected void dismissAlertPopUp(){
        try {
            Alert alert = driver.switchTo().alert();
            alert.dismiss();
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }
    }


    protected String getCurrentUrl(){
        return driver.getCurrentUrl();
    }
    @Test
    public void test(){
        convertFloatToHrsMinsAndSeconds(0);
    }
}
