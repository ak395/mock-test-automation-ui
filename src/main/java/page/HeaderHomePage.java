package page;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.support.PageFactory;
import page.Header.HeaderSection;

@Getter
public class HeaderHomePage extends BasePage {

    private HeaderSection headerSection;

    public HeaderHomePage()
    {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        headerSection = new HeaderSection(driver);
    }


}
