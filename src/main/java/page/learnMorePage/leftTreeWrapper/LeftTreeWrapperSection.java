package page.learnMorePage.leftTreeWrapper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.learnMorePage.searchContainer.SearchContainerSection;
import verification.Verify;

import java.util.Optional;

public class LeftTreeWrapperSection extends BasePage {

    private LeftTreeWrapperSectionLocators locators;

    @FindBy(className = "childrenOfTree")
    private WebElement rootElement;

    public LeftTreeWrapperSection(WebDriver driver) {
        this.driver = driver;
        locators = new LeftTreeWrapperSectionLocators();
        PageFactory.initElements(driver, this);

    }

    public WebElement getCurrentTopicName() {
        return getElement(rootElement, locators.topicName);
    }

    public void matchingConceptName() {

        waitForPageToLoad();
        wait(5000);
        WebElement element = getCurrentTopicName();
        SearchContainerSection searchContainerSection = new SearchContainerSection(driver);
        Verify.assertEquals(element.getText(), searchContainerSection.getSearchContainerText(), "Text not Matching", Optional.of(true));

    }
}