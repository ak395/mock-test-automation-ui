package page.learnMorePage;

import driver.DriverProvider;
import lombok.Getter;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.learnMorePage.descriptionContainer.DescriptionSection;
import page.learnMorePage.leftTreeWrapper.LeftTreeWrapperSection;
import page.learnMorePage.searchContainer.SearchContainerSection;

@Getter
public class LearnMoreHomePage extends BasePage{

        private LeftTreeWrapperSection leftTreeWrapperSection;
        private SearchContainerSection searchContainerSection;
        private DescriptionSection descriptionSection;
        public LearnMoreHomePage() {
            driver = DriverProvider.getDriver();

            PageFactory.initElements(driver, this);

            leftTreeWrapperSection = new LeftTreeWrapperSection(driver);
            searchContainerSection = new SearchContainerSection(driver);
            descriptionSection = new DescriptionSection(driver);

        }

    }