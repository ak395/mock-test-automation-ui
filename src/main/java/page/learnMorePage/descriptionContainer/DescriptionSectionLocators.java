package page.learnMorePage.descriptionContainer;

import org.openqa.selenium.By;

public class DescriptionSectionLocators {

    By descriptionWidget = By.className("descriptionWrapper2");
    By descriptionHeading = By.className("desp-head");
}
