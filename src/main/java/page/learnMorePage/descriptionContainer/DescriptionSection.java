package page.learnMorePage.descriptionContainer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

public class DescriptionSection extends BasePage {

    private DescriptionSectionLocators locators;

    @FindBy(className = "description")
    private WebElement rootElement;

    public DescriptionSection(WebDriver driver) {
        this.driver = driver;
        locators = new DescriptionSectionLocators();
        PageFactory.initElements(driver, this);

    }

    public WebElement getDescriptionWidgetElement() {
        return getElement(rootElement,locators.descriptionWidget);
    }

    public WebElement getDescriptionHeadingElement() {
        return getElement(rootElement,locators.descriptionHeading);
    }

    public void isDescriptionWidgetDisplayed() {
        wait(5000);
        Verify.assertTrue(getDescriptionWidgetElement().isDisplayed(),"Widget Not Displayed", Optional.of(true));
        Verify.assertTrue(getDescriptionHeadingElement().isDisplayed(),"Widget Not Displayed",Optional.of(true));

    }
}
