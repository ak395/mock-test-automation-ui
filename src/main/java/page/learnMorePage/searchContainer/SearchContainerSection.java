package page.learnMorePage.searchContainer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class SearchContainerSection extends BasePage {

    private SearchContainerSectionLocators locators;


    @FindBy(className = "search-container")
    private WebElement rootElement;

    public SearchContainerSection(WebDriver driver) {
        this.driver = driver;
        locators = new SearchContainerSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchContainerElement() {

        return getElement(rootElement, locators.searchContainer);
    }

    public String getSearchContainerText() {
        wait(5000);
        WebElement searchBox = getSearchContainerElement();
        String searchBoxText = searchBox.getAttribute("value");
        return searchBoxText;

    }
}
