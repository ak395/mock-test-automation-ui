package page.practicePage;

import driver.DriverProvider;
import entities.AMockOrPracticeTest;
import entities.practice.PracticePlanner;
import lombok.Getter;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.practicePage.dashboard.DashboardSection;
import page.sections.headerWrapper.HeaderWrapperSection;
import page.sections.leftFilter.LeftFilterSection;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Getter
public class PractiseHomePage extends BasePage {

    private DashboardSection dashboardSection;
    private LeftFilterSection  leftFilterSection;
    private HeaderWrapperSection headerWrapperSection;
    private PracticeTestSection practiceTestSection;

    public PractiseHomePage(){
        driver = DriverProvider.getDriver();

        PageFactory.initElements(driver, this);

        dashboardSection = new DashboardSection(driver);
        leftFilterSection = new LeftFilterSection(driver);
        headerWrapperSection = new HeaderWrapperSection(driver);
        practiceTestSection = new PracticeTestSection(driver);
    }

    public void selectTest(AMockOrPracticeTest test) {
        dashboardSection.selectGoal(test.getGoal());
        leftFilterSection.selectTest(test);
    }

    public void clickOnStartPracticeTestButton() {
        practiceTestSection.clickOnFirstStartPracticeButton();
        switchToActiveTab();
        waitForPageToLoad();
    }

    public void isStartPracticeButtonVisible() {
        assertTrue(practiceTestSection.isStartPracticeButtonVisible());
    }
    public void clickOnResumePracticeButton(){
        practiceTestSection.clickOnResumePracticeButton();
        switchToActiveTab();
        waitForPageToLoad();
    }

    public void verifyNumberOfQuestionsAttempted(PracticePlanner practicePlanner) {
        int actualNumberOfQuestions = practiceTestSection.getActualNumberOfQuestionsAttempted();
        assertEquals(actualNumberOfQuestions, practicePlanner.getPracticeQuestionList().size(),
                "Number of questions attempted:"
                        + practicePlanner.getPracticeQuestionList().size() + "does not match with actual number of questions shown: "
                        + actualNumberOfQuestions);
        verifyCorrectAttempts(practicePlanner);
        verifyIncorrectAttempts(practicePlanner);
    }

    public void verifyCorrectAttempts(PracticePlanner practicePlanner) {
        int actualCorrectAttempts = practiceTestSection.getNumberOfQuestionsAttemptedCorrectly();
        assertEquals(actualCorrectAttempts, practicePlanner.getNumberOfCorrectAnswers(),
                "Number of questions attempted correctly are:"
                        + practicePlanner.getNumberOfCorrectAnswers() + "does not match with actual number of questions shown: "
                        + actualCorrectAttempts);
    }

    public void verifyIncorrectAttempts(PracticePlanner practicePlanner) {
        int actualIncorrectAttempts = practiceTestSection.getNumberOfQuestionsAttemptedIncorrectly();
        assertEquals(actualIncorrectAttempts, practicePlanner.getNumberOfIncorrectAnswers(),
                "Number of questions attempted correctly are:"
                        + practicePlanner.getNumberOfIncorrectAnswers() + "does not match with actual number of questions shown: "
                        + actualIncorrectAttempts);
    }
}
