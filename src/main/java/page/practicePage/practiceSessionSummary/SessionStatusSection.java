package page.practicePage.practiceSessionSummary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

public class SessionStatusSection extends BasePage {

    @FindBy(css = ".button-session-end")
    private WebElement endSessionButton;

    @FindBy(css = ".button-session-continue")
    private WebElement resumeSessionButton;

    SessionStatusSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnEndSessionButton(){
        waitForElementToBeVisible(endSessionButton);
        jsClick(endSessionButton);
    }

    public void clickOnResumeSessionButton(){
        waitForElementToBeVisible(resumeSessionButton);
        resumeSessionButton.click();
    }
}
