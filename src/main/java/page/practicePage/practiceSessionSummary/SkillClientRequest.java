package page.practicePage.practiceSessionSummary;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SkillClientRequest {
    private List<String> question_codes;
}
