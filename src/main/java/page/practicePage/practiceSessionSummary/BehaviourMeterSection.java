package page.practicePage.practiceSessionSummary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

class BehaviourMeterSection extends BasePage {

    @FindBy(css = ".behaviour-status-image.DEFAULT")
    private WebElement defaultBeahiourMeterElement;

    @FindBy(css = "div.IN_CONTROL")
    private WebElement incontrolBehaviourElement;

    @FindBy(css = "div.OVERCONFIDENCE")
    private WebElement overconfidenceBehaviourElement;

    @FindBy(css = "div.SLOW")
    private WebElement slowBehaviourElement;

    @FindBy(css = "div.STRUGGLING")
    private WebElement strugglingBehaviourElement;

    @FindBy(css = "div.FLUKES")
    private WebElement flukesBehaviourElement;

    BehaviourMeterSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    boolean isDefaultBehaviourMeterDisplayed(){
        waitForElementToBeVisible(defaultBeahiourMeterElement);
        return defaultBeahiourMeterElement.isDisplayed();
    }


    boolean isInControlBehaviourMeterDisplayed() {
        waitForElementToBeVisible(incontrolBehaviourElement);
        return incontrolBehaviourElement.isDisplayed();
    }

    boolean isOverConfidentBehaviourMeterDisplayed() {
        waitForElementToBeVisible(overconfidenceBehaviourElement);
        return overconfidenceBehaviourElement.isDisplayed();
    }

    boolean isSlowBehaviourMeterDisplayed() {
        waitForElementToBeVisible(slowBehaviourElement);
        return slowBehaviourElement.isDisplayed();
    }

    boolean isStrugglingBehaviourMeterDisplayed() {
        waitForElementToBeVisible(strugglingBehaviourElement);
        return strugglingBehaviourElement.isDisplayed();
    }

    boolean isFlukesBehaviourMeterDisplayed() {
        waitForElementToBeVisible(flukesBehaviourElement);
        return flukesBehaviourElement.isDisplayed();
    }
}
