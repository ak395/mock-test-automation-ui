package page.practicePage.practiceSessionSummary;

import entities.questionTypes.practice.PracticeQuestion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import static utils.AttemptType.*;

class QuestionStatusSection extends BasePage {

    @FindBy(css = ".practice-legend-badge__imgMile.perfect")
    private WebElement perfectAttemptStatus;

    @FindBy(css = ".practice-legend-badge__imgMile.wasted")
    private WebElement wastedAttemptStatus;

    @FindBy(css = ".practice-legend-badge__imgMile.overtimecorrect")
    private WebElement overtimeCorrectAttemptStatus;

    @FindBy(css = ".practice-legend-badge__imgMile.overtimeincorrect")
    private WebElement overtimeIncorrectAttemptStatus;

    @FindBy(css = ".practice-legend-badge__imgMile.toofastcorrect")
    private WebElement toofastCorrectBadge;


    QuestionStatusSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    boolean verifyQuestionStatusWithBadgeIsDisplayed(PracticeQuestion question){
        String attempt = question.getAttemptType();
        switch (attempt){

            case PERFECT_ATTEMPT:
                waitForElementToBeVisible(perfectAttemptStatus);
                return perfectAttemptStatus.isDisplayed();

            case WASTED_ATTEMPT:
                waitForElementToBeVisible(wastedAttemptStatus);
                return wastedAttemptStatus.isDisplayed();

            case OVERTIME_CORRECT:
                waitForElementToBeVisible(overtimeCorrectAttemptStatus);
                return overtimeCorrectAttemptStatus.isDisplayed();

            case OVERTIME_INCORRECT:
                waitForElementToBeVisible(overtimeIncorrectAttemptStatus);
                return overtimeIncorrectAttemptStatus.isDisplayed();

            case TOO_FAST_CORRECT:
        waitForElementToBeVisible(toofastCorrectBadge);
                return toofastCorrectBadge.isDisplayed();
        }
        
        return false;
    }
}
