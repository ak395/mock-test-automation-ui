package page.practicePage.practiceSessionSummary;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.ArrayList;
import java.util.List;

@Getter
class SkillsAcquiredSection extends BasePage {


    @FindBy(css = "div.session-summary__skillsRequired--content-btn")
    private List<WebElement> skillsAquiredElementList;

    private List<String> skills;

    SkillsAcquiredSection(WebDriver driver) {
        this.driver = driver;
        skills = new ArrayList<>();
        PageFactory.initElements(driver, this);
    }


     void addSkillsAcquiredToList(){
        skillsAquiredElementList.forEach(x -> {
            addSkills(x.getText());
        });
    }

    private void addSkills(String skill) {
        skills.add(skill);
    }


    public int verifyNoSkillsAreAcquired() {
        return skillsAquiredElementList.size();
    }
}
