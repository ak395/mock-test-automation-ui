package page.practicePage.practiceSessionSummary;

import driver.DriverProvider;
import entities.SkillClientResponse;
import entities.practice.PracticePlanner;
import entities.questionTypes.practice.PracticeQuestion;
import lombok.Getter;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import skillService.SkillClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static utils.AttemptType.OVERTIME_CORRECT;
import static utils.AttemptType.PERFECT_ATTEMPT;

@Getter
public class SessionSummaryHomePage extends BasePage {

    private SessionStatusSection sessionStatusSection;
    private QuestionProgressBarSection questionProgressBarSection;
    private SkillsAcquiredSection skillsAcquiredSection;
    private QuestionStatusSection questionStatusSection;
    private BehaviourMeterSection behaviourMeterSection;

    public SessionSummaryHomePage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
        sessionStatusSection = new SessionStatusSection(driver);
        questionProgressBarSection = new QuestionProgressBarSection(driver);
        skillsAcquiredSection = new SkillsAcquiredSection(driver);
        questionStatusSection = new QuestionStatusSection(driver);
        behaviourMeterSection = new BehaviourMeterSection(driver);
    }

    public void clickOnEndSession() {
        sessionStatusSection.clickOnEndSessionButton();
        questionProgressBarSection = new QuestionProgressBarSection(driver);
    }

    public void verifyNumberOfQuestionsAttemptedAs(PracticePlanner practicePlanner) {
        int actualNumberOfQuestionsAttempted = getQuestionProgressBarSection().getNumberOfQuestionsAttempted();
        assertEquals(actualNumberOfQuestionsAttempted, practicePlanner.getPracticeQuestionList().size(),
                "Expected number of questions attempted shown in session summary:"
                        + practicePlanner.getPracticeQuestionList().size() + "doesn't match with number actual number shown in UI: "
                        + actualNumberOfQuestionsAttempted);
    }

    public void clickOnResumeSessionButton() {
        getSessionStatusSection().clickOnResumeSessionButton();
    }

    public List<String> getSkillsAcquiredForAnsweringQuestion() {
        skillsAcquiredSection.addSkillsAcquiredToList();
        return skillsAcquiredSection.getSkills();
    }

    public void verifySkillsAcquired(PracticePlanner practicePlanner) throws IOException {
        List<String> questionCodesList = new ArrayList<>();
        practicePlanner.getPracticeQuestionList().forEach(x -> questionCodesList.add(x.getQuestionCode()));

        SkillClientResponse skillClientResponse = new SkillClient().getSkillsByQuestionCode(questionCodesList);
        List<String> actualSkills = getSkillsAcquiredForAnsweringQuestion();
        List<String> expectedSkills = skillClientResponse.getResult();
        actualSkills.forEach( skill ->
                assertTrue(expectedSkills.contains(skill), "Actual and expected skills in session summary page, did not match"));
    }

    public void verifyNoSkillsAreAcquired() {
        int actualSkillsSize = skillsAcquiredSection.verifyNoSkillsAreAcquired();
        assertEquals(actualSkillsSize, 0, "Actual and expected skills in session summary page, did not match");
    }

    public void verifyQuestionStatus(PracticePlanner practicePlanner) {
        for (PracticeQuestion question : practicePlanner.getPracticeQuestionList()) {
            assertTrue(getQuestionStatusSection().verifyQuestionStatusWithBadgeIsDisplayed(question),
                    "Question Status is not displayed for Badge: " + question.getAttemptType());
        }
    }

    public void verifyDefaultBehaviourMeterIsDisplayed() {
        assertTrue(behaviourMeterSection.isDefaultBehaviourMeterDisplayed());
    }
    
    public void verifySlowBehaviourMeterIsDisplayed() {
        assertTrue(behaviourMeterSection.isSlowBehaviourMeterDisplayed());
    }

    public void verifyInControlBehaviourMeterIsDisplayed() {
        assertTrue(behaviourMeterSection.isInControlBehaviourMeterDisplayed());
    }

    public void verifyOverConfidentBehaviourMeterIsDisplayed() {
        assertTrue(behaviourMeterSection.isOverConfidentBehaviourMeterDisplayed());
    }

    public void verifyStrugglingBehaviourMeterIsDisplayed() {
        assertTrue(behaviourMeterSection.isStrugglingBehaviourMeterDisplayed());
    }

    public void verifyFlukesBehaviourMeterIsDisplayed() {
        assertTrue(behaviourMeterSection.isFlukesBehaviourMeterDisplayed());
    }

    public void verifySkippedQuestionCount(PracticePlanner practicePlanner) {
        assertEquals(getQuestionProgressBarSection().getNumberOfQuestionsSkipped(), practicePlanner.getNumberOfSkippedQuestions());
    }

    public void verifySessionAccuracy(PracticePlanner planner) {
        String expectedAccuracy = planner.getSessionAccuracy();
        assertEquals(getQuestionProgressBarSection().getSessionAccuracy(), expectedAccuracy);
    }
}
