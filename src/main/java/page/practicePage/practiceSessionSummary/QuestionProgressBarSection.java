package page.practicePage.practiceSessionSummary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

class QuestionProgressBarSection extends BasePage {

    @FindBy(css = "div.session-summaryProgressBar-total")
    private WebElement totalNumberOfQuestionsAttempted;

    @FindBy(xpath = "//div[contains(text(), \"Question Skipped\")]//span")
    private WebElement numberOfQuestionsSkipped;

    @FindBy(xpath = "//div[contains(text(), \"Session Accuracy\")]//span")
    private WebElement sessionAccuracyText;

    QuestionProgressBarSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    int getNumberOfQuestionsAttempted() {
        waitForElementToBeVisible(totalNumberOfQuestionsAttempted);
        return Integer.parseInt(totalNumberOfQuestionsAttempted.getText().split("/")[0]);
    }

    int getNumberOfQuestionsSkipped(){
        waitForElementToBeVisible(numberOfQuestionsSkipped);
        return Integer.parseInt(numberOfQuestionsSkipped.getText());
    }

    String getSessionAccuracy() {
        waitForElementToBeVisible(sessionAccuracyText);
        return sessionAccuracyText.getText();
    }
}
