package page.practicePage.dashboard;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import page.BasePage;

public class DashboardSection extends BasePage {
    private DashboardSectionLocators locators;

    @FindBy(className = "dashboard")
    private WebElement rootElement;

    public DashboardSection(WebDriver driver) {
        this.driver = driver;
        locators = new DashboardSectionLocators();
        PageFactory.initElements(driver, this);
    }

    private WebElement getGoalDropDownElement() {
        return getElement(rootElement, locators.selectGoal);
    }

    public void selectGoal(String goal) {

        WebElement element = getGoalDropDownElement();

        Select selectGoalElement = new Select(element);
        selectGoalElement.selectByVisibleText(goal.toLowerCase());
        waitForPageToLoad();

    }
}
