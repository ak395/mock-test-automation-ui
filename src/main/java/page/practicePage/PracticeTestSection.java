package page.practicePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;

import java.util.List;

public class PracticeTestSection extends BasePage {


    @FindBy(css = ".custom-button-component.pack-practice-btn.default.capsular.filled.onhover-style")
    private WebElement startPracticeButton;

    @FindBy(css = ".practice.resume")
    private WebElement resumePracticeTestButton;

    @FindBy(css = ".view-summary")
    private WebElement viewSummaryButton;

    @FindBy(css = "span.statistics-element-title")
    private List<WebElement> numberOfQuestionsAttempted;

    @FindBy(css = "div.header-statistics")
    private WebElement summaryElement;


    PracticeTestSection(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    void clickOnFirstStartPracticeButton() {
        scrollToView(startPracticeButton);
        waitForElementToBeVisible(startPracticeButton);
        jsClick(startPracticeButton);
    }

    void clickOnResumePracticeButton() {
        scrollToView(resumePracticeTestButton);
        waitForElementToBeVisible(resumePracticeTestButton);
        jsClick(resumePracticeTestButton);
    }

    int getActualNumberOfQuestionsAttempted() {
        waitForElementToBeVisible(summaryElement);
        return Integer.parseInt(numberOfQuestionsAttempted.get(1).getText()) + Integer.parseInt(numberOfQuestionsAttempted.get(2).getText());
    }

    boolean isStartPracticeButtonVisible() {
        waitForElementToBeVisible(startPracticeButton);
        return startPracticeButton.isDisplayed();
    }

    int getNumberOfQuestionsAttemptedCorrectly() {
        waitForElementToBeVisible(summaryElement);
        return Integer.parseInt(numberOfQuestionsAttempted.get(1).getText());
    }

    int getNumberOfQuestionsAttemptedIncorrectly() {
        waitForElementToBeVisible(summaryElement);
        return Integer.parseInt(numberOfQuestionsAttempted.get(2).getText());
    }
}
