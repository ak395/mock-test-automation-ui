package page.sections.leftFilter;

import entities.AMockOrPracticeTest;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.BasePage;
import verification.Verify;

import java.util.Optional;

public class LeftFilterSection extends BasePage {

    private LeftFilterSectionLocators locators;

    @FindBy(css="div[class *= 'map-left'")
    private WebElement rootElement;

    public LeftFilterSection(WebDriver driver) {
        this.driver = driver;
        locators = new LeftFilterSectionLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getSelectDropDown1Element() {
        return getElement(rootElement, locators.dropDown1);
    }

    public WebElement getSelectDropDown2Element() {
        return getElement(rootElement, locators.dropDown2);
    }

    public WebElement getSelectDropDown3Element() {
        return getElement(rootElement, locators.dropDown3);
    }

    public WebElement getSelectDropDown4Element() {
        return getElement(rootElement, locators.dropDown4);
    }

    public WebElement getResetFiltersElement() {
        return getElement(rootElement, locators.resetFilters);
    }

    public WebElement getStartSolvingButtonElement()                            {
        return getElement(rootElement, locators.startSolvingButton);
    }

    @Step("Clicking on Reset filters link on left pane")
    public void resetFilters() {
        WebElement selectDropDown2Element = getSelectDropDown2Element();

        getResetFiltersElement().click();
        waitForPageToLoad();
        waitForElementToBeInvisible(selectDropDown2Element);

    }

    @Step("Clicking on Start Solving Button")
    public void clickOnStartSolvingButton(){
        WebElement startSolvingButton = getStartSolvingButtonElement();
        startSolvingButton.click();
    }

    @Step("Verify all filters have been reset")
    public void assertThatFiltersHaveBeenReset() {
        Verify.assertFalse(isElementPresent(rootElement, locators.dropDown2), "Select TestType should not be seen", Optional.of(true));
        Verify.assertFalse(isElementPresent(rootElement, locators.dropDown3), "Select Subject should not be seen", Optional.of(true));
        Verify.assertFalse(isElementPresent(rootElement, locators.dropDown4), "Select Chapter should not be seen", Optional.of(true));
    }

    @Step("User sees test has been selected ({test.name}, {test.category}, {test.subject}, {test.topic})")
    public void assertTestHasBeenSelected(AMockOrPracticeTest test) {

        String expectedUrl = String.format("test/%s/%s/%s/%s",
                test.getName(),
                test.getCategory(),
                test.getSubject(),
                test.getTopic()).toLowerCase().replaceAll(" ", "-");

        Verify.assertTrue(driver.getCurrentUrl().endsWith(expectedUrl), "message", Optional.of(true));

    }


    @Step("User selects mock test -> ({test.name}, {test.category}, {test.subject}, {test.topic})")
    public void selectTest(AMockOrPracticeTest test) {
        selectDropDown1(test.getName());
        selectDropDown2(test.getCategory());
        selectDropDown3(test.getSubject());
        selectDropDown4(test.getTopic());
    }

    private void selectDropDown4(String chapter) {
        WebElement element = getSelectDropDown4Element();

        Select selectGoalElement = new Select(element);
        selectGoalElement.selectByVisibleText(chapter);
        waitForPageToLoad();
        new WebDriverWait(driver, 20).until(ExpectedConditions.urlContains(chapter.toLowerCase().replaceAll(" ", "-")));

    }

    private void selectDropDown3(String subject) {
        WebElement element = getSelectDropDown3Element();

        Select selectGoalElement = new Select(element);
        selectGoalElement.selectByVisibleText(subject);
        waitForPageToLoad();

    }

    private void selectDropDown2(String testType) {
        WebElement element = getSelectDropDown2Element();

        Select selectGoalElement = new Select(element);
        selectGoalElement.selectByVisibleText(testType);
        waitForPageToLoad();

    }

    private void selectDropDown1(String exam) {
        WebElement element = getSelectDropDown1Element();

        Select selectGoalElement = new Select(element);
        selectGoalElement.selectByVisibleText(exam);
        waitForPageToLoad();

    }

    @Step("User sees URL to be : {url}")
    public void assertUrlHasBeenResetTo(String url) {
        Verify.assertEquals(driver.getCurrentUrl(), url, "Url not matching", Optional.of(true));

    }
}
