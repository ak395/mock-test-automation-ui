package page.sections.leftFilter;

import entities.MockTest;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import page.BasePage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LeftFilter extends BasePage {

    private LeftFilterLocators leftFilterLocators;

    @CacheLookup
    @FindBy(css = ".root-wrapper")
    private WebElement rootElement;

    @FindBy(css = ".no-transition")
    private WebElement examRootElement;

    @FindBy(css = ".childOfChild")
    private WebElement subjectRootElement;

    @FindBy(className = "common-padding")
    private List<WebElement> descriptionWidget;

    @FindBy(className = "close-open-button")
    private WebElement openCLoseLeftFilter;

    @FindBy(css = ".node-wrapper")
    private WebElement left;

    @FindBy(css = ".close-tree")
    private WebElement closeLeftTree;

    @FindBy(css = ".node-wrapper >.childTree .childName")
    private List<WebElement> exam;

    @FindBy(css = ".childOfChild .childName")
    private List<WebElement> subSubject;

    @FindBy(css = ".childOfChild .childName")
    private List<WebElement> concept;

    @FindBy(css = ".practice-board-container.common-spacing")
    private WebElement practiceDescription;

    @FindBy(css = ".custom-button-component.practice-btn")
    private WebElement startPractice;

    @FindBy(css = ".footer-text .title")
    private WebElement practiceTitle;


    public LeftFilter(WebDriver driver) {
        this.driver = driver;
        leftFilterLocators = new LeftFilterLocators();
        PageFactory.initElements(driver, this);
    }

    public List<WebElement> getExam() {
        return getListOfElements(examRootElement, leftFilterLocators.examsName);
    }

    public List<WebElement> getSubject() {
        return getListOfElements(subjectRootElement, leftFilterLocators.subjectName);
    }

    public void selectExamName(MockTest examName) {
        for (WebElement webElement : exam) {
            if (webElement.getText().equalsIgnoreCase(examName.getExam())) {
                webElement.click();
                break;
            }
        }

    }

    public void selectSubjectName(MockTest subjectName) {
        for (WebElement webElement : getSubject()) {
            if (webElement.getText().equalsIgnoreCase(subjectName.getSubject())) {
                webElement.click();
                break;
            }
        }
    }

    public void selectSubSubjectName(MockTest subSubjectName) {
        for (WebElement webElement : subSubject) {
            if (webElement.getText().equalsIgnoreCase(subSubjectName.getSubSubject())) {
                webElement.click();
                break;
            }
        }
    }

    public void selectConceptName(MockTest conceptName) {
        waitForPageToLoad();
        for (WebElement conceptSearch : concept) {
            if (conceptSearch.getText().equalsIgnoreCase(conceptName.getConcept())) {
                conceptSearch.click();
                break;
            }
        }
    }

    public void verifyDescriptionForAllExams() {
        SoftAssert softAssert = new SoftAssert();
        exam.stream().map(e -> e.getText()).collect(Collectors.toList()).forEach(el ->
        {
            waitForElementTobeClickable(driver.findElement(By.xpath(String.format("//div[text()='%s']", el))));
            driver.findElement(By.xpath(String.format("//div[text()='%s']", el))).click();
            boolean isDisplay = descriptionWidget.size() > 0;

            softAssert.assertTrue(isDisplay, "Description widget is not displaying for exam " + el);

        });
        softAssert.assertAll();

    }

    public void verifyLeftFilterDisplaying() {
        Assert.assertTrue(left.isDisplayed());
    }

    public void openCloseLeftFilterSection() {
        openCLoseLeftFilter.click();
    }

    public void verifyLeftFilterNotDisplaying() {
        Assert.assertTrue(closeLeftTree.isDisplayed());
    }

    public void verifyPracticeOnTestListingForAllExam() {
        SoftAssert softAssert = new SoftAssert();
        exam.stream().map(e -> e.getText()).collect(Collectors.toList()).forEach(el ->
        {
            try {
                Assert.assertTrue(practiceDescription.isDisplayed(), "Practice Description widget is not displaying");
                Assert.assertTrue(startPractice.isDisplayed(), "Start button is not available");
            } catch (NoSuchElementException exception) {
                softAssert.fail("Practice is not available for" + " " + el);
            }
            waitForElementTobeClickable(driver.findElement(By.xpath(String.format("//div[text()='%s']", el))));
            driver.findElement(By.xpath(String.format("//div[text()='%s']", el))).click();
            driver.navigate().refresh();
        });
        softAssert.assertAll();
    }

    public void verifyUserNavigateToPracticeForAllExam(){
        SoftAssert softAssert = new SoftAssert();
        exam.stream().map(e -> e.getText()).collect(Collectors.toList()).forEach(el ->
        {
            try {
                startPractice.click();
                ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
                driver.switchTo().window(tabs.get(1));
                Assert.assertEquals(practiceTitle.getText(), "Welcome to Embibe Practice !", "User is not landed on practice page");
                //driver.close();
                driver.switchTo().window(tabs.get(0));
            } catch (NoSuchElementException exception) {
                softAssert.fail("Practice is not available for" + " " + el);
            }
            waitForElementTobeClickable(driver.findElement(By.xpath(String.format("//div[text()='%s']", el))));
            driver.findElement(By.xpath(String.format("//div[text()='%s']", el))).click();
        });
        softAssert.assertAll();
    }

//    public void verifySubjectDescription(){
//        subSubject.stream().map(e->e.getText()).collect(Collectors.toList()).forEach(el ->
//                waitForElementTobeClickable(driver.findElement(By.xpath(String.format("//div[text()='%s']", el))));
//                driver.findElement(By.xpath(String.format("//div[text()='%s']", el))).click();
//     }
}
