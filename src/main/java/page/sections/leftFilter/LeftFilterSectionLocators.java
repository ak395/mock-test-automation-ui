package page.sections.leftFilter;

import org.openqa.selenium.By;
import page.BasePage;

public class LeftFilterSectionLocators extends BasePage {

    By dropDown1 = By.cssSelector("select[ga-track *= 'level 1'");
    By dropDown2 = By.cssSelector("select[ga-track *= 'level 2'");
    By dropDown3 = By.cssSelector("select[ga-track *= 'level 3'");
    By dropDown4 = By.cssSelector("select[ga-track *= 'level 4'");

    By resetFilters = By.className("breadcrumb__reset_filter");
    By startSolvingButton = By.cssSelector("button[ga-track *= 'start solving'");


}
