package page.sections;

import driver.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import utils.Properties;
import verification.Verify;

import java.util.Optional;

public class ForgotPasswordSection extends BasePage {

    @FindBy(className = "forgotPasswordWrapper")
    private WebElement forgotPasswordPopUp;

    @FindBy(id = "inputvalueError")
    private WebElement emailtextBoxError;

    @FindBy(className = "resp-but ")
    private WebElement resetPassword;

    @FindBy(id = "inputvalue")
    private WebElement emailTextBox;

    public ForgotPasswordSection() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }


    public void verifyForgotPasswordPopUpIsDisplayed() {
        waitForElementToBeVisible(forgotPasswordPopUp);
        Verify.assertTrue(forgotPasswordPopUp.isDisplayed(),"Pop up is not displayed", Optional.of(true));
    }

    public void verifyforgetPassowrdWithUnregisteredID() {
        emailTextBox.sendKeys(Properties.unregisteredEmailId);
        resetPassword.click();
        Verify.assertEquals(emailtextBoxError.getText(),"Email is not registered with us","Text not Matching",Optional.of(true));

    }

    public void verifyForgetPasswordWithUnregisteredPhNo() {
        emailTextBox.clear();
        emailTextBox.sendKeys((Properties.unregisteredMobileNo));
        resetPassword.click();
        Verify.assertEquals(emailtextBoxError.getText(),"Mobile is not registered with us","Text not Matching",Optional.of(true));

    }


}
