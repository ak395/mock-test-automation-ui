package page.sections.headerWrapper;

import org.openqa.selenium.By;


public class HeaderWrapperLocators  {

    By loginButton = By.className("create-profile");
    By loginEmailIdArea = By.id("emailArea");
    By loginPasswordArea = By.id("passwordArea");
    By rankupTab = By.linkText("RANKUP");
    By startNow = By.linkText("START NOW");

}
