package page.sections.headerWrapper;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import verification.Verify;

import java.util.List;
import java.util.Optional;
public class HeaderWrapperSection extends BasePage {
    private HeaderWrapperLocators locators;

    @FindBy(className = "center-right-nav-wrapper")
    private WebElement rootElement;

    @FindBy(className = "login-signup-container")
    private WebElement loginPopUpRootElement;

    @FindBy(className = "create-profile")
    private WebElement loginButton;


    @FindBy(linkText = "START NOW")
    private WebElement startNow;




    public HeaderWrapperSection(WebDriver driver) {
        this.driver = driver;
        locators = new HeaderWrapperLocators();
        PageFactory.initElements(driver, this);
    }

    public WebElement getLoginElement() {
        return getElement(rootElement, locators.loginButton);
    }

    public WebElement getLoginEmailIdArea() {
        return getElement(loginPopUpRootElement, locators.loginEmailIdArea);
    }

    public WebElement getLoginPasswordArea() {
        return getElement(loginPopUpRootElement, locators.loginPasswordArea);
    }

    public WebElement getRankUpTabElement() {
        return getElement(rootElement, locators.rankupTab);
    }


    @Step("Existing User is logging in with credentials")
    public void login(String emailID, String password) {
        waitForPageToLoad();
        WebElement loginButton = getLoginElement();
        Boolean result = isAttributePresent(loginButton, "ng-click");
        loginButton.click();
        waitForPageToLoad();

        if (result) {
            getLoginEmailIdArea().sendKeys(emailID);
            getLoginPasswordArea().sendKeys(password);
        } else {
            getLoginEmailIdArea().sendKeys(emailID);
            getLoginPasswordArea().sendKeys(password);

        }

    }

    @Step("Clicks on rankUp Tab in the Header Section")
    public void rankUp() {
        waitForPageToLoad();
        getRankUpTabElement().click();
    }

    @Step("Verifies that Url has to the given URL")
    public void assertUrlHasSetTo(String url) {

        wait(5000);
        Verify.assertEquals(driver.getCurrentUrl(), url, "url not matching", Optional.of(true));
    }

    @Step("Assert that RankUp Tab is present in the Header Section")
    public void assertRankUpTabIsPresentInHeader() {
        Boolean result = isElementPresent(rootElement, locators.rankupTab);
        Verify.assertTrue(result, "successful", Optional.of(true));

    }

    @Step("click on login button")
    public void clickOnLoginButton() {
        waitForPageToLoad();
        loginButton.click();
        Verify.assertTrue(loginPopUpRootElement.isDisplayed(), "Login Pop up Element is not Displayed", Optional.of(true));
    }



}
