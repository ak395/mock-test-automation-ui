package mockTestService;

import com.jayway.jsonpath.JsonPath;
import io.restassured.http.Header;
import io.restassured.response.ResponseBody;
import org.json.JSONObject;
import page.BasePage;
import utils.Properties;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;

public class MockTestClient extends BasePage {

    private String cookiesName;

    public String getTestCode(String cookies, String mockTestPath) {

        if (System.getProperty("env").equalsIgnoreCase("preprod")) cookiesName = "preprod-embibe-token";
        else if (System.getProperty("env").equalsIgnoreCase("production"))
            cookiesName = "embibe-token";

        return given().header(new Header(cookiesName, cookies)).
                when().queryParam("mocktest_bundle_path", mockTestPath).
                get(Properties.getMockTestCode).
                andReturn().body().jsonPath().get("result.code");
    }

    public ResponseBody getMockTestFullPaperInfo(String cookies, String mockTestPath){
        String testCode = getTestCode(cookies,mockTestPath);
        System.out.println("Test Code "+ testCode);
        ResponseBody responseBody = given().header(new Header("embibe-token",cookies)).when()
                .queryParam("view_solutions","true")
                .queryParam("referer_service","test_microservice")
                .get(Properties.getMockTestFullInfo + String.format("%s",testCode));
//        responseBody.prettyPrint();
        return responseBody;
    }
    public ResponseBody getMockTestData(String cookies, String mockTestPath) {
        return given().header(new Header("embibe-token", cookies)).when().
                get(Properties.getMockTestApi + String.format("%s/questions", getTestCode(cookies, mockTestPath))).
                andReturn().body();


    }

    public String getSessionId(String cookies, String mockTestPath) {
        String testCode = getTestCode(cookies, mockTestPath);
        ResponseBody response = given().header("embibe-token", cookies).when().
                get(Properties.getMockTestApi + String.format("%s/session", testCode)).andReturn().body();
        return response.jsonPath().get(String.format("%s.sessionId", testCode));
    }


    public ResponseBody getSummary(String cookies, String mockTestPath) {
        String sessionId = getSessionId(cookies, mockTestPath);
        ResponseBody responseBody = given().header("embibe-token", cookies).when().get(Properties.getMockTestApi + String.format("%s/summary", sessionId)).andReturn().body();
        responseBody.prettyPrint();
        return responseBody;


    }

    @SuppressWarnings(value = {"deprecation", "unchecked"})
    public int getPositiveMarksForQuestion(ResponseBody responseBody, String questionID) {
        int marks;
        if (responseBody != null) {
            List<LinkedHashMap<String, Object>> obj = JsonPath.read(responseBody.asString(), "$.paper.sections.*.questions.*");
            LinkedHashMap<String, Object> object = obj.stream().filter(t -> t.get("code").equals(questionID)).findFirst()
                    .orElse(null);

            marks = object == null ? 0 : ((LinkedHashMap<String, Double>) object.get("marking_scheme")).get("pmarks").intValue();

            return marks;
        }
        return 0;
    }


    public int getNegativeMarksForForQuestion(ResponseBody response, String QuestionCode) {
        AtomicInteger mark = new AtomicInteger();
        if (response != null) {
            response.jsonPath().getMap("paper.sections").keySet().forEach(e ->
            {
                List subjectWiseQuestion = response.jsonPath().get(String.format("paper.sections.%s.questions", e));
                IntStream.range(0, subjectWiseQuestion.size()).forEach(i -> {
                    JSONObject questions = new JSONObject(response.asString()).getJSONObject("paper").getJSONObject("sections").getJSONObject(e.toString()).
                            optJSONArray("questions").getJSONObject(i);
                    Object code = questions.get("code");
                    if (code.toString().equalsIgnoreCase(QuestionCode))
                        mark.set(Integer.parseInt(questions.getJSONObject("marking_scheme").get("nmarks").toString().replace(".0", "")));
                });


            });
        }
        return mark.get();
    }


    public int getQuestionCount(ResponseBody response) {
        return Math.round(response.jsonPath().get("question_count"));

    }

    public int getMaximumMarks(ResponseBody response) {
        return Math.round(response.jsonPath().get("max_marks"));

    }

    public int getTestTime(ResponseBody response) {
        return Math.round(response.jsonPath().get("duration"));

    }

    public int getMockTestMinimumAvgScore(ResponseBody response) {
        return Math.round(response.jsonPath().get("min_avg_score"));

    }

    public int getMockTestMinimumGoodScore(ResponseBody response) {
        return Math.round(response.jsonPath().get("min_good_score"));

    }

    public int getMockTestCutOffMarks(ResponseBody response) {
        return Math.round(response.jsonPath().get("cutoff"));

    }


    public String getVersionForQuestion(ResponseBody response, String questionCode) {

        AtomicReference<String> version = new AtomicReference<>();

        if (response != null) {
            response.jsonPath().getMap("paper.sections").keySet().stream().forEach(e ->
            {
                List subjectWiseQuestion = response.jsonPath().get(String.format("paper.sections.%s.questions", e));
                IntStream.range(0, subjectWiseQuestion.size()).forEach(i -> {
                    JSONObject paper = new JSONObject(response.asString()).getJSONObject("paper").getJSONObject("sections").getJSONObject(e.toString()).
                            optJSONArray("questions").getJSONObject(i);
                    Object code = paper.get("code");
                    if (code.toString().equalsIgnoreCase(questionCode)) {

                        version.set((paper.get("version").toString()));

                    }
                });


            });
        }
        return version.get();
    }

    public List<String> getDifficultyOfQuestions(ResponseBody responseBody){
        List<String> difficultyBands = new ArrayList<>();
        if(responseBody != null) {
            responseBody.jsonPath().getMap("data.paper.sections").keySet().stream().forEach(e->{
                    List sectionWiseQuestions = responseBody.jsonPath().get(String.format("data.paper.sections.%s.questions",e));
                    IntStream.range(0,sectionWiseQuestions.size()).forEach(i -> {
                        JSONObject difficultyBand = new JSONObject(responseBody.asString()).getJSONObject("data").getJSONObject("paper").getJSONObject("sections").getJSONObject(e.toString()).
                                optJSONArray("questions").getJSONObject(i);
                        Object band = difficultyBand.get("difficulty_band");
                        difficultyBands.add(band.toString());
                    });

            });


        }
        return difficultyBands;
    }

    public float getAttemptedTimeSpentFromSummaryApi(String cookies, String mockTestPath, String difficultyLevel) {
        String sessionId = getSessionId(cookies, mockTestPath);
        float attemptedTimeSpent = 0;
        ResponseBody responseBody = given().header("embibe-token", cookies).when().get(Properties.getMockTestApi + String.format("%s/summary", sessionId)).andReturn().body();
        if (responseBody != null) {
//

            attemptedTimeSpent = from(responseBody.asString()).get(String.format("difficultyWiseSummary.%s.attemptedTimeSpent", difficultyLevel));
            System.out.println("The Attempted Time Spent Value: " + attemptedTimeSpent);
        }

        return attemptedTimeSpent;
    }

    public int getTotalQuestionsFromSummaryApi(String cookies,String mockTestPath, String difficultyLevel) {

        String sessionId = getSessionId(cookies, mockTestPath);
        int totalQuestions = 0;
        ResponseBody responseBody = given().header("embibe-token", cookies).when().get(Properties.getMockTestApi + String.format("%s/summary", sessionId)).andReturn().body();
        if (responseBody != null) {

            totalQuestions = from(responseBody.asString()).get(String.format("difficultyWiseSummary.%s.totalQuestions", difficultyLevel));
            System.out.println("The total Questions: " + totalQuestions);
        }

        return totalQuestions;
    }

    public List<Integer> getSectionWiseTotalQuestionsFromSummaryApi(String cookies, String mockTestPath) {
        String sessionId = getSessionId(cookies, mockTestPath);

        List<Integer> totalQuestionsSectionWise = new ArrayList<>();

        ResponseBody responseBody = given().header("embibe-token", cookies).when().get(Properties.getMockTestApi + String.format("%s/summary", sessionId)).andReturn().body();
        if (responseBody != null) {

            responseBody.jsonPath().getMap(String.format("sectionSummaries")).keySet().stream().forEach(section ->{
               int numberOfQuestions = from(responseBody.asString()).get(String.format("sectionSummaries.numberOfQuestions"));
               totalQuestionsSectionWise.add(numberOfQuestions);
            } );
        }

        System.out.println("The Questions in List" + totalQuestionsSectionWise);
       return  totalQuestionsSectionWise;
    }


}
