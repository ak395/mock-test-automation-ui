package builders;

import constants.*;
import entities.AMockOrPracticeTest;


public class AMockOrPracticeTestBuilder {

    private AMockOrPracticeTest aMockOrPracticeTest;

    public AMockOrPracticeTestBuilder() {
        this.aMockOrPracticeTest = new AMockOrPracticeTest();
        aMockOrPracticeTest.setGoal(Goals.GOAL_ENGINEERING);
        aMockOrPracticeTest.setName(Exams.EXAM_JEE_MAIN);
        aMockOrPracticeTest.setCategory(TestTypes.TEST_TYPE_CHAPTERWISE_TEST);
        aMockOrPracticeTest.setSubject(Subjects.SUBJECT_PHYSICS);
        aMockOrPracticeTest.setTopic(Chapters.CHAPTER_CIRCULAR_MOTION);
    }

    public AMockOrPracticeTestBuilder withGoal(String goal) {
        aMockOrPracticeTest.setGoal(goal);
        return this;
    }

    public AMockOrPracticeTestBuilder withDropDown1(String dropDown1Text) {
        aMockOrPracticeTest.setName(dropDown1Text);
        return this;
    }

    public AMockOrPracticeTestBuilder withDropDown2(String dropDown2Text) {
        aMockOrPracticeTest.setCategory(dropDown2Text);
        return this;
    }

    public AMockOrPracticeTestBuilder withDropDown3(String dropDown3Text) {
        aMockOrPracticeTest.setSubject(dropDown3Text);
        return this;
    }

    public AMockOrPracticeTestBuilder withDropDown4(String dropDown4Text) {
        aMockOrPracticeTest.setTopic(dropDown4Text);
        return this;
    }

    public AMockOrPracticeTest build() {
        return aMockOrPracticeTest;
    }
}
