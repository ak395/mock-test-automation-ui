package builders;

import entities.MockTest;

public class MockTestBuilder {

    private MockTest mockTest;

    public MockTestBuilder(){

        this.mockTest = new MockTest();
    }

    public MockTestBuilder withExamName(String exam){
        mockTest.setExam(exam);
        return this;
    }

    public MockTestBuilder withSubjectName(String subject){
        mockTest.setSubject(subject);
        return this;
    }

    public MockTestBuilder withSubSubject(String subSubject) {
        mockTest.setSubSubject(subSubject);
        return this;
    }

    public MockTestBuilder withConcept(String concept){
        mockTest.setConcept(concept);
        return this;
    }

    public MockTest build(){
        return mockTest;
    }
}
